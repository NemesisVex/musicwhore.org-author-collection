/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var CollectionFilter = function () {
	this.$collection = window.$collection;
	
	this.filter_none = function () {
		return this.$collection;
	}
	
	this.filter = function (query_term) {
		var results = [];
		$.each(this.$collection, function () {
			if (this.Artist == query_term || this.Title == query_term || this.Label == query_term || this.Format == query_term) {
				results.push(this)
			}
		})
		return results;
	};
	
	this.filter_by_artist = function (query_term) {
		return this.filter_by_field(query_term, 'Artist');
	}
	
	this.filter_by_label = function (query_term) {
		return this.filter_by_field(query_term, 'Label');
	}
	
	this.filter_by_format = function (query_term) {
		return this.filter_by_field(query_term, 'Format');
	}
	
	this.filter_by_field = function (query_term, field) {
		var results = [];
		$.each(this.$collection, function () {
			var regex = eval('/^' + query_term + '/gi');
			var matches = this[field].match(regex);
			if (matches != null) {
				results.push(this);
			}
		})
		results = this.sort_results(results, field, true);
		return results;
	}
	
	this.sort_results = function (results, property, is_ascending) {
		if (is_ascending == null) {is_ascending = true;}
		var sorted_results = results.sort(function (a, b) {
			if (is_ascending) {
				return (a[property] > b[property]) ? 1 : ((a[property] < b[property]) ? -1 : 0);
			} else {
				return (b[property] > a [property]) ? 1 : ((b[property] < a[property]) ? -1 : 0);
			}
		});
		return sorted_results;
	}
};
