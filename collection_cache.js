/**
 * Requires jQuery.
 */

var $index = [];

$.each($collection, function () {
	if ($.inArray(this.Artist, $index) == -1) {
		$index.push(this.Artist);
	}
	if ($.inArray(this.Title, $index) == -1) {
		$index.push(this.Title);
	}
	if ($.inArray(this.Label, $index) == -1) {
		$index.push(this.Label);
	}
	if ($.inArray(this.Format, $index) == -1) {
		$index.push(this.Format);
	}
});