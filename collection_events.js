$(function () {
	$.each($collection, function () {
		this.label = this.value = String(this.Artist + ' ' + this.Title + ' ' + this.Label + ' ' + this.Format).toLowerCase();
	});
	
	$('#collection-table').tablesorter();
	
	var collection_filter = new CollectionFilter;
	
	var populate_table = function (results) {
		if (results.length > 0) {
			var r = 0;
			$.each(results, function () {
				// Replace the first row, then clone it for each subsequent entry.
				var row_data = $('#template-collection-row').clone();
				// Rename the ID of the element.
				row_data.attr('id', 'collection-row-' + r);
				
				var is_not_in_collection = (this['Collection Status'] == 'Not In Collection');
				var strikethru_text = function (text, status) {
					if (status == true) {
						return '<strike>' + text + '</strike>';
					}
					return text;
				};
				// Populate the table cells.
				row_data.children('.artist').html(strikethru_text(this['Artist'], is_not_in_collection));
				row_data.children('.title').html(strikethru_text(this['Title'], is_not_in_collection));
				row_data.children('.label').html(strikethru_text(this['Label'], is_not_in_collection));
				row_data.children('.original-release-date').html(strikethru_text(this['Original Release Date'], is_not_in_collection));
				row_data.children('.release-date').html(strikethru_text(this['Release Date'], is_not_in_collection));
				row_data.children('.format').html(strikethru_text(this['Format'], is_not_in_collection));
				row_data.children('.upc').html(strikethru_text(this['UPC (Barcode)'], is_not_in_collection));
				row_data.children('.status').html(strikethru_text(this['Collection Status'], is_not_in_collection));
				// Display the result.
				row_data.appendTo($('#collection-content'));
				++r;
			});

			// Inform tablesorter about the update.
			$('#collection-table').trigger('update');
		}
	};
	
	var clear_box = function () {
		$('#query-terms').val('');
	};
	
	var clear_table = function () {
		$('th[class*=headerSortDown]').removeClass('headerSortDown');
		$('th[class*=headerSortUp]').removeClass('headerSortUp');
		$('tr[id^=collection-row]').remove();
	}
	
	$('#clear-box').click(function () {
		$('#query-terms').val('');

		// Clear the table
		clear_box();
		clear_table();
	});
	$('ul.artist-alpha-list li a').click(function () {
		var clicked_href = String(this.href).split('#');
		var query_term = clicked_href[1];

		// Clear the table
		clear_table();
		
		// Get the results
		var results = collection_filter.filter_by_artist(query_term);
		
		// Populate table
		populate_table(results);
		
		return false;
	});
	$('ul.label-alpha-list li a').click(function () {
		var clicked_href = String(this.href).split('#');
		var query_term = clicked_href[1];

		// Clear the table
		clear_table();
		
		// Get the results
		var results = collection_filter.filter_by_label(query_term);
		
		// Populate table
		populate_table(results);

		return false;
	});
	$('ul.format-list li a').click(function () {
		var clicked_href = String(this.href).split('#');
		var query_term = decodeURIComponent(clicked_href[1]);

		// Clear the table
		clear_table();
		
		// Get the results
		var results = collection_filter.filter_by_format(query_term);
		
		// Populate table
		populate_table(results);
		
		return false;
	});
	$('#view-all').click(function () {
		// Clear the table
		clear_table();
		
		// Get the results
		var results = collection_filter.filter_none();
		
		// Populate table
		populate_table(results);

		return false;
	});
	$('#query-terms').autocomplete({
		source: $index,
		select: function (event, ui) {
			// Clear the table
			clear_table();
			$('#query-terms').val($('#query-terms').val());
			
			// Get the results
			var results = collection_filter.filter(ui.item.value);
			
			// Populate table
			populate_table(results);

			return false;
		}
	});
});
