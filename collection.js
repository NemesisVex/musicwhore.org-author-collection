﻿var $collection = [
  {
    "Artist":"8 1/2 Souvenirs",
    "Title":"Happy Feet",
    "Original Release Date":"6/30/1998",
    "Release Date":"6/30/1998",
    "Format":"CD",
    "UPC (Barcode)":090266322626,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"8 1/2 Souvenirs",
    "Title":"Souvonica",
    "Original Release Date":"8/5/1997",
    "Release Date":"8/5/1997",
    "Format":"CD",
    "UPC (Barcode)":796621812329,
    "Label":"Continental",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"10,000 Maniacs",
    "Title":"Music From The Motion Picture",
    "Original Release Date":"2/26/2013",
    "Release Date":"2/26/2013",
    "Format":"MP3",
    "UPC (Barcode)":711574709120,
    "Label":"ORG",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"10,000 Maniacs",
    "Title":"Campfire Songs: The Popular, Obscure & Unknown Recordings Of 10,000 Manics",
    "Original Release Date":"1/27/2004",
    "Release Date":"1/27/2004",
    "Format":"CD",
    "UPC (Barcode)":081227390020,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"10,000 Maniacs",
    "Title":"Love Among The Ruins",
    "Original Release Date":"6/17/1997",
    "Release Date":"6/17/1997",
    "Format":"CD",
    "UPC (Barcode)":720642500927,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"10,000 Maniacs",
    "Title":"Hope Chest: The Fredonia Recordings",
    "Original Release Date":"7/1/1991",
    "Release Date":"7/1/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075596096249,
    "Label":"Elektra",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"10,000 Maniacs",
    "Title":"Blind Man's Zoo",
    "Original Release Date":"5/16/1989",
    "Release Date":"5/16/1989",
    "Format":"CD",
    "UPC (Barcode)":075596081528,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"10,000 Maniacs",
    "Title":"Blind Man's Zoo",
    "Original Release Date":"5/16/1989",
    "Release Date":"5/16/1989",
    "Format":"LP",
    "UPC (Barcode)":075596081511,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"10,000 Maniacs",
    "Title":"Trouble Me",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"7 Inch",
    "UPC (Barcode)":075596929875,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"10,000 Maniacs",
    "Title":"In My Tribe",
    "Original Release Date":"7/27/1987",
    "Release Date":"7/27/1987",
    "Format":"LP",
    "UPC (Barcode)":075596073813,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"10,000 Maniacs",
    "Title":"In My Tribe",
    "Original Release Date":"7/27/1987",
    "Release Date":"7/27/1987",
    "Format":"CD",
    "UPC (Barcode)":075596073820,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"10,000 Maniacs",
    "Title":"The Wishing Chair",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"LP",
    "UPC (Barcode)":075596042819,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"10,000 Maniacs",
    "Title":"The Wishing Chair",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"CD",
    "UPC (Barcode)":075596042826,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"98 Degrees",
    "Title":"2.0",
    "Original Release Date":"5/7/2013",
    "Release Date":"5/7/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"EOne",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"98 Degrees",
    "Title":"Revelation",
    "Original Release Date":"9/26/2000",
    "Release Date":"9/26/2000",
    "Format":"CD",
    "UPC (Barcode)":601215935424,
    "Label":"Universal",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABBA",
    "Title":"The Visitors",
    "Original Release Date":"11/30/1981",
    "Release Date":"10/16/2001",
    "Format":"CD",
    "UPC (Barcode)":731454996525,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABBA",
    "Title":"When All Is Said and Done",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABBA",
    "Title":"The Magic Of Abba",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"LP",
    "UPC (Barcode)":022775951448,
    "Label":"K-Tel",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABBA",
    "Title":"The Winner Takes It All",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABBA",
    "Title":"Greatest Hits, Vol. 2",
    "Original Release Date":"10/29/1979",
    "Release Date":"10/29/1979",
    "Format":"LP",
    "UPC (Barcode)":075678146725,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABBA",
    "Title":"Take a Chance on Me",
    "Original Release Date":"1977",
    "Release Date":"1977",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABBA",
    "Title":"ABBA",
    "Original Release Date":"4/21/1975",
    "Release Date":"4/21/1975",
    "Format":"LP",
    "UPC (Barcode)":731454996020,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABC",
    "Title":"How To Be a ... Zillionaire!",
    "Original Release Date":"1985",
    "Release Date":"11/14/2005",
    "Format":"CD",
    "UPC (Barcode)":602498337813,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABC",
    "Title":"The Lexicon of Love",
    "Original Release Date":"1982",
    "Release Date":"2/5/2002",
    "Format":"CD",
    "UPC (Barcode)":731453825024,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABC",
    "Title":"The Lexicon of Love",
    "Original Release Date":"6/25/1982",
    "Release Date":"2/5/2002",
    "Format":"Stream",
    "UPC (Barcode)":731453825024,
    "Label":"Mercury",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"ABC",
    "Title":"Alphabet City",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":042283239111,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABC",
    "Title":"When Smokey Sings",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABC",
    "Title":"How to Be a Zillionaire!",
    "Original Release Date":"January 1985",
    "Release Date":"January 1985",
    "Format":"LP",
    "UPC (Barcode)":042282490414,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABC",
    "Title":"Be Near Me",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ABC",
    "Title":"How to Be a Zillionaire!",
    "Original Release Date":"January 1985",
    "Release Date":"January 1985",
    "Format":"Stream",
    "UPC (Barcode)":042282490414,
    "Label":"Mercury",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"ACO",
    "Title":"Luck",
    "Original Release Date":"1/18/2012",
    "Release Date":"1/18/2012",
    "Format":"CD",
    "UPC (Barcode)":4543034030859,
    "Label":"AWDR/LR2",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ACO",
    "Title":"devil's hands",
    "Original Release Date":"10/6/2010",
    "Release Date":"10/6/2010",
    "Format":"CD",
    "UPC (Barcode)":4543034026227,
    "Label":"Naturalize Entertainment",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ACO",
    "Title":"devil's hands",
    "Original Release Date":"10/6/2010",
    "Release Date":"10/6/2010",
    "Format":"MP3",
    "UPC (Barcode)":4543034026227,
    "Label":"Naturalize Entertainment",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ACO",
    "Title":"mask",
    "Original Release Date":"2/22/2006",
    "Release Date":"2/22/2006",
    "Format":"CD",
    "UPC (Barcode)":4582117985624,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ACO",
    "Title":"irony",
    "Original Release Date":"6/18/2003",
    "Release Date":"6/18/2003",
    "Format":"CD",
    "UPC (Barcode)":4582117981657,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ACO",
    "Title":"Machi",
    "Original Release Date":"5/21/2003",
    "Release Date":"5/21/2003",
    "Format":"CD",
    "UPC (Barcode)":4582117981619,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ACO",
    "Title":"material",
    "Original Release Date":"5/23/2001",
    "Release Date":"5/23/2001",
    "Format":"CD",
    "UPC (Barcode)":4988009038506,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ACO",
    "Title":"Heart wo Moyashite",
    "Original Release Date":"11/1/2000",
    "Release Date":"11/1/2000",
    "Format":"CD",
    "UPC (Barcode)":4988009035901,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ACO",
    "Title":"absolute ego",
    "Original Release Date":"12/15/1999",
    "Release Date":"12/15/1999",
    "Format":"CD",
    "UPC (Barcode)":4988009031408,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ACO",
    "Title":"Kittenish Love",
    "Original Release Date":"4/21/1998",
    "Release Date":"4/21/1998",
    "Format":"CD",
    "UPC (Barcode)":4988009022604,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Adams",
    "Title":"Son Of Chamber Symphony / String Quartet",
    "Original Release Date":"6/21/2011",
    "Release Date":"6/21/2011",
    "Format":"CD",
    "UPC (Barcode)":075597980080,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Adams",
    "Title":"Son Of Chamber Symphony / String Quartet",
    "Original Release Date":"6/21/2011",
    "Release Date":"6/21/2011",
    "Format":"MP3",
    "UPC (Barcode)":075597980080,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Adams",
    "Title":"Hallelujah Junction",
    "Original Release Date":"10/7/2008",
    "Release Date":"10/7/2008",
    "Format":"CD",
    "UPC (Barcode)":075597989212,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Adams",
    "Title":"Harmonielehre (City Of Birmingham Symphony Orchestra Feat. Conductor: Simon Rattle)",
    "Original Release Date":"4/12/1994",
    "Release Date":"4/12/1994",
    "Format":"CD",
    "UPC (Barcode)":724355505125,
    "Label":"EMI Classics",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Adams",
    "Title":"American Elegies",
    "Original Release Date":"3/5/1991",
    "Release Date":"3/5/1991",
    "Format":"CD",
    "UPC (Barcode)":075597924923,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Adams",
    "Title":"American Elegies",
    "Original Release Date":"2/22/1991",
    "Release Date":"2/22/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075597924947,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"John Adams",
    "Title":"Nixon in China",
    "Original Release Date":"1988",
    "Release Date":"10/25/1990",
    "Format":"CD",
    "UPC (Barcode)":075597917727,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Adams",
    "Title":"Nixon in China",
    "Original Release Date":"1988",
    "Release Date":"10/25/1990",
    "Format":"MP3",
    "UPC (Barcode)":075597917727,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Adams",
    "Title":"The Chairman Dances",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":075597914429,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Adams",
    "Title":"The Chairman Dances",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"Cassette",
    "UPC (Barcode)":075597914443,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"John Adams",
    "Title":"Harmonielehre",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"LP",
    "UPC (Barcode)":075597911510,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Adele",
    "Title":"21",
    "Original Release Date":"2/22/2011",
    "Release Date":"2/22/2011",
    "Format":"MP3",
    "UPC (Barcode)":886974469926,
    "Label":"XL",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"a-ha",
    "Title":"Take on Me",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"a-ha",
    "Title":"The Sun Always Shines on TV",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"AJICO",
    "Title":"Fukamidori",
    "Original Release Date":"2/7/2001",
    "Release Date":"2/7/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002412396,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"AJICO",
    "Title":"Pepin",
    "Original Release Date":"6/27/2001",
    "Release Date":"6/27/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002417827,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"AJICO",
    "Title":"AJICO Show",
    "Original Release Date":"7/25/2001",
    "Release Date":"7/25/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002417865,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Akasau",
    "Title":"Akasau",
    "Original Release Date":"7/27/2004",
    "Release Date":"7/27/2004",
    "Format":"MP3",
    "UPC (Barcode)":794017303123,
    "Label":"Domo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Alarm Will Sound",
    "Title":"a/rhythmia",
    "Original Release Date":"9/15/2009",
    "Release Date":"9/15/2009",
    "Format":"CD",
    "UPC (Barcode)":075597993301,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Alarm Will Sound",
    "Title":"a/rhythmia",
    "Original Release Date":"9/15/2009",
    "Release Date":"9/15/2009",
    "Format":"Stream",
    "UPC (Barcode)":075597993301,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Alaya Vijana",
    "Title":"Alaya Vijana",
    "Original Release Date":"2/4/2004",
    "Release Date":"2/4/2004",
    "Format":"CD",
    "UPC (Barcode)":4540399095042,
    "Label":"MusicRobita/High Contrast",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Matt Alber",
    "Title":"I Wanna Dance with Somebody",
    "Original Release Date":"7/24/2012",
    "Release Date":"7/24/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Matt Alber/Lonesome Dandelion",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Matt Alber",
    "Title":"Constant Crows",
    "Original Release Date":"11/15/2011",
    "Release Date":"11/15/2011",
    "Format":"CD",
    "UPC (Barcode)":729440588953,
    "Label":"Matt Alber/Lonesome Dandelion",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Matt Alber",
    "Title":"Hide Nothing",
    "Original Release Date":"10/7/2008",
    "Release Date":"10/7/2008",
    "Format":"MP3",
    "UPC (Barcode)":661868169225,
    "Label":"Tommy Boy Silver",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Matt Alber",
    "Title":"Hide Nothing",
    "Original Release Date":"11/18/2008",
    "Release Date":"11/18/2008",
    "Format":"CD",
    "UPC (Barcode)":661868169225,
    "Label":"Tommy Boy Silver",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Matt Alber",
    "Title":"Star of Wonder",
    "Original Release Date":"12/2/2008",
    "Release Date":"12/2/2008",
    "Format":"MP3",
    "UPC (Barcode)":661868260465,
    "Label":"Tommy Boy Silver",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Alice In Chains",
    "Title":"Facelift",
    "Original Release Date":"8/10/1990",
    "Release Date":"8/10/1990",
    "Format":"Cassette",
    "UPC (Barcode)":074644607543,
    "Label":"Columbia",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Franghiz Ali-Zadeh",
    "Title":"Crossings",
    "Original Release Date":"3/18/1997",
    "Release Date":"3/18/1997",
    "Format":"CD",
    "UPC (Barcode)":789368542925,
    "Label":"BIS",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Franghiz Ali-Zadeh; Kronos Quartet",
    "Title":"Mugam Sayagi: Music Of Franghiz Ali-Zadeh",
    "Original Release Date":"1/11/2005",
    "Release Date":"1/11/2005",
    "Format":"CD",
    "UPC (Barcode)":075597980424,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"All About Eve",
    "Title":"Scarlet And Other Stories",
    "Original Release Date":"10/16/1989",
    "Release Date":"2/24/2003",
    "Format":"CD",
    "UPC (Barcode)":042283896529,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"All About Eve",
    "Title":"Scarlet and Other Stories",
    "Original Release Date":"11/21/1989",
    "Release Date":"11/21/1989",
    "Format":"Cassette",
    "UPC (Barcode)":042283896543,
    "Label":"Polygram",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"All About Eve",
    "Title":"All About Eve",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"Cassette",
    "UPC (Barcode)":042283426047,
    "Label":"Polygram",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Sam Amidon",
    "Title":"Bright Sunny South",
    "Original Release Date":"5/14/2013",
    "Release Date":"5/14/2013",
    "Format":"CD",
    "UPC (Barcode)":075597961942,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sam Amidon",
    "Title":"I See The Sign",
    "Original Release Date":"4/20/2010",
    "Release Date":"4/20/2010",
    "Format":"MP3",
    "UPC (Barcode)":880319458323,
    "Label":"Bedroom Community",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sam Amidon",
    "Title":"All Is Well",
    "Original Release Date":"2/5/2008",
    "Release Date":"2/5/2008",
    "Format":"MP3",
    "UPC (Barcode)":5060096472926,
    "Label":"Bedroom Community",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sam Amidon",
    "Title":"All Is Well",
    "Original Release Date":"2/5/2008",
    "Release Date":"2/5/2008",
    "Format":"CD",
    "UPC (Barcode)":5060096472926,
    "Label":"Bedroom Community",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"... And You Will Know Us by the Trail of Dead",
    "Title":"Lost Songs",
    "Original Release Date":"10/23/2012",
    "Release Date":"10/23/2012",
    "Format":"CD",
    "UPC (Barcode)":885417060126,
    "Label":"Superball Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"... And You Will Know Us by the Trail of Dead",
    "Title":"Tao Of The Dead",
    "Original Release Date":"2/8/2011",
    "Release Date":"2/8/2011",
    "Format":"CD",
    "UPC (Barcode)":885417045529,
    "Label":"Richter Scale",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"... And You Will Know Us by the Trail of Dead",
    "Title":"The Century of Self",
    "Original Release Date":"2/17/2009",
    "Release Date":"2/17/2009",
    "Format":"CD",
    "UPC (Barcode)":719488350131,
    "Label":"Richter Scale",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"... And You Will Know Us by the Trail of Dead",
    "Title":"The Century of Self",
    "Original Release Date":"2/17/2009",
    "Release Date":"2/17/2009",
    "Format":"MP3",
    "UPC (Barcode)":719488350131,
    "Label":"Richter Scale",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"... And You Will Know Us by the Trail of Dead",
    "Title":"Festival Thyme",
    "Original Release Date":"10/28/2008",
    "Release Date":"10/28/2008",
    "Format":"MP3",
    "UPC (Barcode)":719488350124,
    "Label":"Richter Scale",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"... And You Will Know Us by the Trail of Dead",
    "Title":"So Divided",
    "Original Release Date":"11/14/2006",
    "Release Date":"11/14/2006",
    "Format":"CD",
    "UPC (Barcode)":602517084179,
    "Label":"Interscope",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"... And You Will Know Us by the Trail of Dead",
    "Title":"Worlds Apart",
    "Original Release Date":"1/25/2005",
    "Release Date":"1/25/2005",
    "Format":"CD",
    "UPC (Barcode)":602498635308,
    "Label":"Interscope",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"... And You Will Know Us by the Trail of Dead",
    "Title":"Source Tags & Codes",
    "Original Release Date":"2/26/2002",
    "Release Date":"2/26/2002",
    "Format":"CD",
    "UPC (Barcode)":606949323622,
    "Label":"Interscope",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"... And You Will Know Us by the Trail of Dead",
    "Title":"Madonna",
    "Original Release Date":"10/19/1999",
    "Release Date":"10/19/1999",
    "Format":"CD",
    "UPC (Barcode)":036172947126,
    "Label":"Merge",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"... And You Will Know Us by the Trail of Dead",
    "Title":"... And You Will Know Us by the Trail of Dead",
    "Original Release Date":"1/20/1998",
    "Release Date":"1/20/1998",
    "Format":"CD",
    "UPC (Barcode)":036172996629,
    "Label":"Trance Syndicate",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Timothy Andres",
    "Title":"Home Stretch",
    "Original Release Date":"7/26/2013",
    "Release Date":"7/26/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Timothy Andres",
    "Title":"Shy and Mighty",
    "Original Release Date":"5/18/2010",
    "Release Date":"5/18/2010",
    "Format":"CD",
    "UPC (Barcode)":075597980288,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Animal Logic",
    "Title":"Animal Logic",
    "Original Release Date":"9/27/1989",
    "Release Date":"9/27/1989",
    "Format":"Cassette",
    "UPC (Barcode)":077771302046,
    "Label":"I.R.S.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Animotion",
    "Title":"Obsession",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ann Sally",
    "Title":"Brand-New Orleans",
    "Original Release Date":"4/27/2005",
    "Release Date":"4/27/2005",
    "Format":"CD",
    "UPC (Barcode)":4988112414969,
    "Label":"Video Arts Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Antony and the Johnsons",
    "Title":"Swanlights",
    "Original Release Date":"10/12/2010",
    "Release Date":"10/12/2010",
    "Format":"MP3",
    "UPC (Barcode)":883870057325,
    "Label":"Secretly Canadian",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Antony and the Johnsons",
    "Title":"The Crying Light",
    "Original Release Date":"1/26/2009",
    "Release Date":"1/26/2009",
    "Format":"MP3",
    "UPC (Barcode)":0656605019420,
    "Label":"Secretly Canadian",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Antony and the Johnsons",
    "Title":"The Crying Light",
    "Original Release Date":"1/20/2009",
    "Release Date":"1/20/2009",
    "Format":"CD",
    "UPC (Barcode)":656605019420,
    "Label":"Secretly Canadian",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Antony and the Johnsons",
    "Title":"Another World",
    "Original Release Date":"10/7/2008",
    "Release Date":"10/7/2008",
    "Format":"MP3",
    "UPC (Barcode)":656605019321,
    "Label":"Secretly Canadian",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Antony and the Johnsons",
    "Title":"I Am a Bird Now",
    "Original Release Date":"2/1/2005",
    "Release Date":"2/1/2005",
    "Format":"CD",
    "UPC (Barcode)":656605010526,
    "Label":"Secretly Canadian",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Antony and the Johnsons",
    "Title":"Antony and the Johnsons",
    "Original Release Date":"7/20/2004",
    "Release Date":"7/20/2004",
    "Format":"CD",
    "UPC (Barcode)":656605010427,
    "Label":"Secretly Canadian",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Antony and the Johnsons",
    "Title":"Antony and the Johnsons",
    "Original Release Date":"7/20/2004",
    "Release Date":"7/20/2004",
    "Format":"MP3",
    "UPC (Barcode)":656605010427,
    "Label":"Secretly Canadian",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Arcade Fire",
    "Title":"The Suburbs",
    "Original Release Date":"8/3/2010",
    "Release Date":"8/3/2010",
    "Format":"MP3",
    "UPC (Barcode)":602527426297,
    "Label":"Merge",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Arcade Fire",
    "Title":"Neon Bible",
    "Original Release Date":"3/6/2007",
    "Release Date":"3/6/2007",
    "Format":"CD",
    "UPC (Barcode)":673855028521,
    "Label":"Merge",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Arcade Fire",
    "Title":"Funeral",
    "Original Release Date":"9/14/2004",
    "Release Date":"9/14/2004",
    "Format":"CD",
    "UPC (Barcode)":036172955527,
    "Label":"Merge",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Arcadia",
    "Title":"So Red the Rose (Special Edition)",
    "Original Release Date":"1985",
    "Release Date":"4/20/2010",
    "Format":"CD",
    "UPC (Barcode)":509960668127,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Arcadia",
    "Title":"So Red The Rose",
    "Original Release Date":"November 1985",
    "Release Date":"7/2/1991",
    "Format":"CD",
    "UPC (Barcode)":077779635825,
    "Label":"Capitol",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Arcadia",
    "Title":"The Promise",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Arcadia",
    "Title":"So Red The Rose",
    "Original Release Date":"November 1985",
    "Release Date":"November 1985",
    "Format":"LP",
    "UPC (Barcode)":077771242816,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Arcadia",
    "Title":"Election Day",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Craig Armstrong",
    "Title":"As If To Nothing",
    "Original Release Date":"4/16/2002",
    "Release Date":"4/16/2002",
    "Format":"CD",
    "UPC (Barcode)":724381190722,
    "Label":"Source",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Craig Armstrong",
    "Title":"The Space Between Us",
    "Original Release Date":"2/24/1998",
    "Release Date":"2/24/1998",
    "Format":"CD",
    "UPC (Barcode)":017046962728,
    "Label":"Melankolic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"Influence",
    "Original Release Date":"7/19/2010",
    "Release Date":"7/19/2010",
    "Format":"CD",
    "UPC (Barcode)":698458821225,
    "Label":"ZTT",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"Who's Afraid Of...",
    "Original Release Date":"1984",
    "Release Date":"10/25/2005",
    "Format":"CD",
    "UPC (Barcode)":827912024128,
    "Label":"ZTT",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"The Seduction Of Claude Debussy",
    "Original Release Date":"6/29/1999",
    "Release Date":"6/29/1999",
    "Format":"CD",
    "UPC (Barcode)":601215323528,
    "Label":"ZTT",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"The Best of the Art Of Noise",
    "Original Release Date":"1992",
    "Release Date":"1/14/1997",
    "Format":"CD",
    "UPC (Barcode)":010467410823,
    "Label":"Discovery",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"In No Sense? Nonsense!",
    "Original Release Date":"1987",
    "Release Date":"11/12/1993",
    "Format":"CD",
    "UPC (Barcode)":752790101725,
    "Label":"China",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"In No Sense? Nonsense!",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":044114157014,
    "Label":"China",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"Dragnet",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"China",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"In Visible Silence",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"LP",
    "UPC (Barcode)":044114152811,
    "Label":"China",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"Peter Gunn",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"7 Inch",
    "UPC (Barcode)":044114298670,
    "Label":"China",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"Paranoimia",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"7 Inch",
    "UPC (Barcode)":041144300274,
    "Label":"China",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"Legacy",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"China",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"In Visible Silence",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"CD",
    "UPC (Barcode)":044114152828,
    "Label":"China",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"Legs",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":044114293279,
    "Label":"China",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"Who's Afraid Of...",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"LP",
    "UPC (Barcode)":075679017918,
    "Label":"ZTT",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Art of Noise",
    "Title":"Beat Box/Close (To the Edit)",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ART-SCHOOL",
    "Title":"Love/Hate",
    "Original Release Date":"11/12/2003",
    "Release Date":"11/12/2003",
    "Format":"CD",
    "UPC (Barcode)":4988006187894,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ashanti",
    "Title":"Ashanti",
    "Original Release Date":"4/2/2002",
    "Release Date":"4/2/2002",
    "Format":"CD",
    "UPC (Barcode)":731458683025,
    "Label":"BlackGround",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ASIAN KUNG-FU GENERATION",
    "Title":"LANDMARK",
    "Original Release Date":"9/12/2012",
    "Release Date":"9/12/2012",
    "Format":"CD",
    "UPC (Barcode)":4562292975542,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ASIAN KUNG-FU GENERATION",
    "Title":"BEST HIT AKG",
    "Original Release Date":"1/18/2012",
    "Release Date":"1/18/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ASIAN KUNG-FU GENERATION",
    "Title":"World World World",
    "Original Release Date":"3/5/2008",
    "Release Date":"3/5/2008",
    "Format":"CD",
    "UPC (Barcode)":4582117988113,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ASIAN KUNG-FU GENERATION",
    "Title":"Sol-fa",
    "Original Release Date":"10/20/2004",
    "Release Date":"10/20/2004",
    "Format":"CD",
    "UPC (Barcode)":4582117983255,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Asylum Street Spankers",
    "Title":"Hot Lunch",
    "Original Release Date":"2/23/1999",
    "Release Date":"2/23/1999",
    "Format":"CD",
    "UPC (Barcode)":788862990126,
    "Label":"Cold Spring",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Asylum Street Spankers",
    "Title":"Spanks for the Memories",
    "Original Release Date":"11/5/1996",
    "Release Date":"11/5/1996",
    "Format":"CD",
    "UPC (Barcode)":715971106025,
    "Label":"Watermelon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Aterciopelados",
    "Title":"Caribe Atómico",
    "Original Release Date":"7/13/1998",
    "Release Date":"7/13/1998",
    "Format":"CD",
    "UPC (Barcode)":743215945024,
    "Label":"BMG Latin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Aterciopelados",
    "Title":"El Dorado",
    "Original Release Date":"10/24/1995",
    "Release Date":"10/24/1995",
    "Format":"MP3",
    "UPC (Barcode)":0743212620429,
    "Label":"BMG Latin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Aterciopelados",
    "Title":"El Dorado",
    "Original Release Date":"10/24/1995",
    "Release Date":"10/24/1995",
    "Format":"Stream",
    "UPC (Barcode)":0743212620429,
    "Label":"BMG Latin",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"avengers in sci-fi",
    "Title":"avenger strikes back",
    "Original Release Date":"8/9/2006",
    "Release Date":"8/9/2006",
    "Format":"CD",
    "UPC (Barcode)":4522197070575,
    "Label":"K-PLAN",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The B-52's",
    "Title":"Funplex",
    "Original Release Date":"3/25/2008",
    "Release Date":"3/25/2008",
    "Format":"CD",
    "UPC (Barcode)":094922873071,
    "Label":"Astralwerks",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The B-52's",
    "Title":"Cosmic Thing",
    "Original Release Date":"6/6/1989",
    "Release Date":"6/6/1989",
    "Format":"CD",
    "UPC (Barcode)":075992585422,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The B-52's",
    "Title":"Cosmic Thing",
    "Original Release Date":"6/23/1989",
    "Release Date":"6/23/1989",
    "Format":"Cassette",
    "UPC (Barcode)":075992585446,
    "Label":"Sire",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The B-52's",
    "Title":"Rock Lobster/Private Idaho",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The B-52's",
    "Title":"The B-52's",
    "Original Release Date":"1979",
    "Release Date":"1979",
    "Format":"MP3",
    "UPC (Barcode)":075992739726,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Carl Phillippe Emmanuel Bach; Bernard Brauchli",
    "Title":"Keyboard Works Of Carl Philipp Emanuel Bach",
    "Original Release Date":"10/27/1993",
    "Release Date":"10/27/1993",
    "Format":"CD",
    "UPC (Barcode)":045591018621,
    "Label":"Titanic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Carl Phillippe Emmanuel Bach; François Chaplin",
    "Title":"Keyboard Sonatas",
    "Original Release Date":"7/28/1998",
    "Release Date":"7/28/1998",
    "Format":"CD",
    "UPC (Barcode)":730099464024,
    "Label":"Naxos",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Johann Sebastian Bach; Glenn Gould",
    "Title":"A State of Wonder: The Complete Goldberg Variations 1955 & 1981",
    "Original Release Date":"9/3/2002",
    "Release Date":"9/3/2002",
    "Format":"CD",
    "UPC (Barcode)":696998770324,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Johann Sebastian Bach; Gustav Leonhardt",
    "Title":"Inventions & Sinfonias",
    "Original Release Date":"8/9/1999",
    "Release Date":"8/9/1999",
    "Format":"CD",
    "UPC (Barcode)":074646186923,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Back Horn",
    "Title":"Shinzoo Orchestra",
    "Original Release Date":"11/13/2002",
    "Release Date":"11/13/2002",
    "Format":"CD",
    "UPC (Barcode)":4988002438198,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Bad Plus",
    "Title":"For All I Care",
    "Original Release Date":"2/3/2009",
    "Release Date":"2/3/2009",
    "Format":"MP3",
    "UPC (Barcode)":053361314828,
    "Label":"Heads Up",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Bad Plus",
    "Title":"For All I Care",
    "Original Release Date":"2/3/2009",
    "Release Date":"2/3/2009",
    "Format":"CD",
    "UPC (Barcode)":053361314828,
    "Label":"Heads Up",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Bad Plus",
    "Title":"These are the Vistas",
    "Original Release Date":"2/11/2003",
    "Release Date":"2/11/2003",
    "Format":"CD",
    "UPC (Barcode)":696998704022,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Bad Plus",
    "Title":"The Bad Plus",
    "Original Release Date":"11/15/2001",
    "Release Date":"11/15/2001",
    "Format":"MP3",
    "UPC (Barcode)":758661333028,
    "Label":"Fresh Sounds",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Anita Baker",
    "Title":"Rapture",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"LP",
    "UPC (Barcode)":075596044417,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Anita Baker",
    "Title":"Rapture",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"MP3",
    "UPC (Barcode)":075596044424,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bananarama",
    "Title":"Venus",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"London",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Band Aid",
    "Title":"Do They Know It's Christmas",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"7 Inch",
    "UPC (Barcode)":074640474972,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bang On A Can All-Stars",
    "Title":"Renegade Heaven",
    "Original Release Date":"3/13/2001",
    "Release Date":"3/13/2001",
    "Format":"MP3",
    "UPC (Barcode)":660355750328,
    "Label":"Cantaloupe",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bang On A Can All-Stars",
    "Title":"Music For Airports",
    "Original Release Date":"2/24/1998",
    "Release Date":"2/24/1998",
    "Format":"CD",
    "UPC (Barcode)":731453684720,
    "Label":"Point",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bang On A Can All-Stars",
    "Title":"Cheating, Lying, Stealing",
    "Original Release Date":"4/2/1996",
    "Release Date":"4/2/1996",
    "Format":"CD",
    "UPC (Barcode)":074646225424,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bang On A Can All-Stars",
    "Title":"Industry",
    "Original Release Date":"4/25/1995",
    "Release Date":"4/25/1995",
    "Format":"CD",
    "UPC (Barcode)":074646648322,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bang On A Can All-Stars; Don Byron",
    "Title":"A Ballad for Many",
    "Original Release Date":"6/13/2006",
    "Release Date":"6/13/2006",
    "Format":"MP3",
    "UPC (Barcode)":0713746303624,
    "Label":"Cantaloupe",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mandy Barnett",
    "Title":"I've Got A Right To Cry",
    "Original Release Date":"4/13/1999",
    "Release Date":"4/13/1999",
    "Format":"CD",
    "UPC (Barcode)":649443104623,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Béla Bartók",
    "Title":"Music for Strings, Percussion and Celesta",
    "Original Release Date":"7/1/1991",
    "Release Date":"7/1/1991",
    "Format":"CD",
    "UPC (Barcode)":028943035224,
    "Label":"Decca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Béla Bartók; Vermeer Quartet",
    "Title":"String Quartets (Complete)",
    "Original Release Date":"5/17/2005",
    "Release Date":"5/17/2005",
    "Format":"CD",
    "UPC (Barcode)":747313254323,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Béla Bartók; Vermeer Quartet",
    "Title":"String Quartets (Complete)",
    "Original Release Date":"5/17/2005",
    "Release Date":"5/17/2005",
    "Format":"MP3",
    "UPC (Barcode)":747313254323,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Basia",
    "Title":"Time and Tide",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":074644076714,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"BBMak",
    "Title":"Sooner or Later",
    "Original Release Date":"5/16/2000",
    "Release Date":"5/16/2000",
    "Format":"CD",
    "UPC (Barcode)":720616226020,
    "Label":"Hollywood",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Beatles",
    "Title":"All You Need Is Love",
    "Original Release Date":"1967",
    "Release Date":"1967",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Beaux-Arts Quartet",
    "Title":"Diamond: String Quartet No. 4/Barber: String Quartet",
    "Original Release Date":"1965",
    "Release Date":"1965",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven",
    "Title":"'Razumovsky' Quartets, Op. 59",
    "Original Release Date":"1/10/2006",
    "Release Date":"1/10/2006",
    "Format":"MP3",
    "UPC (Barcode)":093046742362,
    "Label":"Harmonia Mundi",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven",
    "Title":"Piano Concerto No. 5 in E-flat major 'Emperor'/Grosse Fuge",
    "Original Release Date":"1/6/2004",
    "Release Date":"1/6/2004",
    "Format":"CD",
    "UPC (Barcode)":724358561623,
    "Label":"Classics for Pleasure",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven",
    "Title":"Symphonies No. 4 & No. 7",
    "Original Release Date":"6/2/1992",
    "Release Date":"6/2/1992",
    "Format":"CD",
    "UPC (Barcode)":074644815825,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven",
    "Title":"Symphonies No. 2 & No. 5",
    "Original Release Date":"1/3/1992",
    "Release Date":"1/3/1992",
    "Format":"CD",
    "UPC (Barcode)":074644765120,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven",
    "Title":"Symphonies No. 1 & No. 6 (Pastoral)",
    "Original Release Date":"8/19/1991",
    "Release Date":"8/19/1991",
    "Format":"CD",
    "UPC (Barcode)":074644653229,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven",
    "Title":"Symphony No. 9 & Fidelio Overture",
    "Original Release Date":"8/19/1991",
    "Release Date":"8/19/1991",
    "Format":"CD",
    "UPC (Barcode)":074644653328,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven",
    "Title":"Symphonies No. 3 (Eroica) & No .8",
    "Original Release Date":"4/5/1991",
    "Release Date":"4/5/1991",
    "Format":"CD",
    "UPC (Barcode)":074644632828,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven; Vladimir Ashkenazy",
    "Title":"Piano Sonatas No. 14, 23 & 8",
    "Original Release Date":"3/11/2003",
    "Release Date":"3/11/2003",
    "Format":"CD",
    "UPC (Barcode)":028947384526,
    "Label":"Decca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven; Emerson String Quartet",
    "Title":"The Late String Quartets",
    "Original Release Date":"7/1/2003",
    "Release Date":"7/1/2003",
    "Format":"CD",
    "UPC (Barcode)":028947434122,
    "Label":"Deutsche Grammophon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven; Isabelle Faust; Alexander Melnikov",
    "Title":"Violin Concerto/Kreutzer Sonata",
    "Original Release Date":"9/11/2007",
    "Release Date":"9/11/2007",
    "Format":"MP3",
    "UPC (Barcode)":794881847525,
    "Label":"Harmonia Mundi",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven; Lindsay Quartet",
    "Title":"The Late Beethoven Quartets, Vol. 2",
    "Original Release Date":"12/15/1993",
    "Release Date":"12/15/1993",
    "Format":"MP3",
    "UPC (Barcode)":743625060225,
    "Label":"ASV Living Era",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven; Rudolf Serkin",
    "Title":"Piano Concerto No. 5, in E Flat Major, Op. 73 'Emperor'",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"MP3",
    "UPC (Barcode)":089408006524,
    "Label":"Telarc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ludwig Van Beethoven; Stuttgart Piano Trio",
    "Title":"Piano Trios, Op. 70, Nos. 1 & 2",
    "Original Release Date":"2/8/1996",
    "Release Date":"2/8/1996",
    "Format":"MP3",
    "UPC (Barcode)":730099594820,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Garrin Benfield",
    "Title":"Where Joy Kills Sorrow",
    "Original Release Date":"10/8/2004",
    "Release Date":"10/8/2004",
    "Format":"CD",
    "UPC (Barcode)":783707980423,
    "Label":"Zacksongs Music",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Alban Berg; Kronos Quartet",
    "Title":"Lyric Suite",
    "Original Release Date":"8/19/2003",
    "Release Date":"8/19/2003",
    "Format":"CD",
    "UPC (Barcode)":075597969627,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sarah Bettens",
    "Title":"Scream",
    "Original Release Date":"8/23/2005",
    "Release Date":"8/23/2005",
    "Format":"MP3",
    "UPC (Barcode)":614992004320,
    "Label":"Hybrid",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Björk",
    "Title":"Biophilia",
    "Original Release Date":"10/11/2011",
    "Release Date":"10/11/2011",
    "Format":"CD",
    "UPC (Barcode)":075597964080,
    "Label":"Universal",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Björk",
    "Title":"Volta",
    "Original Release Date":"5/8/2007",
    "Release Date":"5/8/2007",
    "Format":"CD",
    "UPC (Barcode)":075678998980,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Björk",
    "Title":"Medúlla",
    "Original Release Date":"8/31/2004",
    "Release Date":"8/31/2004",
    "Format":"CD",
    "UPC (Barcode)":075596298421,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Björk",
    "Title":"Greatest Hits",
    "Original Release Date":"11/5/2002",
    "Release Date":"11/5/2002",
    "Format":"CD",
    "UPC (Barcode)":075596278720,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Björk",
    "Title":"Homogenic",
    "Original Release Date":"9/23/1997",
    "Release Date":"9/23/1997",
    "Format":"CD",
    "UPC (Barcode)":075596206129,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Björk",
    "Title":"Post",
    "Original Release Date":"6/13/1995",
    "Release Date":"6/13/1995",
    "Format":"CD",
    "UPC (Barcode)":075596174022,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Björk; Tríó Guðmundar Ingólfssonar",
    "Title":"Gling-Gló",
    "Original Release Date":"October 1990",
    "Release Date":"9/2/2003",
    "Format":"CD",
    "UPC (Barcode)":827954006120,
    "Label":"One Little Indian",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Black Lipstick",
    "Title":"Sincerely, ...",
    "Original Release Date":"1/28/2005",
    "Release Date":"1/28/2005",
    "Format":"MP3",
    "UPC (Barcode)":655035021522,
    "Label":"Peek-a-Boo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Black Lipstick",
    "Title":"Converted Thieves",
    "Original Release Date":"4/29/2003",
    "Release Date":"4/29/2003",
    "Format":"CD",
    "UPC (Barcode)":655035021225,
    "Label":"Peek-a-Boo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"James Blake",
    "Title":"Overgrown",
    "Original Release Date":"4/9/2013",
    "Release Date":"4/9/2013",
    "Format":"CD",
    "UPC (Barcode)":602537324729,
    "Label":"Universal",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"James Blake",
    "Title":"Overgrown",
    "Original Release Date":"4/9/2013",
    "Release Date":"4/9/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Universal",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"James Blake",
    "Title":"James Blake",
    "Original Release Date":"3/22/2011",
    "Release Date":"3/22/2011",
    "Format":"CD",
    "UPC (Barcode)":602527554709,
    "Label":"Universal Republic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"James Blake",
    "Title":"James Blake",
    "Original Release Date":"3/22/2011",
    "Release Date":"3/22/2011",
    "Format":"MP3",
    "UPC (Barcode)":602527554709,
    "Label":"Universal Republic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"BLEACH",
    "Title":"bleachstone",
    "Original Release Date":"7/7/2009",
    "Release Date":"7/7/2009",
    "Format":"CD",
    "UPC (Barcode)":4547292128150,
    "Label":"Highwave",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"BLEACH",
    "Title":"Kien",
    "Original Release Date":"6/6/2008",
    "Release Date":"6/6/2008",
    "Format":"CD",
    "UPC (Barcode)":4547292126156,
    "Label":"Highwave",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"BLEACH",
    "Title":"Migi mo Hidari mo Shihaisuru wa Kyoo mo Niku wo Kui YODARE wo Tarasu",
    "Original Release Date":"5/5/2006",
    "Release Date":"5/5/2006",
    "Format":"CD",
    "UPC (Barcode)":4547292122158,
    "Label":"Highwave",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"BLEACH",
    "Title":"Bleach",
    "Original Release Date":"12/12/2003",
    "Release Date":"12/12/2003",
    "Format":"CD",
    "UPC (Barcode)":4547292117154,
    "Label":"Highwave",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"BLEACH",
    "Title":"Kibakuzai",
    "Original Release Date":"3/27/2002",
    "Release Date":"3/27/2002",
    "Format":"CD",
    "UPC (Barcode)":4544403121369,
    "Label":"Ripley/Micro Force",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"BLEACH",
    "Title":"Hadaka no Jyoou",
    "Original Release Date":"12/12/2001",
    "Release Date":"12/12/2001",
    "Format":"CD",
    "UPC (Barcode)":4988006176935,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mary J. Blige",
    "Title":"Love & Life",
    "Original Release Date":"8/26/2003",
    "Release Date":"8/26/2003",
    "Format":"CD",
    "UPC (Barcode)":602498606117,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bloc Party",
    "Title":"A Weekend in the City",
    "Original Release Date":"2/6/2007",
    "Release Date":"2/6/2007",
    "Format":"MP3",
    "UPC (Barcode)":075679459824,
    "Label":"Vice",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bloc Party",
    "Title":"Silent Alarm",
    "Original Release Date":"3/22/2005",
    "Release Date":"3/22/2005",
    "Format":"MP3",
    "UPC (Barcode)":075679381521,
    "Label":"Vice",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"bloodthirsty butchers",
    "Title":"Guitarist wo Korosanaide",
    "Original Release Date":"5/16/2007",
    "Release Date":"5/16/2007",
    "Format":"CD",
    "UPC (Barcode)":4582293470068,
    "Label":"391tone",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"bloodthirsty butchers",
    "Title":"Banging The Drum",
    "Original Release Date":"4/6/2005",
    "Release Date":"4/6/2005",
    "Format":"CD",
    "UPC (Barcode)":4988001936350,
    "Label":"Nippon Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"bloodthirsty butchers",
    "Title":"birdy",
    "Original Release Date":"3/3/2004",
    "Release Date":"3/3/2004",
    "Format":"CD",
    "UPC (Barcode)":4988001996330,
    "Label":"Nippon Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"bloodthirsty butchers",
    "Title":"blue on red (CCCD)",
    "Original Release Date":"9/29/2003",
    "Release Date":"9/29/2003",
    "Format":"CD",
    "UPC (Barcode)":4988006187795,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"bloodthirsty butchers",
    "Title":"green on red",
    "Original Release Date":"9/17/2003",
    "Release Date":"9/17/2003",
    "Format":"CD",
    "UPC (Barcode)":4988001952831,
    "Label":"Nippon Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"bloodthirsty butchers",
    "Title":"Kooya ni Okeru bloodthirsty butchers",
    "Original Release Date":"1/29/2003",
    "Release Date":"1/29/2003",
    "Format":"CD",
    "UPC (Barcode)":4988006181922,
    "Label":"Parlophone Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"bloodthirsty butchers",
    "Title":"yamane",
    "Original Release Date":"8/22/2001",
    "Release Date":"8/22/2001",
    "Format":"CD",
    "UPC (Barcode)":4988006175150,
    "Label":"Parlophone Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"bloodthirsty butchers",
    "Title":"Bloodthirsty Butchers",
    "Original Release Date":"1/29/1996",
    "Release Date":"1/29/1996",
    "Format":"CD",
    "UPC (Barcode)":718751930728,
    "Label":"Bacteria Sour",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bloque",
    "Title":"Bloque",
    "Original Release Date":"10/6/1998",
    "Release Date":"10/6/1998",
    "Format":"CD",
    "UPC (Barcode)":093624706021,
    "Label":"Luaka Bop",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Blowoff",
    "Title":"Blowoff",
    "Original Release Date":"10/17/2006",
    "Release Date":"10/17/2006",
    "Format":"CD",
    "UPC (Barcode)":800314895626,
    "Label":"Full Frequency Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Blowoff",
    "Title":"Blowoff",
    "Original Release Date":"10/17/2006",
    "Release Date":"10/17/2006",
    "Format":"MP3",
    "UPC (Barcode)":800314895626,
    "Label":"Full Frequency Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"BoA",
    "Title":"BoA",
    "Original Release Date":"3/17/2009",
    "Release Date":"3/17/2009",
    "Format":"MP3",
    "UPC (Barcode)":044003709287,
    "Label":"SM Entertainment USA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bonkin' Clapper",
    "Title":"Bonkanesia",
    "Original Release Date":"2/20/2002",
    "Release Date":"2/20/2002",
    "Format":"CD",
    "UPC (Barcode)":4542351000118,
    "Label":"Belo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bonnie Pink",
    "Title":"Chasing Hope",
    "Original Release Date":"7/25/2012",
    "Release Date":"7/25/2012",
    "Format":"CD",
    "UPC (Barcode)":4943674119332,
    "Label":"Warner Music Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bonnie Pink",
    "Title":"Golden Tears",
    "Original Release Date":"9/21/2005",
    "Release Date":"9/21/2005",
    "Format":"CD",
    "UPC (Barcode)":4943674058532,
    "Label":"Warner Music Japan",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Bonnie Pink",
    "Title":"Even So",
    "Original Release Date":"5/12/2004",
    "Release Date":"5/12/2004",
    "Format":"CD",
    "UPC (Barcode)":4943674050932,
    "Label":"Organon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bonnie Pink",
    "Title":"Present",
    "Original Release Date":"2/19/2003",
    "Release Date":"2/19/2003",
    "Format":"CD",
    "UPC (Barcode)":4943674038381,
    "Label":"Organon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bonnie Pink",
    "Title":"Let Go",
    "Original Release Date":"4/5/2000",
    "Release Date":"4/5/2000",
    "Format":"CD",
    "UPC (Barcode)":4988029477033,
    "Label":"EastWest Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Boom Boom Satellites",
    "Title":"Full of Elevating Pleasures",
    "Original Release Date":"3/7/2006",
    "Release Date":"3/7/2006",
    "Format":"CD",
    "UPC (Barcode)":828915002625,
    "Label":"Tofu",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Boom Boom Satellites",
    "Title":"Umbra",
    "Original Release Date":"2/7/2001",
    "Release Date":"2/7/2001",
    "Format":"CD",
    "UPC (Barcode)":4988009499796,
    "Label":"Sony Music Entertainment",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Boom Boom Satellites",
    "Title":"Out Loud",
    "Original Release Date":"6/1/1999",
    "Release Date":"6/1/1999",
    "Format":"CD",
    "UPC (Barcode)":074646991121,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Brídín Brennan",
    "Title":"Eyes of Innocence",
    "Original Release Date":"8/19/2005",
    "Release Date":"8/19/2005",
    "Format":"MP3",
    "UPC (Barcode)":5392000014831,
    "Label":"MDM",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Máire Brennan",
    "Title":"Máire",
    "Original Release Date":"9/15/1992",
    "Release Date":"9/15/1992",
    "Format":"CD",
    "UPC (Barcode)":075678242120,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Edie Brickell and New Bohemians",
    "Title":"Shooting Rubberbands At The Stars",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":075992419215,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sarah Brightman",
    "Title":"The Songs That Got Away",
    "Original Release Date":"6/30/1989",
    "Release Date":"6/30/1989",
    "Format":"Cassette",
    "UPC (Barcode)":042283911642,
    "Label":"Polygram",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"the brilliant green",
    "Title":"The Winter Album",
    "Original Release Date":"12/4/2002",
    "Release Date":"12/4/2002",
    "Format":"CD",
    "UPC (Barcode)":4562104040659,
    "Label":"DefStar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"the brilliant green",
    "Title":"Los Angeles",
    "Original Release Date":"1/1/2001",
    "Release Date":"1/1/2001",
    "Format":"CD",
    "UPC (Barcode)":4988010102609,
    "Label":"DefStar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"the brilliant green",
    "Title":"Terra 2001",
    "Original Release Date":"9/16/1999",
    "Release Date":"9/16/1999",
    "Format":"CD",
    "UPC (Barcode)":4716331832628,
    "Label":"DefStar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"the brilliant green",
    "Title":"the brilliant green",
    "Original Release Date":"9/19/1998",
    "Release Date":"9/19/1998",
    "Format":"CD",
    "UPC (Barcode)":4988009436890,
    "Label":"DefStar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"William Brittelle",
    "Title":"Loving the Chambered Nautilis",
    "Original Release Date":"6/5/2012",
    "Release Date":"6/5/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"New Amsterdam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Benjamin Britten",
    "Title":"War Requiem / Sinfonia Da Requiem / Ballad Of Heroes",
    "Original Release Date":"9/30/1992",
    "Release Date":"9/30/1992",
    "Format":"CD",
    "UPC (Barcode)":095115898321,
    "Label":"Chandos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Benjamin Britten",
    "Title":"War Requiem / Sinfonia da Requiem / Ballad of Heroes",
    "Original Release Date":"9/30/1992",
    "Release Date":"9/30/1992",
    "Format":"MP3",
    "UPC (Barcode)":095115898321,
    "Label":"Chandos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Brothers Johnson",
    "Title":"Stomp!",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bruce Brubaker",
    "Title":"Time Curve",
    "Original Release Date":"6/16/2009",
    "Release Date":"6/16/2009",
    "Format":"MP3",
    "UPC (Barcode)":026724680625,
    "Label":"Arabesque",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bruce Brubaker",
    "Title":"Hope Street Tunnel Blues",
    "Original Release Date":"7/16/2007",
    "Release Date":"7/16/2007",
    "Format":"MP3",
    "UPC (Barcode)":026724679827,
    "Label":"Arabesque",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bruce Brubaker",
    "Title":"Inner Cities",
    "Original Release Date":"9/2/2003",
    "Release Date":"9/2/2003",
    "Format":"CD",
    "UPC (Barcode)":026724677625,
    "Label":"Arabesque",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bruce Brubaker",
    "Title":"Inner Cities",
    "Original Release Date":"9/2/2003",
    "Release Date":"9/2/2003",
    "Format":"MP3",
    "UPC (Barcode)":026724677625,
    "Label":"Arabesque",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bruce Brubaker",
    "Title":"Glass Cage",
    "Original Release Date":"10/10/2000",
    "Release Date":"10/10/2000",
    "Format":"MP3",
    "UPC (Barcode)":026724674426,
    "Label":"Arabesque",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Buffalo Daughter",
    "Title":"Pshychic",
    "Original Release Date":"3/29/2004",
    "Release Date":"3/29/2004",
    "Format":"CD",
    "UPC (Barcode)":5033197245928,
    "Label":"V2",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Buffalo Daughter",
    "Title":"I",
    "Original Release Date":"3/5/2002",
    "Release Date":"3/5/2002",
    "Format":"CD",
    "UPC (Barcode)":607217705126,
    "Label":"Emperor Norton",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Buffalo Daughter",
    "Title":"New Rock",
    "Original Release Date":"3/10/1998",
    "Release Date":"3/10/1998",
    "Format":"CD",
    "UPC (Barcode)":758148005226,
    "Label":"Grand Royal",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bugy Craxone",
    "Title":"sorry, I will scream here",
    "Original Release Date":"6/9/2004",
    "Release Date":"6/9/2004",
    "Format":"CD",
    "UPC (Barcode)":4997184825337,
    "Label":"ZubRockA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bugy Craxone",
    "Title":"Northern Hymns",
    "Original Release Date":"8/21/2002",
    "Release Date":"8/21/2002",
    "Format":"CD",
    "UPC (Barcode)":4988002434640,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bulgarian State Radio & Television Female Vocal Choir",
    "Title":"Le Mystère des voix Bulgares, Vol. 2",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"CD",
    "UPC (Barcode)":075597920123,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bulgarian State Radio & Television Female Vocal Choir",
    "Title":"Le Mystère de Voix Bulgares, Vol. 2",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"Cassette",
    "UPC (Barcode)":075597920147,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Bulgarian State Radio & Television Female Vocal Choir",
    "Title":"Le Mystère de Voix Bulgares",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":075597916515,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bulgarian State Radio & Television Female Vocal Choir",
    "Title":"Le Mystère de Voix Bulgares",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":075597916522,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bulgarian State Radio & Television Female Vocal Choir",
    "Title":"Le Mystère de Voix Bulgares",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"Cassette",
    "UPC (Barcode)":075597916546,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"50 Words For Snow",
    "Original Release Date":"11/22/2011",
    "Release Date":"11/22/2011",
    "Format":"CD",
    "UPC (Barcode)":045778718627,
    "Label":"Fish People",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"Wild Man",
    "Original Release Date":"10/18/2011",
    "Release Date":"10/18/2011",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Fish People",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"Director's Cut",
    "Original Release Date":"5/31/2011",
    "Release Date":"5/31/2011",
    "Format":"CD",
    "UPC (Barcode)":5099902777221,
    "Label":"Fish People",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"Aerial",
    "Original Release Date":"11/8/2005",
    "Release Date":"2005",
    "Format":"CD",
    "UPC (Barcode)":827969777220,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"The Kick Inside",
    "Original Release Date":"2/17/1978",
    "Release Date":"7/14/1997",
    "Format":"CD",
    "UPC (Barcode)":077774601221,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"The Red Shoes",
    "Original Release Date":"11/2/1993",
    "Release Date":"11/2/1993",
    "Format":"MP3",
    "UPC (Barcode)":0724382727729,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"The Red Shoes",
    "Original Release Date":"11/2/1993",
    "Release Date":"11/2/1993",
    "Format":"CD",
    "UPC (Barcode)":0724382727729,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"The Sensual World",
    "Original Release Date":"9/29/1989",
    "Release Date":"9/29/1989",
    "Format":"CD",
    "UPC (Barcode)":074644416442,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"The Sensual World",
    "Original Release Date":"9/29/1989",
    "Release Date":"9/29/1989",
    "Format":"Cassette",
    "UPC (Barcode)":074644416442,
    "Label":"Columbia",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"The Whole Story",
    "Original Release Date":"11/10/1986",
    "Release Date":"11/10/1986",
    "Format":"CD",
    "UPC (Barcode)":077774641425,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"Hounds Of Love",
    "Original Release Date":"9/20/1985",
    "Release Date":"9/20/1985",
    "Format":"CD",
    "UPC (Barcode)":077774616423,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"The Dreaming",
    "Original Release Date":"9/13/1982",
    "Release Date":"9/13/1982",
    "Format":"CD",
    "UPC (Barcode)":077774636124,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"Never for Ever",
    "Original Release Date":"9/8/1980",
    "Release Date":"9/8/1980",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"Never for Ever",
    "Original Release Date":"9/8/1980",
    "Release Date":"9/8/1980",
    "Format":"CD",
    "UPC (Barcode)":077774636025,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"The Kick Inside",
    "Original Release Date":"2/17/1978",
    "Release Date":"2/17/1978",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kate Bush",
    "Title":"Lionheart",
    "Original Release Date":"1978",
    "Release Date":"1978",
    "Format":"CD",
    "UPC (Barcode)":07774606523,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Butchies",
    "Title":"Make Yr Life",
    "Original Release Date":"4/6/2004",
    "Release Date":"4/6/2004",
    "Format":"CD",
    "UPC (Barcode)":634457206625,
    "Label":"Yep Roc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Chris Butler",
    "Title":"The Museum of Me, Vol. 1",
    "Original Release Date":"9/30/2003",
    "Release Date":"9/30/2003",
    "Format":"MP3",
    "UPC (Barcode)":803680292426,
    "Label":"Future Fossil Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Chris Butler",
    "Title":"The Devil Glitch",
    "Original Release Date":"3/11/2000",
    "Release Date":"3/11/2000",
    "Format":"MP3",
    "UPC (Barcode)":669910060025,
    "Label":"Future Fossil Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Chris Butler",
    "Title":"I Feel a Bit Normal Today",
    "Original Release Date":"3/11/2000",
    "Release Date":"3/11/2000",
    "Format":"MP3",
    "UPC (Barcode)":669910061022,
    "Label":"Future Fossil Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Café Tacuba",
    "Title":"El Objeto Antes Llamado Disco",
    "Original Release Date":"10/22/2012",
    "Release Date":"10/22/2012",
    "Format":"CD",
    "UPC (Barcode)":602537179510,
    "Label":"Universal Latino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Café Tacuba",
    "Title":"De Este Lado del Camino",
    "Original Release Date":"8/10/2012",
    "Release Date":"8/10/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Universal Latino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Café Tacuba",
    "Title":"Sino",
    "Original Release Date":"10/9/2007",
    "Release Date":"10/9/2007",
    "Format":"CD",
    "UPC (Barcode)":602517469563,
    "Label":"Universal Latino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Café Tacuba",
    "Title":"Cuatro Caminos",
    "Original Release Date":"7/1/2003",
    "Release Date":"7/1/2003",
    "Format":"CD",
    "UPC (Barcode)":008811324926,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Café Tacuba",
    "Title":"Vale Callampa",
    "Original Release Date":"10/29/2002",
    "Release Date":"10/29/2002",
    "Format":"CD",
    "UPC (Barcode)":008811396626,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Café Tacuba",
    "Title":"Tiempo transcurrido...",
    "Original Release Date":"8/21/2001",
    "Release Date":"8/21/2001",
    "Format":"CD",
    "UPC (Barcode)":809274072429,
    "Label":"WEA Latina",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Café Tacuba",
    "Title":"Reves/Yo Soy",
    "Original Release Date":"7/20/1999",
    "Release Date":"7/20/1999",
    "Format":"CD",
    "UPC (Barcode)":093624737421,
    "Label":"WEA Latina",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Café Tacuba",
    "Title":"Avalancha De éxitos",
    "Original Release Date":"11/5/1996",
    "Release Date":"11/5/1996",
    "Format":"CD",
    "UPC (Barcode)":706301671825,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Café Tacuba",
    "Title":"Avalancha De éxitos",
    "Original Release Date":"11/5/1996",
    "Release Date":"11/5/1996",
    "Format":"MP3",
    "UPC (Barcode)":706301671825,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Café Tacuba",
    "Title":"Re",
    "Original Release Date":"7/22/1994",
    "Release Date":"7/22/1994",
    "Format":"CD",
    "UPC (Barcode)":745099678425,
    "Label":"WEA Latina",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Café Tacuba",
    "Title":"Cafe Tacuba",
    "Original Release Date":"7/28/1992",
    "Release Date":"7/28/1992",
    "Format":"CD",
    "UPC (Barcode)":090317724126,
    "Label":"WEA Latina",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Camouflage",
    "Title":"Voices & Images",
    "Original Release Date":"1988",
    "Release Date":"10/25/1990",
    "Format":"CD",
    "UPC (Barcode)":075678188626,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Camouflage",
    "Title":"The Great Commandment",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"7 Inch",
    "UPC (Barcode)":075678903175,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Camper Van Beethoven",
    "Title":"La Costa Perdida",
    "Original Release Date":"1/22/2013",
    "Release Date":"1/22/2013",
    "Format":"MP3",
    "UPC (Barcode)":795041792020,
    "Label":"429",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Camper Van Beethoven",
    "Title":"Telephone Free Landslide Victory",
    "Original Release Date":"1985",
    "Release Date":"4/27/2004",
    "Format":"MP3",
    "UPC (Barcode)":750078014224,
    "Label":"SpinArt",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Camper Van Beethoven",
    "Title":"I & II",
    "Original Release Date":"January 1986",
    "Release Date":"4/27/2004",
    "Format":"MP3",
    "UPC (Barcode)":750078014125,
    "Label":"Cooking Vinyl",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Camper Van Beethoven",
    "Title":"Telephone Free Landslide Victory",
    "Original Release Date":"1985",
    "Release Date":"3/9/1993",
    "Format":"CD",
    "UPC (Barcode)":711297014228,
    "Label":"Independent Project",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Camper Van Beethoven",
    "Title":"Our Beloved Revolutionary Sweetheart",
    "Original Release Date":"5/24/1988",
    "Release Date":"6/29/1992",
    "Format":"CD",
    "UPC (Barcode)":077778605522,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Camper Van Beethoven",
    "Title":"Key Lime Pie",
    "Original Release Date":"8/30/1989",
    "Release Date":"8/30/1989",
    "Format":"CD",
    "UPC (Barcode)":075679128928,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Camper Van Beethoven",
    "Title":"Key Lime Pie",
    "Original Release Date":"9/5/1989",
    "Release Date":"9/5/1989",
    "Format":"Cassette",
    "UPC (Barcode)":075679128942,
    "Label":"Virgin",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Camper Van Beethoven",
    "Title":"Our Beloved Revolutionary Sweetheart",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"Cassette",
    "UPC (Barcode)":075679091840,
    "Label":"Virgin",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Kim Carnes",
    "Title":"Bette Davis Eyes",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Elliott Carter",
    "Title":"Music of Elliott Carter",
    "Original Release Date":"1991",
    "Release Date":"4/24/2001",
    "Format":"CD",
    "UPC (Barcode)":090438061025,
    "Label":"CRI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Elliott Carter; Pacifica Quartet",
    "Title":"String Quartets Nos 1 & 5",
    "Original Release Date":"1/29/2008",
    "Release Date":"1/29/2008",
    "Format":"MP3",
    "UPC (Barcode)":0075597124927,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Caitlin Cary",
    "Title":"I'm Staying Out",
    "Original Release Date":"4/22/2003",
    "Release Date":"4/22/2003",
    "Format":"CD",
    "UPC (Barcode)":634457204928,
    "Label":"Yep Roc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Caitlin Cary",
    "Title":"While You Weren't Looking",
    "Original Release Date":"3/19/2002",
    "Release Date":"3/19/2002",
    "Format":"CD",
    "UPC (Barcode)":634457202924,
    "Label":"Yep Roc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Johnny Cash",
    "Title":"Unchained",
    "Original Release Date":"6/2/1998",
    "Release Date":"6/2/1998",
    "Format":"CD",
    "UPC (Barcode)":093624309727,
    "Label":"American",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Cause & Effect",
    "Title":"Another Minute",
    "Original Release Date":"1991",
    "Release Date":"1991",
    "Format":"CD",
    "UPC (Barcode)":724451101924,
    "Label":"Sedona Recording Company",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cause & Effect",
    "Title":"Cause & Effect",
    "Original Release Date":"1990",
    "Release Date":"1990",
    "Format":"CD",
    "UPC (Barcode)":030997450021,
    "Label":"Exlie",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Change",
    "Title":"A Lover's Holiday",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tracy Chapman",
    "Title":"Tracy Chapman",
    "Original Release Date":"4/5/1988",
    "Release Date":"4/5/1988",
    "Format":"CD",
    "UPC (Barcode)":075596077422,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tracy Chapman",
    "Title":"Tracy Chapman",
    "Original Release Date":"4/5/1988",
    "Release Date":"4/5/1988",
    "Format":"LP",
    "UPC (Barcode)":075596077415,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"CHARA",
    "Title":"UTAKATA",
    "Original Release Date":"11/2/2011",
    "Release Date":"11/2/2011",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Happy Toy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"CHARA",
    "Title":"Dengon",
    "Original Release Date":"8/3/2011",
    "Release Date":"8/3/2011",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Happy Toy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"CHARA",
    "Title":"union",
    "Original Release Date":"2/28/2007",
    "Release Date":"2/28/2007",
    "Format":"CD",
    "UPC (Barcode)":4988005461087,
    "Label":"Universal Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"CHARA",
    "Title":"Madrigal",
    "Original Release Date":"7/18/2001",
    "Release Date":"7/18/2001",
    "Format":"CD",
    "UPC (Barcode)":4988010224721,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"CHARA",
    "Title":"Caramel Milk: The Best of CHARA",
    "Original Release Date":"11/1/2000",
    "Release Date":"11/1/2000",
    "Format":"CD",
    "UPC (Barcode)":4988010217822,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Charlene",
    "Title":"I've Never Been to Me",
    "Original Release Date":"1979",
    "Release Date":"1979",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Motown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gloria Cheng",
    "Title":"Piano Music of Salonen, Stucky and Lutoslawski",
    "Original Release Date":"7/22/2008",
    "Release Date":"7/22/2008",
    "Format":"MP3",
    "UPC (Barcode)":089408071225,
    "Label":"Telarc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cody ChesnuTT",
    "Title":"Landing On A Hundred",
    "Original Release Date":"10/30/2012",
    "Release Date":"10/30/2012",
    "Format":"CD",
    "UPC (Barcode)":634457573321,
    "Label":"Vibration Vineyard",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cody ChesnuTT",
    "Title":"Landing On A Hundred",
    "Original Release Date":"10/30/2012",
    "Release Date":"10/30/2012",
    "Format":"MP3",
    "UPC (Barcode)":5016958154929,
    "Label":"Vibration Vineyard",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Chiara String Quartet; Jefferson Friedman",
    "Title":"Jefferson Friedman: String Quartets",
    "Original Release Date":"4/26/2011",
    "Release Date":"4/26/2011",
    "Format":"CD",
    "UPC (Barcode)":884501469388,
    "Label":"New Amsterdam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Chiara String Quartet; Jefferson Friedman",
    "Title":"Jefferson Friedman: String Quartets",
    "Original Release Date":"4/26/2011",
    "Release Date":"4/26/2011",
    "Format":"MP3",
    "UPC (Barcode)":884501469388,
    "Label":"New Amsterdam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Chic",
    "Title":"C'est Chic",
    "Original Release Date":"1978",
    "Release Date":"9/15/1992",
    "Format":"Stream",
    "UPC (Barcode)":075678155260,
    "Label":"Atlantic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Chicanery",
    "Title":"Chicanery",
    "Original Release Date":"5/11/2010",
    "Release Date":"5/11/2010",
    "Format":"MP3",
    "UPC (Barcode)":885007104858,
    "Label":"dPulse",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Toni Childs",
    "Title":"House of Hope",
    "Original Release Date":"6/25/1991",
    "Release Date":"6/25/1991",
    "Format":"MP3",
    "UPC (Barcode)":075021535848,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Toni Childs",
    "Title":"House of Hope",
    "Original Release Date":"6/25/1991",
    "Release Date":"6/25/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075021535848,
    "Label":"A&M",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Toni Childs",
    "Title":"Union",
    "Original Release Date":"June 1988",
    "Release Date":"June 1988",
    "Format":"CD",
    "UPC (Barcode)":075021517516,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Toni Childs",
    "Title":"Union",
    "Original Release Date":"June 1988",
    "Release Date":"June 1988",
    "Format":"LP",
    "UPC (Barcode)":075021517516,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Toni Childs",
    "Title":"Don't Walk Away",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"7 Inch",
    "UPC (Barcode)":075021123779,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"China Digs",
    "Title":"Looking for George ...",
    "Original Release Date":"1997",
    "Release Date":"1997",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Cold House",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Choir of Trinity Church",
    "Title":"Candlelight Carols",
    "Original Release Date":"8/3/1991",
    "Release Date":"8/3/1991",
    "Format":"Cassette",
    "UPC (Barcode)":028943045643,
    "Label":"Polygram",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Christ Church Cathedral",
    "Original Release Date":"2/26/2013",
    "Release Date":"2/26/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"ARC",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Live in Concert",
    "Original Release Date":"3/8/2005",
    "Release Date":"3/8/2005",
    "Format":"MP3",
    "UPC (Barcode)":099923967422,
    "Label":"Koch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Rogha: The Best Of Clannad",
    "Original Release Date":"1/28/1997",
    "Release Date":"1/28/1997",
    "Format":"CD",
    "UPC (Barcode)":078636697826,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Banba",
    "Original Release Date":"6/15/1993",
    "Release Date":"6/15/1993",
    "Format":"CD",
    "UPC (Barcode)":075678250323,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Fuaim",
    "Original Release Date":"1982",
    "Release Date":"3/2/1993",
    "Format":"CD",
    "UPC (Barcode)":075678248122,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Magical Ring",
    "Original Release Date":"1983",
    "Release Date":"10/26/1993",
    "Format":"CD",
    "UPC (Barcode)":035627147326,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Macalla",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"LP",
    "UPC (Barcode)":078635806311,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Closer to Your Heart",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Macalla",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"CD",
    "UPC (Barcode)":078635806328,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Crann Ull",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Tara",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Crann Ull",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"CD",
    "UPC (Barcode)":5099207300728,
    "Label":"Shanachie",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Clannad in Concert",
    "Original Release Date":"1978",
    "Release Date":"1978",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Ogham",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Clannad in Concert",
    "Original Release Date":"1978",
    "Release Date":"1978",
    "Format":"CD",
    "UPC (Barcode)":016351793027,
    "Label":"Shanachie",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Dúlamán",
    "Original Release Date":"1976",
    "Release Date":"1976",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Shanachie",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Dúlamán",
    "Original Release Date":"1976",
    "Release Date":"1976",
    "Format":"CD",
    "UPC (Barcode)":016351790828,
    "Label":"Shanachie",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Dúlamán",
    "Original Release Date":"1976",
    "Release Date":"1976",
    "Format":"Cassette",
    "UPC (Barcode)":016351790842,
    "Label":"Shanachie",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Clannad 2",
    "Original Release Date":"1974",
    "Release Date":"1974",
    "Format":"CD",
    "UPC (Barcode)":016351790729,
    "Label":"Shanachie",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Clannad",
    "Title":"Clannad",
    "Original Release Date":"1973",
    "Release Date":"1973",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Philips",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Clovers",
    "Title":"Blue Velvet",
    "Original Release Date":"1955",
    "Release Date":"1955",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Club Nouveau",
    "Title":"Life Love & Pain",
    "Original Release Date":"1986",
    "Release Date":"10/25/1990",
    "Format":"CD",
    "UPC (Barcode)":07599255312,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Club Nouveau",
    "Title":"Life, Love & Pain",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"LP",
    "UPC (Barcode)":075992553117,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"The Best Ban",
    "Original Release Date":"8/15/2011",
    "Release Date":"8/15/2011",
    "Format":"CD",
    "UPC (Barcode)":4988002610631,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Inspired movies",
    "Original Release Date":"6/8/2011",
    "Release Date":"6/8/2011",
    "Format":"CD",
    "UPC (Barcode)":4935228111749,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Emerald",
    "Original Release Date":"8/11/2010",
    "Release Date":"8/11/2010",
    "Format":"CD",
    "UPC (Barcode)":4988002599158,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Nirai Kanai",
    "Original Release Date":"6/9/2010",
    "Release Date":"6/9/2010",
    "Format":"CD",
    "UPC (Barcode)":4988002597055,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Cocco-san no Daidokoro",
    "Original Release Date":"9/16/2009",
    "Release Date":"9/16/2009",
    "Format":"CD",
    "UPC (Barcode)":4988002577552,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Kira Kira",
    "Original Release Date":"7/25/2007",
    "Release Date":"7/25/2007",
    "Format":"CD",
    "UPC (Barcode)":4988002529872,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Dugong no Mieru Oka",
    "Original Release Date":"11/21/2007",
    "Release Date":"11/21/2007",
    "Format":"CD",
    "UPC (Barcode)":4988002537563,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Onsoku Punch",
    "Original Release Date":"2/22/2006",
    "Release Date":"2/22/2006",
    "Format":"CD",
    "UPC (Barcode)":4988002498031,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Hi no Terinagara Ame no Furu",
    "Original Release Date":"5/24/2006",
    "Release Date":"5/24/2006",
    "Format":"CD",
    "UPC (Barcode)":4988002503001,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Zansaian",
    "Original Release Date":"6/21/2006",
    "Release Date":"6/21/2006",
    "Format":"CD",
    "UPC (Barcode)":4988002505425,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Sing A Song ~No Music, No Love Life~",
    "Original Release Date":"11/23/2004",
    "Release Date":"11/23/2004",
    "Format":"CD",
    "UPC (Barcode)":4571191280022,
    "Label":"NMNL",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Heaven's hell",
    "Original Release Date":"12/24/2003",
    "Release Date":"12/24/2003",
    "Format":"CD",
    "UPC (Barcode)":4988002455560,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Hane ~lay down my arms~",
    "Original Release Date":"2/21/2001",
    "Release Date":"2/21/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002412389,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Sangrose",
    "Original Release Date":"4/18/2001",
    "Release Date":"4/18/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002415359,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Yakenogahara",
    "Original Release Date":"4/18/2001",
    "Release Date":"4/18/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002415342,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Best + Ura Best + Mihappyoo-kyokushuu",
    "Original Release Date":"9/5/2001",
    "Release Date":"9/5/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002418824,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Hoshi Ni Negai Wo",
    "Original Release Date":"7/26/2000",
    "Release Date":"7/26/2000",
    "Format":"CD",
    "UPC (Barcode)":4988002404506,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Kemono Michi",
    "Original Release Date":"6/28/2000",
    "Release Date":"6/28/2000",
    "Format":"CD",
    "UPC (Barcode)":4988002402830,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Rapunzel",
    "Original Release Date":"6/14/2000",
    "Release Date":"6/14/2000",
    "Format":"CD",
    "UPC (Barcode)":4988002400171,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Mizukagami",
    "Original Release Date":"4/26/2000",
    "Release Date":"4/26/2000",
    "Format":"CD",
    "UPC (Barcode)":4988002398928,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Polomerria",
    "Original Release Date":"10/14/1999",
    "Release Date":"10/14/1999",
    "Format":"CD",
    "UPC (Barcode)":4988002391974,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Jukai no ito",
    "Original Release Date":"4/14/1999",
    "Release Date":"4/14/1999",
    "Format":"CD",
    "UPC (Barcode)":4988002383054,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Kumoji no Hate",
    "Original Release Date":"10/7/1998",
    "Release Date":"10/7/1998",
    "Format":"CD",
    "UPC (Barcode)":4988002375752,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Raining",
    "Original Release Date":"3/21/1998",
    "Release Date":"3/21/1998",
    "Format":"CD",
    "UPC (Barcode)":4988002365876,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Kumuiuta",
    "Original Release Date":"5/13/1998",
    "Release Date":"5/13/1998",
    "Format":"CD",
    "UPC (Barcode)":4988002367160,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"SXSW '98: Cocco in 'Japan Nite'",
    "Original Release Date":"1998",
    "Release Date":"1998",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Tsuyoku Hakanai Monotachi",
    "Original Release Date":"11/21/1997",
    "Release Date":"11/21/1997",
    "Format":"CD",
    "UPC (Barcode)":4988002360130,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Count Down",
    "Original Release Date":"3/21/1997",
    "Release Date":"3/21/1997",
    "Format":"CD",
    "UPC (Barcode)":4988002347933,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Bougainvillia",
    "Original Release Date":"5/21/1997",
    "Release Date":"5/21/1997",
    "Format":"CD",
    "UPC (Barcode)":4988002351572,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocco",
    "Title":"Cocko",
    "Original Release Date":"11/21/1996",
    "Release Date":"11/21/1996",
    "Format":"CD",
    "UPC (Barcode)":4997184000277,
    "Label":"Bounce",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocteau Twins",
    "Title":"Heaven or Las Vegas",
    "Original Release Date":"9/17/1990",
    "Release Date":"6/3/2003",
    "Format":"MP3",
    "UPC (Barcode)":652637001228,
    "Label":"4AD",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocteau Twins",
    "Title":"Blue Bell Knoll",
    "Original Release Date":"9/19/1988",
    "Release Date":"6/3/2003",
    "Format":"CD",
    "UPC (Barcode)":652637080728,
    "Label":"4AD",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocteau Twins",
    "Title":"Blue Bell Knoll",
    "Original Release Date":"9/19/1988",
    "Release Date":"6/3/2003",
    "Format":"MP3",
    "UPC (Barcode)":652637080728,
    "Label":"4AD",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cocteau Twins",
    "Title":"Treasure",
    "Original Release Date":"October 1984",
    "Release Date":"6/3/2003",
    "Format":"CD",
    "UPC (Barcode)":652637041224,
    "Label":"4AD",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ornette Coleman",
    "Title":"The Shape Of Jazz To Come",
    "Original Release Date":"5/22/1959",
    "Release Date":"5/22/1959",
    "Format":"CD",
    "UPC (Barcode)":075678133923,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ornette Coleman",
    "Title":"The Shape Of Jazz To Come",
    "Original Release Date":"5/22/1959",
    "Release Date":"5/22/1959",
    "Format":"MP3",
    "UPC (Barcode)":075678133923,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Coltrane",
    "Title":"A Love Supreme",
    "Original Release Date":"12/9/1964",
    "Release Date":"6/20/1995",
    "Format":"CD",
    "UPC (Barcode)":011105015523,
    "Label":"Impulse",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Concord String Quartet",
    "Title":"American String Quartets 1950 - 1970",
    "Original Release Date":"9/26/1995",
    "Release Date":"9/26/1995",
    "Format":"CD",
    "UPC (Barcode)":047163514326,
    "Label":"Vox (Classical)",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Concord String Quartet",
    "Title":"Rochberg: Quartet No. 7/Barber: String Quartet",
    "Original Release Date":"",
    "Release Date":"1984",
    "Format":"Cassette",
    "UPC (Barcode)":075597801743,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Condor44",
    "Title":"db",
    "Original Release Date":"6/21/2002",
    "Release Date":"6/21/2002",
    "Format":"CD",
    "UPC (Barcode)":4514306005349,
    "Label":"Daizawa",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Harry Connick, Jr.",
    "Title":"She",
    "Original Release Date":"7/12/1994",
    "Release Date":"7/12/1994",
    "Format":"CD",
    "UPC (Barcode)":074646437629,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Matthew Robert Cooper",
    "Title":"Miniatures",
    "Original Release Date":"8/11/2008",
    "Release Date":"8/11/2008",
    "Format":"MP3",
    "UPC (Barcode)":723721393755,
    "Label":"Gaarden",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Core of Soul",
    "Title":"'Over the Time' Time Is Over",
    "Original Release Date":"2/19/2003",
    "Release Date":"2/19/2003",
    "Format":"CD",
    "UPC (Barcode)":4988006182233,
    "Label":"Capitol Japan",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Core of Soul",
    "Title":"Natural Beauty",
    "Original Release Date":"2/27/2002",
    "Release Date":"2/27/2002",
    "Format":"CD",
    "UPC (Barcode)":4988006177772,
    "Label":"Capitol Japan",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"John Corigliano",
    "Title":"Tournaments Overture/Elegy/Piano Concerto/Gazebo Dances",
    "Original Release Date":"3/11/2003",
    "Release Date":"3/11/2003",
    "Format":"MP3",
    "UPC (Barcode)":809157000020,
    "Label":"First Edition",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Corigliano",
    "Title":"Symphony No 1",
    "Original Release Date":"8/10/1991",
    "Release Date":"8/10/1991",
    "Format":"CD",
    "UPC (Barcode)":022924560125,
    "Label":"Erato",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jayne Cortez and the Firespitters",
    "Title":"Borders of Disorderly Time",
    "Original Release Date":"2003",
    "Release Date":"2003",
    "Format":"CD",
    "UPC (Barcode)":659057728322,
    "Label":"Bola Press",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jayne Cortez and the Firespitters",
    "Title":"Taking The Blues Back Home",
    "Original Release Date":"8/6/1996",
    "Release Date":"8/6/1996",
    "Format":"CD",
    "UPC (Barcode)":731453191822,
    "Label":"Harmolodic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jayne Cortez and the Firespitters",
    "Title":"Cheerful & Optimistic",
    "Original Release Date":"1994",
    "Release Date":"1994",
    "Format":"CD",
    "UPC (Barcode)":019940940123,
    "Label":"Bola Press",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cowboy Junkies",
    "Title":"The Caution Horses",
    "Original Release Date":"2/20/1990",
    "Release Date":"2/20/1990",
    "Format":"Cassette",
    "UPC (Barcode)":078635205848,
    "Label":"RCA",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Cramps",
    "Title":"Stay Sick!",
    "Original Release Date":"1/10/1990",
    "Release Date":"1/10/1990",
    "Format":"Cassette",
    "UPC (Barcode)":018777354349,
    "Label":"Enigma",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Julee Cruise",
    "Title":"Floating Into the Night",
    "Original Release Date":"6/1/1990",
    "Release Date":"6/1/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075992585941,
    "Label":"Warner Bros.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Julee Cruise",
    "Title":"Floating Into The Night",
    "Original Release Date":"9/12/1989",
    "Release Date":"9/12/1989",
    "Format":"CD",
    "UPC (Barcode)":075992585927,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"George Crumb",
    "Title":"Varizioni/Echoes of Time and the River",
    "Original Release Date":"3/11/2003",
    "Release Date":"3/11/2003",
    "Format":"MP3",
    "UPC (Barcode)":809157000082,
    "Label":"First Edition",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"George Crumb",
    "Title":"Ancient Voices of Children / Music for a Summer Evening (Makrokosmos III)",
    "Original Release Date":"1975",
    "Release Date":"1975",
    "Format":"CD",
    "UPC (Barcode)":075597914924,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Billy Crystal",
    "Title":"You Look Marvelous",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Culture Club",
    "Title":"Waking Up With the House on Fire",
    "Original Release Date":"July 1984",
    "Release Date":"July 1984",
    "Format":"LP",
    "UPC (Barcode)":074643988117,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Culture Club",
    "Title":"The War Song",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"7 Inch",
    "UPC (Barcode)":074640463877,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dagashi-Kashi",
    "Title":"Jingai Kagami no Heya",
    "Original Release Date":"4/16/2002",
    "Release Date":"4/16/2002",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Romandokoro",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Damnations TX",
    "Title":"Half Mad Moon",
    "Original Release Date":"2/16/1999",
    "Release Date":"2/16/1999",
    "Format":"CD",
    "UPC (Barcode)":643443103121,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Michael Daugherty",
    "Title":"Metropolis Symphony",
    "Original Release Date":"9/29/2009",
    "Release Date":"9/29/2009",
    "Format":"MP3",
    "UPC (Barcode)":636943963524,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Dead Betties",
    "Title":"This Is My Brain on Drugs",
    "Original Release Date":"May 2008",
    "Release Date":"May 2008",
    "Format":"CD",
    "UPC (Barcode)":842841016822,
    "Label":"",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Dead Betties",
    "Title":"Nightmare Sequence",
    "Original Release Date":"7/3/2007",
    "Release Date":"7/3/2007",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Cordless",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dead Can Dance",
    "Title":"Anastasis",
    "Original Release Date":"8/14/2012",
    "Release Date":"8/14/2012",
    "Format":"CD",
    "UPC (Barcode)":843798000933,
    "Label":"PAIS America",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dead Can Dance",
    "Title":"A Passage In Time",
    "Original Release Date":"10/20/1991",
    "Release Date":"1998",
    "Format":"CD",
    "UPC (Barcode)":652637101027,
    "Label":"4AD",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dead Can Dance",
    "Title":"Spiritchaser",
    "Original Release Date":"6/25/1996",
    "Release Date":"6/25/1996",
    "Format":"CD",
    "UPC (Barcode)":093624623021,
    "Label":"4AD",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dead Can Dance",
    "Title":"Aion",
    "Original Release Date":"1990",
    "Release Date":"1990",
    "Format":"CD",
    "UPC (Barcode)":093624557524,
    "Label":"4AD",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Dead Milkmen",
    "Title":"Beelzebubba",
    "Original Release Date":"1988",
    "Release Date":"7/1/1993",
    "Format":"CD",
    "UPC (Barcode)":018777254526,
    "Label":"Restless",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Dead Milkmen",
    "Title":"Soul Rotation",
    "Original Release Date":"4/14/1992",
    "Release Date":"4/14/1992",
    "Format":"Cassette",
    "UPC (Barcode)":720616129444,
    "Label":"Hollywood",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Dead Milkmen",
    "Title":"Metaphysical Graffiti",
    "Original Release Date":"4/10/1990",
    "Release Date":"4/10/1990",
    "Format":"Cassette",
    "UPC (Barcode)":018777356442,
    "Label":"Enigma",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Dead Milkmen",
    "Title":"Beelzebubba",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"Cassette",
    "UPC (Barcode)":018777335140,
    "Label":"Enigma",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Dead or Alive",
    "Title":"You Spin Me Around (Like a Record)",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"7 Inch",
    "UPC (Barcode)":074640489471,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Death Cab For Cutie",
    "Title":"Codes And Keys",
    "Original Release Date":"5/31/2011",
    "Release Date":"5/31/2011",
    "Format":"CD",
    "UPC (Barcode)":075678827044,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Death Cab For Cutie",
    "Title":"Codes And Keys",
    "Original Release Date":"5/31/2011",
    "Release Date":"5/31/2011",
    "Format":"MP3",
    "UPC (Barcode)":075678827044,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Death Cab For Cutie",
    "Title":"Narrow Stairs",
    "Original Release Date":"5/13/2008",
    "Release Date":"5/13/2008",
    "Format":"CD",
    "UPC (Barcode)":075678994654,
    "Label":"Atlantic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Death Cab For Cutie",
    "Title":"Plans",
    "Original Release Date":"8/30/2005",
    "Release Date":"8/30/2005",
    "Format":"CD",
    "UPC (Barcode)":075678383427,
    "Label":"Atlantic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Death Cab For Cutie",
    "Title":"Transatlanticism",
    "Original Release Date":"11/4/2003",
    "Release Date":"11/4/2003",
    "Format":"CD",
    "UPC (Barcode)":655173103265,
    "Label":"Barsuk",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Death Cab For Cutie",
    "Title":"The Photo Album",
    "Original Release Date":"10/9/2001",
    "Release Date":"10/9/2001",
    "Format":"CD",
    "UPC (Barcode)":655173102121,
    "Label":"Barsuk",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Decemberists",
    "Title":"The King Is Dead",
    "Original Release Date":"1/25/2011",
    "Release Date":"1/25/2011",
    "Format":"CD",
    "UPC (Barcode)":5099994754728,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Decemberists",
    "Title":"Castaways And Cutouts",
    "Original Release Date":"5/21/2002",
    "Release Date":"5/21/2002",
    "Format":"CD",
    "UPC (Barcode)":759656039727,
    "Label":"Kill Rock Stars",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Decemberists",
    "Title":"Castaways And Cutouts",
    "Original Release Date":"5/21/2002",
    "Release Date":"5/21/2002",
    "Format":"MP3",
    "UPC (Barcode)":759656039727,
    "Label":"Kill Rock Stars",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Deee-Lite",
    "Title":"World Clique",
    "Original Release Date":"8/7/1990",
    "Release Date":"8/7/1990",
    "Format":"CD",
    "UPC (Barcode)":075596095723,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Deee-Lite",
    "Title":"World Clique",
    "Original Release Date":"8/2/1990",
    "Release Date":"8/2/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075596095747,
    "Label":"Elektra",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Lana Del Rey",
    "Title":"Born To Die",
    "Original Release Date":"1/31/2012",
    "Release Date":"1/31/2012",
    "Format":"MP3",
    "UPC (Barcode)":602527931081,
    "Label":"Interscope",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jeremy Denk",
    "Title":"Ligeti / Beethoven",
    "Original Release Date":"5/14/2012",
    "Release Date":"5/14/2012",
    "Format":"CD",
    "UPC (Barcode)":075597962192,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Department of Eagles",
    "Title":"In Ear Park",
    "Original Release Date":"10/7/2008",
    "Release Date":"10/7/2008",
    "Format":"CD",
    "UPC (Barcode)":652637281828,
    "Label":"4AD",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Department of Eagles",
    "Title":"In Ear Park",
    "Original Release Date":"10/7/2008",
    "Release Date":"10/7/2008",
    "Format":"MP3",
    "UPC (Barcode)":652637281828,
    "Label":"4AD",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Department of Eagles",
    "Title":"Cold Nose",
    "Original Release Date":"2005",
    "Release Date":"7/31/2007",
    "Format":"MP3",
    "UPC (Barcode)":646315920222,
    "Label":"American Dust",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Depeche Mode",
    "Title":"The Singles 86>98",
    "Original Release Date":"10/6/1998",
    "Release Date":"10/6/1998",
    "Format":"CD",
    "UPC (Barcode)":093624711025,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Depeche Mode",
    "Title":"Violator",
    "Original Release Date":"2/22/1990",
    "Release Date":"2/22/1990",
    "Format":"CD",
    "UPC (Barcode)":075992608121,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Depeche Mode",
    "Title":"Music for the Masses",
    "Original Release Date":"9/29/1987",
    "Release Date":"9/29/1987",
    "Format":"CD",
    "UPC (Barcode)":075992561426,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Depeche Mode",
    "Title":"Music For The Masses",
    "Original Release Date":"9/29/1987",
    "Release Date":"9/29/1987",
    "Format":"LP",
    "UPC (Barcode)":075992561419,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"detroit7",
    "Title":"detroit7",
    "Original Release Date":"3/10/2009",
    "Release Date":"3/10/2009",
    "Format":"CD",
    "UPC (Barcode)":794017308920,
    "Label":"Daruma",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"detroit7",
    "Title":"Third Star From The Earth",
    "Original Release Date":"1/17/2008",
    "Release Date":"1/17/2008",
    "Format":"CD",
    "UPC (Barcode)":4519552102965,
    "Label":"rudie&records",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"detroit7",
    "Title":"GREAT Romantic",
    "Original Release Date":"11/29/2006",
    "Release Date":"11/29/2006",
    "Format":"CD",
    "UPC (Barcode)":4988006209091,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Devils",
    "Title":"Dark Circles",
    "Original Release Date":"10/8/2002",
    "Release Date":"5/6/2003",
    "Format":"CD",
    "UPC (Barcode)":723724559424,
    "Label":"TapeModern",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Dirty Projectors",
    "Title":"Bitte Orca",
    "Original Release Date":"6/8/2009",
    "Release Date":"6/8/2009",
    "Format":"MP3",
    "UPC (Barcode)":5034202022923,
    "Label":"Domino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Beth Ditto",
    "Title":"Beth Ditto Ep",
    "Original Release Date":"3/8/2011",
    "Release Date":"3/8/2011",
    "Format":"MP3",
    "UPC (Barcode)":886978529725,
    "Label":"Deconstruction",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"DJ Krush",
    "Title":"Shinsou ~The Message At The Depth~",
    "Original Release Date":"2/11/2003",
    "Release Date":"2/11/2003",
    "Format":"CD",
    "UPC (Barcode)":766927105227,
    "Label":"Red Ink",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"DJ Krush",
    "Title":"Zen",
    "Original Release Date":"9/11/2001",
    "Release Date":"9/11/2001",
    "Format":"CD",
    "UPC (Barcode)":766925570720,
    "Label":"Red Ink",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Do As Infinity",
    "Title":"Need Your Love",
    "Original Release Date":"2/16/2005",
    "Release Date":"2/16/2005",
    "Format":"CD",
    "UPC (Barcode)":4988064176212,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Do As Infinity",
    "Title":"True Song",
    "Original Release Date":"12/26/2002",
    "Release Date":"12/26/2002",
    "Format":"CD",
    "UPC (Barcode)":4988064172054,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Do As Infinity",
    "Title":"New World",
    "Original Release Date":"2/21/2001",
    "Release Date":"2/21/2001",
    "Format":"CD",
    "UPC (Barcode)":4988064118809,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Do As Infinity",
    "Title":"Deep Forest",
    "Original Release Date":"9/19/2001",
    "Release Date":"9/19/2001",
    "Format":"CD",
    "UPC (Barcode)":4988064119813,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Do As Infinity",
    "Title":"Break of Dawn",
    "Original Release Date":"3/23/2000",
    "Release Date":"3/23/2000",
    "Format":"CD",
    "UPC (Barcode)":4988064118045,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Billy Dortch",
    "Title":"Heart Clean",
    "Original Release Date":"12/14/2009",
    "Release Date":"12/14/2009",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Red Queen Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Billy Dortch",
    "Title":"Goodbye, Goodbye",
    "Original Release Date":"1/29/2009",
    "Release Date":"1/29/2009",
    "Format":"MP3",
    "UPC (Barcode)":859701211723,
    "Label":"Red Queen Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Billy Dortch",
    "Title":"Breathe",
    "Original Release Date":"3/2/2009",
    "Release Date":"3/2/2009",
    "Format":"MP3",
    "UPC (Barcode)":859701467052,
    "Label":"Red Queen Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Double Edge",
    "Title":"U.S. Choice",
    "Original Release Date":"4/20/1994",
    "Release Date":"4/20/1994",
    "Format":"CD",
    "UPC (Barcode)":090438063722,
    "Label":"CRI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Leslie Dowdall",
    "Title":"Out There",
    "Original Release Date":"1998",
    "Release Date":"1998",
    "Format":"CD",
    "UPC (Barcode)":5390172391170,
    "Label":"LD Productions",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Leslie Dowdall",
    "Title":"No Guilt No Guile",
    "Original Release Date":"1/27/1997",
    "Release Date":"1/27/1997",
    "Format":"CD",
    "UPC (Barcode)":5019148922209,
    "Label":"Grapevine",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"downy",
    "Title":"untitled fourth album",
    "Original Release Date":"7/22/2004",
    "Release Date":"7/22/2004",
    "Format":"CD",
    "UPC (Barcode)":4540957004837,
    "Label":"guiderat",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"downy",
    "Title":"untitled third album",
    "Original Release Date":"5/8/2003",
    "Release Date":"5/8/2003",
    "Format":"CD",
    "UPC (Barcode)":4540957002550,
    "Label":"guiderat",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dr.StrangeLove",
    "Title":"The River Of Blue Blood",
    "Original Release Date":"3/3/2004",
    "Release Date":"3/3/2004",
    "Format":"CD",
    "UPC (Barcode)":4582152550054,
    "Label":"Atsugua",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dr.StrangeLove",
    "Title":"Twin Suns",
    "Original Release Date":"8/18/1999",
    "Release Date":"8/18/1999",
    "Format":"CD",
    "UPC (Barcode)":4988013014404,
    "Label":"Pony Canyon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dr.StrangeLove",
    "Title":"Dr.Strange Love",
    "Original Release Date":"6/4/1997",
    "Release Date":"6/4/1997",
    "Format":"CD",
    "UPC (Barcode)":4988013785236,
    "Label":"Pony Canyon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dr.StrangeLove",
    "Title":"Escape",
    "Original Release Date":"11/19/1997",
    "Release Date":"11/19/1997",
    "Format":"CD",
    "UPC (Barcode)":4988013804432,
    "Label":"Pony Canyon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dr.StrangeLove",
    "Title":"Eyes in the Sky",
    "Original Release Date":"1996",
    "Release Date":"1996",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Superfood",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dr.StrangeLove",
    "Title":"Way Out",
    "Original Release Date":"1995",
    "Release Date":"1995",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Superfood",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dragon Ash",
    "Title":"Lily of Da Valley",
    "Original Release Date":"3/14/2001",
    "Release Date":"3/14/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002413218,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dragon Ash",
    "Title":"Viva La Revolution",
    "Original Release Date":"7/23/1999",
    "Release Date":"7/23/1999",
    "Format":"CD",
    "UPC (Barcode)":4715202200719,
    "Label":"Forward",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dream Syndicate",
    "Title":"The Days of Wine and Roses",
    "Original Release Date":"1982",
    "Release Date":"7/17/2001",
    "Format":"MP3",
    "UPC (Barcode)":081227993726,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Minnie Driver",
    "Title":"Everything I've Got In My Pocket",
    "Original Release Date":"10/5/2004",
    "Release Date":"10/5/2004",
    "Format":"MP3",
    "UPC (Barcode)":601143107221,
    "Label":"Rounder",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Drums",
    "Title":"Portamento",
    "Original Release Date":"9/6/2011",
    "Release Date":"9/6/2011",
    "Format":"MP3",
    "UPC (Barcode)":602527786346,
    "Label":"Frenchkiss",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Drums",
    "Title":"The Drums",
    "Original Release Date":"9/14/2010",
    "Release Date":"9/14/2010",
    "Format":"CD",
    "UPC (Barcode)":602527369099,
    "Label":"Downtown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Drums",
    "Title":"The Drums",
    "Original Release Date":"9/14/2010",
    "Release Date":"9/14/2010",
    "Format":"MP3",
    "UPC (Barcode)":602527369099,
    "Label":"Downtown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dry & Heavy",
    "Title":"Full Contact",
    "Original Release Date":"11/7/2000",
    "Release Date":"11/7/2000",
    "Format":"CD",
    "UPC (Barcode)":751937156123,
    "Label":"BSI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"DSCO",
    "Title":"Indian Winter",
    "Original Release Date":"2001",
    "Release Date":"2001",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Forty-Four",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Dukes of the Stratosphear",
    "Title":"Chips from the Chocolate Fireball",
    "Original Release Date":"6/18/2001",
    "Release Date":"6/18/2001",
    "Format":"CD",
    "UPC (Barcode)":724385073229,
    "Label":"Caroline",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Dukes of the Stratosphear",
    "Title":"Chips from the Chocolate Fireball",
    "Original Release Date":"6/18/2001",
    "Release Date":"6/18/2001",
    "Format":"Stream",
    "UPC (Barcode)":0724385073250,
    "Label":"Caroline",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Judy Dunaway",
    "Title":"Mother of Balloon Music",
    "Original Release Date":"7/25/2006",
    "Release Date":"7/25/2006",
    "Format":"MP3",
    "UPC (Barcode)":726708664824,
    "Label":"Innova",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Judy Dunaway",
    "Title":"Judy Dunaway and the Evan Gallagher Little Band",
    "Original Release Date":"1993",
    "Release Date":"1993",
    "Format":"CD",
    "UPC (Barcode)":786497059423,
    "Label":"AMF Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Judy Dunaway",
    "Title":"Judy Dunaway",
    "Original Release Date":"1990",
    "Release Date":"1990",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Lost",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robbie Dupree",
    "Title":"Hot Rod Hearts",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"A Diamond In The Mind",
    "Original Release Date":"7/10/2012",
    "Release Date":"7/10/2012",
    "Format":"CD",
    "UPC (Barcode)":826992026725,
    "Label":"Eagle Rock",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"All You Need Is Now",
    "Original Release Date":"3/22/2011",
    "Release Date":"3/22/2011",
    "Format":"CD",
    "UPC (Barcode)":807315170226,
    "Label":"S-Curve",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Boys Keep Swinging",
    "Original Release Date":"12/20/2010",
    "Release Date":"12/20/2010",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Manimal",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Notorious (Special Edition)",
    "Original Release Date":"11/18/1986",
    "Release Date":"10/12/2010",
    "Format":"CD",
    "UPC (Barcode)":5099963360424,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Big Thing (Special Edition)",
    "Original Release Date":"10/18/1988",
    "Release Date":"10/12/2010",
    "Format":"CD",
    "UPC (Barcode)":5099963377729,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Seven And The Ragged Tiger (Deluxe Edition)",
    "Original Release Date":"11/21/1983",
    "Release Date":"5/18/2010",
    "Format":"CD",
    "UPC (Barcode)":5099962609722,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Duran Duran (Deluxe Edition)",
    "Original Release Date":"6/15/1981",
    "Release Date":"5/18/2010",
    "Format":"CD",
    "UPC (Barcode)":5099960960320,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Rio (2009 Collector's Edition)",
    "Original Release Date":"5/10/1982",
    "Release Date":"10/6/2009",
    "Format":"CD",
    "UPC (Barcode)":5099996563328,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Hammersmith '82!",
    "Original Release Date":"10/6/2009",
    "Release Date":"10/6/2009",
    "Format":"CD",
    "UPC (Barcode)":5099996563892,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Red Carpet Massacre",
    "Original Release Date":"11/13/2007",
    "Release Date":"11/13/2007",
    "Format":"CD",
    "UPC (Barcode)":886970736220,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Beautiful Colors",
    "Original Release Date":"2005",
    "Release Date":"2005",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"What Happens Tomorrow CD2",
    "Original Release Date":"1/31/2005",
    "Release Date":"1/31/2005",
    "Format":"CD",
    "UPC (Barcode)":5099767565025,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Arena (Remastered)",
    "Original Release Date":"November 1984",
    "Release Date":"6/1/2004",
    "Format":"CD",
    "UPC (Barcode)":724357808521,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Astronaut",
    "Original Release Date":"10/12/2004",
    "Release Date":"10/12/2004",
    "Format":"CD",
    "UPC (Barcode)":827969290026,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Singles, 1986-1995",
    "Original Release Date":"11/2/2004",
    "Release Date":"11/2/2004",
    "Format":"CD",
    "UPC (Barcode)":724354989223,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"(Reach Up for the) Sunrise",
    "Original Release Date":"9/28/2004",
    "Release Date":"9/28/2004",
    "Format":"CD",
    "UPC (Barcode)":098707197628,
    "Label":"Epic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Singles, 1981-1985",
    "Original Release Date":"6/10/2003",
    "Release Date":"6/10/2003",
    "Format":"CD",
    "UPC (Barcode)":724355172822,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Duran Duran (Remastered)",
    "Original Release Date":"6/15/1981",
    "Release Date":"8/5/2003",
    "Format":"CD",
    "UPC (Barcode)":724358480924,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Seven And The Ragged Tiger (Remastered)",
    "Original Release Date":"11/21/1983",
    "Release Date":"8/5/2003",
    "Format":"CD",
    "UPC (Barcode)":724358481129,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Rio (Remastered)",
    "Original Release Date":"5/10/1982",
    "Release Date":"7/3/2001",
    "Format":"CD",
    "UPC (Barcode)":724352591909,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Pop Trash",
    "Original Release Date":"6/13/2000",
    "Release Date":"6/13/2000",
    "Format":"CD",
    "UPC (Barcode)":720616226624,
    "Label":"Hollywood",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Strange Behaviour",
    "Original Release Date":"3/23/1999",
    "Release Date":"3/23/1999",
    "Format":"CD",
    "UPC (Barcode)":724349397224,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Night Versions: Essential Duran Duran",
    "Original Release Date":"4/7/1998",
    "Release Date":"4/7/1998",
    "Format":"CD",
    "UPC (Barcode)":724349392205,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Medazzaland",
    "Original Release Date":"10/14/1997",
    "Release Date":"10/14/1997",
    "Format":"CD",
    "UPC (Barcode)":724383387625,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Thank You",
    "Original Release Date":"4/4/1995",
    "Release Date":"4/4/1995",
    "Format":"CD",
    "UPC (Barcode)":724382941927,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Gemini",
    "Original Release Date":"1994",
    "Release Date":"1994",
    "Format":"CD",
    "UPC (Barcode)":80131330009000,
    "Label":"Red Phantom",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Wedding Album",
    "Original Release Date":"1994",
    "Release Date":"1/1/1994",
    "Format":"CD",
    "UPC (Barcode)":724382827825,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Medicine",
    "Original Release Date":"1994",
    "Release Date":"1994",
    "Format":"CD",
    "UPC (Barcode)":5401995000344,
    "Label":"Midnight Beat",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Come Undone 2",
    "Original Release Date":"6/1/1993",
    "Release Date":"6/1/1993",
    "Format":"CD",
    "UPC (Barcode)":077771598128,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Duran Duran (The Wedding Album)",
    "Original Release Date":"2/23/1993",
    "Release Date":"2/23/1993",
    "Format":"CD",
    "UPC (Barcode)":077779887620,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Notorious",
    "Original Release Date":"11/18/1986",
    "Release Date":"6/29/1993",
    "Format":"CD",
    "UPC (Barcode)":077774641524,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Ultra Rare Trax",
    "Original Release Date":"1993",
    "Release Date":"1993",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Liberty",
    "Original Release Date":"8/13/1990",
    "Release Date":"8/13/1990",
    "Format":"CD",
    "UPC (Barcode)":762185115923,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Liberty",
    "Original Release Date":"8/2/1990",
    "Release Date":"8/2/1990",
    "Format":"Cassette",
    "UPC (Barcode)":077779429240,
    "Label":"Capitol",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Serious",
    "Original Release Date":"1990",
    "Release Date":"1990",
    "Format":"7 Inch",
    "UPC (Barcode)":5099920406578,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Decade",
    "Original Release Date":"10/31/1989",
    "Release Date":"10/31/1989",
    "Format":"CD",
    "UPC (Barcode)":077779317820,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Decade",
    "Original Release Date":"11/8/1989",
    "Release Date":"11/8/1989",
    "Format":"Cassette",
    "UPC (Barcode)":077779317844,
    "Label":"Capitol",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Burning the Ground/Decadance",
    "Original Release Date":"December 1989",
    "Release Date":"December 1989",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Big Thing",
    "Original Release Date":"10/18/1988",
    "Release Date":"10/18/1988",
    "Format":"LP",
    "UPC (Barcode)":077779095810,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"I Don't Want Your Love",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Big Thing",
    "Original Release Date":"10/18/1988",
    "Release Date":"10/18/1988",
    "Format":"CD",
    "UPC (Barcode)":077779095827,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"All She Wants Is",
    "Original Release Date":"December 1988",
    "Release Date":"December 1988",
    "Format":"CD",
    "UPC (Barcode)":077774428729,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"All She Wants Is",
    "Original Release Date":"December 1988",
    "Release Date":"December 1988",
    "Format":"12 Inch",
    "UPC (Barcode)":077771543418,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"All She Wants Is",
    "Original Release Date":"December 1988",
    "Release Date":"December 1988",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Do You Believe in Shame?",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Notorious (Latin Rascals Mix)",
    "Original Release Date":"10/20/1987",
    "Release Date":"10/20/1987",
    "Format":"12 Inch",
    "UPC (Barcode)":077771526619,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Notorious",
    "Original Release Date":"11/18/1986",
    "Release Date":"11/18/1986",
    "Format":"LP",
    "UPC (Barcode)":077771254017,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Skin Trade",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Notorious",
    "Original Release Date":"10/20/1986",
    "Release Date":"10/20/1986",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Skin Trade",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"A View to a Kill",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"New Moon on Monday",
    "Original Release Date":"January 1984",
    "Release Date":"January 1984",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Reflex",
    "Original Release Date":"4/16/1984",
    "Release Date":"4/16/1984",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Reflex",
    "Original Release Date":"4/16/1984",
    "Release Date":"4/16/1984",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Reflex",
    "Original Release Date":"4/16/1984",
    "Release Date":"4/16/1984",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Wild Boys",
    "Original Release Date":"October 1984",
    "Release Date":"October 1984",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Wild Boys",
    "Original Release Date":"October 1984",
    "Release Date":"October 1984",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Arena",
    "Original Release Date":"November 1984",
    "Release Date":"November 1984",
    "Format":"LP",
    "UPC (Barcode)":077771237416,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Reflex (Poster Sleeve)",
    "Original Release Date":"4/16/1984",
    "Release Date":"4/16/1984",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"New Moon on Monday",
    "Original Release Date":"January 1984",
    "Release Date":"January 1984",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Reflex",
    "Original Release Date":"4/16/1984",
    "Release Date":"4/16/1984",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Reflex",
    "Original Release Date":"4/16/1984",
    "Release Date":"4/16/1984",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"The Wild Boys",
    "Original Release Date":"October 1984",
    "Release Date":"October 1984",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Save a Prayer",
    "Original Release Date":"8/9/1982",
    "Release Date":"1984",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Seven and the Ragged Tiger",
    "Original Release Date":"11/21/1983",
    "Release Date":"11/21/1983",
    "Format":"LP",
    "UPC (Barcode)":5099916545410,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Seven and the Ragged Tiger",
    "Original Release Date":"11/21/1983",
    "Release Date":"11/21/1983",
    "Format":"LP",
    "UPC (Barcode)":077771231018,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Duran Duran",
    "Original Release Date":"4/25/1983",
    "Release Date":"4/25/1983",
    "Format":"LP",
    "UPC (Barcode)":077771215810,
    "Label":"Harvest",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Seven and the Ragged Tiger",
    "Original Release Date":"1983",
    "Release Date":"1983",
    "Format":"Cassette",
    "UPC (Barcode)":077774601542,
    "Label":"Capitol",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Is There Something I Should Know?",
    "Original Release Date":"3/19/1983",
    "Release Date":"3/19/1983",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Is There Something I Should Know?",
    "Original Release Date":"3/19/1983",
    "Release Date":"3/19/1983",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Union of the Snake",
    "Original Release Date":"10/17/1983",
    "Release Date":"10/17/1983",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Tiger! Tiger!",
    "Original Release Date":"1983",
    "Release Date":"1983",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Is There Something I Should Know?",
    "Original Release Date":"3/19/1983",
    "Release Date":"3/19/1983",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Is There Something I Should Know?",
    "Original Release Date":"3/19/1983",
    "Release Date":"3/19/1983",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Union of the Snake",
    "Original Release Date":"10/17/1983",
    "Release Date":"10/17/1983",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Union of the Snake",
    "Original Release Date":"10/17/1983",
    "Release Date":"10/17/1983",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Rio",
    "Original Release Date":"5/10/1982",
    "Release Date":"5/10/1982",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Rio",
    "Original Release Date":"5/10/1982",
    "Release Date":"5/10/1982",
    "Format":"LP",
    "UPC (Barcode)":077771221118,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Rio",
    "Original Release Date":"1982",
    "Release Date":"1982",
    "Format":"Cassette",
    "UPC (Barcode)":077774600347,
    "Label":"Capitol",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Hungry Like the Wolf",
    "Original Release Date":"5/4/1982",
    "Release Date":"5/4/1982",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Rio, Part 2",
    "Original Release Date":"11/1/1982",
    "Release Date":"11/1/1982",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Carnival",
    "Original Release Date":"September 1982",
    "Release Date":"September 1982",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Carnival",
    "Original Release Date":"September 1982",
    "Release Date":"September 1982",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Carnival",
    "Original Release Date":"September 1982",
    "Release Date":"September 1982",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Save a Prayer",
    "Original Release Date":"8/9/1982",
    "Release Date":"8/9/1982",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Hungry Like the Wolf",
    "Original Release Date":"5/4/1982",
    "Release Date":"5/4/1982",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Rio",
    "Original Release Date":"11/1/1982",
    "Release Date":"11/1/1982",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Rio",
    "Original Release Date":"11/1/1982",
    "Release Date":"11/1/1982",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Hungry Like the Wolf",
    "Original Release Date":"5/4/1982",
    "Release Date":"5/4/1982",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Duran Duran",
    "Original Release Date":"6/15/1981",
    "Release Date":"6/15/1981",
    "Format":"LP",
    "UPC (Barcode)":077771215810,
    "Label":"Harvest",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Planet Earth",
    "Original Release Date":"2/2/1981",
    "Release Date":"2/2/1981",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Planet Earth",
    "Original Release Date":"2/2/1981",
    "Release Date":"2/2/1981",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Girls on Film",
    "Original Release Date":"7/13/1981",
    "Release Date":"7/13/1981",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Girls on Film",
    "Original Release Date":"7/13/1981",
    "Release Date":"7/13/1981",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Careless Memories",
    "Original Release Date":"4/20/1981",
    "Release Date":"4/20/1981",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Careless Memories",
    "Original Release Date":"4/20/1981",
    "Release Date":"4/20/1981",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Night Versions",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Night Romantics",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"My Own Way",
    "Original Release Date":"11/16/1981",
    "Release Date":"11/16/1981",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Planet Earth",
    "Original Release Date":"2/2/1981",
    "Release Date":"2/2/1981",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"Girls on Film",
    "Original Release Date":"7/13/1981",
    "Release Date":"7/13/1981",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Duran Duran",
    "Title":"My Own Way",
    "Original Release Date":"11/16/1981",
    "Release Date":"11/16/1981",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Antonin Dvorak",
    "Title":"Symphony No. 9 / Slavonic Dances",
    "Original Release Date":"3/10/1992",
    "Release Date":"3/10/1992",
    "Format":"CD",
    "UPC (Barcode)":090317324425,
    "Label":"Teldec",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Antonin Dvorak; Vlach Quartet",
    "Title":"String Quartets, Op. 96 'American' and Op. 106",
    "Original Release Date":"1/23/1996",
    "Release Date":"1/23/1996",
    "Format":"MP3",
    "UPC (Barcode)":730099437127,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Earth, Wind and Fire",
    "Title":"Let Me Talk",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eastern Youth",
    "Title":"Soko kara Nani ga Mieru ka",
    "Original Release Date":"9/2/2003",
    "Release Date":"9/2/2003",
    "Format":"CD",
    "UPC (Barcode)":809965000724,
    "Label":"FiveOne Inc.",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Sheena Easton",
    "Title":"For Your Eyes Only",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Echo and the Bunnymen",
    "Title":"Echo And The Bunnymen",
    "Original Release Date":"7/7/1987",
    "Release Date":"1/27/2004",
    "Format":"MP3",
    "UPC (Barcode)":825646116423,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"eighth blackbird",
    "Title":"Strange Imaginary Animals",
    "Original Release Date":"11/28/2006",
    "Release Date":"11/28/2006",
    "Format":"MP3",
    "UPC (Barcode)":735131909426,
    "Label":"Cedille",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Electric Light Orchestra",
    "Title":"Hold on Tight",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Jet",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Electric Light Orchestra",
    "Title":"Twilight",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Jet",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yvonne Elliman",
    "Title":"The Very Best of Yvonne Elliman",
    "Original Release Date":"3/1/1995",
    "Release Date":"3/1/1995",
    "Format":"CD",
    "UPC (Barcode)":783785100324,
    "Label":"Taragon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Missy Elliott",
    "Title":"The Cookbook",
    "Original Release Date":"7/5/2005",
    "Release Date":"7/5/2005",
    "Format":"CD",
    "UPC (Barcode)":075678377921,
    "Label":"Atlantic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Missy Elliott",
    "Title":"This Is Not A Test",
    "Original Release Date":"11/25/2003",
    "Release Date":"11/25/2003",
    "Format":"CD",
    "UPC (Barcode)":075596290524,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Missy Elliott",
    "Title":"Under Construction",
    "Original Release Date":"11/12/2002",
    "Release Date":"11/12/2002",
    "Format":"CD",
    "UPC (Barcode)":075596281324,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eluvium",
    "Title":"Nightmare Ending",
    "Original Release Date":"5/21/2013",
    "Release Date":"5/21/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eluvium",
    "Title":"Similies",
    "Original Release Date":"2/23/2010",
    "Release Date":"2/23/2010",
    "Format":"MP3",
    "UPC (Barcode)":656605316321,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eluvium",
    "Title":"copia",
    "Original Release Date":"2/20/2007",
    "Release Date":"2/20/2007",
    "Format":"CD",
    "UPC (Barcode)":656605311029,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eluvium",
    "Title":"copia",
    "Original Release Date":"2/20/2007",
    "Release Date":"2/20/2007",
    "Format":"MP3",
    "UPC (Barcode)":656605311029,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eluvium",
    "Title":"When I Live by the Garden and the Sea",
    "Original Release Date":"8/22/2006",
    "Release Date":"8/22/2006",
    "Format":"MP3",
    "UPC (Barcode)":656605310923,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eluvium",
    "Title":"Talk Amongst the Trees",
    "Original Release Date":"3/1/2005",
    "Release Date":"3/1/2005",
    "Format":"MP3",
    "UPC (Barcode)":656605307824,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eluvium",
    "Title":"an accidental memory in the case of death",
    "Original Release Date":"5/4/2004",
    "Release Date":"5/4/2004",
    "Format":"MP3",
    "UPC (Barcode)":656605306629,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eluvium",
    "Title":"an accidental memory in the case of death",
    "Original Release Date":"5/4/2004",
    "Release Date":"5/4/2004",
    "Format":"CD",
    "UPC (Barcode)":656605306629,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eluvium",
    "Title":"Lambent Material",
    "Original Release Date":"5/6/2003",
    "Release Date":"5/6/2003",
    "Format":"MP3",
    "UPC (Barcode)":656605305721,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emerson String Quartet",
    "Title":"American Contemporaries",
    "Original Release Date":"5/7/1994",
    "Release Date":"2/8/2005",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Deutsche Grammophon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enigma",
    "Title":"MCMXCa. D.",
    "Original Release Date":"12/3/1990",
    "Release Date":"6/29/1992",
    "Format":"CD",
    "UPC (Barcode)":077778622420,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enigma",
    "Title":"MCMXC a.D.",
    "Original Release Date":"1990",
    "Release Date":"2/8/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075679164247,
    "Label":"Virgin",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Brian Eno",
    "Title":"Music For Airports",
    "Original Release Date":"1978",
    "Release Date":"8/31/1990",
    "Format":"CD",
    "UPC (Barcode)":017046151627,
    "Label":"Editions EG",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"EnVogue",
    "Title":"Rhino Hi-Five",
    "Original Release Date":"1/10/2010",
    "Release Date":"1/10/2010",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"envy",
    "Title":"All the Footprints You've Ever Left and the Fear Expecting Ahead",
    "Original Release Date":"11/22/2001",
    "Release Date":"1/22/2008",
    "Format":"MP3",
    "UPC (Barcode)":656605312927,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"envy",
    "Title":"Abyssal",
    "Original Release Date":"1/22/2008",
    "Release Date":"1/22/2008",
    "Format":"MP3",
    "UPC (Barcode)":656605313221,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"envy",
    "Title":"Insomniac Doze",
    "Original Release Date":"9/12/2006",
    "Release Date":"9/12/2006",
    "Format":"MP3",
    "UPC (Barcode)":656605311227,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"envy",
    "Title":"Insomniac Doze",
    "Original Release Date":"9/12/2006",
    "Release Date":"9/12/2006",
    "Format":"CD",
    "UPC (Barcode)":656605311227,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"And Winter Came ...",
    "Original Release Date":"11/11/2008",
    "Release Date":"11/11/2008",
    "Format":"CD",
    "UPC (Barcode)":82646933068,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Amarantine",
    "Original Release Date":"11/22/2005",
    "Release Date":"11/22/2005",
    "Format":"CD",
    "UPC (Barcode)":093624947424,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Wild Child",
    "Original Release Date":"8/1/2002",
    "Release Date":"8/1/2002",
    "Format":"CD",
    "UPC (Barcode)":685738737826,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"A Day Without Rain",
    "Original Release Date":"11/21/2000",
    "Release Date":"11/21/2000",
    "Format":"CD",
    "UPC (Barcode)":093624742623,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Only Time",
    "Original Release Date":"11/20/2000",
    "Release Date":"11/20/2000",
    "Format":"CD",
    "UPC (Barcode)":685738573929,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Paint The Sky With Stars - The Best Of Enya",
    "Original Release Date":"11/11/1997",
    "Release Date":"11/11/1997",
    "Format":"CD",
    "UPC (Barcode)":093624683520,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Only If ...",
    "Original Release Date":"11/18/1997",
    "Release Date":"11/18/1997",
    "Format":"CD",
    "UPC (Barcode)":054391726629,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"On My Way Home",
    "Original Release Date":"11/22/1996",
    "Release Date":"11/22/1996",
    "Format":"CD",
    "UPC (Barcode)":706301479926,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"The Memory of Trees",
    "Original Release Date":"12/5/1995",
    "Release Date":"12/5/1995",
    "Format":"CD",
    "UPC (Barcode)":093624610625,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"The Frog Prince",
    "Original Release Date":"1985",
    "Release Date":"4/28/1995",
    "Format":"CD",
    "UPC (Barcode)":731455109924,
    "Label":"Spectrum",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"The Celts",
    "Original Release Date":"November 1992",
    "Release Date":"6/27/1995",
    "Format":"CD",
    "UPC (Barcode)":093624568124,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Oíche Chiún (Silent Night) (Single)",
    "Original Release Date":"10/24/1995",
    "Release Date":"10/24/1995",
    "Format":"CD",
    "UPC (Barcode)":093624066026,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Book Of Days",
    "Original Release Date":"8/1/1992",
    "Release Date":"8/1/1992",
    "Format":"CD",
    "UPC (Barcode)":745099041724,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Shepherd Moons",
    "Original Release Date":"11/19/1991",
    "Release Date":"11/19/1991",
    "Format":"CD",
    "UPC (Barcode)":075992675523,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Watermark",
    "Original Release Date":"9/19/1988",
    "Release Date":"1/10/1989",
    "Format":"CD",
    "UPC (Barcode)":075992677424,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Watermark",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":075992423311,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Orinoco Flow",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"7 Inch",
    "UPC (Barcode)":075992763370,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Enya",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"CD",
    "UPC (Barcode)":075678184222,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Enya",
    "Title":"Enya",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"Cassette",
    "UPC (Barcode)":075678184246,
    "Label":"Atlantic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Eponymous 4",
    "Title":"Work Release Program, Vol. 1: Imprint",
    "Original Release Date":"4/19/2009",
    "Release Date":"4/19/2009",
    "Format":"MP3",
    "UPC (Barcode)":884502019339,
    "Label":"Observant",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eponymous 4",
    "Title":"Work Release Program, Vol. 2: Kaze no Uta wo Kike",
    "Original Release Date":"5/19/2009",
    "Release Date":"5/19/2009",
    "Format":"MP3",
    "UPC (Barcode)":884502072341,
    "Label":"Observant",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eponymous 4",
    "Title":"Work Release Program, Vol. 3: Restraint",
    "Original Release Date":"8/18/2009",
    "Release Date":"8/18/2009",
    "Format":"MP3",
    "UPC (Barcode)":884502096453,
    "Label":"Observant",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eponymous 4",
    "Title":"enigmatics",
    "Original Release Date":"6/24/2008",
    "Release Date":"6/24/2008",
    "Format":"CD",
    "UPC (Barcode)":634479796210,
    "Label":"Observant",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Erasure",
    "Title":"The Innocents",
    "Original Release Date":"5/24/1988",
    "Release Date":"10/25/1990",
    "Format":"CD",
    "UPC (Barcode)":075992573061,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Erasure",
    "Title":"The Innocents",
    "Original Release Date":"5/24/1988",
    "Release Date":"10/25/1990",
    "Format":"MP3",
    "UPC (Barcode)":075992573061,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Erasure",
    "Title":"The Innocents",
    "Original Release Date":"5/24/1988",
    "Release Date":"10/25/1990",
    "Format":"Stream",
    "UPC (Barcode)":075992573061,
    "Label":"Reprise",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Eurythmics",
    "Title":"Savage",
    "Original Release Date":"November 1987",
    "Release Date":"11/15/2005",
    "Format":"MP3",
    "UPC (Barcode)":078635679410,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eurythmics",
    "Title":"Savage",
    "Original Release Date":"November 1987",
    "Release Date":"11/15/2005",
    "Format":"CD",
    "UPC (Barcode)":078635679410,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eurythmics",
    "Title":"Sweet Dreams",
    "Original Release Date":"1/21/1983",
    "Release Date":"11/15/2005",
    "Format":"CD",
    "UPC (Barcode)":828765611527,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eurythmics",
    "Title":"Don't Ask Me Why",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Arista",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eurythmics",
    "Title":"Savage",
    "Original Release Date":"November 1987",
    "Release Date":"November 1987",
    "Format":"LP",
    "UPC (Barcode)":078635679410,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eurythmics",
    "Title":"Revenge",
    "Original Release Date":"July 1986",
    "Release Date":"July 1986",
    "Format":"LP",
    "UPC (Barcode)":078635584714,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eurythmics",
    "Title":"Missionary Man",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eurythmics",
    "Title":"Be Yourself Tonight",
    "Original Release Date":"May 1985",
    "Release Date":"May 1985",
    "Format":"LP",
    "UPC (Barcode)":078635542912,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eurythmics",
    "Title":"1984 - For The Love Of Big Brother",
    "Original Release Date":"November 1984",
    "Release Date":"November 1984",
    "Format":"LP",
    "UPC (Barcode)":078635537116,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eurythmics",
    "Title":"Touch",
    "Original Release Date":"November 1983",
    "Release Date":"November 1983",
    "Format":"LP",
    "UPC (Barcode)":078635491715,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Eurythmics",
    "Title":"Sweet Dreams",
    "Original Release Date":"1/21/1983",
    "Release Date":"1/21/1983",
    "Format":"LP",
    "UPC (Barcode)":078635468113,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Everything But The Girl",
    "Title":"Like The Deserts Miss the Rain",
    "Original Release Date":"3/11/2003",
    "Release Date":"3/11/2003",
    "Format":"CD",
    "UPC (Barcode)":081227383824,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Everything But The Girl",
    "Title":"Temperamental",
    "Original Release Date":"9/28/1999",
    "Release Date":"9/28/1999",
    "Format":"CD",
    "UPC (Barcode)":075678321429,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Everything But The Girl",
    "Title":"Walking Wounded",
    "Original Release Date":"5/21/1996",
    "Release Date":"5/21/1996",
    "Format":"CD",
    "UPC (Barcode)":075678291227,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Everything But The Girl",
    "Title":"Amplified Heart",
    "Original Release Date":"7/19/1994",
    "Release Date":"7/19/1994",
    "Format":"CD",
    "UPC (Barcode)":075678260520,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Everything But The Girl",
    "Title":"Idlewild",
    "Original Release Date":"February 1988",
    "Release Date":"February 1988",
    "Format":"MP3",
    "UPC (Barcode)":075992572125,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ex-Boyfriends",
    "Title":"Line In / Line Out",
    "Original Release Date":"4/13/2010",
    "Release Date":"4/13/2010",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Ex-Boyfriends",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ex-Boyfriends",
    "Title":"In With",
    "Original Release Date":"12/4/2007",
    "Release Date":"12/4/2007",
    "Format":"CD",
    "UPC (Barcode)":653225007226,
    "Label":"Absolutely Kosher",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Ex-Boyfriends",
    "Title":"In With",
    "Original Release Date":"12/4/2007",
    "Release Date":"12/4/2007",
    "Format":"MP3",
    "UPC (Barcode)":653225007226,
    "Label":"Absolutely Kosher",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ex-Boyfriends",
    "Title":"Dear John",
    "Original Release Date":"2/21/2006",
    "Release Date":"2/21/2006",
    "Format":"MP3",
    "UPC (Barcode)":653225005529,
    "Label":"Absolutely Kosher",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ex-Boyfriends",
    "Title":"Dear John",
    "Original Release Date":"2/21/2006",
    "Release Date":"2/21/2006",
    "Format":"CD",
    "UPC (Barcode)":653225005529,
    "Label":"Absolutely Kosher",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"eX-Girl",
    "Title":"Endangered Species",
    "Original Release Date":"4/13/2004",
    "Release Date":"4/13/2004",
    "Format":"CD",
    "UPC (Barcode)":721616031324,
    "Label":"Alternative Tentacles",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"eX-Girl",
    "Title":"Luna Rosé",
    "Original Release Date":"11/6/2002",
    "Release Date":"11/6/2002",
    "Format":"CD",
    "UPC (Barcode)":4988064853250,
    "Label":"Rhythm Republic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"eX-Girl",
    "Title":"Pop Muzik",
    "Original Release Date":"7/7/2001",
    "Release Date":"7/7/2001",
    "Format":"CD",
    "UPC (Barcode)":4544738100107,
    "Label":"Hi-Boom",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"eX-Girl",
    "Title":"The Legend of the Waterbreakers",
    "Original Release Date":"6/25/2001",
    "Release Date":"6/25/2001",
    "Format":"CD",
    "UPC (Barcode)":612505187225,
    "Label":"Kikipoo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"eX-Girl",
    "Title":"Back to the Mono Kero!",
    "Original Release Date":"5/15/2001",
    "Release Date":"5/15/2001",
    "Format":"CD",
    "UPC (Barcode)":689230001522,
    "Label":"Ipecac",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"eX-Girl",
    "Title":"Big When Far, Small When Close",
    "Original Release Date":"5/9/2000",
    "Release Date":"5/9/2000",
    "Format":"CD",
    "UPC (Barcode)":4948722039204,
    "Label":"Compozilla",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"eX-Girl",
    "Title":"Kero! Kero! Kero!",
    "Original Release Date":"3/10/1999",
    "Release Date":"3/10/1999",
    "Format":"CD",
    "UPC (Barcode)":4523854500206,
    "Label":"Compozilla",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"eX-Girl",
    "Title":"Heppoco Pou",
    "Original Release Date":"6/1/1998",
    "Release Date":"6/1/1998",
    "Format":"CD",
    "UPC (Barcode)":667341607628,
    "Label":"Compozilla",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Explosions In The Sky",
    "Title":"Take Care, Take Care, Take Care",
    "Original Release Date":"4/26/2011",
    "Release Date":"4/26/2011",
    "Format":"CD",
    "UPC (Barcode)":602527663142,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Explosions In The Sky",
    "Title":"All Of A Sudden I Miss Everyone",
    "Original Release Date":"2/20/2007",
    "Release Date":"2/20/2007",
    "Format":"CD",
    "UPC (Barcode)":656605309927,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Explosions In The Sky",
    "Title":"All Of A Sudden I Miss Everyone",
    "Original Release Date":"2/20/2007",
    "Release Date":"2/20/2007",
    "Format":"MP3",
    "UPC (Barcode)":656605309927,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Explosions In The Sky",
    "Title":"Travels In Constants, Volume 21: The Rescue",
    "Original Release Date":"10/11/2005",
    "Release Date":"10/11/2005",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Explosions In The Sky",
    "Title":"How Strange, Innocence",
    "Original Release Date":"1/17/2000",
    "Release Date":"10/11/2005",
    "Format":"CD",
    "UPC (Barcode)":656605308524,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Explosions In The Sky",
    "Title":"How Strange, Innocence",
    "Original Release Date":"1/17/2000",
    "Release Date":"10/11/2005",
    "Format":"MP3",
    "UPC (Barcode)":656605308524,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Explosions In The Sky",
    "Title":"The Earth Is Not A Cold Dead Place",
    "Original Release Date":"11/4/2003",
    "Release Date":"11/4/2003",
    "Format":"CD",
    "UPC (Barcode)":656605306124,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Explosions In The Sky",
    "Title":"Those Who Tell the Truth Shall Die, Those Who Tell the Truth Shall Live Forever",
    "Original Release Date":"9/4/2001",
    "Release Date":"9/4/2001",
    "Format":"MP3",
    "UPC (Barcode)":656605303420,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Explosions In The Sky",
    "Title":"Those Who Tell the Truth Shall Die, Those Who Tell the Truth Shall Live Forever",
    "Original Release Date":"9/4/2001",
    "Release Date":"9/4/2001",
    "Format":"CD",
    "UPC (Barcode)":656605303420,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Exposé",
    "Title":"Exposure",
    "Original Release Date":"1987",
    "Release Date":"10/25/1990",
    "Format":"MP3",
    "UPC (Barcode)":078221844123,
    "Label":"Arista",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Exposé",
    "Title":"Exposure",
    "Original Release Date":"1987",
    "Release Date":"10/25/1990",
    "Format":"Stream",
    "UPC (Barcode)":078221844123,
    "Label":"Arista",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Exposé",
    "Title":"Exposure",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":078221844116,
    "Label":"Arista",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Extreme",
    "Title":"Pornograffiti",
    "Original Release Date":"7/26/1990",
    "Release Date":"7/26/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075021531345,
    "Label":"A&M",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Fairground Attraction",
    "Title":"The First of a Million Kisses",
    "Original Release Date":"5/23/1988",
    "Release Date":"5/23/1988",
    "Format":"LP",
    "UPC (Barcode)":078635859614,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Fairground Attraction",
    "Title":"Perfect",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"7 Inch",
    "UPC (Barcode)":078635878578,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Faith No More",
    "Title":"Who Cares A Lot? Greatest Hits",
    "Original Release Date":"12/8/1998",
    "Release Date":"12/8/1998",
    "Format":"CD",
    "UPC (Barcode)":093624714927,
    "Label":"Slash",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Faith No More",
    "Title":"The Real Thing",
    "Original Release Date":"6/15/1989",
    "Release Date":"6/15/1989",
    "Format":"LP",
    "UPC (Barcode)":075992587822,
    "Label":"Slash",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Faith No More",
    "Title":"The Real Thing",
    "Original Release Date":"6/15/1989",
    "Release Date":"6/15/1989",
    "Format":"CD",
    "UPC (Barcode)":075992587822,
    "Label":"Slash",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Faith No More",
    "Title":"The Real Thing",
    "Original Release Date":"6/19/1989",
    "Release Date":"6/19/1989",
    "Format":"Cassette",
    "UPC (Barcode)":075992587846,
    "Label":"Slash",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Falco",
    "Title":"Rock Me Amadeus",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"farida's cafe",
    "Title":"mile a minute",
    "Original Release Date":"3/21/2002",
    "Release Date":"3/21/2002",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"MIDI Creative",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Gabriel Fauré ",
    "Title":"Requiem and other choral music",
    "Original Release Date":"1988",
    "Release Date":"2/29/2000",
    "Format":"CD",
    "UPC (Barcode)":040888010920,
    "Label":"Collegium",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gabriel Fauré ",
    "Title":"Requiem",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"LP",
    "UPC (Barcode)":028941924315,
    "Label":"Deutsche Grammophon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gabriel Fauré ; Maurice Duruflé",
    "Title":"Faure: Requiem, Op.48 / Durufle: Requiem, Op.9",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":089408013522,
    "Label":"Telarc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"FEED",
    "Title":"9 Songs",
    "Original Release Date":"6/27/2001",
    "Release Date":"6/27/2001",
    "Format":"CD",
    "UPC (Barcode)":4540957000938,
    "Label":"Tripmaster",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"FEED",
    "Title":"Make Every Stardust Shimmer!",
    "Original Release Date":"3/23/2000",
    "Release Date":"3/23/2000",
    "Format":"CD",
    "UPC (Barcode)":4517331153450,
    "Label":"DE-I",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Morton Feldman",
    "Title":"The Viola in My Life",
    "Original Release Date":"6/10/1994",
    "Release Date":"11/1/2006",
    "Format":"MP3",
    "UPC (Barcode)":093228065722,
    "Label":"New World",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Morton Feldman",
    "Title":"The Viola in My Life/False Patterns and the Extended Relationship/Why Patterns?",
    "Original Release Date":"6/10/1994",
    "Release Date":"11/1/2006",
    "Format":"CD",
    "UPC (Barcode)":093228065722,
    "Label":"New World",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Morton Feldman",
    "Title":"The Rothko Chapel / For Stephan Wolpe / Christian Wolff in Cambridge",
    "Original Release Date":"9/1/2002",
    "Release Date":"9/1/2002",
    "Format":"MP3",
    "UPC (Barcode)":040888302322,
    "Label":"Hanssler Classics",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Morton Feldman",
    "Title":"Only",
    "Original Release Date":"6/10/1996",
    "Release Date":"6/10/1996",
    "Format":"MP3",
    "UPC (Barcode)":022551008526,
    "Label":"New Albion",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Morton Feldman; Flux Quartet",
    "Title":"String Quartet No. 2",
    "Original Release Date":"11/26/2002",
    "Release Date":"11/26/2002",
    "Format":"CD",
    "UPC (Barcode)":764593011224,
    "Label":"Mode",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Montserrat Figueras",
    "Title":"Ninna Nanna",
    "Original Release Date":"2/11/2003",
    "Release Date":"2/11/2003",
    "Format":"CD",
    "UPC (Barcode)":7619986098265,
    "Label":"AliaVox",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Roberta Flack",
    "Title":"Softly With These Songs: The Best of Roberta Flack",
    "Original Release Date":"6/22/1993",
    "Release Date":"6/22/1993",
    "Format":"CD",
    "UPC (Barcode)":075678249822,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Flaming Lips",
    "Title":"Embryonic",
    "Original Release Date":"10/13/2009",
    "Release Date":"10/13/2009",
    "Format":"Stream",
    "UPC (Barcode)":093624973379,
    "Label":"Warner Bros.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Flaming Lips",
    "Title":"At War with the Mystics",
    "Original Release Date":"4/4/2006",
    "Release Date":"4/4/2006",
    "Format":"CD",
    "UPC (Barcode)":093624996620,
    "Label":"Warner Bros.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Flaming Lips",
    "Title":"Yoshimi Battles The Pink Robots",
    "Original Release Date":"7/16/2002",
    "Release Date":"7/16/2002",
    "Format":"CD",
    "UPC (Barcode)":093624814122,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Flaming Lips",
    "Title":"The Soft Bulletin",
    "Original Release Date":"6/22/1999",
    "Release Date":"6/22/1999",
    "Format":"CD",
    "UPC (Barcode)":093624687627,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Flaming Lips",
    "Title":"Zaireeka",
    "Original Release Date":"10/28/1997",
    "Release Date":"10/28/1997",
    "Format":"CD",
    "UPC (Barcode)":093624680420,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Fleetwood Mac",
    "Title":"Behind the Mask",
    "Original Release Date":"3/29/1990",
    "Release Date":"3/29/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075992611145,
    "Label":"Warner Bros.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Fleetwood Mac",
    "Title":"Greatest Hits",
    "Original Release Date":"11/22/1988",
    "Release Date":"11/22/1988",
    "Format":"CD",
    "UPC (Barcode)":075992580120,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Fleming Pie",
    "Title":"Fleming Pie",
    "Original Release Date":"12/13/2000",
    "Release Date":"12/13/2000",
    "Format":"CD",
    "UPC (Barcode)":4988006786431,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Renée Fleming",
    "Title":"Dark Hope",
    "Original Release Date":"6/8/2010",
    "Release Date":"6/8/2010",
    "Format":"CD",
    "UPC (Barcode)":602527362052,
    "Label":"Decca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Renée Fleming",
    "Title":"Dark Hope",
    "Original Release Date":"6/8/2010",
    "Release Date":"6/8/2010",
    "Format":"MP3",
    "UPC (Barcode)":602527362052,
    "Label":"Decca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"FLiP",
    "Title":"Kanshou Chuudoku",
    "Original Release Date":"5/20/2009",
    "Release Date":"5/20/2009",
    "Format":"CD",
    "UPC (Barcode)":4547292128181,
    "Label":"Highwave",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"FLiP",
    "Title":"Haha Kara Umareta Hinekure no Uta",
    "Original Release Date":"6/25/2008",
    "Release Date":"6/25/2008",
    "Format":"CD",
    "UPC (Barcode)":4547292126187,
    "Label":"Highwave",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"David Foster",
    "Title":"Symphony Sessions",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":075678179914,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"David Foster",
    "Title":"Winter Games",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"7 Inch",
    "UPC (Barcode)":075678914072,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"fra-foa",
    "Title":"13 Leaves",
    "Original Release Date":"9/19/2002",
    "Release Date":"9/19/2002",
    "Format":"CD",
    "UPC (Barcode)":4988061861159,
    "Label":"Toy's Factory",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"fra-foa",
    "Title":"Chuu no Fuchi",
    "Original Release Date":"2/21/2001",
    "Release Date":"2/21/2001",
    "Format":"CD",
    "UPC (Barcode)":4988061881713,
    "Label":"Toy's Factory",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Frankie Goes to Hollywood",
    "Title":"Relax",
    "Original Release Date":"1983",
    "Release Date":"1983",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Franz Ferdinand",
    "Title":"Franz Ferdinand",
    "Original Release Date":"11/23/2004",
    "Release Date":"11/23/2004",
    "Format":"MP3",
    "UPC (Barcode)":827969363027,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Franz Joseph Haydn",
    "Title":"Haydn: Symphonies Nos. 82, 96, 100",
    "Original Release Date":"2/5/1993",
    "Release Date":"2/5/1993",
    "Format":"MP3",
    "UPC (Barcode)":730099513920,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Franz Joseph Haydn; Kodály Quartet",
    "Title":"String Quartets Op. 20, 'Sun', Nos. 1-3",
    "Original Release Date":"2/15/1994",
    "Release Date":"2/15/1994",
    "Format":"MP3",
    "UPC (Barcode)":730099570121,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bill Frisell",
    "Title":"Nashville",
    "Original Release Date":"4/29/1997",
    "Release Date":"4/29/1997",
    "Format":"CD",
    "UPC (Barcode)":075597941524,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bill Frisell",
    "Title":"Have a Little Faith",
    "Original Release Date":"2/23/1993",
    "Release Date":"2/23/1993",
    "Format":"CD",
    "UPC (Barcode)":075597930122,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bill Frisell",
    "Title":"Where in the World?",
    "Original Release Date":"10/16/1991",
    "Release Date":"10/16/1991",
    "Format":"CD",
    "UPC (Barcode)":075596118125,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bill Frisell",
    "Title":"Where in the World?",
    "Original Release Date":"10/15/1991",
    "Release Date":"10/15/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075596118149,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Bill Frisell",
    "Title":"Is That You?",
    "Original Release Date":"5/17/1990",
    "Release Date":"5/17/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075596095648,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Bill Frisell",
    "Title":"Before We Were Born",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"Cassette",
    "UPC (Barcode)":075596084345,
    "Label":"Musician",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Bill Frisell",
    "Title":"Before We Were Born",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"Stream",
    "UPC (Barcode)":075596084369,
    "Label":"Musician",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Fugazi",
    "Title":"Repeater + 3 Songs",
    "Original Release Date":"3/20/1990",
    "Release Date":"2005",
    "Format":"CD",
    "UPC (Barcode)":718750734822,
    "Label":"Dischord",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Fugazi",
    "Title":"Red Medicine",
    "Original Release Date":"6/12/1995",
    "Release Date":"6/12/1995",
    "Format":"CD",
    "UPC (Barcode)":718751799028,
    "Label":"Dischord",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Fugazi",
    "Title":"In On The Kill Taker",
    "Original Release Date":"6/30/1993",
    "Release Date":"6/30/1993",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Dischord",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Fugazi",
    "Title":"13 Songs",
    "Original Release Date":"October 1989",
    "Release Date":"4/11/1990",
    "Format":"CD",
    "UPC (Barcode)":718750734020,
    "Label":"Dischord",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Fuji Fabric",
    "Title":"TEENAGER",
    "Original Release Date":"1/23/2008",
    "Release Date":"1/23/2008",
    "Format":"CD",
    "UPC (Barcode)":4988006214101,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Fuji Fabric",
    "Title":"Fuji Fabric",
    "Original Release Date":"11/10/2004",
    "Release Date":"11/10/2004",
    "Format":"CD",
    "UPC (Barcode)":4988006194236,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Furukawa Miki",
    "Title":"Bondage Heart",
    "Original Release Date":"4/23/2008",
    "Release Date":"4/23/2008",
    "Format":"CD",
    "UPC (Barcode)":4988017658086,
    "Label":"BMG Japan",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Furukawa Miki",
    "Title":"Mirrors",
    "Original Release Date":"7/19/2006",
    "Release Date":"7/19/2006",
    "Format":"CD",
    "UPC (Barcode)":4988017641170,
    "Label":"BMG Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Fuzzy Control",
    "Title":"Shine On",
    "Original Release Date":"6/21/2003",
    "Release Date":"6/21/2003",
    "Format":"CD",
    "UPC (Barcode)":4988002448364,
    "Label":"Victor",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Gang of Four",
    "Title":"Content",
    "Original Release Date":"1/25/2011",
    "Release Date":"1/25/2011",
    "Format":"CD",
    "UPC (Barcode)":634457222823,
    "Label":"Yep Roc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gang of Four",
    "Title":"Content",
    "Original Release Date":"1/25/2011",
    "Release Date":"1/25/2011",
    "Format":"MP3",
    "UPC (Barcode)":634457222823,
    "Label":"Yep Roc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gang of Four",
    "Title":"Second Life",
    "Original Release Date":"7/6/2008",
    "Release Date":"7/6/2008",
    "Format":"MP3",
    "UPC (Barcode)":884463097841,
    "Label":"Gang of Four",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gang of Four",
    "Title":"Entertainment! (Remastered)",
    "Original Release Date":"1979",
    "Release Date":"5/17/2005",
    "Format":"CD",
    "UPC (Barcode)":081227842826,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gang of Four",
    "Title":"Return the Gift",
    "Original Release Date":"10/11/2005",
    "Release Date":"10/11/2005",
    "Format":"CD",
    "UPC (Barcode)":638812727022,
    "Label":"V2",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Gang of Four",
    "Title":"Hard/Solid Gold",
    "Original Release Date":"1/21/2003",
    "Release Date":"1/21/2003",
    "Format":"CD",
    "UPC (Barcode)":664140393622,
    "Label":"Wounded Bird",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Garbage",
    "Title":"Absolute Garbage",
    "Original Release Date":"7/24/2007",
    "Release Date":"7/24/2007",
    "Format":"MP3",
    "UPC (Barcode)":00602517375130,
    "Label":"Almo Sounds",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Marvin Gaye",
    "Title":"What's Going On",
    "Original Release Date":"5/20/1971",
    "Release Date":"4/7/1998",
    "Format":"CD",
    "UPC (Barcode)":731453088320,
    "Label":"Motown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Marvin Gaye",
    "Title":"What's Going On",
    "Original Release Date":"5/20/1971",
    "Release Date":"5/20/1971",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Motown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gaytheist",
    "Title":"Stealth Beats",
    "Original Release Date":"8/21/2012",
    "Release Date":"8/21/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Good to Die",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Geinoh Yamashirogumi",
    "Title":"Ecophony Rinne",
    "Original Release Date":"10/21/1994",
    "Release Date":"10/21/1994",
    "Format":"CD",
    "UPC (Barcode)":4988002300952,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Geinoh Yamashirogumi",
    "Title":"Nyumon (Best Selection of Geinoh Yamashiro Gumi)",
    "Original Release Date":"10/21/1994",
    "Release Date":"10/21/1994",
    "Format":"CD",
    "UPC (Barcode)":4988002300969,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gentleman Reg",
    "Title":"Jet Black",
    "Original Release Date":"2/24/2009",
    "Release Date":"2/24/2009",
    "Format":"MP3",
    "UPC (Barcode)":827590400023,
    "Label":"Arts & Crafts",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Carlo Gesualdo; A Sei Voci",
    "Title":"Tenebrae Responsories / Benedictus / Miserere",
    "Original Release Date":"2007",
    "Release Date":"2007",
    "Format":"CD",
    "UPC (Barcode)":825646278220,
    "Label":"Apex",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Benjamin Gibbard",
    "Title":"Former Lives",
    "Original Release Date":"10/16/2012",
    "Release Date":"10/16/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Barsuk",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Girl in a Coma",
    "Title":"Trio B.C.",
    "Original Release Date":"6/2/2009",
    "Release Date":"6/2/2009",
    "Format":"MP3",
    "UPC (Barcode)":748337535122,
    "Label":"Blackheart",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Girl Talk",
    "Title":"Feed the Animals",
    "Original Release Date":"6/19/2008",
    "Release Date":"6/19/2008",
    "Format":"MP3",
    "UPC (Barcode)":5017148119995,
    "Label":"Illegal Art",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Girl Talk",
    "Title":"Feed the Animals",
    "Original Release Date":"11/10/2008",
    "Release Date":"11/10/2008",
    "Format":"CD",
    "UPC (Barcode)":5017148119995,
    "Label":"Illegal Art",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Girl Talk",
    "Title":"Night Ripper",
    "Original Release Date":"5/9/2006",
    "Release Date":"5/9/2006",
    "Format":"CD",
    "UPC (Barcode)":613285933729,
    "Label":"Illegal Art",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Girl Talk",
    "Title":"Unstoppable",
    "Original Release Date":"5/9/2006",
    "Release Date":"5/9/2006",
    "Format":"MP3",
    "UPC (Barcode)":613285963221,
    "Label":"Illegal Art",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Philip Glass",
    "Title":"Einstein On The Beach",
    "Original Release Date":"9/24/1993",
    "Release Date":"1/17/2012",
    "Format":"CD",
    "UPC (Barcode)":075597932324,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Philip Glass",
    "Title":"Symphony No. 3",
    "Original Release Date":"11/22/2005",
    "Release Date":"11/22/2005",
    "Format":"MP3",
    "UPC (Barcode)":07559795812,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Philip Glass",
    "Title":"Glassworks",
    "Original Release Date":"1982",
    "Release Date":"9/30/2003",
    "Format":"MP3",
    "UPC (Barcode)":827969039427,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Philip Glass",
    "Title":"Songs from the Liquid Days",
    "Original Release Date":"1986",
    "Release Date":"10/25/1990",
    "Format":"MP3",
    "UPC (Barcode)":074643956420,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Philip Glass",
    "Title":"Mishima",
    "Original Release Date":"1985",
    "Release Date":"10/25/1990",
    "Format":"CD",
    "UPC (Barcode)":07559791132,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Philip Glass",
    "Title":"1000 Airplanes On The Roof",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"CD",
    "UPC (Barcode)":762185161128,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Philip Glass",
    "Title":"1,000 Airplanes on the Roof",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"Cassette",
    "UPC (Barcode)":075679106544,
    "Label":"Virgin",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Philip Glass",
    "Title":"Dance Nos. 1-5",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"MP3",
    "UPC (Barcode)":074644476521,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Philip Glass",
    "Title":"Mishima",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"LP",
    "UPC (Barcode)":075597911312,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Philip Glass",
    "Title":"Mishima",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"Cassette",
    "UPC (Barcode)":075597911343,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Philip Glass; Bang On A Can All-Stars",
    "Title":"Music in Fifths / Two Pages",
    "Original Release Date":"9/14/2004",
    "Release Date":"9/14/2004",
    "Format":"MP3",
    "UPC (Barcode)":713746289324,
    "Label":"Cantaloupe",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Philip Glass; Kronos Quartet",
    "Title":"Dracula",
    "Original Release Date":"8/31/1999",
    "Release Date":"8/31/1999",
    "Format":"CD",
    "UPC (Barcode)":075597954227,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gnarls Barkley",
    "Title":"The Odd Couple",
    "Original Release Date":"3/25/2008",
    "Release Date":"3/25/2008",
    "Format":"CD",
    "UPC (Barcode)":075678994692,
    "Label":"Atlantic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Gnarls Barkley",
    "Title":"St. Elsewhere",
    "Original Release Date":"5/9/2006",
    "Release Date":"5/9/2006",
    "Format":"CD",
    "UPC (Barcode)":878037000320,
    "Label":"Downtown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"GO!GO!7188",
    "Title":"Best of GO!GO!",
    "Original Release Date":"3/15/2006",
    "Release Date":"3/15/2006",
    "Format":"CD",
    "UPC (Barcode)":4988006204652,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Go-Betweens",
    "Title":"16 Lovers Lane",
    "Original Release Date":"August 1988",
    "Release Date":"8/5/2003",
    "Format":"MP3",
    "UPC (Barcode)":607618200626,
    "Label":"Beggars Banquet",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Godley & Creme",
    "Title":"Cry",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ari Gold",
    "Title":"Transport Systems",
    "Original Release Date":"9/4/2007",
    "Release Date":"9/4/2007",
    "Format":"CD",
    "UPC (Barcode)":650773000235,
    "Label":"Gold 18",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ari Gold",
    "Title":"Space Under Sun",
    "Original Release Date":"6/8/2004",
    "Release Date":"6/8/2004",
    "Format":"CD",
    "UPC (Barcode)":783707825526,
    "Label":"Gold 18",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ari Gold",
    "Title":"Ari Gold",
    "Original Release Date":"2000",
    "Release Date":"2000",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Gold 18",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Osvaldo Golijov",
    "Title":"Oceana",
    "Original Release Date":"7/10/2007",
    "Release Date":"7/10/2007",
    "Format":"MP3",
    "UPC (Barcode)":028947764267,
    "Label":"Deutsche Grammophon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Osvaldo Golijov",
    "Title":"Oceana",
    "Original Release Date":"7/10/2007",
    "Release Date":"7/10/2007",
    "Format":"CD",
    "UPC (Barcode)":028947764267,
    "Label":"Deutsche Grammophon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Henryk Górecki",
    "Title":"Concerto for Piano and Strings/Three Pieces in Old Style",
    "Original Release Date":"2/14/1995",
    "Release Date":"2/14/1995",
    "Format":"CD",
    "UPC (Barcode)":756055124627,
    "Label":"Conifer",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Henryk Górecki",
    "Title":"Symphony No. 3",
    "Original Release Date":"5/5/1992",
    "Release Date":"5/5/1992",
    "Format":"CD",
    "UPC (Barcode)":075597928228,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Henryk Górecki; Kronos Quartet",
    "Title":"String Quartet no. 3 ... songs are sung",
    "Original Release Date":"3/20/2007",
    "Release Date":"3/20/2007",
    "Format":"CD",
    "UPC (Barcode)":075597999334,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Henryk Górecki; Kronos Quartet",
    "Title":"String Quartet No. 1, Op. 62 / String Quartet No. 2, Op. 64",
    "Original Release Date":"6/29/1993",
    "Release Date":"6/29/1993",
    "Format":"CD",
    "UPC (Barcode)":075597931921,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gossip",
    "Title":"A Joyful Noise",
    "Original Release Date":"5/22/2012",
    "Release Date":"5/22/2012",
    "Format":"CD",
    "UPC (Barcode)":886919826524,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Gossip",
    "Title":"Music for Men",
    "Original Release Date":"6/23/2009",
    "Release Date":"6/23/2009",
    "Format":"MP3",
    "UPC (Barcode)":886975501229,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Gossip",
    "Title":"Music for Men",
    "Original Release Date":"6/23/2009",
    "Release Date":"6/23/2009",
    "Format":"Stream",
    "UPC (Barcode)":886975501229,
    "Label":"Columbia",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Gossip",
    "Title":"Standing In The Way Of Control",
    "Original Release Date":"1/24/2006",
    "Release Date":"1/24/2006",
    "Format":"MP3",
    "UPC (Barcode)":759656042222,
    "Label":"Kill Rock Stars",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Gossip",
    "Title":"Standing In The Way Of Control",
    "Original Release Date":"1/24/2006",
    "Release Date":"1/24/2006",
    "Format":"CD",
    "UPC (Barcode)":759656042222,
    "Label":"Kill Rock Stars",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"GRAPEVINE",
    "Title":"TWANGS",
    "Original Release Date":"7/15/2009",
    "Release Date":"7/15/2009",
    "Format":"MP3",
    "UPC (Barcode)":4988013858749,
    "Label":"Pony Canyon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Alexander Grechaninov",
    "Title":"Passion Week, Op. 58",
    "Original Release Date":"4/24/2007",
    "Release Date":"4/24/2007",
    "Format":"MP3",
    "UPC (Barcode)":095115504420,
    "Label":"Chandos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jay Greenberg",
    "Title":"Symphony No. 5 & Quintet For Strings",
    "Original Release Date":"8/15/2006",
    "Release Date":"8/15/2006",
    "Format":"CD",
    "UPC (Barcode)":828768180426,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Greene String Quartet",
    "Title":"The String Machine",
    "Original Release Date":"5/14/1991",
    "Release Date":"5/14/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075679163240,
    "Label":"Virgin",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Johnny Greenwood; Krzysztof Penderecki",
    "Title":"Threnody For The Victims Of Hiroshima / Popcorn Superhet Receiver / Polymorphia / 48 Responses To Polymorphia",
    "Original Release Date":"3/12/2012",
    "Release Date":"3/12/2012",
    "Format":"CD",
    "UPC (Barcode)":075597962512,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Patty Griffin",
    "Title":"Children Running Through",
    "Original Release Date":"2/6/2007",
    "Release Date":"2/6/2007",
    "Format":"MP3",
    "UPC (Barcode)":880882157425,
    "Label":"ATO",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Patty Griffin",
    "Title":"1000 Kisses",
    "Original Release Date":"4/9/2002",
    "Release Date":"4/9/2002",
    "Format":"CD",
    "UPC (Barcode)":791022150421,
    "Label":"ATO",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Patty Griffin",
    "Title":"Flaming Red",
    "Original Release Date":"6/23/1998",
    "Release Date":"6/23/1998",
    "Format":"CD",
    "UPC (Barcode)":731454090728,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Patty Griffin",
    "Title":"Living With Ghosts",
    "Original Release Date":"5/21/1996",
    "Release Date":"5/21/1996",
    "Format":"CD",
    "UPC (Barcode)":731454049023,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Grizzly Bear",
    "Title":"Shields",
    "Original Release Date":"9/17/2012",
    "Release Date":"9/17/2012",
    "Format":"MP3",
    "UPC (Barcode)":801061022921,
    "Label":"Warp",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Grizzly Bear",
    "Title":"Veckatimest",
    "Original Release Date":"5/26/2009",
    "Release Date":"5/26/2009",
    "Format":"MP3",
    "UPC (Barcode)":0801061018221,
    "Label":"Warp",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Grizzly Bear",
    "Title":"Friend EP",
    "Original Release Date":"11/6/2007",
    "Release Date":"11/6/2007",
    "Format":"MP3",
    "UPC (Barcode)":801061016326,
    "Label":"Warp",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Grizzly Bear",
    "Title":"Yellow House",
    "Original Release Date":"9/5/2006",
    "Release Date":"9/5/2006",
    "Format":"MP3",
    "UPC (Barcode)":801061014728,
    "Label":"Warp",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Grizzly Bear",
    "Title":"Horn of Plenty",
    "Original Release Date":"11/8/2005",
    "Release Date":"11/8/2005",
    "Format":"CD",
    "UPC (Barcode)":827175001522,
    "Label":"Kanine",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Grizzly Bear",
    "Title":"Horn of Plenty",
    "Original Release Date":"11/8/2005",
    "Release Date":"11/8/2005",
    "Format":"MP3",
    "UPC (Barcode)":827175001522,
    "Label":"Kanine",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guadacanal Diary",
    "Title":"Always Saturday",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"7 Inch",
    "UPC (Barcode)":075596931670,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pelle Gudmundsun-Holmgreen; Kronos Quartet; Paul Hillier",
    "Title":"Kronos Plays Holmgreen",
    "Original Release Date":"10/28/2008",
    "Release Date":"10/28/2008",
    "Format":"Hybrid SACD",
    "UPC (Barcode)":747313154869,
    "Label":"Da Capo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guided By Voices",
    "Title":"English Little League",
    "Original Release Date":"4/30/2013",
    "Release Date":"4/30/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"GBV",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guided By Voices",
    "Title":"Class Clown Spots A UFO",
    "Original Release Date":"6/12/2012",
    "Release Date":"6/12/2012",
    "Format":"MP3",
    "UPC (Barcode)":655035112220,
    "Label":"GBV",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guided By Voices",
    "Title":"Let's Go Eat The Factory",
    "Original Release Date":"1/12/2012",
    "Release Date":"1/12/2012",
    "Format":"MP3",
    "UPC (Barcode)":655035081625,
    "Label":"GBV",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guided By Voices",
    "Title":"Isolation Drills",
    "Original Release Date":"4/3/2001",
    "Release Date":"4/3/2001",
    "Format":"CD",
    "UPC (Barcode)":016581216020,
    "Label":"TVT",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guided By Voices",
    "Title":"Mag Earwhig!",
    "Original Release Date":"5/20/1997",
    "Release Date":"5/20/1997",
    "Format":"MP3",
    "UPC (Barcode)":0744861024125,
    "Label":"Matador",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guided By Voices",
    "Title":"Propeller",
    "Original Release Date":"1992",
    "Release Date":"11/5/1996",
    "Format":"MP3",
    "UPC (Barcode)":753417004924,
    "Label":"Scat",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guided By Voices",
    "Title":"Alien Lanes",
    "Original Release Date":"4/4/1995",
    "Release Date":"4/4/1995",
    "Format":"MP3",
    "UPC (Barcode)":744861012320,
    "Label":"Matador",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guided By Voices",
    "Title":"Bee Thousand",
    "Original Release Date":"6/20/1994",
    "Release Date":"6/20/1994",
    "Format":"CD",
    "UPC (Barcode)":753417003521,
    "Label":"Scat",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guns N' Roses",
    "Title":"Chinese Democracy",
    "Original Release Date":"11/23/2008",
    "Release Date":"11/23/2008",
    "Format":"CD",
    "UPC (Barcode)":602517906075,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guns N' Roses",
    "Title":"Use Your Illusion II",
    "Original Release Date":"9/17/1991",
    "Release Date":"9/17/1991",
    "Format":"CD",
    "UPC (Barcode)":720642442029,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guns N' Roses",
    "Title":"Use Your Illusion I",
    "Original Release Date":"9/17/1991",
    "Release Date":"9/17/1991",
    "Format":"Cassette",
    "UPC (Barcode)":720642441541,
    "Label":"Geffen",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Guns N' Roses",
    "Title":"Use Your Illusion II",
    "Original Release Date":"9/17/1991",
    "Release Date":"9/17/1991",
    "Format":"Cassette",
    "UPC (Barcode)":720642442043,
    "Label":"Geffen",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Guns N' Roses",
    "Title":"Appetite For Destruction",
    "Original Release Date":"7/21/1987",
    "Release Date":"7/21/1987",
    "Format":"CD",
    "UPC (Barcode)":075992414821,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Guns N' Roses",
    "Title":"Appetite for Destruction",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"Cassette",
    "UPC (Barcode)":720642421147,
    "Label":"Geffen",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Hajime Chitose",
    "Title":"Occident",
    "Original Release Date":"8/4/2010",
    "Release Date":"8/4/2010",
    "Format":"CD",
    "UPC (Barcode)":4988010024635,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hajime Chitose",
    "Title":"Hotaruboshi",
    "Original Release Date":"7/2/2008",
    "Release Date":"7/2/2008",
    "Format":"CD",
    "UPC (Barcode)":4988010020224,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hajime Chitose",
    "Title":"Cassini",
    "Original Release Date":"7/16/2008",
    "Release Date":"7/16/2008",
    "Format":"CD",
    "UPC (Barcode)":4988010020187,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hajime Chitose",
    "Title":"Hanadairo",
    "Original Release Date":"5/10/2006",
    "Release Date":"5/10/2006",
    "Format":"CD",
    "UPC (Barcode)":4988010016982,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hajime Chitose",
    "Title":"Kataritsugukoto",
    "Original Release Date":"11/23/2005",
    "Release Date":"11/23/2005",
    "Format":"CD",
    "UPC (Barcode)":4988010015565,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hajime Chitose",
    "Title":"Fuyu no Hainumikaze",
    "Original Release Date":"8/4/2004",
    "Release Date":"8/4/2004",
    "Format":"CD",
    "UPC (Barcode)":4988010010980,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hajime Chitose",
    "Title":"Nomad Soul",
    "Original Release Date":"9/3/2003",
    "Release Date":"9/3/2003",
    "Format":"Hybrid SACD",
    "UPC (Barcode)":4988010006877,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hajime Chitose",
    "Title":"Hainumikaze",
    "Original Release Date":"7/10/2002",
    "Release Date":"7/10/2002",
    "Format":"CD",
    "UPC (Barcode)":4988010002268,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hajime Chitose",
    "Title":"Kotonoha",
    "Original Release Date":"8/1/2001",
    "Release Date":"8/1/2001",
    "Format":"CD",
    "UPC (Barcode)":4948722095996,
    "Label":"Office Augusta",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hajime Chitose",
    "Title":"Hajime Chitose",
    "Original Release Date":"3/10/2001",
    "Release Date":"3/10/2001",
    "Format":"CD",
    "UPC (Barcode)":4948722095989,
    "Label":"Office Augusta",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hamada Mari",
    "Title":"Tomorrow",
    "Original Release Date":"10/19/1991",
    "Release Date":"10/19/1991",
    "Format":"CD",
    "UPC (Barcode)":4988067003065,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Glen Hansard; Marketa Irglova",
    "Title":"The Swell Season",
    "Original Release Date":"8/22/2006",
    "Release Date":"8/22/2006",
    "Format":"MP3",
    "UPC (Barcode)":036172612529,
    "Label":"Overcoat",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Harbison",
    "Title":"Ulysses' Bow/Samuel Chapter",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Hard Bargain",
    "Original Release Date":"4/26/2011",
    "Release Date":"4/26/2011",
    "Format":"CD",
    "UPC (Barcode)":075597976786,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"All I Intended To Be",
    "Original Release Date":"6/10/2008",
    "Release Date":"6/10/2008",
    "Format":"CD",
    "UPC (Barcode)":075597992854,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Songbird: Rare Tracks and Forgotten Gems",
    "Original Release Date":"9/18/2007",
    "Release Date":"9/18/2007",
    "Format":"CD",
    "UPC (Barcode)":081227474423,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Quarter Moon in a Ten Cent Town",
    "Original Release Date":"1978",
    "Release Date":"2/24/2004",
    "Format":"CD",
    "UPC (Barcode)":081227811129,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Luxury Liner",
    "Original Release Date":"1976",
    "Release Date":"2/24/2004",
    "Format":"CD",
    "UPC (Barcode)":081227811020,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Luxury Liner",
    "Original Release Date":"1976",
    "Release Date":"2/24/2004",
    "Format":"Stream",
    "UPC (Barcode)":081227811068,
    "Label":"Rhino",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Quarter Moon in a Ten Cent Town",
    "Original Release Date":"1978",
    "Release Date":"2/24/2004",
    "Format":"Stream",
    "UPC (Barcode)":0081227811129,
    "Label":"Rhino",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Stumble Into Grace",
    "Original Release Date":"9/23/2003",
    "Release Date":"9/23/2003",
    "Format":"CD",
    "UPC (Barcode)":075597980523,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Red Dirt Girl",
    "Original Release Date":"9/12/2000",
    "Release Date":"9/12/2000",
    "Format":"CD",
    "UPC (Barcode)":075597961621,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Spyboy",
    "Original Release Date":"8/11/1998",
    "Release Date":"8/11/1998",
    "Format":"CD",
    "UPC (Barcode)":063292500124,
    "Label":"Eminent",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Portraits",
    "Original Release Date":"10/8/1996",
    "Release Date":"10/8/1996",
    "Format":"CD",
    "UPC (Barcode)":093624530824,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Building The Wrecking Ball",
    "Original Release Date":"1995",
    "Release Date":"1995",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Asylum",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Wrecking Ball",
    "Original Release Date":"9/26/1995",
    "Release Date":"9/26/1995",
    "Format":"CD",
    "UPC (Barcode)":075596185424,
    "Label":"Asylum",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Pieces of the Sky",
    "Original Release Date":"1975",
    "Release Date":"1/5/1990",
    "Format":"CD",
    "UPC (Barcode)":075992724425,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Bluebird",
    "Original Release Date":"1/10/1989",
    "Release Date":"1/10/1989",
    "Format":"CD",
    "UPC (Barcode)":075992577625,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Thirteen",
    "Original Release Date":"September 1986",
    "Release Date":"September 1986",
    "Format":"LP",
    "UPC (Barcode)":075992535212,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"The Ballad Of Sally Rose",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"LP",
    "UPC (Barcode)":075992520515,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"White Shoes",
    "Original Release Date":"1983",
    "Release Date":"1983",
    "Format":"LP",
    "UPC (Barcode)":075992396110,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Last Date",
    "Original Release Date":"1982",
    "Release Date":"1982",
    "Format":"LP",
    "UPC (Barcode)":075992374019,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Cimarron",
    "Original Release Date":"6/15/1981",
    "Release Date":"6/15/1981",
    "Format":"LP",
    "UPC (Barcode)":075992360319,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Evangeline",
    "Original Release Date":"April 1981",
    "Release Date":"April 1981",
    "Format":"LP",
    "UPC (Barcode)":075992350815,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Roses In The Snow",
    "Original Release Date":"May 1980",
    "Release Date":"May 1980",
    "Format":"MP3",
    "UPC (Barcode)":075992342223,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Gliding Bird",
    "Original Release Date":"1979",
    "Release Date":"1979",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Emus",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Luxury Liner",
    "Original Release Date":"1976",
    "Release Date":"1976",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris",
    "Title":"Pieces of the Sky",
    "Original Release Date":"1975",
    "Release Date":"1975",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Emmylou Harris; Rodney Crowell",
    "Title":"Old Yellow Moon",
    "Original Release Date":"2/26/2013",
    "Release Date":"2/26/2013",
    "Format":"CD",
    "UPC (Barcode)":075597959994,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Lou Harrison",
    "Title":"Music Of Lou Harrison",
    "Original Release Date":"1991",
    "Release Date":"6/10/1994",
    "Format":"CD",
    "UPC (Barcode)":090438061322,
    "Label":"CRI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"PJ Harvey",
    "Title":"Stories From The City, Stories From The Sea",
    "Original Release Date":"10/31/2000",
    "Release Date":"10/31/2000",
    "Format":"CD",
    "UPC (Barcode)":731454814423,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hatakeyama Miyuki",
    "Title":"CHRONICLE 2001-2009",
    "Original Release Date":"6/24/2009",
    "Release Date":"6/24/2009",
    "Format":"CD",
    "UPC (Barcode)":4988064462186,
    "Label":"rhythm zone",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hatakeyama Miyuki",
    "Title":"Reflection",
    "Original Release Date":"4/5/2006",
    "Release Date":"4/5/2006",
    "Format":"CD",
    "UPC (Barcode)":4988064453634,
    "Label":"Avex Trax",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Hatakeyama Miyuki",
    "Title":"Wild and Gentle",
    "Original Release Date":"8/6/2003",
    "Release Date":"8/6/2003",
    "Format":"CD",
    "UPC (Barcode)":4532518300830,
    "Label":"chordiary",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hatakeyama Miyuki",
    "Title":"Diving into your mind",
    "Original Release Date":"3/27/2002",
    "Release Date":"3/27/2002",
    "Format":"CD",
    "UPC (Barcode)":4532518300656,
    "Label":"chordiary",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hatakeyama Miyuki",
    "Title":"Fragile",
    "Original Release Date":"9/26/2002",
    "Release Date":"9/26/2002",
    "Format":"CD",
    "UPC (Barcode)":4532518300731,
    "Label":"chordiary",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hayashi Asuca",
    "Title":"Saki",
    "Original Release Date":"5/21/2003",
    "Release Date":"5/21/2003",
    "Format":"CD",
    "UPC (Barcode)":4988006184046,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Franz Josef Haydn",
    "Title":"Symphonies 92, 94 & 96",
    "Original Release Date":"4/1/2003",
    "Release Date":"4/1/2003",
    "Format":"CD",
    "UPC (Barcode)":696998728424,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Darren Hayes",
    "Title":"Secret Codes And Battleships",
    "Original Release Date":"10/25/2011",
    "Release Date":"10/25/2011",
    "Format":"MP3",
    "UPC (Barcode)":5099973022329,
    "Label":"Powdered Sugar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Darren Hayes",
    "Title":"This Delicate Thing We've Made",
    "Original Release Date":"8/21/2007",
    "Release Date":"8/21/2007",
    "Format":"CD",
    "UPC (Barcode)":634457191426,
    "Label":"Sugar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Darren Hayes",
    "Title":"The Tension and the Spark",
    "Original Release Date":"9/27/2004",
    "Release Date":"9/27/2004",
    "Format":"MP3",
    "UPC (Barcode)":827969210420,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Murray Head",
    "Title":"One Night in Bangkok",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Heart",
    "Title":"Tell It Like It Is",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Heart Bazaar",
    "Title":"Collector",
    "Original Release Date":"2/21/2001",
    "Release Date":"2/21/2001",
    "Format":"CD",
    "UPC (Barcode)":4988006171596,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Heart Bazaar",
    "Title":"Saihate",
    "Original Release Date":"4/25/2001",
    "Release Date":"4/25/2001",
    "Format":"CD",
    "UPC (Barcode)":4988006172791,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Heart Bazaar",
    "Title":"Digitalis",
    "Original Release Date":"9/20/2000",
    "Release Date":"9/20/2000",
    "Format":"CD",
    "UPC (Barcode)":4988006168398,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hem",
    "Title":"Departure And Farewell",
    "Original Release Date":"4/2/2013",
    "Release Date":"4/2/2013",
    "Format":"CD",
    "UPC (Barcode)":634457852026,
    "Label":"Waveland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hem",
    "Title":"Twelfth Night",
    "Original Release Date":"10/26/2009",
    "Release Date":"10/26/2009",
    "Format":"MP3",
    "UPC (Barcode)":067003087520,
    "Label":"Nettwerk",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hem",
    "Title":"Home Again, Home Again",
    "Original Release Date":"6/26/2007",
    "Release Date":"6/26/2007",
    "Format":"MP3",
    "UPC (Barcode)":067003641920,
    "Label":"Waveland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hem",
    "Title":"No Word From Tom",
    "Original Release Date":"2/7/2006",
    "Release Date":"2/7/2006",
    "Format":"CD",
    "UPC (Barcode)":067003047425,
    "Label":"Waveland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hem",
    "Title":"Funnel Cloud",
    "Original Release Date":"9/5/2006",
    "Release Date":"9/5/2006",
    "Format":"CD",
    "UPC (Barcode)":067003060523,
    "Label":"Waveland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hem",
    "Title":"Eveningland",
    "Original Release Date":"10/5/2004",
    "Release Date":"10/5/2004",
    "Format":"CD",
    "UPC (Barcode)":011661324022,
    "Label":"Waveland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hem",
    "Title":"I'm Talking with My Mouth EP",
    "Original Release Date":"9/9/2002",
    "Release Date":"9/9/2002",
    "Format":"MP3",
    "UPC (Barcode)":5033281004899,
    "Label":"Waveland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hem",
    "Title":"Rabbit Songs",
    "Original Release Date":"6/11/2002",
    "Release Date":"6/11/2002",
    "Format":"CD",
    "UPC (Barcode)":032862013126,
    "Label":"Waveland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Michael Hersch",
    "Title":"Symphonies Nos. 1 & 2 / Fracta / Arrache",
    "Original Release Date":"11/21/2006",
    "Release Date":"11/21/2006",
    "Format":"MP3",
    "UPC (Barcode)":636943928127,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hey Willpower",
    "Title":"P.D.A.",
    "Original Release Date":"1/22/2008",
    "Release Date":"1/22/2008",
    "Format":"CD",
    "UPC (Barcode)":656605493725,
    "Label":"Tomlab",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hey Willpower",
    "Title":"Dance EP",
    "Original Release Date":"5/1/2007",
    "Release Date":"5/1/2007",
    "Format":"MP3",
    "UPC (Barcode)":678277109223,
    "Label":"Cochon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Hidden Cameras",
    "Title":"Mississauga Goddam",
    "Original Release Date":"10/19/2004",
    "Release Date":"10/19/2004",
    "Format":"MP3",
    "UPC (Barcode)":060768323728,
    "Label":"Sanctuary",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Higurashi Aiha",
    "Title":"Platonic",
    "Original Release Date":"5/25/2005",
    "Release Date":"5/25/2005",
    "Format":"CD",
    "UPC (Barcode)":4582117983996,
    "Label":"Ki/oon",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Higurashi Aiha",
    "Title":"Born Beautiful",
    "Original Release Date":"1/7/2004",
    "Release Date":"1/7/2004",
    "Format":"CD",
    "UPC (Barcode)":4582117982302,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Lauryn Hill",
    "Title":"The Miseducation of Lauryn Hill",
    "Original Release Date":"8/25/1998",
    "Release Date":"8/25/1998",
    "Format":"CD",
    "UPC (Barcode)":074646903520,
    "Label":"Ruffhouse",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"James William Hindle",
    "Title":"Town Feeling",
    "Original Release Date":"6/28/2005",
    "Release Date":"6/28/2005",
    "Format":"MP3",
    "UPC (Barcode)":655037094920,
    "Label":"Badman",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"James William Hindle",
    "Title":"Town Feeling",
    "Original Release Date":"6/28/2005",
    "Release Date":"6/28/2005",
    "Format":"CD",
    "UPC (Barcode)":655037094920,
    "Label":"Badman",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Hiroshima",
    "Title":"East",
    "Original Release Date":"2/28/1989",
    "Release Date":"2/28/1989",
    "Format":"CD",
    "UPC (Barcode)":074644502220,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hiroshima",
    "Title":"East",
    "Original Release Date":"2/28/1989",
    "Release Date":"2/28/1989",
    "Format":"MP3",
    "UPC (Barcode)":074644502213,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hiroshima",
    "Title":"East",
    "Original Release Date":"2/28/1989",
    "Release Date":"2/28/1989",
    "Format":"LP",
    "UPC (Barcode)":074644502213,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hiroshima",
    "Title":"East",
    "Original Release Date":"2/28/1989",
    "Release Date":"2/28/1989",
    "Format":"Stream",
    "UPC (Barcode)":074644502213,
    "Label":"Epic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Hiroshima",
    "Title":"Go",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":074644067910,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hiroshima",
    "Title":"Go",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":074644067927,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hiroshima",
    "Title":"Another Place",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"MP3",
    "UPC (Barcode)":074643993814,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hiroshima",
    "Title":"Another Place",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"LP",
    "UPC (Barcode)":074643993814,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hiroshima",
    "Title":"Third Generation",
    "Original Release Date":"1983",
    "Release Date":"1983",
    "Format":"LP",
    "UPC (Barcode)":074643870818,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hiroshima",
    "Title":"Odori",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"LP",
    "UPC (Barcode)":078221834513,
    "Label":"Arista",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hitoto You",
    "Title":"Tsukitenshin",
    "Original Release Date":"12/18/2002",
    "Release Date":"12/18/2002",
    "Format":"CD",
    "UPC (Barcode)":4988001947325,
    "Label":"Nippon Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Hives",
    "Title":"Tyrannosaurus Hives",
    "Original Release Date":"7/20/2004",
    "Release Date":"7/20/2004",
    "Format":"MP3",
    "UPC (Barcode)":602498669907,
    "Label":"Interscope",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Hives",
    "Title":"Veni Vidi Vicious",
    "Original Release Date":"4/30/2002",
    "Release Date":"4/30/2002",
    "Format":"CD",
    "UPC (Barcode)":093624832720,
    "Label":"Burning Heart",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robin Holcomb",
    "Title":"John Brown's Body",
    "Original Release Date":"6/27/2006",
    "Release Date":"6/27/2006",
    "Format":"CD",
    "UPC (Barcode)":702397771623,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robin Holcomb",
    "Title":"The Big Time",
    "Original Release Date":"6/18/2002",
    "Release Date":"6/18/2002",
    "Format":"CD",
    "UPC (Barcode)":075597965322,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robin Holcomb",
    "Title":"Little Three",
    "Original Release Date":"3/26/1996",
    "Release Date":"3/26/1996",
    "Format":"CD",
    "UPC (Barcode)":075597936629,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robin Holcomb",
    "Title":"Rockabye",
    "Original Release Date":"9/1/1992",
    "Release Date":"9/1/1992",
    "Format":"CD",
    "UPC (Barcode)":075596128926,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robin Holcomb",
    "Title":"Robin Holcomb",
    "Original Release Date":"9/24/1990",
    "Release Date":"9/24/1990",
    "Format":"CD",
    "UPC (Barcode)":075596098328,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robin Holcomb",
    "Title":"Robin Holcomb",
    "Original Release Date":"9/24/1990",
    "Release Date":"9/24/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075596098342,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Robin Holcomb",
    "Title":"Larks, They Crazy",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Sound Aspects",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robin Holcomb; Wayne Horvitz",
    "Title":"Solos - Robin Holcomb/Wayne Horvitz",
    "Original Release Date":"12/14/2004",
    "Release Date":"12/14/2004",
    "Format":"CD",
    "UPC (Barcode)":774355155028,
    "Label":"Songlines",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robin Holcomb; Talking Pictures; Wayne Horvitz",
    "Title":"The Point Of It All",
    "Original Release Date":"9/14/2010",
    "Release Date":"9/14/2010",
    "Format":"CD",
    "UPC (Barcode)":774355158425,
    "Label":"Songlines",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robin Holcomb; Talking Pictures; Wayne Horvitz",
    "Title":"The Point Of It All",
    "Original Release Date":"9/14/2010",
    "Release Date":"9/14/2010",
    "Format":"MP3",
    "UPC (Barcode)":774355158425,
    "Label":"Songlines",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hoodoo Gurus",
    "Title":"Stoneage Romeos",
    "Original Release Date":"1986",
    "Release Date":"10/24/2006",
    "Format":"MP3",
    "UPC (Barcode)":881626903421,
    "Label":"Hoodoo Gurus",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hoodoo Gurus",
    "Title":"Kinky",
    "Original Release Date":"4/23/1991",
    "Release Date":"4/23/1991",
    "Format":"Cassette",
    "UPC (Barcode)":078635300949,
    "Label":"RCA",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Hoodoo Gurus",
    "Title":"Magnum Cum Louder",
    "Original Release Date":"6/28/1989",
    "Release Date":"6/28/1989",
    "Format":"CD",
    "UPC (Barcode)":078635978124,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hoodoo Gurus",
    "Title":"Magnum Cum Louder",
    "Original Release Date":"6/29/1989",
    "Release Date":"6/29/1989",
    "Format":"Cassette",
    "UPC (Barcode)":078635978148,
    "Label":"RCA",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Wayne Horvitz",
    "Title":"The President",
    "Original Release Date":"1987",
    "Release Date":"2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Periplum",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz",
    "Title":"Joe Hill: 16 Actions for Orchestra, Voices, and Soloist",
    "Original Release Date":"4/1/2008",
    "Release Date":"4/1/2008",
    "Format":"CD",
    "UPC (Barcode)":093228067221,
    "Label":"New World",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz",
    "Title":"Whispers, Hymns and a Murmur",
    "Original Release Date":"2/21/2006",
    "Release Date":"2/21/2006",
    "Format":"CD",
    "UPC (Barcode)":702397802129,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz",
    "Title":"Film Music 1998-2001",
    "Original Release Date":"9/24/2002",
    "Release Date":"9/24/2002",
    "Format":"CD",
    "UPC (Barcode)":702397751427,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz",
    "Title":"Otis Span & other compositions",
    "Original Release Date":"2001",
    "Release Date":"2001",
    "Format":"CD",
    "UPC (Barcode)":631016001028,
    "Label":"Periplum",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz",
    "Title":"American Bandstand",
    "Original Release Date":"2/15/2000",
    "Release Date":"2/15/2000",
    "Format":"CD",
    "UPC (Barcode)":774355152829,
    "Label":"Songlines",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz",
    "Title":"Monologue",
    "Original Release Date":"5/20/1997",
    "Release Date":"5/20/1997",
    "Format":"CD",
    "UPC (Barcode)":789507613325,
    "Label":"Cavity Search",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz",
    "Title":"This New Generation",
    "Original Release Date":"1987",
    "Release Date":"7/1/1991",
    "Format":"CD",
    "UPC (Barcode)":075596075923,
    "Label":"Musician",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz",
    "Title":"This New Generation",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":075596075916,
    "Label":"Musician",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz",
    "Title":"This New Generation",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"Cassette",
    "UPC (Barcode)":075596075947,
    "Label":"Musician",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Wayne Horvitz; 4+1 Ensemble",
    "Title":"From a Window",
    "Original Release Date":"8/24/2001",
    "Release Date":"8/24/2001",
    "Format":"CD",
    "UPC (Barcode)":4988044900806,
    "Label":"Avant",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; 4+1 Ensemble",
    "Title":"4 + 1 Ensemble",
    "Original Release Date":"9/1/1998",
    "Release Date":"9/1/1998",
    "Format":"CD",
    "UPC (Barcode)":750447322424,
    "Label":"Intuition",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Gravitas Quartet",
    "Title":"One Dance Alone",
    "Original Release Date":"4/8/2008",
    "Release Date":"4/8/2008",
    "Format":"CD",
    "UPC (Barcode)":774355157121,
    "Label":"Songlines",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Gravitas Quartet",
    "Title":"Way Out East",
    "Original Release Date":"6/13/2006",
    "Release Date":"6/13/2006",
    "Format":"CD",
    "UPC (Barcode)":774355155820,
    "Label":"Songlines",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Butch Morris; Robert Previte",
    "Title":"Todos Santos",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Sound Aspects",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Butch Morris; Robert Previte",
    "Title":"Nine Below Zero",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Sound Aspects",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Pigpen",
    "Title":"Daylight",
    "Original Release Date":"11/11/1997",
    "Release Date":"11/11/1997",
    "Format":"CD",
    "UPC (Barcode)":764483015424,
    "Label":"Tim/Kerr",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Pigpen",
    "Title":"Live In Poland",
    "Original Release Date":"5/7/1996",
    "Release Date":"5/7/1996",
    "Format":"CD",
    "UPC (Barcode)":789507612229,
    "Label":"Cavity Search",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Pigpen",
    "Title":"Miss Ann",
    "Original Release Date":"10/17/1995",
    "Release Date":"10/17/1995",
    "Format":"CD",
    "UPC (Barcode)":764483006927,
    "Label":"Tim/Kerr",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Pigpen",
    "Title":"Halfrack",
    "Original Release Date":"5/7/1993",
    "Release Date":"4/16/1995",
    "Format":"CD",
    "UPC (Barcode)":764483004725,
    "Label":"Tim/Kerr",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Pigpen",
    "Title":"V As In Victim",
    "Original Release Date":"8/20/1994",
    "Release Date":"8/20/1994",
    "Format":"CD",
    "UPC (Barcode)":4988044900271,
    "Label":"Avant",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; The President",
    "Title":"Miracle Mile",
    "Original Release Date":"3/10/1992",
    "Release Date":"3/10/1992",
    "Format":"CD",
    "UPC (Barcode)":075597927825,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; The President",
    "Title":"Miracle Mile",
    "Original Release Date":"3/10/1992",
    "Release Date":"3/10/1992",
    "Format":"Cassette",
    "UPC (Barcode)":075597927849,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Wayne Horvitz; The President",
    "Title":"Bring Yr Camera",
    "Original Release Date":"1989",
    "Release Date":"7/1/1991",
    "Format":"CD",
    "UPC (Barcode)":075596079921,
    "Label":"Musician",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; The President",
    "Title":"Bring Yr Camera",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"Cassette",
    "UPC (Barcode)":075596079945,
    "Label":"Musician",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Sweeter Than the Day",
    "Title":"A Walk in the Dark",
    "Original Release Date":"4/1/2008",
    "Release Date":"4/1/2008",
    "Format":"CD",
    "UPC (Barcode)":751937326823,
    "Label":"Wayne Horvitz",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Sweeter Than the Day",
    "Title":"Sweeter Than the Day",
    "Original Release Date":"3/19/2002",
    "Release Date":"3/19/2002",
    "Format":"CD",
    "UPC (Barcode)":774355153659,
    "Label":"Songlines",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Zony Mash",
    "Title":"Upper Egypt",
    "Original Release Date":"2/8/2000",
    "Release Date":"2/8/2000",
    "Format":"CD",
    "UPC (Barcode)":035828025928,
    "Label":"Knitting Factory",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Zony Mash",
    "Title":"Brand Spankin' New",
    "Original Release Date":"9/15/1998",
    "Release Date":"9/15/1998",
    "Format":"CD",
    "UPC (Barcode)":035828022323,
    "Label":"Knitting Factory",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Zony Mash",
    "Title":"Cold Spell",
    "Original Release Date":"2/18/1997",
    "Release Date":"2/18/1997",
    "Format":"CD",
    "UPC (Barcode)":035828020121,
    "Label":"Knitting Factory",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wayne Horvitz; Zony Mash + Horns",
    "Title":"Live At The Royal Room",
    "Original Release Date":"12/30/2012",
    "Release Date":"12/30/2012",
    "Format":"CD",
    "UPC (Barcode)":616892104544,
    "Label":"Wayne Horvitz",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hothouse Flowers",
    "Title":"I'm Sorry",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"7 Inch",
    "UPC (Barcode)":042288631774,
    "Label":"London",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Huang Ruo; International Contemporary Ensemble",
    "Title":"Chamber Concerto Cycle",
    "Original Release Date":"2/27/2007",
    "Release Date":"2/27/2007",
    "Format":"MP3",
    "UPC (Barcode)":636943932223,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hüsker Dü",
    "Title":"New Day Rising",
    "Original Release Date":"1985",
    "Release Date":"10/25/1990",
    "Format":"MP3",
    "UPC (Barcode)":018861003122,
    "Label":"SST",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hüsker Dü",
    "Title":"New Day Rising",
    "Original Release Date":"1985",
    "Release Date":"10/25/1990",
    "Format":"CD",
    "UPC (Barcode)":018861003122,
    "Label":"SST",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hüsker Dü",
    "Title":"Metal Circus",
    "Original Release Date":"1983",
    "Release Date":"10/25/1990",
    "Format":"MP3",
    "UPC (Barcode)":018861002026,
    "Label":"SST",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hüsker Dü",
    "Title":"Flip Your Wig",
    "Original Release Date":"10/25/1985",
    "Release Date":"10/25/1985",
    "Format":"MP3",
    "UPC (Barcode)":884385263201,
    "Label":"SST",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hüsker Dü",
    "Title":"Zen Arcade",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"SST",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hyakkei",
    "Title":"Standing Still in a Moving Scene",
    "Original Release Date":"6/7/2006",
    "Release Date":"6/7/2006",
    "Format":"CD",
    "UPC (Barcode)":4562147290332,
    "Label":"Human Highway",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Hyde",
    "Title":"Roentgen",
    "Original Release Date":"3/27/2002",
    "Release Date":"3/27/2002",
    "Format":"CD",
    "UPC (Barcode)":4582117980414,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nina Hynes",
    "Title":"Really, Really Do",
    "Original Release Date":"10/5/2007",
    "Release Date":"10/5/2007",
    "Format":"MP3",
    "UPC (Barcode)":880918059822,
    "Label":"Kitty-Yo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nina Hynes",
    "Title":"Mono Prix",
    "Original Release Date":"8/30/2002",
    "Release Date":"8/30/2002",
    "Format":"CD",
    "UPC (Barcode)":689232070786,
    "Label":"Reverb",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nina Hynes",
    "Title":"Staros",
    "Original Release Date":"8/30/2002",
    "Release Date":"8/30/2002",
    "Format":"CD",
    "UPC (Barcode)":689232071042,
    "Label":"Reverb",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nina Hynes",
    "Title":"Can I Sleep Now?",
    "Original Release Date":"2001",
    "Release Date":"2001",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Reverb",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nina Hynes",
    "Title":"Creation",
    "Original Release Date":"5/1/1999",
    "Release Date":"5/1/1999",
    "Format":"CD",
    "UPC (Barcode)":653837410025,
    "Label":"Reverb",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Icehouse",
    "Title":"Man of Colours",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":044114159216,
    "Label":"Chrysalis",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"i-dep",
    "Title":"Meeting Point",
    "Original Release Date":"7/28/2004",
    "Release Date":"7/28/2004",
    "Format":"CD",
    "UPC (Barcode)":4582144340106,
    "Label":"AZtribe",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Idle Warship",
    "Title":"Habits of the Heart",
    "Original Release Date":"11/1/2011",
    "Release Date":"11/1/2011",
    "Format":"MP3",
    "UPC (Barcode)":856594002032,
    "Label":"Blacksmith/Talibra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Idle Warship",
    "Title":"Black Snake Moan",
    "Original Release Date":"12/2/2008",
    "Release Date":"12/2/2008",
    "Format":"MP3",
    "UPC (Barcode)":859701204800,
    "Label":"Idle Warship",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Idle Warship",
    "Title":"Screamin'",
    "Original Release Date":"6/4/2008",
    "Release Date":"6/4/2008",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Blacksmith/Talibra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"James Iha",
    "Title":"Let It Come Down",
    "Original Release Date":"2/10/1998",
    "Release Date":"2/10/1998",
    "Format":"CD",
    "UPC (Barcode)":724384541125,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Leo Imai",
    "Title":"Laser Rain",
    "Original Release Date":"4/22/2009",
    "Release Date":"4/22/2009",
    "Format":"CD",
    "UPC (Barcode)":4988006219809,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Leo Imai",
    "Title":"Fix Neon",
    "Original Release Date":"2/27/2008",
    "Release Date":"2/27/2008",
    "Format":"CD",
    "UPC (Barcode)":4988006214798,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Leo Imai",
    "Title":"Metro",
    "Original Release Date":"1/30/2008",
    "Release Date":"1/30/2008",
    "Format":"CD",
    "UPC (Barcode)":4988006214347,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Leo Imai",
    "Title":"Blue Technique",
    "Original Release Date":"11/27/2007",
    "Release Date":"11/27/2007",
    "Format":"CD",
    "UPC (Barcode)":4988006213319,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Leo Imai",
    "Title":"CITY FOLK",
    "Original Release Date":"9/20/2006",
    "Release Date":"9/20/2006",
    "Format":"CD",
    "UPC (Barcode)":4562199520098,
    "Label":"UuTwo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"In Tua Nua",
    "Title":"Vaudeville (Remastered)",
    "Original Release Date":"1986",
    "Release Date":"5/7/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Car W.S.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"In Tua Nua",
    "Title":"The Long Acre (Remastered)",
    "Original Release Date":"1987",
    "Release Date":"5/7/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Car W.S.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"In Tua Nua",
    "Title":"When the Night Came Down on Sunset",
    "Original Release Date":"9/18/2007",
    "Release Date":"9/18/2007",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"In Tua Nua",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"In Tua Nua",
    "Title":"The Long Acre",
    "Original Release Date":"1988",
    "Release Date":"5/1/1992",
    "Format":"CD",
    "UPC (Barcode)":5012981252625,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"In Tua Nua",
    "Title":"The Long Acre",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"Cassette",
    "UPC (Barcode)":075679094841,
    "Label":"Virgin",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"In Tua Nua",
    "Title":"The Long Acre",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":5012981252618,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"In Tua Nua",
    "Title":"The Long Acre",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":075679094810,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"In Tua Nua",
    "Title":"Vaudeville",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":5012981242121,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Information Society",
    "Title":"Information Society",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":075992569118,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Information Society",
    "Title":"Information Society",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"CD",
    "UPC (Barcode)":075992569125,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jack Ingram",
    "Title":"Electric",
    "Original Release Date":"6/4/2002",
    "Release Date":"6/4/2002",
    "Format":"CD",
    "UPC (Barcode)":696998593022,
    "Label":"Lucky Dog",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jack Ingram",
    "Title":"Livin' Or Dyin'",
    "Original Release Date":"3/25/1997",
    "Release Date":"3/25/1997",
    "Format":"CD",
    "UPC (Barcode)":601215304626,
    "Label":"Rising Tide",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Innocence Mission",
    "Title":"The Innocence Mission",
    "Original Release Date":"8/25/1989",
    "Release Date":"8/25/1989",
    "Format":"Cassette",
    "UPC (Barcode)":075021527447,
    "Label":"A&M",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"INXS",
    "Title":"What You Need",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"itsnotyouitsme",
    "Title":"Everybody's Pain Is Magnificent",
    "Original Release Date":"9/27/2011",
    "Release Date":"9/27/2011",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"New Amsterdam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Charles Ives; Samuel Barber; Emerson String Quartet",
    "Title":"American Originals",
    "Original Release Date":"1/19/1993",
    "Release Date":"1/19/1993",
    "Format":"CD",
    "UPC (Barcode)":028943586429,
    "Label":"Deutsche Grammophon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Charles Ives; Samuel Barber; Emerson String Quartet",
    "Title":"American Originals",
    "Original Release Date":"1/19/1993",
    "Release Date":"1/19/1993",
    "Format":"MP3",
    "UPC (Barcode)":028943586429,
    "Label":"Deutsche Grammophon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Charles Ives; Concord String Quartet",
    "Title":"String Quartets Nos. 1 and 2",
    "Original Release Date":"1975",
    "Release Date":"1975",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Janet Jackson",
    "Title":"Discipline",
    "Original Release Date":"2/26/2008",
    "Release Date":"2/26/2008",
    "Format":"CD",
    "UPC (Barcode)":602517613553,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Janet Jackson",
    "Title":"Design Of A Decade 1986-1996",
    "Original Release Date":"10/10/1995",
    "Release Date":"10/10/1995",
    "Format":"CD",
    "UPC (Barcode)":731454039925,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Janet Jackson",
    "Title":"Janet Jackson's Rhythm Nation 1814",
    "Original Release Date":"8/24/1989",
    "Release Date":"8/24/1989",
    "Format":"LP",
    "UPC (Barcode)":075021392021,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Janet Jackson",
    "Title":"Janet Jackson's Rhythm Nation 1814",
    "Original Release Date":"8/24/1989",
    "Release Date":"8/24/1989",
    "Format":"CD",
    "UPC (Barcode)":075021392021,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Janet Jackson",
    "Title":"Rhythm Nation 1814",
    "Original Release Date":"9/15/1989",
    "Release Date":"9/15/1989",
    "Format":"Cassette",
    "UPC (Barcode)":075021392045,
    "Label":"A&M",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Janet Jackson",
    "Title":"Control",
    "Original Release Date":"1/26/1986",
    "Release Date":"1/26/1986",
    "Format":"CD",
    "UPC (Barcode)":075021390522,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Janet Jackson",
    "Title":"Control",
    "Original Release Date":"1/26/1986",
    "Release Date":"1/26/1986",
    "Format":"LP",
    "UPC (Barcode)":075021390515,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Janet Jackson",
    "Title":"When I Think of You",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jane's Addiction",
    "Title":"Ritual De Lo Habitual",
    "Original Release Date":"8/13/1990",
    "Release Date":"8/13/1990",
    "Format":"CD",
    "UPC (Barcode)":075992599320,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Joan Jeanrenaud",
    "Title":"Strange Toys",
    "Original Release Date":"6/24/2008",
    "Release Date":"6/24/2008",
    "Format":"CD",
    "UPC (Barcode)":827912076752,
    "Label":"Talking House",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Joan Jeanrenaud",
    "Title":"Metamorphosis",
    "Original Release Date":"11/12/2002",
    "Release Date":"11/12/2002",
    "Format":"CD",
    "UPC (Barcode)":022551012028,
    "Label":"New Albion",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Joan Jeanrenaud; PC Muñoz",
    "Title":"Pop-Pop",
    "Original Release Date":"8/24/2010",
    "Release Date":"8/24/2010",
    "Format":"CD",
    "UPC (Barcode)":0610074243854,
    "Label":"Deconet",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Joan Jeanrenaud; PC Muñoz",
    "Title":"Pop-Pop",
    "Original Release Date":"8/24/2010",
    "Release Date":"8/24/2010",
    "Format":"MP3",
    "UPC (Barcode)":0610074243854,
    "Label":"Deconet",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Jets",
    "Title":"Crush on You",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Jets",
    "Title":"Private Number",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Billy Joel",
    "Title":"Storm Front",
    "Original Release Date":"9/29/1989",
    "Release Date":"9/29/1989",
    "Format":"Cassette",
    "UPC (Barcode)":074644436648,
    "Label":"Columbia",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"John & Mary",
    "Title":"The Pinwheel Galaxy",
    "Original Release Date":"7/15/2003",
    "Release Date":"7/15/2003",
    "Format":"CD",
    "UPC (Barcode)":692863038223,
    "Label":"Pinwheel Galaxy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John & Mary",
    "Title":"Victory Gardens",
    "Original Release Date":"1991",
    "Release Date":"1991",
    "Format":"Cassette",
    "UPC (Barcode)":014431020346,
    "Label":"Rykodisc",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"John Legend",
    "Title":"Get Lifted",
    "Original Release Date":"12/20/2005",
    "Release Date":"12/20/2005",
    "Format":"CD",
    "UPC (Barcode)":828767618166,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Johnny Hates Jazz",
    "Title":"Magnetized",
    "Original Release Date":"5/26/2013",
    "Release Date":"5/26/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Interaction",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Johnny Hates Jazz",
    "Title":"Turn Back The Clock",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":075679086013,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Johnny Hates Jazz",
    "Title":"Turn Back The Clock",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":075679086020,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Scott Johnson",
    "Title":"John Somebody",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"Cassette",
    "UPC (Barcode)":075597913347,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Freedy Johnston",
    "Title":"This Perfect World",
    "Original Release Date":"6/28/1994",
    "Release Date":"6/28/1994",
    "Format":"CD",
    "UPC (Barcode)":075596165525,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jónsi",
    "Title":"Go",
    "Original Release Date":"4/6/2010",
    "Release Date":"4/6/2010",
    "Format":"CD",
    "UPC (Barcode)":5099962617826,
    "Label":"XL",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Adam Joseph",
    "Title":"How I Seem to Be",
    "Original Release Date":"12/2/2003",
    "Release Date":"12/2/2003",
    "Format":"MP3",
    "UPC (Barcode)":022000155863,
    "Label":"Jah",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Juanes",
    "Title":"Un dia normal",
    "Original Release Date":"5/21/2002",
    "Release Date":"5/21/2002",
    "Format":"CD",
    "UPC (Barcode)":044001753220,
    "Label":"Surco",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Juanes",
    "Title":"Fijate Bien",
    "Original Release Date":"10/17/2000",
    "Release Date":"10/17/2000",
    "Format":"CD",
    "UPC (Barcode)":601215956320,
    "Label":"Surco",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Damien Jurado And Gathered In Song",
    "Title":"I Break Chairs",
    "Original Release Date":"2/19/2002",
    "Release Date":"2/19/2002",
    "Format":"CD",
    "UPC (Barcode)":098787057126,
    "Label":"Sub Pop",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"K.U.K.L.",
    "Title":"The Eye",
    "Original Release Date":"September 1984",
    "Release Date":"4/20/2004",
    "Format":"MP3",
    "UPC (Barcode)":501695804228,
    "Label":"One Little Indian",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"KAREN",
    "Title":"sunday girl in silence",
    "Original Release Date":"8/26/2009",
    "Release Date":"8/26/2009",
    "Format":"CD",
    "UPC (Barcode)":4514306010039,
    "Label":"Daizawa",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"KAREN",
    "Title":"MAGGOT IN TEARS",
    "Original Release Date":"5/2/2008",
    "Release Date":"5/2/2008",
    "Format":"CD",
    "UPC (Barcode)":4514306009453,
    "Label":"Daizawa",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Richard Kastle",
    "Title":"Streetwise",
    "Original Release Date":"2/22/1991",
    "Release Date":"2/22/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075679162441,
    "Label":"Virgin",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Tommy Keene",
    "Title":"In the Late Bright",
    "Original Release Date":"2/17/2009",
    "Release Date":"2/17/2009",
    "Format":"MP3",
    "UPC (Barcode)":634457506121,
    "Label":"Second Motion",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tommy Keene",
    "Title":"Crashing the Ether",
    "Original Release Date":"4/4/2006",
    "Release Date":"4/4/2006",
    "Format":"MP3",
    "UPC (Barcode)":634457701021,
    "Label":"Eleven Thirty",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Aram Khachaturian",
    "Title":"Piano Concerto/Sonatina/Toccata",
    "Original Release Date":"11/23/2004",
    "Release Date":"11/23/2004",
    "Format":"MP3",
    "UPC (Barcode)":743625303728,
    "Label":"ASV Living Era",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kicell",
    "Title":"magic hour",
    "Original Release Date":"1/23/2008",
    "Release Date":"1/23/2008",
    "Format":"CD",
    "UPC (Barcode)":4543034014200,
    "Label":"Kabuka Rhythm",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Kicell",
    "Title":"Tabi",
    "Original Release Date":"5/21/2005",
    "Release Date":"5/21/2005",
    "Format":"CD",
    "UPC (Barcode)":4988002479023,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kicell",
    "Title":"Mado ni Chikyuu",
    "Original Release Date":"2/18/2004",
    "Release Date":"2/18/2004",
    "Format":"CD",
    "UPC (Barcode)":4988002456970,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kicell",
    "Title":"Kinmirai",
    "Original Release Date":"10/23/2002",
    "Release Date":"10/23/2002",
    "Format":"CD",
    "UPC (Barcode)":4988002438204,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kicell",
    "Title":"Yume",
    "Original Release Date":"6/20/2001",
    "Release Date":"6/20/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002416288,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Killer Kiwis",
    "Title":"The Right Hat is No Hat",
    "Original Release Date":"1993",
    "Release Date":"1993",
    "Format":"Cassette",
    "UPC (Barcode)":null,
    "Label":"",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Killers",
    "Title":"Sam's Town",
    "Original Release Date":"10/3/2006",
    "Release Date":"10/3/2006",
    "Format":"CD",
    "UPC (Barcode)":602517026759,
    "Label":"Island",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Killers",
    "Title":"Hot Fuss",
    "Original Release Date":"6/15/2004",
    "Release Date":"6/15/2004",
    "Format":"CD",
    "UPC (Barcode)":602498622773,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"KIMONOS",
    "Title":"Kimonos",
    "Original Release Date":"11/17/2010",
    "Release Date":"11/17/2010",
    "Format":"CD",
    "UPC (Barcode)":4988006225343,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Kiss Offs",
    "Title":"Rock Bottom",
    "Original Release Date":"2/27/2001",
    "Release Date":"2/27/2001",
    "Format":"MP3",
    "UPC (Barcode)":655035020921,
    "Label":"Peek-a-Boo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Kiss Offs",
    "Title":"Goodbye Private Life",
    "Original Release Date":"12/28/1999",
    "Release Date":"12/28/1999",
    "Format":"CD",
    "UPC (Barcode)":645185002014,
    "Label":"Peek-a-Boo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Klezmatics",
    "Title":"Wonder Wheel",
    "Original Release Date":"7/25/2006",
    "Release Date":"7/25/2006",
    "Format":"MP3",
    "UPC (Barcode)":857764001336,
    "Label":"Jewish Music Group",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Klezmatics",
    "Title":"Wonder Wheel",
    "Original Release Date":"7/25/2006",
    "Release Date":"7/25/2006",
    "Format":"CD",
    "UPC (Barcode)":857764001336,
    "Label":"Jewish Music Group",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Klezmatics",
    "Title":"Rise Up! Sheyt Oyf!",
    "Original Release Date":"5/13/2003",
    "Release Date":"5/13/2003",
    "Format":"CD",
    "UPC (Barcode)":011661319721,
    "Label":"Rounder",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Klezmatics",
    "Title":"Between Two Worlds",
    "Original Release Date":"1998",
    "Release Date":"1998",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Aviv",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Klezmatics",
    "Title":"Jews With Horns",
    "Original Release Date":"6/6/1995",
    "Release Date":"6/6/1995",
    "Format":"CD",
    "UPC (Barcode)":048248403221,
    "Label":"Green Linnet",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Klymaxx",
    "Title":"Meeting in the Ladies Room",
    "Original Release Date":"11/30/1983",
    "Release Date":"11/30/1983",
    "Format":"MP3",
    "UPC (Barcode)":076731106427,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jordan Knight",
    "Title":"Unfinished",
    "Original Release Date":"5/31/2011",
    "Release Date":"5/31/2011",
    "Format":"MP3",
    "UPC (Barcode)":099923214328,
    "Label":"EOne",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jordan Knight",
    "Title":"Jordan Knight",
    "Original Release Date":"5/25/1999",
    "Release Date":"5/25/1999",
    "Format":"CD",
    "UPC (Barcode)":606949032227,
    "Label":"Interscope",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mark Knopfler and Emmylou Harris",
    "Title":"All the Roadrunning",
    "Original Release Date":"4/25/2006",
    "Release Date":"4/25/2006",
    "Format":"CD",
    "UPC (Barcode)":093624415428,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Jennifer Koh; Reiko Uchida",
    "Title":"String Poetic",
    "Original Release Date":"4/8/2008",
    "Release Date":"4/8/2008",
    "Format":"MP3",
    "UPC (Barcode)":735131910323,
    "Label":"Cedille",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jennifer Koh; Reiko Uchida",
    "Title":"String Poetic",
    "Original Release Date":"4/8/2008",
    "Release Date":"4/8/2008",
    "Format":"CD",
    "UPC (Barcode)":735131910323,
    "Label":"Cedille",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kokeshi Doll",
    "Title":"Pill Korui",
    "Original Release Date":"3/25/2003",
    "Release Date":"3/25/2003",
    "Format":"CD",
    "UPC (Barcode)":4524505181003,
    "Label":"Chameleon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kokeshi Doll",
    "Title":"Kokeshi Doll",
    "Original Release Date":"1/25/2002",
    "Release Date":"1/25/2002",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Chameleon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Glenn Kotche",
    "Title":"Mobile",
    "Original Release Date":"3/7/2006",
    "Release Date":"3/7/2006",
    "Format":"CD",
    "UPC (Barcode)":075597992724,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kraftwerk",
    "Title":"Computer World",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"MP3",
    "UPC (Barcode)":07599235492,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Levi Kreis",
    "Title":"The Gospel According to Levi",
    "Original Release Date":"1/30/2007",
    "Release Date":"1/30/2007",
    "Format":"CD",
    "UPC (Barcode)":700261206677,
    "Label":"Levi Kreis/Goldenrod",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Levi Kreis",
    "Title":"One of the Ones",
    "Original Release Date":"6/27/2006",
    "Release Date":"6/27/2006",
    "Format":"CD",
    "UPC (Barcode)":783707229201,
    "Label":"Levi Kreis/Goldenrod",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Levi Kreis",
    "Title":"One of the Ones",
    "Original Release Date":"2005",
    "Release Date":"6/27/2006",
    "Format":"MP3",
    "UPC (Barcode)":783707229201,
    "Label":"Levi Kreis/Goldenrod",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gidon Kremer",
    "Title":"De Profundis",
    "Original Release Date":"9/14/2010",
    "Release Date":"9/14/2010",
    "Format":"CD",
    "UPC (Barcode)":075597996999,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gidon Kremer",
    "Title":"Silencio",
    "Original Release Date":"10/10/2000",
    "Release Date":"10/10/2000",
    "Format":"CD",
    "UPC (Barcode)":075597958225,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Music Of Vladimir Martynov",
    "Original Release Date":"1/17/2012",
    "Release Date":"1/17/2012",
    "Format":"CD",
    "UPC (Barcode)":075597962796,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Floodplain",
    "Original Release Date":"5/19/2009",
    "Release Date":"5/19/2009",
    "Format":"CD",
    "UPC (Barcode)":075597982886,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Kronos Quartet Plays Sigur Rós",
    "Original Release Date":"9/4/2007",
    "Release Date":"9/4/2007",
    "Format":"MP3",
    "UPC (Barcode)":075593074523,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Cadenza on the Night Plain",
    "Original Release Date":"1987",
    "Release Date":"1/31/2006",
    "Format":"CD",
    "UPC (Barcode)":031257150927,
    "Label":"Rykodisc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"El Sinaloense Remixes",
    "Original Release Date":"2002",
    "Release Date":"2002",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Nuevo",
    "Original Release Date":"4/9/2002",
    "Release Date":"4/9/2002",
    "Format":"CD",
    "UPC (Barcode)":075597964929,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Caravan",
    "Original Release Date":"4/18/2000",
    "Release Date":"4/18/2000",
    "Format":"CD",
    "UPC (Barcode)":075597949025,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"25 Years",
    "Original Release Date":"10/27/1998",
    "Release Date":"10/27/1998",
    "Format":"CD",
    "UPC (Barcode)":075597950427,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Early Music (Lachrymæ Antiquæ)",
    "Original Release Date":"9/16/1997",
    "Release Date":"9/16/1997",
    "Format":"CD",
    "UPC (Barcode)":075597945720,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"The Complete Landmark Sessions",
    "Original Release Date":"4/1/1997",
    "Release Date":"4/1/1997",
    "Format":"CD",
    "UPC (Barcode)":604123201124,
    "Label":"Landmark",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Howl, U.S.A.",
    "Original Release Date":"6/4/1996",
    "Release Date":"6/4/1996",
    "Format":"CD",
    "UPC (Barcode)":075597937220,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Released 1985-1995",
    "Original Release Date":"10/24/1995",
    "Release Date":"10/24/1995",
    "Format":"CD",
    "UPC (Barcode)":075597939422,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Night Prayers",
    "Original Release Date":"9/6/1994",
    "Release Date":"9/6/1994",
    "Format":"CD",
    "UPC (Barcode)":075597934625,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"In Formation",
    "Original Release Date":"12/17/1993",
    "Release Date":"12/17/1993",
    "Format":"MP3",
    "UPC (Barcode)":030911100919,
    "Label":"Reference",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"At the Grave of Richard Wagner",
    "Original Release Date":"9/28/1993",
    "Release Date":"9/28/1993",
    "Format":"CD",
    "UPC (Barcode)":075597931822,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Short Stories",
    "Original Release Date":"3/9/1993",
    "Release Date":"3/9/1993",
    "Format":"CD",
    "UPC (Barcode)":075597931020,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Pieces of Africa",
    "Original Release Date":"3/3/1992",
    "Release Date":"3/3/1992",
    "Format":"CD",
    "UPC (Barcode)":075597927528,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Pieces of Africa",
    "Original Release Date":"3/3/1992",
    "Release Date":"3/3/1992",
    "Format":"Cassette",
    "UPC (Barcode)":075597927542,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Black Angels",
    "Original Release Date":"6/21/1990",
    "Release Date":"6/21/1990",
    "Format":"CD",
    "UPC (Barcode)":075597924220,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Black Angels",
    "Original Release Date":"6/21/1990",
    "Release Date":"6/21/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075597924244,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Winter Was Hard",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":075597918113,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Winter Was Hard",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"CD",
    "UPC (Barcode)":075597918120,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Winter Was Hard",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"Cassette",
    "UPC (Barcode)":075597918144,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"White Man Sleeps",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":075597916317,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"White Man Sleeps",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":075597916324,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"White Man Sleeps",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"Cassette",
    "UPC (Barcode)":075597916348,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Purple Haze",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Music by Sculthorpe, Sallinen, Glass, Nancarrow, Hendrix",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"LP",
    "UPC (Barcode)":075597911114,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Music by Sculthorpe, Sallinen, Glass, Nancarrow, Hendrix",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"CD",
    "UPC (Barcode)":075597911121,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Music by Sculthorpe, Sallinen, Glass, Nancarrow, Hendrix",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"Cassette",
    "UPC (Barcode)":075597911145,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Kronos Quartet",
    "Title":"Monk Suite",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"LP",
    "UPC (Barcode)":025218150514,
    "Label":"Landmark",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet; Asha Bholse",
    "Title":"You've Stolen My Heart",
    "Original Release Date":"8/23/2005",
    "Release Date":"8/23/2005",
    "Format":"CD",
    "UPC (Barcode)":075597985627,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet; Franz Schubert Quartett; Wilanow Quartet",
    "Title":"Gloria Coates, Hartm Ut Pascher, Bronislaw K. Przybylski, Zibigniew Bargielski",
    "Original Release Date":"1995",
    "Release Date":"1995",
    "Format":"CD",
    "UPC (Barcode)":4020771945733,
    "Label":"ProViva",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet; Bob Ostertag",
    "Title":"All The Rage",
    "Original Release Date":"10/26/1993",
    "Release Date":"10/26/1993",
    "Format":"CD",
    "UPC (Barcode)":075597933222,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet; Kimmo Pohjonen; Samuli Kosminen",
    "Title":"Uniko",
    "Original Release Date":"3/1/2011",
    "Release Date":"3/1/2011",
    "Format":"CD",
    "UPC (Barcode)":0761195118528,
    "Label":"Ondine",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet; Kimmo Pohjonen; Samuli Kosminen",
    "Title":"Uniko",
    "Original Release Date":"3/1/2011",
    "Release Date":"3/1/2011",
    "Format":"MP3",
    "UPC (Barcode)":0761195118528,
    "Label":"Ondine",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet; Alim & Fargana Qasimov; Homayun Sakhi",
    "Title":"Rainbow",
    "Original Release Date":"3/30/2010",
    "Release Date":"3/30/2010",
    "Format":"CD",
    "UPC (Barcode)":093074052723,
    "Label":"Smitsonian Folkways",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet; Tan Dun",
    "Title":"Ghost Opera",
    "Original Release Date":"3/18/1997",
    "Release Date":"3/18/1997",
    "Format":"CD",
    "UPC (Barcode)":075597944525,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kronos Quartet; The Tiger Lillies",
    "Title":"The Gorey End",
    "Original Release Date":"9/16/2003",
    "Release Date":"9/16/2003",
    "Format":"MP3",
    "UPC (Barcode)":724355751324,
    "Label":"EMI Classics",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kuriyama Chiaki",
    "Title":"CIRCUS",
    "Original Release Date":"3/16/2011",
    "Release Date":"3/16/2011",
    "Format":"CD",
    "UPC (Barcode)":4562104047405,
    "Label":"DefStar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kuriyama Chiaki",
    "Title":"Ryuusei no Namida",
    "Original Release Date":"2/24/2010",
    "Release Date":"2/24/2010",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"DefStar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nick Lachey",
    "Title":"All in My Head",
    "Original Release Date":"9/1/2009",
    "Release Date":"9/1/2009",
    "Format":"MP3",
    "UPC (Barcode)":884977313239,
    "Label":"Jive",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nick Lachey",
    "Title":"Patience",
    "Original Release Date":"2008",
    "Release Date":"2008",
    "Format":"MP3",
    "UPC (Barcode)":888880946249,
    "Label":"Jive",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nick Lachey",
    "Title":"Ordinary Day",
    "Original Release Date":"12/18/2007",
    "Release Date":"12/18/2007",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Jive",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nick Lachey",
    "Title":"What's Left of Me",
    "Original Release Date":"8/8/2006",
    "Release Date":"8/8/2006",
    "Format":"CD",
    "UPC (Barcode)":828768486122,
    "Label":"Jive",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Lamya",
    "Title":"Learning From Falling",
    "Original Release Date":"7/30/2002",
    "Release Date":"7/30/2002",
    "Format":"CD",
    "UPC (Barcode)":808132003223,
    "Label":"J",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"David Lang",
    "Title":"The Little Match Girl Passion",
    "Original Release Date":"7/9/2009",
    "Release Date":"7/9/2009",
    "Format":"MP3",
    "UPC (Barcode)":093046749668,
    "Label":"Harmonia Mundi",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"k.d. lang",
    "Title":"Ingenue",
    "Original Release Date":"3/17/1992",
    "Release Date":"3/17/1992",
    "Format":"MP3",
    "UPC (Barcode)":075992684026,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"L'Arc~en~Ciel",
    "Title":"NEXUS 4 / SHINE",
    "Original Release Date":"8/27/2008",
    "Release Date":"8/27/2008",
    "Format":"CD",
    "UPC (Barcode)":4582117988755,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"L'Arc~en~Ciel",
    "Title":"NEXUS 4/SHINE",
    "Original Release Date":"8/27/2008",
    "Release Date":"8/27/2008",
    "Format":"Stream",
    "UPC (Barcode)":4582117988755,
    "Label":"Ki/oon",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"L'Arc~en~Ciel",
    "Title":"Smile",
    "Original Release Date":"6/29/2004",
    "Release Date":"6/29/2004",
    "Format":"MP3",
    "UPC (Barcode)":828915001024,
    "Label":"Tofu",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"L'Arc~en~Ciel",
    "Title":"Real",
    "Original Release Date":"8/30/2000",
    "Release Date":"8/30/2000",
    "Format":"CD",
    "UPC (Barcode)":4988009033303,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"L'Arc~en~Ciel",
    "Title":"ray",
    "Original Release Date":"7/1/1999",
    "Release Date":"7/1/1999",
    "Format":"CD",
    "UPC (Barcode)":4988009028309,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"L'Arc~en~Ciel",
    "Title":"True",
    "Original Release Date":"12/12/1996",
    "Release Date":"12/12/1996",
    "Format":"CD",
    "UPC (Barcode)":4716331818929,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"L'Arc~en~Ciel",
    "Title":"Heavenly",
    "Original Release Date":"9/1/1995",
    "Release Date":"9/1/1995",
    "Format":"CD",
    "UPC (Barcode)":4988009011400,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Last Exit",
    "Title":"Iron Path",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"Cassette",
    "UPC (Barcode)":075679101549,
    "Label":"Virgin",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Last Forever",
    "Title":"Trainfare Home",
    "Original Release Date":"8/15/2000",
    "Release Date":"8/15/2000",
    "Format":"MP3",
    "UPC (Barcode)":603497100361,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Last Forever",
    "Title":"Trainfare Home",
    "Original Release Date":"8/15/2000",
    "Release Date":"8/15/2000",
    "Format":"Stream",
    "UPC (Barcode)":603497100361,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"LCD Soundsystem",
    "Title":"This Is Happening",
    "Original Release Date":"5/18/2010",
    "Release Date":"5/18/2010",
    "Format":"CD",
    "UPC (Barcode)":5099930990326,
    "Label":"DFA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ernesto Lecuona",
    "Title":"The Ultimate Collection",
    "Original Release Date":"2/11/1997",
    "Release Date":"2/11/1997",
    "Format":"CD",
    "UPC (Barcode)":090266867127,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Left",
    "Title":"The man-eating window says you were killed while sleeping",
    "Original Release Date":"2001",
    "Release Date":"2001",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Forty-Four",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Annie Lennox",
    "Title":"Songs Of Mass Destruction",
    "Original Release Date":"10/2/2007",
    "Release Date":"10/2/2007",
    "Format":"CD",
    "UPC (Barcode)":886971526028,
    "Label":"Arista",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Annie Lennox",
    "Title":"Little Bird",
    "Original Release Date":"2/1/1993",
    "Release Date":"2/1/1993",
    "Format":"CD",
    "UPC (Barcode)":078221252225,
    "Label":"Arista",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Annie Lennox",
    "Title":"Diva",
    "Original Release Date":"5/12/1992",
    "Release Date":"5/12/1992",
    "Format":"MP3",
    "UPC (Barcode)":078221870429,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Huey Lewis & The News",
    "Title":"Fore!",
    "Original Release Date":"September 1986",
    "Release Date":"September 1986",
    "Format":"LP",
    "UPC (Barcode)":044114153412,
    "Label":"Chrysalis",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Huey Lewis & The News",
    "Title":"Sports",
    "Original Release Date":"October 1984",
    "Release Date":"October 1984",
    "Format":"LP",
    "UPC (Barcode)":044114141211,
    "Label":"Chrysalis",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Huey Lewis & The News",
    "Title":"If This Is It",
    "Original Release Date":"1983",
    "Release Date":"1983",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Chrysalis",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Huey Lewis & The News",
    "Title":"Picture This",
    "Original Release Date":"4/2/1982",
    "Release Date":"4/2/1982",
    "Format":"LP",
    "UPC (Barcode)":044114134015,
    "Label":"Chrysalis",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Stewart Lewis",
    "Title":"In Formation",
    "Original Release Date":"6/10/2008",
    "Release Date":"6/10/2008",
    "Format":"MP3",
    "UPC (Barcode)":121292028616,
    "Label":"here!Tunes",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"La Ley",
    "Title":"Uno",
    "Original Release Date":"2/22/2000",
    "Release Date":"2/22/2000",
    "Format":"CD",
    "UPC (Barcode)":685738159420,
    "Label":"WEA Latina",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ivri Lider",
    "Title":"Mishehu Paam",
    "Original Release Date":"8/3/2012",
    "Release Date":"8/3/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Helicon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ivri Lider",
    "Title":"Beketzev A'hid Batnu'ot Shell Haguf (The Steady Rhythm Of Body Movements)",
    "Original Release Date":"9/16/2008",
    "Release Date":"9/16/2008",
    "Format":"MP3",
    "UPC (Barcode)":884385045357,
    "Label":"Helicon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ivri Lider",
    "Title":"Yoter Tov Klum Me'kimat",
    "Original Release Date":"1999",
    "Release Date":"11/22/2005",
    "Format":"MP3",
    "UPC (Barcode)":669910547052,
    "Label":"Helicon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ivri Lider",
    "Title":"Melatef V'meshaker",
    "Original Release Date":"1997",
    "Release Date":"11/22/2005",
    "Format":"MP3",
    "UPC (Barcode)":669910546956,
    "Label":"Helicon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ivri Lider",
    "Title":"Ze Lo Oto Davar",
    "Original Release Date":"5/12/2005",
    "Release Date":"5/12/2005",
    "Format":"MP3",
    "UPC (Barcode)":669910056066,
    "Label":"Helicon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ivri Lider",
    "Title":"Ha'anashim Ha'chadashim",
    "Original Release Date":"2002",
    "Release Date":"11/22/2005",
    "Format":"MP3",
    "UPC (Barcode)":669910547151,
    "Label":"Helicon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Peter Lieberson; Lorraine Hunt-Lieberson",
    "Title":"Lorraine Hunt Lieberson Sings Peter Lieberson: Neruda Songs",
    "Original Release Date":"12/19/2006",
    "Release Date":"12/19/2006",
    "Format":"MP3",
    "UPC (Barcode)":075597995428,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Peter Lieberson; Lorraine Hunt-Lieberson",
    "Title":"Lorraine Hunt Lieberson sings Peter Lieberson: Neruda Songs",
    "Original Release Date":"12/19/2006",
    "Release Date":"12/19/2006",
    "Format":"CD",
    "UPC (Barcode)":075597995428,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"György Ligeti",
    "Title":"Ligeti Edition 7",
    "Original Release Date":"1/20/1998",
    "Release Date":"1/20/1998",
    "Format":"MP3",
    "UPC (Barcode)":0774646230922,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"György Ligeti",
    "Title":"Ligeti Edition 4: Vocal Works",
    "Original Release Date":"1/21/1997",
    "Release Date":"1/21/1997",
    "Format":"CD",
    "UPC (Barcode)":074646231128,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"György Ligeti; Arditti Quartet",
    "Title":"Ligeti Edition 1: String Quartets and Duets",
    "Original Release Date":"1/21/1997",
    "Release Date":"1/21/1997",
    "Format":"CD",
    "UPC (Barcode)":074646230626,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"György Ligeti; Idil Biret",
    "Title":"Études: Books I and II (1-14a)",
    "Original Release Date":"3/18/2003",
    "Release Date":"3/18/2003",
    "Format":"CD",
    "UPC (Barcode)":747313577729,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Lisa Lisa And Cult Jam",
    "Title":"Spanish Fly",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":074644047714,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Lisa Lisa And Cult Jam",
    "Title":"Lisa Lisa & Cult Jam with Full Force",
    "Original Release Date":"2/10/1985",
    "Release Date":"2/10/1985",
    "Format":"LP",
    "UPC (Barcode)":074644013511,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Lisa Lisa And Cult Jam",
    "Title":"Lisa Lisa & Cult Jam with Full Force",
    "Original Release Date":"2/10/1985",
    "Release Date":"2/10/1985",
    "Format":"Stream",
    "UPC (Barcode)":074644013511,
    "Label":"Columbia",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Living Colour",
    "Title":"Time's Up",
    "Original Release Date":"8/20/1990",
    "Release Date":"8/20/1990",
    "Format":"CD",
    "UPC (Barcode)":074644620221,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Living Colour",
    "Title":"Vivid",
    "Original Release Date":"5/3/1988",
    "Release Date":"5/3/1988",
    "Format":"LP",
    "UPC (Barcode)":074644409918,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Living Colour",
    "Title":"Vivid",
    "Original Release Date":"5/3/1988",
    "Release Date":"5/3/1988",
    "Format":"CD",
    "UPC (Barcode)":074644409925,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Andrew Lloyd Webber",
    "Title":"Requiem",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"CD",
    "UPC (Barcode)":077774714624,
    "Label":"Angel",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The London Chamber Orchestra",
    "Title":"Minimalist (LCO8)",
    "Original Release Date":"1990",
    "Release Date":"1990",
    "Format":"CD",
    "UPC (Barcode)":075679116826,
    "Label":"Virgin Classics",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The London Chamber Orchestra",
    "Title":"Under The Eye Of Heaven",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"CD",
    "UPC (Barcode)":07567908172,
    "Label":"Virgin Classics",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Longwave",
    "Title":"The Strangest Things",
    "Original Release Date":"3/18/2003",
    "Release Date":"3/18/2003",
    "Format":"MP3",
    "UPC (Barcode)":078636817927,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"lostage",
    "Title":"ECHOES",
    "Original Release Date":"7/11/2012",
    "Release Date":"7/11/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Throat",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"lostage",
    "Title":"CONTEXT",
    "Original Release Date":"8/3/2011",
    "Release Date":"8/3/2011",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Throat",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"lostage",
    "Title":"LOSTAGE",
    "Original Release Date":"6/2/2010",
    "Release Date":"6/2/2010",
    "Format":"MP3",
    "UPC (Barcode)":4580296180151,
    "Label":"Avocado",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Love & Money",
    "Title":"Hallelujah Man",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"7 Inch",
    "UPC (Barcode)":042287059678,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Love and Rockets",
    "Title":"Love and Rockets",
    "Original Release Date":"9/4/1989",
    "Release Date":"9/4/1989",
    "Format":"MP3",
    "UPC (Barcode)":078635971521,
    "Label":"Beggars Banquet",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Love Gods",
    "Title":"Hujja Hujja Fishla",
    "Original Release Date":"1993",
    "Release Date":"1993",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Playtpus",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"LOVE PSYCHEDELICO",
    "Title":"ABBOT KINNEY",
    "Original Release Date":"1/13/2010",
    "Release Date":"1/13/2010",
    "Format":"CD",
    "UPC (Barcode)":4988002588305,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"LOVE PSYCHEDELICO",
    "Title":"This Is Love Psychedelico",
    "Original Release Date":"5/20/2008",
    "Release Date":"5/20/2008",
    "Format":"CD",
    "UPC (Barcode)":081227993412,
    "Label":"Hacktone",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"LOVE PSYCHEDELICO",
    "Title":"Love Psychedelic Orchestra",
    "Original Release Date":"1/9/2002",
    "Release Date":"1/9/2002",
    "Format":"CD",
    "UPC (Barcode)":4988002424450,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"LOVE PSYCHEDELICO",
    "Title":"The Greatest Hits",
    "Original Release Date":"1/11/2001",
    "Release Date":"1/11/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002408641,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Low Line Caller",
    "Title":"Continuing Cities",
    "Original Release Date":"January 2007",
    "Release Date":"January 2007",
    "Format":"CD",
    "UPC (Barcode)":825479047826,
    "Label":"",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Luminous Orange",
    "Title":"Drop You Vivid Colours",
    "Original Release Date":"11/25/2002",
    "Release Date":"11/25/2002",
    "Format":"CD",
    "UPC (Barcode)":634479138638,
    "Label":"tonevendor(japan)",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Witold Lutoslawski; Kronos Quartet",
    "Title":"String Quartet",
    "Original Release Date":"1/16/1991",
    "Release Date":"1/16/1991",
    "Format":"CD",
    "UPC (Barcode)":075597925524,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Witold Lutoslawski; Kronos Quartet",
    "Title":"String Quartet",
    "Original Release Date":"1/22/1991",
    "Release Date":"1/22/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075597925548,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Liam Lynch",
    "Title":"Fake Songs",
    "Original Release Date":"4/8/2003",
    "Release Date":"4/8/2003",
    "Format":"CD",
    "UPC (Barcode)":724358374308,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Loretta Lynn",
    "Title":"Van Lear Rose",
    "Original Release Date":"4/27/2004",
    "Release Date":"4/27/2004",
    "Format":"CD",
    "UPC (Barcode)":602498189559,
    "Label":"Interscope",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yo-Yo Ma",
    "Title":"Silk Road Journey",
    "Original Release Date":"4/16/2002",
    "Release Date":"4/16/2002",
    "Format":"CD",
    "UPC (Barcode)":696998978225,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Macha",
    "Title":"Macha",
    "Original Release Date":"11/3/1998",
    "Release Date":"11/3/1998",
    "Format":"CD",
    "UPC (Barcode)":604978001726,
    "Label":"JetSet",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Talitha MacKenzie",
    "Title":"Spiorad",
    "Original Release Date":"9/24/1996",
    "Release Date":"9/24/1996",
    "Format":"CD",
    "UPC (Barcode)":016351780324,
    "Label":"Shanachie",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Talitha MacKenzie",
    "Title":"Solas",
    "Original Release Date":"4/5/1994",
    "Release Date":"4/5/1994",
    "Format":"CD",
    "UPC (Barcode)":016351798428,
    "Label":"Shanachie",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Mad Capsule Markets",
    "Title":"Osc-Dis (oscillator in distortion)",
    "Original Release Date":"9/18/2001",
    "Release Date":"9/18/2001",
    "Format":"CD",
    "UPC (Barcode)":660200206925,
    "Label":"Palm Pictures",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Mad Capsule Markets",
    "Title":"010",
    "Original Release Date":"7/11/2001",
    "Release Date":"7/11/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002417100,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"Hard Candy",
    "Original Release Date":"4/29/2008",
    "Release Date":"4/29/2008",
    "Format":"CD",
    "UPC (Barcode)":0093624988496,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"Confessions On A Dance Floor",
    "Original Release Date":"11/15/2005",
    "Release Date":"11/15/2005",
    "Format":"CD",
    "UPC (Barcode)":093624946021,
    "Label":"Maverick",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"GHV2 - Greatest Hits Volume 2",
    "Original Release Date":"11/13/2001",
    "Release Date":"11/13/2001",
    "Format":"CD",
    "UPC (Barcode)":093624800026,
    "Label":"Maverick",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"Music",
    "Original Release Date":"9/19/2000",
    "Release Date":"9/19/2000",
    "Format":"CD",
    "UPC (Barcode)":093624759829,
    "Label":"Maverick",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"Ray of Light",
    "Original Release Date":"3/3/1998",
    "Release Date":"3/3/1998",
    "Format":"CD",
    "UPC (Barcode)":093624684725,
    "Label":"Maverick",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"Something to Remember",
    "Original Release Date":"11/7/1995",
    "Release Date":"11/7/1995",
    "Format":"CD",
    "UPC (Barcode)":093624610021,
    "Label":"Maverick",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"Bedtime Stories",
    "Original Release Date":"10/25/1994",
    "Release Date":"10/25/1994",
    "Format":"CD",
    "UPC (Barcode)":093624576723,
    "Label":"Maverick",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"Bedtime Stories",
    "Original Release Date":"10/25/1994",
    "Release Date":"10/25/1994",
    "Format":"Cassette",
    "UPC (Barcode)":093624576747,
    "Label":"Maverick",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"Erotica",
    "Original Release Date":"10/20/1992",
    "Release Date":"10/20/1992",
    "Format":"CD",
    "UPC (Barcode)":093624503125,
    "Label":"Maverick",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"The Immaculate Collection",
    "Original Release Date":"1991",
    "Release Date":"1991",
    "Format":"Cassette",
    "UPC (Barcode)":075992644044,
    "Label":"Maverick",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"The Immaculate Collection",
    "Original Release Date":"12/8/1990",
    "Release Date":"12/8/1990",
    "Format":"CD",
    "UPC (Barcode)":075992644020,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"I'm Breathless",
    "Original Release Date":"5/10/1990",
    "Release Date":"5/10/1990",
    "Format":"CD",
    "UPC (Barcode)":075992620925,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"I'm Breathless",
    "Original Release Date":"5/11/1990",
    "Release Date":"5/11/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075992620949,
    "Label":"Sire",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"Like A Prayer",
    "Original Release Date":"3/21/1989",
    "Release Date":"3/21/1989",
    "Format":"LP",
    "UPC (Barcode)":075992584418,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"Like A Prayer",
    "Original Release Date":"3/21/1989",
    "Release Date":"3/21/1989",
    "Format":"CD",
    "UPC (Barcode)":075992584425,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"Like a Prayer",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"Cassette",
    "UPC (Barcode)":075992584449,
    "Label":"Sire",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Madonna",
    "Title":"La Isla Bonita",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"7 Inch",
    "UPC (Barcode)":075592842574,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Magnetic Fields",
    "Title":"Love At The Bottom Of The Sea",
    "Original Release Date":"3/6/2012",
    "Release Date":"3/6/2012",
    "Format":"MP3",
    "UPC (Barcode)":5034202028529,
    "Label":"Merge",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Magnetic Fields",
    "Title":"Realism",
    "Original Release Date":"1/26/2010",
    "Release Date":"1/26/2010",
    "Format":"MP3",
    "UPC (Barcode)":075597982190,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Magnetic Fields",
    "Title":"Distortion",
    "Original Release Date":"1/15/2008",
    "Release Date":"1/15/2008",
    "Format":"CD",
    "UPC (Barcode)":075597996548,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Manhattan Transfer",
    "Title":"Brasil",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":075678180316,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Manhattan Transfer",
    "Title":"The Best of the Manhattan Transfer",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"LP",
    "UPC (Barcode)":075678158216,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Manhattan Transfer",
    "Title":"Extensions",
    "Original Release Date":"10/31/1979",
    "Release Date":"10/31/1979",
    "Format":"CD",
    "UPC (Barcode)":075678156526,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Manhattan Transfer",
    "Title":"Extensions",
    "Original Release Date":"10/31/1979",
    "Release Date":"10/31/1979",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Marilyn Manson",
    "Title":"Antichrist Superstar",
    "Original Release Date":"10/8/1996",
    "Release Date":"10/8/1996",
    "Format":"CD",
    "UPC (Barcode)":606949008628,
    "Label":"Nothing",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Maserati",
    "Title":"Inventions for the New Season",
    "Original Release Date":"6/19/2007",
    "Release Date":"6/19/2007",
    "Format":"MP3",
    "UPC (Barcode)":656605312026,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"MASS OF THE FERMENTING DREGS",
    "Title":"World Is Yours",
    "Original Release Date":"1/21/2009",
    "Release Date":"1/21/2009",
    "Format":"CD",
    "UPC (Barcode)":4580296180076,
    "Label":"Avocado",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"MASS OF THE FERMENTING DREGS",
    "Title":"MASS OF THE FERMENTING DREGS",
    "Original Release Date":"1/16/2008",
    "Release Date":"1/16/2008",
    "Format":"CD",
    "UPC (Barcode)":4997184881555,
    "Label":"Avocado",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mazzy Star",
    "Title":"She Hangs Brightly",
    "Original Release Date":"11/12/1991",
    "Release Date":"11/12/1991",
    "Format":"CD",
    "UPC (Barcode)":077779650828,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mazzy Star",
    "Title":"She Hangs Brightly",
    "Original Release Date":"6/4/1991",
    "Release Date":"6/4/1991",
    "Format":"Cassette",
    "UPC (Barcode)":023138007741,
    "Label":"Rough Trade",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Paul McCartney",
    "Title":"Coming Up",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Audra McDonald",
    "Title":"Happy Songs",
    "Original Release Date":"9/17/2002",
    "Release Date":"9/17/2002",
    "Format":"CD",
    "UPC (Barcode)":075597964523,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Audra McDonald",
    "Title":"How Glory Goes",
    "Original Release Date":"2/29/2000",
    "Release Date":"2/29/2000",
    "Format":"CD",
    "UPC (Barcode)":075597958027,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sarah McLachlan",
    "Title":"Solace",
    "Original Release Date":"1/28/1992",
    "Release Date":"1/28/1992",
    "Format":"Cassette",
    "UPC (Barcode)":078221863148,
    "Label":"Arista",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Sarah McLachlan",
    "Title":"Touch",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"CD",
    "UPC (Barcode)":078221859424,
    "Label":"Arista",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sarah McLachlan",
    "Title":"Touch",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"Cassette",
    "UPC (Barcode)":078221859448,
    "Label":"Arista",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Mean Machine",
    "Title":"Cream",
    "Original Release Date":"11/14/2001",
    "Release Date":"11/14/2001",
    "Format":"CD",
    "UPC (Barcode)":4988010227029,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Meco",
    "Title":"Star Wars Theme/Cantina Band",
    "Original Release Date":"1977",
    "Release Date":"1977",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Millenium",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"meister",
    "Title":"I Met the Music",
    "Original Release Date":"11/26/2004",
    "Release Date":"11/26/2004",
    "Format":"CD",
    "UPC (Barcode)":4562104041359,
    "Label":"DefStar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Felix Mendelssohn",
    "Title":"Symphony No. 3 'Scottish' / Symphony No. 4 'Italian'",
    "Original Release Date":"1991",
    "Release Date":"2011",
    "Format":"CD",
    "UPC (Barcode)":825646739103,
    "Label":"Apex",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Felix Mendelssohn; Lincoln Center Chamber Music Society",
    "Title":"Sextet for Piano and Strings/String Octet in E-flat major",
    "Original Release Date":"11/26/2002",
    "Release Date":"11/26/2002",
    "Format":"MP3",
    "UPC (Barcode)":013491326627,
    "Label":"Delos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Natalie Merchant",
    "Title":"Leave Your Sleep",
    "Original Release Date":"5/18/2010",
    "Release Date":"5/18/2010",
    "Format":"CD",
    "UPC (Barcode)":075597980394,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tift Merritt",
    "Title":"Tambourine",
    "Original Release Date":"8/24/2004",
    "Release Date":"8/24/2004",
    "Format":"CD",
    "UPC (Barcode)":602498623374,
    "Label":"Lost Highway",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Olivier Messiaen; Amici Ensemble",
    "Title":"Quartet for the End of Time",
    "Original Release Date":"6/19/2001",
    "Release Date":"6/19/2001",
    "Format":"CD",
    "UPC (Barcode)":636943482421,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Metalchicks",
    "Title":"Gabba",
    "Original Release Date":"3/14/2007",
    "Release Date":"3/14/2007",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Metallica",
    "Title":"Metallica",
    "Original Release Date":"8/6/1991",
    "Release Date":"8/6/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075596111348,
    "Label":"Elektra",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Pat Metheny",
    "Title":"Tap: John Zorn's Book Of Angels, Vol. 20",
    "Original Release Date":"5/21/2013",
    "Release Date":"5/21/2013",
    "Format":"MP3",
    "UPC (Barcode)":702397830726,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"m-flo",
    "Title":"Astromantic",
    "Original Release Date":"5/26/2004",
    "Release Date":"5/26/2004",
    "Format":"CD",
    "UPC (Barcode)":4988064451234,
    "Label":"rhythm zone",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"m-flo",
    "Title":"Expo Expo",
    "Original Release Date":"3/28/2001",
    "Release Date":"3/28/2001",
    "Format":"CD",
    "UPC (Barcode)":4988064450213,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"m-flo",
    "Title":"Planet Shining",
    "Original Release Date":"2/23/2000",
    "Release Date":"2/23/2000",
    "Format":"CD",
    "UPC (Barcode)":4988064450060,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Midnight Oil",
    "Title":"Essential Oils",
    "Original Release Date":"11/2/2012",
    "Release Date":"11/2/2012",
    "Format":"CD",
    "UPC (Barcode)":0887254976325,
    "Label":"Sony Music Entertainment",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Midnight Oil",
    "Title":"Diesel And Dust",
    "Original Release Date":"2/15/1988",
    "Release Date":"5/6/2008",
    "Format":"CD",
    "UPC (Barcode)":886971827422,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Midnight Oil",
    "Title":"Redneck Wonderland",
    "Original Release Date":"11/3/1998",
    "Release Date":"11/3/1998",
    "Format":"CD",
    "UPC (Barcode)":074646968222,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Midnight Oil",
    "Title":"20.000 Watt R.S.L.",
    "Original Release Date":"11/4/1997",
    "Release Date":"11/4/1997",
    "Format":"CD",
    "UPC (Barcode)":074646884829,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Midnight Oil",
    "Title":"Breathe",
    "Original Release Date":"10/15/1996",
    "Release Date":"10/15/1996",
    "Format":"CD",
    "UPC (Barcode)":074646788226,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Midnight Oil",
    "Title":"Blue Sky Mining",
    "Original Release Date":"2/9/1990",
    "Release Date":"2/9/1990",
    "Format":"CD",
    "UPC (Barcode)":074644539820,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Midnight Oil",
    "Title":"Blue Sky Mining",
    "Original Release Date":"2/7/1990",
    "Release Date":"2/7/1990",
    "Format":"Cassette",
    "UPC (Barcode)":074644539844,
    "Label":"Columbia",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Midnight Oil",
    "Title":"Postcard Without a Place",
    "Original Release Date":"4/20/1990",
    "Release Date":"4/20/1990",
    "Format":"Cassette",
    "UPC (Barcode)":074644614541,
    "Label":"Columbia",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Midnight Oil",
    "Title":"Diesel And Dust",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":074644096712,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Midnight Oil",
    "Title":"Diesel and Dust",
    "Original Release Date":"1/25/1988",
    "Release Date":"1/25/1988",
    "Format":"CD",
    "UPC (Barcode)":074644096729,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Midnight Oil",
    "Title":"Red Sails In The Sunset",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"CD",
    "UPC (Barcode)":074643998727,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Midnight Oil",
    "Title":"Red Sails in the Sunset",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"Cassette",
    "UPC (Barcode)":074643998741,
    "Label":"Columbia",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Mikami Chisako",
    "Title":"Watashi wa Anata no Uchuu",
    "Original Release Date":"11/17/2004",
    "Release Date":"11/17/2004",
    "Format":"CD",
    "UPC (Barcode)":4988005375698,
    "Label":"Universal Japan",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Stephanie Mills",
    "Title":"Never Knew Love Like This Before",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"20th Century",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Minako",
    "Title":"Suck It Till Your Life Ends Mata wa Shine Made Sono Mama Yatte",
    "Original Release Date":"12/19/2001",
    "Release Date":"12/19/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002419647,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kylie Minogue",
    "Title":"Aphrodite",
    "Original Release Date":"7/6/2010",
    "Release Date":"7/6/2010",
    "Format":"CD",
    "UPC (Barcode)":5099964290324,
    "Label":"Astralwerks",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kylie Minogue",
    "Title":"X",
    "Original Release Date":"4/1/2008",
    "Release Date":"4/1/2008",
    "Format":"CD",
    "UPC (Barcode)":5099921299421,
    "Label":"Capitol/Astralwerks",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kylie Minogue",
    "Title":"Fever",
    "Original Release Date":"2/26/2002",
    "Release Date":"2/26/2002",
    "Format":"CD",
    "UPC (Barcode)":724353767020,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Minutemen",
    "Title":"Project: Mersh",
    "Original Release Date":"February 1985",
    "Release Date":"February 1985",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"SST",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mirah",
    "Title":"Advisory Committee",
    "Original Release Date":"3/19/2002",
    "Release Date":"3/19/2002",
    "Format":"MP3",
    "UPC (Barcode)":789856113521,
    "Label":"K.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Missile Girl Scoot",
    "Title":"Missile Girl Scoot",
    "Original Release Date":"2/26/2003",
    "Release Date":"2/26/2003",
    "Format":"CD",
    "UPC (Barcode)":4988006182202,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Missile Girl Scoot",
    "Title":"Wanderland",
    "Original Release Date":"3/7/2001",
    "Release Date":"3/7/2001",
    "Format":"CD",
    "UPC (Barcode)":4988006172425,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Missile Girl Scoot",
    "Title":"Fiesta!",
    "Original Release Date":"2/9/2000",
    "Release Date":"2/9/2000",
    "Format":"CD",
    "UPC (Barcode)":4988006165564,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Missile Girl Scoot",
    "Title":"Let's Go to the Beach + Yanny",
    "Original Release Date":"6/9/2000",
    "Release Date":"6/9/2000",
    "Format":"CD",
    "UPC (Barcode)":4948722037651,
    "Label":"Monsoon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Modern English",
    "Title":"After the Snow",
    "Original Release Date":"1982",
    "Release Date":"10/6/1992",
    "Format":"MP3",
    "UPC (Barcode)":093624508823,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Molotov",
    "Title":"Desde Rusia Con Amor",
    "Original Release Date":"5/15/2012",
    "Release Date":"5/15/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Universal Latino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Molotov",
    "Title":"Eternamiente",
    "Original Release Date":"10/16/2007",
    "Release Date":"10/16/2007",
    "Format":"CD",
    "UPC (Barcode)":602517423596,
    "Label":"Universal Latino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Molotov",
    "Title":"Con Todo Respeto",
    "Original Release Date":"10/26/2004",
    "Release Date":"10/26/2004",
    "Format":"CD",
    "UPC (Barcode)":602498250112,
    "Label":"Surco",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Molotov",
    "Title":"Dance and Dense Denso",
    "Original Release Date":"2/25/2003",
    "Release Date":"2/25/2003",
    "Format":"CD",
    "UPC (Barcode)":044006666129,
    "Label":"Surco",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Molotov",
    "Title":"¿Donde Jugaran Las Niñas?",
    "Original Release Date":"8/26/1997",
    "Release Date":"8/26/1997",
    "Format":"CD",
    "UPC (Barcode)":639374003128,
    "Label":"Surco",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Janelle Monáe",
    "Title":"The ArchAndroid",
    "Original Release Date":"5/18/2010",
    "Release Date":"5/18/2010",
    "Format":"CD",
    "UPC (Barcode)":075678989834,
    "Label":"Bad Boy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Janelle Monáe",
    "Title":"Metropolis: The Chase Suite",
    "Original Release Date":"8/12/2008",
    "Release Date":"8/12/2008",
    "Format":"CD",
    "UPC (Barcode)":075678993282,
    "Label":"Bad Boy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Janelle Monáe",
    "Title":"Metropolis: The Chase Suite",
    "Original Release Date":"8/12/2008",
    "Release Date":"8/12/2008",
    "Format":"MP3",
    "UPC (Barcode)":075678993282,
    "Label":"Bad Boy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Meredith Monk",
    "Title":"Book of Days",
    "Original Release Date":"1990",
    "Release Date":"4/26/1994",
    "Format":"CD",
    "UPC (Barcode)":781182139923,
    "Label":"ECM New Series",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Meredith Monk",
    "Title":"Facing North",
    "Original Release Date":"11/2/1992",
    "Release Date":"11/2/1992",
    "Format":"MP3",
    "UPC (Barcode)":028943743921,
    "Label":"ECM New Series",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Meredith Monk",
    "Title":"Book of Days",
    "Original Release Date":"5/22/1990",
    "Release Date":"5/22/1990",
    "Format":"Cassette",
    "UPC (Barcode)":042283962446,
    "Label":"ECM New Series",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"mono",
    "Title":"For My Parents",
    "Original Release Date":"9/4/2012",
    "Release Date":"9/4/2012",
    "Format":"CD",
    "UPC (Barcode)":656605321325,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"mono",
    "Title":"Holy Ground: NYC Live",
    "Original Release Date":"4/27/2010",
    "Release Date":"4/27/2010",
    "Format":"CD",
    "UPC (Barcode)":656605315928,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"mono",
    "Title":"Hymn to the Immortal Wind",
    "Original Release Date":"3/24/2009",
    "Release Date":"3/24/2009",
    "Format":"CD",
    "UPC (Barcode)":656605314822,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"mono",
    "Title":"Gone",
    "Original Release Date":"9/11/2007",
    "Release Date":"9/11/2007",
    "Format":"CD",
    "UPC (Barcode)":4562147290523,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"mono",
    "Title":"Gone",
    "Original Release Date":"9/11/2007",
    "Release Date":"9/11/2007",
    "Format":"MP3",
    "UPC (Barcode)":4562147290523,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"mono",
    "Title":"You Are There",
    "Original Release Date":"4/11/2006",
    "Release Date":"4/11/2006",
    "Format":"CD",
    "UPC (Barcode)":656605309828,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"mono",
    "Title":"New York Soundtracks",
    "Original Release Date":"2/18/2004",
    "Release Date":"2/18/2004",
    "Format":"CD",
    "UPC (Barcode)":4562147290042,
    "Label":"Human Highway",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"mono",
    "Title":"Walking Cloud And Deep Red Sky, Flag Fluttered And The Sun Shined",
    "Original Release Date":"10/5/2004",
    "Release Date":"10/5/2004",
    "Format":"CD",
    "UPC (Barcode)":656605306827,
    "Label":"Human Highway",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"mono",
    "Title":"One Step More And You Die",
    "Original Release Date":"4/8/2003",
    "Release Date":"4/8/2003",
    "Format":"CD",
    "UPC (Barcode)":639980003123,
    "Label":"Arena Rock",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"mono",
    "Title":"Under the Pipal Tree",
    "Original Release Date":"11/20/2001",
    "Release Date":"11/20/2001",
    "Format":"CD",
    "UPC (Barcode)":702397723721,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"mono",
    "Title":"Hey,You.EP",
    "Original Release Date":"9/8/2000",
    "Release Date":"9/8/2000",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Forty-Four",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"mono; World's End Girlfriend",
    "Title":"Palmless Prayer/Mass Murder Refrain",
    "Original Release Date":"9/12/2006",
    "Release Date":"9/12/2006",
    "Format":"MP3",
    "UPC (Barcode)":656605310824,
    "Label":"Temporary Residence",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"moools",
    "Title":"DUB NARCOTIC SESSION",
    "Original Release Date":"12/1/2006",
    "Release Date":"12/1/2006",
    "Format":"CD",
    "UPC (Barcode)":4582237810837,
    "Label":"moools entertainment",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Alanis Morissette",
    "Title":"Jagged Little Pill",
    "Original Release Date":"6/13/1995",
    "Release Date":"6/13/1995",
    "Format":"CD",
    "UPC (Barcode)":093624590125,
    "Label":"Maverick",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Morrissey",
    "Title":"Years of Refusal",
    "Original Release Date":"2/17/2009",
    "Release Date":"2/17/2009",
    "Format":"MP3",
    "UPC (Barcode)":028947814351,
    "Label":"Lost Highway",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Morrissey",
    "Title":"I'm Throwing My Arms Around Paris",
    "Original Release Date":"2/10/2009",
    "Release Date":"2/10/2009",
    "Format":"7 Inch",
    "UPC (Barcode)":028947815396,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Morrissey",
    "Title":"Ringleader Of The Tormentors",
    "Original Release Date":"4/4/2006",
    "Release Date":"4/4/2006",
    "Format":"MP3",
    "UPC (Barcode)":060768601420,
    "Label":"Sanctuary",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"MO'SOME TONEBENDER",
    "Title":"Best Of Worst",
    "Original Release Date":"4/6/2011",
    "Release Date":"4/6/2011",
    "Format":"CD",
    "UPC (Barcode)":4988001453109,
    "Label":"Nippon Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"MO'SOME TONEBENDER",
    "Title":"MO'SOME TONEBENDER",
    "Original Release Date":"3/19/2011",
    "Release Date":"3/19/2011",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"No Evil",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"MO'SOME TONEBENDER",
    "Title":"iTunes Live Session EP",
    "Original Release Date":"6/25/2008",
    "Release Date":"6/25/2008",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Nipponophone",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bob Mould",
    "Title":"Life and Times",
    "Original Release Date":"4/7/2009",
    "Release Date":"4/7/2009",
    "Format":"CD",
    "UPC (Barcode)":045778701421,
    "Label":"Anti",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bob Mould",
    "Title":"Life and Times",
    "Original Release Date":"4/7/2009",
    "Release Date":"4/7/2009",
    "Format":"Stream",
    "UPC (Barcode)":045778701421,
    "Label":"Anti",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Bob Mould",
    "Title":"District Line",
    "Original Release Date":"2/5/2008",
    "Release Date":"2/5/2008",
    "Format":"CD",
    "UPC (Barcode)":045778691029,
    "Label":"Anti",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bob Mould",
    "Title":"Body of Song",
    "Original Release Date":"7/26/2005",
    "Release Date":"7/26/2005",
    "Format":"CD",
    "UPC (Barcode)":634457209121,
    "Label":"Yep Roc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bob Mould",
    "Title":"See a Little Light",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"7 Inch",
    "UPC (Barcode)":075679919076,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mountain Con",
    "Title":"Sancho Panza",
    "Original Release Date":"4/11/2006",
    "Release Date":"4/11/2006",
    "Format":"MP3",
    "UPC (Barcode)":678277119529,
    "Label":"Hidden Peak",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wolfgang Amadeus Mozart",
    "Title":"Piano Concertos Nos. 20 & 23; 2 Rondos",
    "Original Release Date":"11/8/2005",
    "Release Date":"11/8/2005",
    "Format":"CD",
    "UPC (Barcode)":028947570509,
    "Label":"Deutsche Grammophon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wolfgang Amadeus Mozart",
    "Title":"Requiem (K.626); Exsultate, Jubilate (K.165, 158a)",
    "Original Release Date":"1/29/2002",
    "Release Date":"1/29/2002",
    "Format":"CD",
    "UPC (Barcode)":696998984929,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wolfgang Amadeus Mozart",
    "Title":"Gran Partita, K. 361/Divertimento in D major, K. 205",
    "Original Release Date":"10/5/2000",
    "Release Date":"10/5/2000",
    "Format":"MP3",
    "UPC (Barcode)":0730099422628,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wolfgang Amadeus Mozart",
    "Title":"Requiem in D Minor, K. 626 (New Completion by Robert Levin)",
    "Original Release Date":"8/29/1995",
    "Release Date":"8/29/1995",
    "Format":"CD",
    "UPC (Barcode)":089408041020,
    "Label":"Telarc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wolfgang Amadeus Mozart",
    "Title":"Symphonies Nos. 41, 25 & 32",
    "Original Release Date":"2/5/1993",
    "Release Date":"2/5/1993",
    "Format":"MP3",
    "UPC (Barcode)":730099511322,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wolfgang Amadeus Mozart",
    "Title":"Sinfonia Concertante",
    "Original Release Date":"7/12/1991",
    "Release Date":"7/12/1991",
    "Format":"Cassette",
    "UPC (Barcode)":028942139947,
    "Label":"Polygram",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Wolfgang Amadeus Mozart",
    "Title":"Symphonies Nos. 29, 35 'Haffner' and 40",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"CD",
    "UPC (Barcode)":028942048621,
    "Label":"Philips",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wolfgang Amadeus Mozart; Alfred Brendel; William Klien",
    "Title":"Concerto For Two Pianos In E Flat, No.10, K.365; Sonata For Two Pianos In D, K.448",
    "Original Release Date":"11/9/1990",
    "Release Date":"11/9/1990",
    "Format":"MP3",
    "UPC (Barcode)":889253409934,
    "Label":"Tuxedo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wolfgang Amadeus Mozart; Julia Fischer",
    "Title":"Sinfonia Concertante in E flat major, K. 364 / Rondo in C major, K. 373 / Concertone in C major, K. 190",
    "Original Release Date":"10/30/2007",
    "Release Date":"10/30/2007",
    "Format":"MP3",
    "UPC (Barcode)":827949009860,
    "Label":"Pentatone",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wolfgang Amadeus Mozart; Guarneri String Quartet",
    "Title":"String Quartets Nos. 18 & 19",
    "Original Release Date":"9/15/1992",
    "Release Date":"9/15/1992",
    "Format":"Cassette",
    "UPC (Barcode)":028943207621,
    "Label":"Philips",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Wolfgang Amadeus Mozart; John O'Connor",
    "Title":"Piano Concertos Nos. 20 & 22",
    "Original Release Date":"3/23/1993",
    "Release Date":"3/23/1993",
    "Format":"CD",
    "UPC (Barcode)":089408030826,
    "Label":"Telarc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wolfgang Amadeus Mozart; John O'Connor",
    "Title":"Piano Concertos Nos. 20 & 22",
    "Original Release Date":"3/23/1993",
    "Release Date":"3/23/1993",
    "Format":"MP3",
    "UPC (Barcode)":089408030826,
    "Label":"Telarc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mr. Bungle",
    "Title":"Mr. Bungle",
    "Original Release Date":"8/6/1991",
    "Release Date":"8/6/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075992664042,
    "Label":"Reprise",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Nico Muhly",
    "Title":"Drones",
    "Original Release Date":"11/20/2012",
    "Release Date":"11/20/2012",
    "Format":"CD",
    "UPC (Barcode)":880319596926,
    "Label":"Bedroom Community",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nico Muhly",
    "Title":"Seeing Is Believing",
    "Original Release Date":"6/21/2011",
    "Release Date":"6/21/2011",
    "Format":"CD",
    "UPC (Barcode)":028947827313,
    "Label":"Decca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nico Muhly",
    "Title":"I Drink The Air Before Me",
    "Original Release Date":"9/21/2010",
    "Release Date":"9/21/2010",
    "Format":"CD",
    "UPC (Barcode)":082947825708,
    "Label":"Decca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nico Muhly",
    "Title":"A Good Understanding (Los Angeles Master Chorale Feat. Conductor: Grant Gershon)",
    "Original Release Date":"9/21/2010",
    "Release Date":"9/21/2010",
    "Format":"CD",
    "UPC (Barcode)":028947825067,
    "Label":"Decca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nico Muhly",
    "Title":"Mothertongue",
    "Original Release Date":"7/22/2008",
    "Release Date":"7/22/2008",
    "Format":"MP3",
    "UPC (Barcode)":632662556825,
    "Label":"Brassland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nico Muhly",
    "Title":"Mothertongue",
    "Original Release Date":"7/22/2008",
    "Release Date":"7/22/2008",
    "Format":"CD",
    "UPC (Barcode)":632662556825,
    "Label":"Brassland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nico Muhly",
    "Title":"Speaks Volumes",
    "Original Release Date":"1/23/2007",
    "Release Date":"1/23/2007",
    "Format":"CD",
    "UPC (Barcode)":859700049594,
    "Label":"Bedroom Community",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nico Muhly; Bruce Brubaker",
    "Title":"Drones & Piano",
    "Original Release Date":"5/21/2012",
    "Release Date":"5/21/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Bedroom Community",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nico Muhly; Johnny Greenwood; Richard Reed Parry; Edwin Outwater",
    "Title":"From Here On Out",
    "Original Release Date":"9/20/2011",
    "Release Date":"9/20/2011",
    "Format":"CD",
    "UPC (Barcode)":774204999223,
    "Label":"Analekta",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nico Muhly; Johnny Greenwood; Richard Reed Parry; Edwin Outwater",
    "Title":"From Here On Out",
    "Original Release Date":"9/20/2011",
    "Release Date":"9/20/2011",
    "Format":"MP3",
    "UPC (Barcode)":774204999223,
    "Label":"Analekta",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mummy the Peepshow",
    "Title":"Electric Rollergirl",
    "Original Release Date":"10/27/2000",
    "Release Date":"10/27/2000",
    "Format":"CD",
    "UPC (Barcode)":4514306003253,
    "Label":"Benten",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mummy the Peepshow",
    "Title":"This is Egg Speaking...",
    "Original Release Date":"6/25/1999",
    "Release Date":"6/25/1999",
    "Format":"CD",
    "UPC (Barcode)":4514306001624,
    "Label":"Benten",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mummy the Peepshow",
    "Title":"Mummy Bullion",
    "Original Release Date":"2/25/1998",
    "Release Date":"2/25/1998",
    "Format":"CD",
    "UPC (Barcode)":4514306000771,
    "Label":"Benten",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Modest Petrovich Mussorgsky",
    "Title":"Pictures at an Exhibition",
    "Original Release Date":"",
    "Release Date":"",
    "Format":"Cassette",
    "UPC (Barcode)":null,
    "Label":"",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"My Bloody Valentine",
    "Title":"Loveless",
    "Original Release Date":"11/5/1991",
    "Release Date":"11/5/1991",
    "Format":"CD",
    "UPC (Barcode)":075992675925,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"My Bloody Valentine",
    "Title":"Loveless",
    "Original Release Date":"11/5/1991",
    "Release Date":"11/5/1991",
    "Format":"Stream",
    "UPC (Barcode)":075992675925,
    "Label":"Sire",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Mylab",
    "Title":"Mylab",
    "Original Release Date":"2/3/2004",
    "Release Date":"2/3/2004",
    "Format":"CD",
    "UPC (Barcode)":694205030124,
    "Label":"Terminus",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"N.E.R.D.",
    "Title":"In Search Of...(Version 2)",
    "Original Release Date":"3/12/2002",
    "Release Date":"3/12/2002",
    "Format":"CD",
    "UPC (Barcode)":724381152126,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"N.W.A.",
    "Title":"Straight Outta Compton",
    "Original Release Date":"1988",
    "Release Date":"9/23/2002",
    "Format":"CD",
    "UPC (Barcode)":724353793623,
    "Label":"Priority",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nagai Mariko",
    "Title":"Washing",
    "Original Release Date":"6/26/1991",
    "Release Date":"6/26/1991",
    "Format":"CD",
    "UPC (Barcode)":4988027002336,
    "Label":"Funhouse",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nakamori Akina",
    "Title":"Cruise",
    "Original Release Date":"7/25/1989",
    "Release Date":"7/17/1991",
    "Format":"CD",
    "UPC (Barcode)":4988014704281,
    "Label":"Warner-Pioneer",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nakamori Akina",
    "Title":"Cross My Palm",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"Cassette",
    "UPC (Barcode)":075678203749,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NANANINE",
    "Title":"9 Tone",
    "Original Release Date":"2/8/2002",
    "Release Date":"2/8/2002",
    "Format":"CD",
    "UPC (Barcode)":4514306004861,
    "Label":"High Line",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NANANINE",
    "Title":"Shaff-rhythm",
    "Original Release Date":"1/26/2001",
    "Release Date":"1/26/2001",
    "Format":"CD",
    "UPC (Barcode)":4514306003697,
    "Label":"High Line",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NANANINE",
    "Title":"Middle Tempo",
    "Original Release Date":"9/21/2001",
    "Release Date":"9/21/2001",
    "Format":"CD",
    "UPC (Barcode)":4514306004496,
    "Label":"High Line",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Richard Nanes",
    "Title":"Symphonies Nos. 1 & 2",
    "Original Release Date":"",
    "Release Date":"1987",
    "Format":"Cassette",
    "UPC (Barcode)":093477121125,
    "Label":"Delfon",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Nena",
    "Title":"It's All In The Game",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"LP",
    "UPC (Barcode)":074644014419,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nena",
    "Title":"99 Luftballons",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"LP",
    "UPC (Barcode)":074643929417,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nena",
    "Title":"99 Red Balloons",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"7 Inch",
    "UPC (Barcode)":074640410871,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nena",
    "Title":"99 Luftballons",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"Cassette",
    "UPC (Barcode)":074643929448,
    "Label":"Epic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Nena",
    "Title":"99 Luftballons",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"Stream",
    "UPC (Barcode)":074643929417,
    "Label":"Epic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Carol Nethen",
    "Title":"The View from the Bridge",
    "Original Release Date":"1/10/1990",
    "Release Date":"1/10/1990",
    "Format":"Cassette",
    "UPC (Barcode)":076742620547,
    "Label":"Narada",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"neuma",
    "Title":"Mado",
    "Original Release Date":"9/10/2003",
    "Release Date":"9/10/2003",
    "Format":"CD",
    "UPC (Barcode)":4514306006735,
    "Label":"Kazemachi",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Neurotic Outsiders",
    "Title":"Neurotic Outsiders",
    "Original Release Date":"9/10/1996",
    "Release Date":"9/10/1996",
    "Format":"CD",
    "UPC (Barcode)":093624629023,
    "Label":"Maverick",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Neutral Milk Hotel",
    "Title":"Everything Is",
    "Original Release Date":"9/18/2001",
    "Release Date":"9/18/2001",
    "Format":"MP3",
    "UPC (Barcode)":656605600529,
    "Label":"Orange Twin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Neutral Milk Hotel",
    "Title":"In the Aeroplane Over the Sea",
    "Original Release Date":"2/10/1998",
    "Release Date":"2/10/1998",
    "Format":"LP",
    "UPC (Barcode)":036172943616,
    "Label":"Merge",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Neutral Milk Hotel",
    "Title":"In the Aeroplane, Over the Sea",
    "Original Release Date":"2/10/1998",
    "Release Date":"2/10/1998",
    "Format":"MP3",
    "UPC (Barcode)":036172943623,
    "Label":"Merge",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Neutral Milk Hotel",
    "Title":"In the Aeroplane Over the Sea",
    "Original Release Date":"2/10/1998",
    "Release Date":"2/10/1998",
    "Format":"CD",
    "UPC (Barcode)":036172943623,
    "Label":"Merge",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Neutral Milk Hotel",
    "Title":"On Avery Island",
    "Original Release Date":"3/26/1996",
    "Release Date":"3/26/1996",
    "Format":"MP3",
    "UPC (Barcode)":361729403252,
    "Label":"Merge",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"New York Composers Orchestra",
    "Title":"First Program in Standard Time",
    "Original Release Date":"12/8/1992",
    "Release Date":"12/8/1992",
    "Format":"MP3",
    "UPC (Barcode)":093228041825,
    "Label":"New World",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NiNa",
    "Title":"NiNa",
    "Original Release Date":"11/1/1999",
    "Release Date":"11/1/1999",
    "Format":"CD",
    "UPC (Barcode)":4988009465395,
    "Label":"Sony Music Entertainment",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NIRGILIS",
    "Title":"New Standard",
    "Original Release Date":"5/26/2004",
    "Release Date":"5/26/2004",
    "Format":"CD",
    "UPC (Barcode)":4537649869065,
    "Label":"Toy's Factory",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NIRGILIS",
    "Title":"Tennis",
    "Original Release Date":"4/30/2003",
    "Release Date":"4/30/2003",
    "Format":"CD",
    "UPC (Barcode)":4988021000321,
    "Label":"Chukuri",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nirvana",
    "Title":"Nevermind (Deluxe Edition)",
    "Original Release Date":"1991",
    "Release Date":"9/27/2011",
    "Format":"CD",
    "UPC (Barcode)":602527779034,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nirvana",
    "Title":"Bleach (20th Anniversary Deluxe Edition)",
    "Original Release Date":"June 1989",
    "Release Date":"11/3/2009",
    "Format":"CD",
    "UPC (Barcode)":098787083422,
    "Label":"Sub Pop",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nirvana",
    "Title":"Nirvana",
    "Original Release Date":"10/29/2002",
    "Release Date":"10/29/2002",
    "Format":"CD",
    "UPC (Barcode)":606949350727,
    "Label":"Sub Pop",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nirvana",
    "Title":"Nevermind",
    "Original Release Date":"9/24/1991",
    "Release Date":"9/24/1991",
    "Format":"CD",
    "UPC (Barcode)":720642442524,
    "Label":"DGC",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nirvana",
    "Title":"Bleach",
    "Original Release Date":"June 1989",
    "Release Date":"10/14/1991",
    "Format":"CD",
    "UPC (Barcode)":098787003420,
    "Label":"Sub Pop",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nomiya Maki",
    "Title":"Miss Maki Nomiya Sings",
    "Original Release Date":"7/27/2000",
    "Release Date":"7/27/2000",
    "Format":"CD",
    "UPC (Barcode)":4988001454892,
    "Label":"Readymade",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"noodles",
    "Title":"God Cable",
    "Original Release Date":"2/14/2003",
    "Release Date":"2/14/2003",
    "Format":"CD",
    "UPC (Barcode)":4948722115052,
    "Label":"Delicious",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"noodles",
    "Title":"long long chain",
    "Original Release Date":"12/12/2001",
    "Release Date":"12/12/2001",
    "Format":"CD",
    "UPC (Barcode)":4948722090380,
    "Label":"Delicious",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"NOW Ensemble",
    "Title":"Awake",
    "Original Release Date":"4/26/2011",
    "Release Date":"4/26/2011",
    "Format":"CD",
    "UPC (Barcode)":884501472463,
    "Label":"New Amsterdam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NOW Ensemble",
    "Title":"Awake",
    "Original Release Date":"4/26/2011",
    "Release Date":"4/26/2011",
    "Format":"MP3",
    "UPC (Barcode)":884501472463,
    "Label":"New Amsterdam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Now It's Overhead",
    "Title":"Dark Light Daybreak",
    "Original Release Date":"9/12/2006",
    "Release Date":"9/12/2006",
    "Format":"CD",
    "UPC (Barcode)":648401009729,
    "Label":"Saddle Creek",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Now It's Overhead",
    "Title":"Fall Back Open",
    "Original Release Date":"3/9/2004",
    "Release Date":"3/9/2004",
    "Format":"CD",
    "UPC (Barcode)":648401005820,
    "Label":"Saddle Creek",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Gary Numan",
    "Title":"Cars",
    "Original Release Date":"1979",
    "Release Date":"1979",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"OMOIDE IN MY HEAD 1 ~BEST & B SIDES~",
    "Original Release Date":"3/2/2005",
    "Release Date":"3/2/2005",
    "Format":"CD",
    "UPC (Barcode)":4988006196391,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"OMOIDE IN MY HEAD 2 ~Kiroku Series 1~",
    "Original Release Date":"6/22/2005",
    "Release Date":"6/22/2005",
    "Format":"CD",
    "UPC (Barcode)":4988006197848,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"OMOIDE IN MY HEAD 2 ~Kiroku Series 2~",
    "Original Release Date":"6/22/2005",
    "Release Date":"6/22/2005",
    "Format":"CD",
    "UPC (Barcode)":4988006197855,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"OMOIDE IN MY HEAD 4 ~(Chin) NG & RARE TRACKS~",
    "Original Release Date":"12/14/2005",
    "Release Date":"12/14/2005",
    "Format":"CD",
    "UPC (Barcode)":4988006203143,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"Sapporo OMOIDE IN MY HEAD Jootai",
    "Original Release Date":"1/29/2003",
    "Release Date":"1/29/2003",
    "Format":"CD",
    "UPC (Barcode)":4988006181793,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"NUM-AMI-DABUTZ",
    "Original Release Date":"3/20/2002",
    "Release Date":"3/20/2002",
    "Format":"CD",
    "UPC (Barcode)":4988006177987,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"NUM-HEAVYMETALLIC",
    "Original Release Date":"4/26/2002",
    "Release Date":"4/26/2002",
    "Format":"CD",
    "UPC (Barcode)":4988006178205,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"URBAN GUITAR SAYONARA",
    "Original Release Date":"5/31/2000",
    "Release Date":"5/31/2000",
    "Format":"CD",
    "UPC (Barcode)":4988006166738,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"SAPPUKEI",
    "Original Release Date":"7/19/2000",
    "Release Date":"7/19/2000",
    "Format":"CD",
    "UPC (Barcode)":4988006167476,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"Teppu Surudokunatte",
    "Original Release Date":"11/29/2000",
    "Release Date":"11/29/2000",
    "Format":"CD",
    "UPC (Barcode)":4988006170445,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"SCHOOL GIRL DISTORTIONAL ADDICT",
    "Original Release Date":"7/23/1999",
    "Release Date":"7/23/1999",
    "Format":"CD",
    "UPC (Barcode)":4988006161047,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"DESTRUCTION BABY",
    "Original Release Date":"9/22/1999",
    "Release Date":"9/22/1999",
    "Format":"CD",
    "UPC (Barcode)":4988006162518,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"Toumei Shoujo",
    "Original Release Date":"5/26/1999",
    "Release Date":"5/26/1999",
    "Format":"CD",
    "UPC (Barcode)":4988006160255,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"Shibuya ROCKTRANSFORMED Joutai",
    "Original Release Date":"12/16/1999",
    "Release Date":"12/16/1999",
    "Format":"CD",
    "UPC (Barcode)":4988006164727,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"SCHOOL GIRL BYE BYE",
    "Original Release Date":"2/10/1999",
    "Release Date":"2/10/1999",
    "Format":"CD",
    "UPC (Barcode)":4545991100606,
    "Label":"K.O.G.A.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"NUMBER GIRL",
    "Title":"DRUNKEN HEARTED",
    "Original Release Date":"1998",
    "Release Date":"1998",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"automatic kiss",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Michael Nyman",
    "Title":"The Piano: Composer's Cut Vol. III",
    "Original Release Date":"10/17/2005",
    "Release Date":"10/17/2005",
    "Format":"CD",
    "UPC (Barcode)":5060099970054,
    "Label":"MN",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Michael Nyman",
    "Title":"The Piano Concerto/Where the Bee Dances",
    "Original Release Date":"8/25/1998",
    "Release Date":"8/25/1998",
    "Format":"MP3",
    "UPC (Barcode)":636943416822,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Michael Nyman; Balanescu Quartet",
    "Title":"String Quartets Nos. 1-3",
    "Original Release Date":"6/14/1991",
    "Release Date":"6/14/1991",
    "Format":"CD",
    "UPC (Barcode)":028943309325,
    "Label":"Argo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Michael Nyman; Balanescu Quartet",
    "Title":"String Quartets Nos. 1-3",
    "Original Release Date":"6/14/1991",
    "Release Date":"1991",
    "Format":"MP3",
    "UPC (Barcode)":028943309325,
    "Label":"Argo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"OBLIVION DUST",
    "Title":"OBLIVION DUST",
    "Original Release Date":"1/23/2008",
    "Release Date":"1/23/2008",
    "Format":"CD",
    "UPC (Barcode)":4945817145567,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"OBLIVION DUST",
    "Title":"Radio Song: Best of Oblivion Dust",
    "Original Release Date":"8/8/2001",
    "Release Date":"8/8/2001",
    "Format":"CD",
    "UPC (Barcode)":4945817170705,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"OBLIVION DUST",
    "Title":"Butterfly Head",
    "Original Release Date":"11/22/2000",
    "Release Date":"11/22/2000",
    "Format":"CD",
    "UPC (Barcode)":4945817170699,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"OBLIVION DUST",
    "Title":"Reborn",
    "Original Release Date":"12/16/1999",
    "Release Date":"12/16/1999",
    "Format":"CD",
    "UPC (Barcode)":4945817170668,
    "Label":"Cutting Edge",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Frank Ocean",
    "Title":"Channel Orange",
    "Original Release Date":"7/17/2012",
    "Release Date":"7/17/2012",
    "Format":"CD",
    "UPC (Barcode)":602527755281,
    "Label":"Def Jam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Frank Ocean",
    "Title":"Channel Orange",
    "Original Release Date":"7/17/2012",
    "Release Date":"7/17/2012",
    "Format":"MP3",
    "UPC (Barcode)":602527755281,
    "Label":"Def Jam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sinéad O'Connor",
    "Title":"Theology",
    "Original Release Date":"6/26/2007",
    "Release Date":"6/26/2007",
    "Format":"MP3",
    "UPC (Barcode)":099923423720,
    "Label":"Koch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sinéad O'Connor",
    "Title":"Faith And Courage",
    "Original Release Date":"6/13/2000",
    "Release Date":"6/13/2000",
    "Format":"CD",
    "UPC (Barcode)":075678333729,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sinéad O'Connor",
    "Title":"So Far... The Best of Sinead O'Connor",
    "Original Release Date":"11/25/1997",
    "Release Date":"11/25/1997",
    "Format":"CD",
    "UPC (Barcode)":724382368526,
    "Label":"Ensign",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sinéad O'Connor",
    "Title":"I Do Not Want What I Haven't Got",
    "Original Release Date":"2/28/1990",
    "Release Date":"2/28/1990",
    "Format":"CD",
    "UPC (Barcode)":094632175922,
    "Label":"Ensign",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sinéad O'Connor",
    "Title":"I Do Not Want What I Haven't Got",
    "Original Release Date":"3/12/1990",
    "Release Date":"3/12/1990",
    "Format":"Cassette",
    "UPC (Barcode)":094632175946,
    "Label":"Ensign",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Sinéad O'Connor",
    "Title":"The Lion and the Cobra",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":044114161219,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sinéad O'Connor",
    "Title":"Mandinka",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"7 Inch",
    "UPC (Barcode)":044114320777,
    "Label":"Ensign",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sinéad O'Connor",
    "Title":"The Lion And The Cobra",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":094632161222,
    "Label":"Ensign",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Office",
    "Title":"Mecca",
    "Original Release Date":"1/7/2009",
    "Release Date":"1/7/2009",
    "Format":"MP3",
    "UPC (Barcode)":781444910222,
    "Label":"Quak!Media",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Office",
    "Title":"A Night at the Ritz",
    "Original Release Date":"9/25/2007",
    "Release Date":"9/25/2007",
    "Format":"MP3",
    "UPC (Barcode)":794043909528,
    "Label":"Scratchie",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Office",
    "Title":"A Night at the Ritz",
    "Original Release Date":"9/25/2007",
    "Release Date":"9/25/2007",
    "Format":"CD",
    "UPC (Barcode)":794043909528,
    "Label":"Scratchie",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Keisho Ohno",
    "Title":"Inside Out",
    "Original Release Date":"4/17/2005",
    "Release Date":"4/17/2005",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Sound Works",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"The Old 97's",
    "Title":"Too Far to Care (Deluxe Edition)",
    "Original Release Date":"10/9/2012",
    "Release Date":"10/9/2012",
    "Format":"CD",
    "UPC (Barcode)":81665102770,
    "Label":"Omnivore",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Old 97's",
    "Title":"Too Far To Care",
    "Original Release Date":"6/17/1997",
    "Release Date":"6/17/1997",
    "Format":"CD",
    "UPC (Barcode)":075596205023,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Onitsuka Chihiro",
    "Title":"Ken to Kaede",
    "Original Release Date":"4/20/2011",
    "Release Date":"4/20/2011",
    "Format":"CD",
    "UPC (Barcode)":4988018319788,
    "Label":"FLME",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Onitsuka Chihiro",
    "Title":"Dorothy",
    "Original Release Date":"11/17/2009",
    "Release Date":"11/17/2009",
    "Format":"CD",
    "UPC (Barcode)":4988005574022,
    "Label":"Universal Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Onitsuka Chihiro",
    "Title":"LAS VEGAS",
    "Original Release Date":"10/31/2007",
    "Release Date":"10/31/2007",
    "Format":"CD",
    "UPC (Barcode)":4988005477958,
    "Label":"Universal Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Onitsuka Chihiro",
    "Title":"Singles 2000-2003",
    "Original Release Date":"9/7/2005",
    "Release Date":"9/7/2005",
    "Format":"CD",
    "UPC (Barcode)":4988006201354,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Onitsuka Chihiro",
    "Title":"Sugar High",
    "Original Release Date":"12/11/2002",
    "Release Date":"12/11/2002",
    "Format":"CD",
    "UPC (Barcode)":4988006181571,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Onitsuka Chihiro",
    "Title":"Insomnia",
    "Original Release Date":"3/7/2001",
    "Release Date":"3/7/2001",
    "Format":"CD",
    "UPC (Barcode)":4988006789517,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Ordinaires",
    "Title":"One",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"Cassette",
    "UPC (Barcode)":032862001147,
    "Label":"Bar/None",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Ore wa Konna Mon Ja Nai",
    "Title":"2",
    "Original Release Date":"2/10/2007",
    "Release Date":"2/10/2007",
    "Format":"CD",
    "UPC (Barcode)":4582237811209,
    "Label":"manso",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tarik O'Regan",
    "Title":"Threshold of Night",
    "Original Release Date":"9/9/2008",
    "Release Date":"9/9/2008",
    "Format":"Hybrid SACD",
    "UPC (Barcode)":093046749064,
    "Label":"Harmonia Mundi",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Orgy",
    "Title":"Candyass",
    "Original Release Date":"8/18/1998",
    "Release Date":"8/18/1998",
    "Format":"CD",
    "UPC (Barcode)":093624692324,
    "Label":"Elementree",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Oriental Love Ring",
    "Title":"In This World",
    "Original Release Date":"8/10/2010",
    "Release Date":"8/10/2010",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Oriental Love Ring",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Oriental Love Ring",
    "Title":"In This World",
    "Original Release Date":"8/10/2010",
    "Release Date":"8/10/2010",
    "Format":"CD",
    "UPC (Barcode)":800828310721,
    "Label":"Oriental Love Ring",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Outfield",
    "Title":"Play Deep",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"MP3",
    "UPC (Barcode)":074644002720,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Outkast",
    "Title":"Speakerboxxx/The Love Below",
    "Original Release Date":"9/23/2003",
    "Release Date":"9/23/2003",
    "Format":"CD",
    "UPC (Barcode)":828765013321,
    "Label":"Aquemini",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ozomatli",
    "Title":"Ozomatli",
    "Original Release Date":"6/16/1998",
    "Release Date":"6/16/1998",
    "Format":"CD",
    "UPC (Barcode)":705178002022,
    "Label":"Almo Sounds",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robert Palmer",
    "Title":"Riptide",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"LP",
    "UPC (Barcode)":075679047113,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pansy Division",
    "Title":"Absurd Pop Song Romance",
    "Original Release Date":"9/8/1998",
    "Release Date":"9/8/1998",
    "Format":"CD",
    "UPC (Barcode)":763361019820,
    "Label":"Lookout!",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Papaya Paranoia",
    "Title":"Rosepink",
    "Original Release Date":"11/6/2002",
    "Release Date":"11/6/2002",
    "Format":"CD",
    "UPC (Barcode)":4562117654041,
    "Label":"Fake & Co.",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Papaya Paranoia",
    "Title":"Tamayura",
    "Original Release Date":"2000",
    "Release Date":"2000",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Fake & Co.",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"The Alan Parsons Project",
    "Title":"Time",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Arista",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Arvo Pärt",
    "Title":"Tabula Rasa",
    "Original Release Date":"1984",
    "Release Date":"11/16/1999",
    "Format":"MP3",
    "UPC (Barcode)":028947638780,
    "Label":"ECM New Series",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Harry Partch; Kronos Quartet",
    "Title":"U.S. Highball: A Musical Account Of Slim's Transcontinental Hobo Trip",
    "Original Release Date":"8/19/2003",
    "Release Date":"8/19/2003",
    "Format":"CD",
    "UPC (Barcode)":075597969726,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dolly Parton",
    "Title":"9 to 5",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dolly Parton; Linda Ronstadt; Emmylou Harris",
    "Title":"Trio II",
    "Original Release Date":"2/9/1999",
    "Release Date":"2/9/1999",
    "Format":"CD",
    "UPC (Barcode)":075596227520,
    "Label":"Asylum",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dolly Parton; Linda Ronstadt; Emmylou Harris",
    "Title":"Trio",
    "Original Release Date":"February 1987",
    "Release Date":"February 1987",
    "Format":"LP",
    "UPC (Barcode)":075992549114,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dolly Parton; Linda Ronstadt; Emmylou Harris",
    "Title":"Trio",
    "Original Release Date":"February 1987",
    "Release Date":"February 1987",
    "Format":"CD",
    "UPC (Barcode)":075992549127,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pearl Jam",
    "Title":"Ten",
    "Original Release Date":"8/27/1991",
    "Release Date":"8/27/1991",
    "Format":"CD",
    "UPC (Barcode)":074644785722,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pebbles",
    "Title":"Always",
    "Original Release Date":"8/17/1990",
    "Release Date":"8/17/1990",
    "Format":"CD",
    "UPC (Barcode)":008811002527,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pedro The Lion",
    "Title":"Control",
    "Original Release Date":"4/16/2002",
    "Release Date":"4/16/2002",
    "Format":"CD",
    "UPC (Barcode)":792258107227,
    "Label":"jade tree",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Krzysztof Penderecki",
    "Title":"Orchestral Works Vol. 1",
    "Original Release Date":"2/22/2000",
    "Release Date":"2/22/2000",
    "Format":"CD",
    "UPC (Barcode)":636943449127,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pet Shop Boys",
    "Title":"It's a Sin",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pet Shop Boys",
    "Title":"Please",
    "Original Release Date":"5/2/1986",
    "Release Date":"5/2/1986",
    "Format":"LP",
    "UPC (Barcode)":077771719318,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pet Shop Boys",
    "Title":"West End Girls",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Michala Petri",
    "Title":"Movements",
    "Original Release Date":"5/29/2007",
    "Release Date":"5/29/2007",
    "Format":"MP3",
    "UPC (Barcode)":747313153169,
    "Label":"Da Capo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"PE'Z",
    "Title":"Tsukushinbou",
    "Original Release Date":"3/9/2005",
    "Release Date":"3/9/2005",
    "Format":"CD",
    "UPC (Barcode)":4527583005667,
    "Label":"Roadrunner",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"PE'Z",
    "Title":"Suzumushi",
    "Original Release Date":"1/4/2005",
    "Release Date":"1/4/2005",
    "Format":"CD",
    "UPC (Barcode)":4988006192539,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"PE'Z",
    "Title":"Chitosedori",
    "Original Release Date":"11/16/2005",
    "Release Date":"11/16/2005",
    "Format":"CD",
    "UPC (Barcode)":4527583006145,
    "Label":"Roadrunner",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Piano Circus",
    "Title":"Steve Reich - Six Pianos / Terry Riley - In C",
    "Original Release Date":"10/11/1990",
    "Release Date":"10/11/1990",
    "Format":"CD",
    "UPC (Barcode)":028943038027,
    "Label":"Argo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Astor Piazzolla; Kronos Quartet",
    "Title":"Five Tango Sensations",
    "Original Release Date":"1/22/1991",
    "Release Date":"1/22/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075597925449,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Tobias Picker",
    "Title":"Symphony No. 2/String Quartet No. 1",
    "Original Release Date":"8/7/1990",
    "Release Date":"5/28/1992",
    "Format":"CD",
    "UPC (Barcode)":075597924626,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tobias Picker",
    "Title":"Symphony No. 2/String Quartet No. 1",
    "Original Release Date":"8/7/1990",
    "Release Date":"8/7/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075597924640,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Pillows",
    "Title":"Penalty Life",
    "Original Release Date":"5/3/2005",
    "Release Date":"5/3/2005",
    "Format":"CD",
    "UPC (Barcode)":013023532021,
    "Label":"King",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Pixies",
    "Title":"Surfer Rosa",
    "Original Release Date":"1988",
    "Release Date":"1/28/1992",
    "Format":"CD",
    "UPC (Barcode)":075596129527,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pixies",
    "Title":"Doolittle",
    "Original Release Date":"4/18/1989",
    "Release Date":"4/18/1989",
    "Format":"CD",
    "UPC (Barcode)":075596085625,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pizzicato Five",
    "Title":"Playboy & Playgirl",
    "Original Release Date":"4/20/1999",
    "Release Date":"4/20/1999",
    "Format":"CD",
    "UPC (Barcode)":744861033325,
    "Label":"Matador",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pizzicato Five",
    "Title":"Happy End Of The World",
    "Original Release Date":"9/8/1997",
    "Release Date":"9/8/1997",
    "Format":"CD",
    "UPC (Barcode)":724385918124,
    "Label":"Matador",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pizzicato Five",
    "Title":"The Sound of Music",
    "Original Release Date":"10/31/1995",
    "Release Date":"10/31/1995",
    "Format":"CD",
    "UPC (Barcode)":075679262226,
    "Label":"Matador",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Pizzicato Five",
    "Title":"Made in USA",
    "Original Release Date":"10/21/1994",
    "Release Date":"10/21/1994",
    "Format":"CD",
    "UPC (Barcode)":744861009924,
    "Label":"Matador",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Platters",
    "Title":"20th Century Masters: Millenium Series: Best of the Platters",
    "Original Release Date":"8/17/1999",
    "Release Date":"8/17/1999",
    "Format":"Stream",
    "UPC (Barcode)":731454641425,
    "Label":"Island / Mercury",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Police",
    "Title":"Ghost In The Machine",
    "Original Release Date":"2/10/1981",
    "Release Date":"3/4/2003",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Police",
    "Title":"Zenyatta Mondatta",
    "Original Release Date":"10/3/1980",
    "Release Date":"3/4/2003",
    "Format":"CD",
    "UPC (Barcode)":00606949359720,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Police",
    "Title":"Every Breath You Take - The Classics",
    "Original Release Date":"9/12/1995",
    "Release Date":"9/12/1995",
    "Format":"CD",
    "UPC (Barcode)":731454038027,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Police",
    "Title":"Don't Stand So Close to Me",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Police",
    "Title":"Don't Stand So Close to Me / De Do Do Do, De Da Da Da",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Port Of Notes",
    "Title":"Evening Glow",
    "Original Release Date":"10/29/2004",
    "Release Date":"10/29/2004",
    "Format":"CD",
    "UPC (Barcode)":4535939093008,
    "Label":"Crue-L",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Port Of Notes",
    "Title":"Complain too much",
    "Original Release Date":"1/23/1999",
    "Release Date":"1/23/1999",
    "Format":"CD",
    "UPC (Barcode)":4535939049005,
    "Label":"Crue-L",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Postal Service",
    "Title":"Give Up",
    "Original Release Date":"2/18/2003",
    "Release Date":"2/18/2003",
    "Format":"CD",
    "UPC (Barcode)":098787059526,
    "Label":"Sub Pop",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Power Station",
    "Title":"The Power Station (remastered)",
    "Original Release Date":"1985",
    "Release Date":"4/26/2005",
    "Format":"CD",
    "UPC (Barcode)":724386631503,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Power Station",
    "Title":"Get It On",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Power Station",
    "Title":"Communication",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Power Station",
    "Title":"The Power Station",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"LP",
    "UPC (Barcode)":077771238017,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Power Station",
    "Title":"Some Like It Hot",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Prairie Cats",
    "Title":"'Til the Daytime Fades",
    "Original Release Date":"5/15/2001",
    "Release Date":"5/15/2001",
    "Format":"CD",
    "UPC (Barcode)":687474027520,
    "Label":"Behave",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Prairie Cats",
    "Title":"The Big One",
    "Original Release Date":"8/3/1999",
    "Release Date":"8/3/1999",
    "Format":"CD",
    "UPC (Barcode)":670917011323,
    "Label":"Behave",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Pretenders",
    "Title":"Brass in Pocket (I'm Special)",
    "Original Release Date":"1979",
    "Release Date":"1979",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Francis Preve",
    "Title":"Flotsam / Jetsam",
    "Original Release Date":"10/11/2010",
    "Release Date":"10/11/2010",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Different Pieces",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Francis Preve",
    "Title":"Marina",
    "Original Release Date":"4/12/2010",
    "Release Date":"4/12/2010",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Different Pieces",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Francis Preve",
    "Title":"Hasown",
    "Original Release Date":"4/6/2009",
    "Release Date":"4/6/2009",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Different Pieces",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Francis Preve",
    "Title":"Caboose",
    "Original Release Date":"11/30/2008",
    "Release Date":"11/30/2008",
    "Format":"MP3",
    "UPC (Barcode)":879382000119,
    "Label":"Different Pieces",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Francis Preve",
    "Title":"Caboose",
    "Original Release Date":"11/30/2008",
    "Release Date":"11/30/2008",
    "Format":"Stream",
    "UPC (Barcode)":879382000119,
    "Label":"Different Pieces",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Prince",
    "Title":"Parade",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"LP",
    "UPC (Barcode)":075992539517,
    "Label":"Paisley Park",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Prince",
    "Title":"Parade",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"CD",
    "UPC (Barcode)":075992539524,
    "Label":"Paisley Park",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Prodigy",
    "Title":"Fat Of The Land",
    "Original Release Date":"7/1/1997",
    "Release Date":"7/1/1997",
    "Format":"CD",
    "UPC (Barcode)":093624660620,
    "Label":"Maverick",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sergei Prokofieff",
    "Title":"Symphonies Nos. 1 & 7; Lieutenant Kijé Suite",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"LP",
    "UPC (Barcode)":5099929029815,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sergei Prokofieff; Frederic Chiu",
    "Title":"Complete Music for Solo Piano",
    "Original Release Date":"8/14/2001",
    "Release Date":"8/14/2001",
    "Format":"MP3",
    "UPC (Barcode)":093046730123,
    "Label":"Harmonia Mundi",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sergei Prokofieff; Barabara Nissman",
    "Title":"Complete Piano Sonatas, Vol. 3",
    "Original Release Date":"9/29/1992",
    "Release Date":"9/29/1992",
    "Format":"Cassette",
    "UPC (Barcode)":723721151423,
    "Label":"Newport Classics",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Gabriel Prokofiev",
    "Title":"String Quartet No. 1",
    "Original Release Date":"8/23/2004",
    "Release Date":"8/23/2004",
    "Format":"MP3",
    "UPC (Barcode)":0666017092423,
    "Label":"Nonclassical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Propaganda",
    "Title":"A Secret Wish",
    "Original Release Date":"1985",
    "Release Date":"10/4/2005",
    "Format":"MP3",
    "UPC (Barcode)":827912024166,
    "Label":"ZTT",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Puffy Amiyumi",
    "Title":"Nice.",
    "Original Release Date":"8/12/2003",
    "Release Date":"8/12/2003",
    "Format":"CD",
    "UPC (Barcode)":032862014222,
    "Label":"Bar/None",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Queen",
    "Title":"Another One Bites the Dust",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Queens Of The Stone Age",
    "Title":"Songs For The Deaf",
    "Original Release Date":"8/27/2002",
    "Release Date":"8/27/2002",
    "Format":"CD",
    "UPC (Barcode)":606949342524,
    "Label":"Interscope",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Quruli",
    "Title":"Tanz Walzer",
    "Original Release Date":"6/27/2007",
    "Release Date":"6/27/2007",
    "Format":"CD",
    "UPC (Barcode)":4988002531790,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Quruli",
    "Title":"Nikki",
    "Original Release Date":"11/23/2005",
    "Release Date":"11/23/2005",
    "Format":"CD",
    "UPC (Barcode)":4988002491292,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Quruli",
    "Title":"Antenna",
    "Original Release Date":"4/30/2004",
    "Release Date":"4/30/2004",
    "Format":"CD",
    "UPC (Barcode)":4988002456956,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Quruli",
    "Title":"SXSW 04 Sampler CD",
    "Original Release Date":"3/19/2004",
    "Release Date":"3/19/2004",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Quruli",
    "Title":"Joze to Tora to Sakana-tachi",
    "Original Release Date":"11/5/2003",
    "Release Date":"11/5/2003",
    "Format":"CD",
    "UPC (Barcode)":4988002452149,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Quruli",
    "Title":"How to Go",
    "Original Release Date":"9/17/2003",
    "Release Date":"9/17/2003",
    "Format":"CD",
    "UPC (Barcode)":4988002451227,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Quruli",
    "Title":"The World Is Mine",
    "Original Release Date":"3/20/2002",
    "Release Date":"3/20/2002",
    "Format":"CD",
    "UPC (Barcode)":4988002428564,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Quruli",
    "Title":"Team Rock",
    "Original Release Date":"2/21/2001",
    "Release Date":"2/21/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002412822,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Quruli",
    "Title":"Zukan",
    "Original Release Date":"1/21/2000",
    "Release Date":"1/21/2000",
    "Format":"CD",
    "UPC (Barcode)":4988002396023,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Quruli",
    "Title":"Sayonara Stranger",
    "Original Release Date":"4/21/1999",
    "Release Date":"4/21/1999",
    "Format":"CD",
    "UPC (Barcode)":4988002383993,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Collapse Into Now",
    "Original Release Date":"3/8/2011",
    "Release Date":"3/8/2011",
    "Format":"MP3",
    "UPC (Barcode)":093624962717,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Accelerate",
    "Original Release Date":"4/1/2008",
    "Release Date":"4/1/2008",
    "Format":"CD",
    "UPC (Barcode)":093624988588,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"And I Feel Fine...The Best Of The I.R.S. Years 1982-1987",
    "Original Release Date":"9/12/2006",
    "Release Date":"9/12/2006",
    "Format":"CD",
    "UPC (Barcode)":094636994123,
    "Label":"I.R.S.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Document",
    "Original Release Date":"1987",
    "Release Date":"1/27/1998",
    "Format":"CD",
    "UPC (Barcode)":724349348028,
    "Label":"I.R.S.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Fables of the Reconstruction",
    "Original Release Date":"6/10/1985",
    "Release Date":"1/27/1998",
    "Format":"Stream",
    "UPC (Barcode)":0724349347953,
    "Label":"I.R.S.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Out Of Time",
    "Original Release Date":"3/12/1991",
    "Release Date":"3/12/1991",
    "Format":"CD",
    "UPC (Barcode)":075992649629,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Out of Time",
    "Original Release Date":"3/12/1991",
    "Release Date":"3/12/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075992649643,
    "Label":"Warner Bros.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Green",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":075992579513,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Stand",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"7 Inch",
    "UPC (Barcode)":075992768870,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Green",
    "Original Release Date":"11/8/1988",
    "Release Date":"11/8/1988",
    "Format":"CD",
    "UPC (Barcode)":075992579520,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Eponymous",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"Cassette",
    "UPC (Barcode)":076732626245,
    "Label":"I.R.S.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Green",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"Cassette",
    "UPC (Barcode)":075992579544,
    "Label":"Warner Bros.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Document",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":076742205911,
    "Label":"I.R.S.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Document",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"Cassette",
    "UPC (Barcode)":076742205942,
    "Label":"I.R.S.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Lifes Rich Pageant",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"CD",
    "UPC (Barcode)":724349347823,
    "Label":"I.R.S.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Lifes Rich Pageant",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"Cassette",
    "UPC (Barcode)":076732578346,
    "Label":"I.R.S.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Lifes Rich Pageant",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"Stream",
    "UPC (Barcode)":0724349347854,
    "Label":"I.R.S.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Reckoning",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"Cassette",
    "UPC (Barcode)":044797004445,
    "Label":"I.R.S.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Reckoning",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"Stream",
    "UPC (Barcode)":044797004421,
    "Label":"I.R.S.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Murmur",
    "Original Release Date":"1983",
    "Release Date":"1983",
    "Format":"CD",
    "UPC (Barcode)":044797001420,
    "Label":"I.R.S.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.M.",
    "Title":"Murmur",
    "Original Release Date":"1983",
    "Release Date":"1983",
    "Format":"Cassette",
    "UPC (Barcode)":044797001444,
    "Label":"I.R.S.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"R.E.O. Speedwagon",
    "Title":"Keep on Loving You",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"R.E.O. Speedwagon",
    "Title":"Take It on the Run",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Radiohead",
    "Title":"There there",
    "Original Release Date":"2003",
    "Release Date":"2003",
    "Format":"7 Inch",
    "UPC (Barcode)":0708761781175,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Radiohead",
    "Title":"Kid A",
    "Original Release Date":"10/3/2000",
    "Release Date":"10/3/2000",
    "Format":"CD",
    "UPC (Barcode)":724352775323,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ray, Goodman & Brown",
    "Title":"Special Lady",
    "Original Release Date":"1979",
    "Release Date":"1979",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich",
    "Title":"2x5 Remixed",
    "Original Release Date":"4/22/2011",
    "Release Date":"4/22/2011",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich",
    "Title":"Remixed 2006",
    "Original Release Date":"8/26/2006",
    "Release Date":"8/26/2006",
    "Format":"MP3",
    "UPC (Barcode)":07559799882,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich",
    "Title":"Phases - A Nonesuch Retrospective",
    "Original Release Date":"9/26/2006",
    "Release Date":"9/26/2006",
    "Format":"CD",
    "UPC (Barcode)":075597996227,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich",
    "Title":"Triple Quartet",
    "Original Release Date":"10/16/2001",
    "Release Date":"10/16/2001",
    "Format":"CD",
    "UPC (Barcode)":075597954623,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich",
    "Title":"Reich Remixed",
    "Original Release Date":"3/2/1999",
    "Release Date":"3/2/1999",
    "Format":"CD",
    "UPC (Barcode)":075597955224,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich",
    "Title":"Early Works",
    "Original Release Date":"1988",
    "Release Date":"10/17/1990",
    "Format":"CD",
    "UPC (Barcode)":075597916942,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich",
    "Title":"Music For 18 Musicians",
    "Original Release Date":"1978",
    "Release Date":"1988",
    "Format":"MP3",
    "UPC (Barcode)":042282141729,
    "Label":"ECM New Series",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich",
    "Title":"Early Works",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"Cassette",
    "UPC (Barcode)":075597916942,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Steve Reich",
    "Title":"Early Works",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"Stream",
    "UPC (Barcode)":075597916942,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Steve Reich",
    "Title":"Sextet / Six Marimbas",
    "Original Release Date":"5/1/1986",
    "Release Date":"1987",
    "Format":"MP3",
    "UPC (Barcode)":075597913828,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich",
    "Title":"The Desert Music",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"LP",
    "UPC (Barcode)":075597910117,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich",
    "Title":"Music For 18 Musicians",
    "Original Release Date":"1978",
    "Release Date":"1978",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"ECM New Series",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich; Bang On A Can All-Stars",
    "Title":"New York Counterpoint/Eight Lines/Four Organs",
    "Original Release Date":"4/25/2000",
    "Release Date":"4/25/2000",
    "Format":"CD",
    "UPC (Barcode)":075597948127,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich; Bang On A Can All-Stars; eighth blackbird",
    "Title":"Double Sextet/2x5",
    "Original Release Date":"9/14/2010",
    "Release Date":"9/14/2010",
    "Format":"CD",
    "UPC (Barcode)":075597978643,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich; Grand Valley State University New Music Ensemble",
    "Title":"Music for 18 Musicians",
    "Original Release Date":"10/16/2007",
    "Release Date":"10/16/2007",
    "Format":"Hybrid SACD",
    "UPC (Barcode)":726708667825,
    "Label":"Innova",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich; Grand Valley State University New Music Ensemble",
    "Title":"Music for 18 Musicians",
    "Original Release Date":"10/16/2007",
    "Release Date":"10/16/2007",
    "Format":"MP3",
    "UPC (Barcode)":726708667825,
    "Label":"Innova",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich; Kronos Quartet; Pat Metheny",
    "Title":"Different Trains/Electric Counterpoint",
    "Original Release Date":"3/3/1989",
    "Release Date":"3/3/1989",
    "Format":"CD",
    "UPC (Barcode)":075597917628,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich; Kronos Quartet; So Percussion",
    "Title":"WTC 9/11/Mallet Quartet/Dance Patterns",
    "Original Release Date":"9/20/2011",
    "Release Date":"9/20/2011",
    "Format":"CD",
    "UPC (Barcode)":075597964578,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich; Marc Meltis; Duke Quartet",
    "Title":"Reich: Different Trains/Piano Phase; Meltis: String Quartet No. 2",
    "Original Release Date":"11/7/2006",
    "Release Date":"11/7/2006",
    "Format":"CD",
    "UPC (Barcode)":680125109729,
    "Label":"Black Box Classics",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich; Marc Meltis; Duke Quartet",
    "Title":"Reich: Different Trains/Piano Phase; Meltis: String Quartet No. 2",
    "Original Release Date":"11/7/2006",
    "Release Date":"11/7/2006",
    "Format":"MP3",
    "UPC (Barcode)":680125109729,
    "Label":"Black Box Classics",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Steve Reich; So Percussion",
    "Title":"Drumming",
    "Original Release Date":"3/8/2005",
    "Release Date":"3/8/2005",
    "Format":"MP3",
    "UPC (Barcode)":713746302245,
    "Label":"Cantaloupe",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rap Reiplinger",
    "Title":"The Best Of Rap",
    "Original Release Date":"1993",
    "Release Date":"3/17/1995",
    "Format":"CD",
    "UPC (Barcode)":790474011021,
    "Label":"Paradise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rap Reiplinger",
    "Title":"Poi Dog with Crabs",
    "Original Release Date":"1992",
    "Release Date":"3/14/1995",
    "Format":"CD",
    "UPC (Barcode)":761268202024,
    "Label":"Mountain Apple",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Reivers",
    "Title":"Translate Slowly",
    "Original Release Date":"1985",
    "Release Date":"6/14/1994",
    "Format":"MP3",
    "UPC (Barcode)":0032431007525,
    "Label":"dB",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Replacements",
    "Title":"Tim",
    "Original Release Date":"October 1985",
    "Release Date":"9/23/2008",
    "Format":"CD",
    "UPC (Barcode)":603497978007,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Replacements",
    "Title":"Let It Be",
    "Original Release Date":"1984",
    "Release Date":"4/22/2008",
    "Format":"CD",
    "UPC (Barcode)":081227993658,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Replacements",
    "Title":"Don't Tell A Soul",
    "Original Release Date":"1989",
    "Release Date":"9/23/2008",
    "Format":"CD",
    "UPC (Barcode)":081227990244,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Replacements",
    "Title":"Let It Be",
    "Original Release Date":"10/2/1984",
    "Release Date":"4/22/2008",
    "Format":"Stream",
    "UPC (Barcode)": 603497978083,
    "Label":"Rhino",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Replacements",
    "Title":"Tim",
    "Original Release Date":"October 1985",
    "Release Date":"9/23/2008",
    "Format":"Stream",
    "UPC (Barcode)":603497978007,
    "Label":"Rhino",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Replacements",
    "Title":"Stink",
    "Original Release Date":"1982",
    "Release Date":"7/18/2006",
    "Format":"MP3",
    "UPC (Barcode)":035058202823,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Replacements",
    "Title":"Don't You Know Who I Think I Was?",
    "Original Release Date":"6/13/2006",
    "Release Date":"6/13/2006",
    "Format":"CD",
    "UPC (Barcode)":081227001322,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Replacements",
    "Title":"Don't Tell A Soul",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"LP",
    "UPC (Barcode)":075592583114,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Replacements",
    "Title":"Don't Tell A Soul",
    "Original Release Date":"2/1/1989",
    "Release Date":"2/1/1989",
    "Format":"CD",
    "UPC (Barcode)":075992583121,
    "Label":"Sire",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Replacements",
    "Title":"Don't Tell a Soul",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"Cassette",
    "UPC (Barcode)":075992583145,
    "Label":"Sire",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Res",
    "Title":"Black.Girls.Rock!",
    "Original Release Date":"2/18/2010",
    "Release Date":"2/18/2010",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Res",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Res",
    "Title":"How I Do",
    "Original Release Date":"6/26/2001",
    "Release Date":"6/26/2001",
    "Format":"CD",
    "UPC (Barcode)":008811231026,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Response",
    "Title":"Response",
    "Original Release Date":"3/5/2003",
    "Release Date":"3/5/2003",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"face near",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Dylan Rice",
    "Title":"Electric Grids & Concrete Towers",
    "Original Release Date":"11/12/2009",
    "Release Date":"11/12/2009",
    "Format":"MP3",
    "UPC (Barcode)":859702601370,
    "Label":"Deep Tissue",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dylan Rice",
    "Title":"The Faces of Victory",
    "Original Release Date":"6/20/2006",
    "Release Date":"6/20/2006",
    "Format":"MP3",
    "UPC (Barcode)":634479320514,
    "Label":"Dylan Rice",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dylan Rice",
    "Title":"Wandering Eyes",
    "Original Release Date":"2/4/2004",
    "Release Date":"2/4/2004",
    "Format":"MP3",
    "UPC (Barcode)":628740712122,
    "Label":"Deep Tissue",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dylan Rice",
    "Title":"Wandering Eyes",
    "Original Release Date":"2/4/2004",
    "Release Date":"2/4/2004",
    "Format":"CD",
    "UPC (Barcode)":628740712122,
    "Label":"Deep Tissue",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Cliff Richard",
    "Title":"Dreaming",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ricky Martin",
    "Title":"Musica + Alma + Sexo",
    "Original Release Date":"2/1/2011",
    "Release Date":"2/1/2011",
    "Format":"CD",
    "UPC (Barcode)":886978346025,
    "Label":"Sony Latin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Terry Riley",
    "Title":"In C",
    "Original Release Date":"1968",
    "Release Date":"3/24/2009",
    "Format":"CD",
    "UPC (Barcode)":886974536826,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Terry Riley",
    "Title":"In C",
    "Original Release Date":"1968",
    "Release Date":"1968",
    "Format":"CD",
    "UPC (Barcode)":074640717826,
    "Label":"Sony Classical",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Terry Riley; Bang On A Can All-Stars",
    "Title":"In C",
    "Original Release Date":"9/11/2001",
    "Release Date":"9/11/2001",
    "Format":"MP3",
    "UPC (Barcode)":713746243227,
    "Label":"Cantaloupe",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Terry Riley; Grand Valley State University New Music Ensemble",
    "Title":"In C Remixed",
    "Original Release Date":"11/17/2009",
    "Release Date":"11/17/2009",
    "Format":"MP3",
    "UPC (Barcode)":726708675820,
    "Label":"Innova",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Terry Riley; Kronos Quartet",
    "Title":"The Cusp of Magic",
    "Original Release Date":"2/5/2008",
    "Release Date":"2/5/2008",
    "Format":"CD",
    "UPC (Barcode)":075597995985,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Terry Riley; Kronos Quartet",
    "Title":"Requiem For Adam",
    "Original Release Date":"9/4/2001",
    "Release Date":"9/4/2001",
    "Format":"CD",
    "UPC (Barcode)":075597963922,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Terry Riley; Kronos Quartet",
    "Title":"Salome Dances for Peace",
    "Original Release Date":"10/20/1989",
    "Release Date":"10/20/1989",
    "Format":"CD",
    "UPC (Barcode)":075597921724,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Terry Riley; Kronos Quartet",
    "Title":"Salome Dance for Peace",
    "Original Release Date":"10/29/1989",
    "Release Date":"10/29/1989",
    "Format":"Cassette",
    "UPC (Barcode)":075597921748,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"LeAnn Rimes",
    "Title":"Blue",
    "Original Release Date":"7/9/1996",
    "Release Date":"7/9/1996",
    "Format":"CD",
    "UPC (Barcode)":715187782129,
    "Label":"Curb",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"RIZE",
    "Title":"Rookey",
    "Original Release Date":"11/22/2000",
    "Release Date":"11/22/2000",
    "Format":"CD",
    "UPC (Barcode)":4988010218621,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bruce Robison",
    "Title":"Eleven Stories",
    "Original Release Date":"4/4/2006",
    "Release Date":"4/4/2006",
    "Format":"CD",
    "UPC (Barcode)":854956001013,
    "Label":"Sustain",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bruce Robison",
    "Title":"Wrapped",
    "Original Release Date":"5/19/1998",
    "Release Date":"5/19/1998",
    "Format":"CD",
    "UPC (Barcode)":074646913420,
    "Label":"Lucky Dog",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Aaron Roche",
    "Title":"!BlurMyEyes",
    "Original Release Date":"7/31/2012",
    "Release Date":"7/31/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"New Amsterdam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Rolling Stones",
    "Title":"Emotional Rescue",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Rolling Stone",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mark Ronson",
    "Title":"Record Collection",
    "Original Release Date":"9/28/2010",
    "Release Date":"9/28/2010",
    "Format":"MP3",
    "UPC (Barcode)":886977363320,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Linda Ronstadt; Emmylou Harris",
    "Title":"Western Wall: The Tucson Sessions",
    "Original Release Date":"8/24/1999",
    "Release Date":"8/24/1999",
    "Format":"CD",
    "UPC (Barcode)":075596240826,
    "Label":"Asylum",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Roomful of Teeth",
    "Title":"Roomful of Teeth",
    "Original Release Date":"10/30/2012",
    "Release Date":"10/30/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"New Amsterdam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ned Rorem",
    "Title":"Eleven Studies for Eleven Players; Piano Concerto in Six Movements",
    "Original Release Date":"2/10/2004",
    "Release Date":"2/10/2004",
    "Format":"MP3",
    "UPC (Barcode)":809157000211,
    "Label":"First Edition",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ned Rorem",
    "Title":"Symphonies Nos. 1-3",
    "Original Release Date":"8/19/2003",
    "Release Date":"8/19/2003",
    "Format":"MP3",
    "UPC (Barcode)":636943914922,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Diana Ross",
    "Title":"Upside Down",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Motown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Arthur Russell",
    "Title":"First Thought Best Thought",
    "Original Release Date":"4/4/2006",
    "Release Date":"4/4/2006",
    "Format":"MP3",
    "UPC (Barcode)":880301100520,
    "Label":"Audika",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Arthur Russell",
    "Title":"Calling Out of Context",
    "Original Release Date":"2/17/2004",
    "Release Date":"2/17/2004",
    "Format":"MP3",
    "UPC (Barcode)":880301100124,
    "Label":"Audika",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Rutter",
    "Title":"Magnificat, The Falcon, Two Festival Anthems",
    "Original Release Date":"5/6/1993",
    "Release Date":"5/6/1993",
    "Format":"CD",
    "UPC (Barcode)":040888011422,
    "Label":"Collegium",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Frederic Rzewski; Ralph van Raat",
    "Title":"The People United Will Never be Defeated",
    "Original Release Date":"3/25/2008",
    "Release Date":"3/25/2008",
    "Format":"MP3",
    "UPC (Barcode)":636943936023,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sacha Sacket",
    "Title":"Aim",
    "Original Release Date":"3/12/2013",
    "Release Date":"3/12/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Phonix",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sacha Sacket",
    "Title":"Low Blow",
    "Original Release Date":"3/12/2013",
    "Release Date":"3/12/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Phonix",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sacha Sacket",
    "Title":"Song for Jamey",
    "Original Release Date":"1/31/2013",
    "Release Date":"1/31/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Phonix",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sacha Sacket",
    "Title":"Viscera",
    "Original Release Date":"6/1/2010",
    "Release Date":"6/1/2010",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Golden Sphinx",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sacha Sacket",
    "Title":"Hermitage",
    "Original Release Date":"11/17/2009",
    "Release Date":"11/17/2009",
    "Format":"CD",
    "UPC (Barcode)":628740130025,
    "Label":"Swedish Pony Productions",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sacha Sacket",
    "Title":"Lovers & Leaders",
    "Original Release Date":"10/9/2007",
    "Release Date":"10/9/2007",
    "Format":"CD",
    "UPC (Barcode)":628740825921,
    "Label":"Golden Sphinx",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sacha Sacket",
    "Title":"shadowed",
    "Original Release Date":"7/6/2004",
    "Release Date":"7/6/2004",
    "Format":"MP3",
    "UPC (Barcode)":641444944224,
    "Label":"Golden Sphinx",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sacha Sacket",
    "Title":"shadowed",
    "Original Release Date":"7/6/2004",
    "Release Date":"7/6/2004",
    "Format":"CD",
    "UPC (Barcode)":641444944224,
    "Label":"Golden Sphinx",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sade",
    "Title":"Soldier of Love",
    "Original Release Date":"2/9/2010",
    "Release Date":"2/9/2010",
    "Format":"CD",
    "UPC (Barcode)":886976393328,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sade",
    "Title":"Lovers Rock",
    "Original Release Date":"11/14/2000",
    "Release Date":"11/14/2000",
    "Format":"CD",
    "UPC (Barcode)":696998518520,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sade",
    "Title":"The Best Of Sade",
    "Original Release Date":"11/8/1994",
    "Release Date":"11/8/1994",
    "Format":"CD",
    "UPC (Barcode)":074646668627,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sade",
    "Title":"Love Deluxe",
    "Original Release Date":"11/11/1992",
    "Release Date":"11/11/1992",
    "Format":"CD",
    "UPC (Barcode)":696998524323,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sade",
    "Title":"Diamond Life",
    "Original Release Date":"1984",
    "Release Date":"12/15/1992",
    "Format":"Cassette",
    "UPC (Barcode)":074643958189,
    "Label":"Epic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Sade",
    "Title":"Promise",
    "Original Release Date":"1986",
    "Release Date":"3/1/1990",
    "Format":"MP3",
    "UPC (Barcode)":5099746557522,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sade",
    "Title":"Stronger Than Pride",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":074644421019,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ryuichi Sakamoto",
    "Title":"Beauty",
    "Original Release Date":"2/21/1990",
    "Release Date":"2/21/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075679129444,
    "Label":"Virgin",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Aulis Sallinen",
    "Title":"Chamber Music",
    "Original Release Date":"9/22/1994",
    "Release Date":"9/22/1994",
    "Format":"MP3",
    "UPC (Barcode)":750582035920,
    "Label":"BIS",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Samamidon",
    "Title":"But This Chicken Proved Falsehearted",
    "Original Release Date":"2/20/2007",
    "Release Date":"2/20/2007",
    "Format":"MP3",
    "UPC (Barcode)":612651007927,
    "Label":"Plug Research",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Santigold",
    "Title":"Master Of My Make-Believe",
    "Original Release Date":"5/1/2012",
    "Release Date":"5/1/2012",
    "Format":"CD",
    "UPC (Barcode)":075678766695,
    "Label":"Downtown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Santigold",
    "Title":"Disparate Youth",
    "Original Release Date":"2/14/2012",
    "Release Date":"2/14/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Downtown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Santigold",
    "Title":"Big Mouth",
    "Original Release Date":"2/7/2012",
    "Release Date":"2/7/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Downtown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Santigold",
    "Title":"Santigold",
    "Original Release Date":"9/23/2008",
    "Release Date":"9/23/2008",
    "Format":"CD",
    "UPC (Barcode)":878037003413,
    "Label":"Downtown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Santigold",
    "Title":"Santigold",
    "Original Release Date":"9/23/2008",
    "Release Date":"9/23/2008",
    "Format":"MP3",
    "UPC (Barcode)":878037003413,
    "Label":"Downtown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sasagawa Miwa",
    "Title":"Oroka na Negai",
    "Original Release Date":"8/29/2012",
    "Release Date":"8/29/2012",
    "Format":"CD",
    "UPC (Barcode)":4945817147745,
    "Label":"Cutting Edge",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sasagawa Miwa",
    "Title":"Mayoi Naku",
    "Original Release Date":"3/7/2007",
    "Release Date":"3/7/2007",
    "Format":"CD",
    "UPC (Barcode)":4988064232215,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sasagawa Miwa",
    "Title":"Yoake",
    "Original Release Date":"2/22/2006",
    "Release Date":"2/22/2006",
    "Format":"CD",
    "UPC (Barcode)":4988064178759,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sasagawa Miwa",
    "Title":"Amata",
    "Original Release Date":"1/19/2005",
    "Release Date":"1/19/2005",
    "Format":"CD",
    "UPC (Barcode)":4988064175765,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sasagawa Miwa",
    "Title":"Himawari",
    "Original Release Date":"10/5/2005",
    "Release Date":"2005",
    "Format":"CD",
    "UPC (Barcode)":4988064308071,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sasagawa Miwa",
    "Title":"Anata Atashi",
    "Original Release Date":"8/25/2004",
    "Release Date":"8/25/2004",
    "Format":"CD",
    "UPC (Barcode)":4988064306053,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sasagawa Miwa",
    "Title":"Jijitsu",
    "Original Release Date":"11/29/2003",
    "Release Date":"11/29/2003",
    "Format":"CD",
    "UPC (Barcode)":4988064173471,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Erik Satie; Daniel Versano; Philippe Entremont",
    "Title":"Piano Music",
    "Original Release Date":"4/1/2003",
    "Release Date":"4/1/2003",
    "Format":"CD",
    "UPC (Barcode)":696998728028,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dudley Saunders",
    "Title":"The Billy White Acre Sessions",
    "Original Release Date":"5/1/2005",
    "Release Date":"5/1/2005",
    "Format":"CD",
    "UPC (Barcode)":783707087702,
    "Label":"Fang",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Joey Scarbury",
    "Title":"Theme from 'Greatest American Hero' (Believe It or Not)",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Domenico Scarlatti; Mayako Soné",
    "Title":"Unpublished Sonatas",
    "Original Release Date":"7/5/1994",
    "Release Date":"7/5/1994",
    "Format":"CD",
    "UPC (Barcode)":745099480622,
    "Label":"Erato",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Domenico Scarlatti; Colin Tinley",
    "Title":"Scarlatti Sonatas",
    "Original Release Date":"8/2/1993",
    "Release Date":"8/2/1993",
    "Format":"CD",
    "UPC (Barcode)":053479010322,
    "Label":"Dorian",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Alfred Schnittke",
    "Title":"Epilogue",
    "Original Release Date":"4/24/2007",
    "Release Date":"4/24/2007",
    "Format":"MP3",
    "UPC (Barcode)":675754983727,
    "Label":"BIS",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Alfred Schnittke",
    "Title":"Concerto Grosso No. 1 / Oboe and Harp Concerto / Piano Concerto",
    "Original Release Date":"7/6/1993",
    "Release Date":"7/6/1993",
    "Format":"MP3",
    "UPC (Barcode)":750582019722,
    "Label":"BIS",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Alfred Schnittke",
    "Title":"Symphony No. 4 / Requiem",
    "Original Release Date":"6/23/1993",
    "Release Date":"6/23/1993",
    "Format":"MP3",
    "UPC (Barcode)":750582030529,
    "Label":"BIS",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Alfred Schnittke",
    "Title":"Chamber Music: Piano Quintet",
    "Original Release Date":"April 1992",
    "Release Date":"April 1992",
    "Format":"MP3",
    "UPC (Barcode)":889253383821,
    "Label":"BIS",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Alfred Schnittke; Kronos Quartet",
    "Title":"The Complete String Quartets of Alfred Schnittke",
    "Original Release Date":"5/19/1998",
    "Release Date":"5/19/1998",
    "Format":"CD",
    "UPC (Barcode)":075597950021,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Arnold Schoenberg",
    "Title":"Pierrot lunaire",
    "Original Release Date":"10/14/2003",
    "Release Date":"10/14/2003",
    "Format":"MP3",
    "UPC (Barcode)":794881703425,
    "Label":"Harmonia Mundi",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Arnold Schoenberg",
    "Title":"Verklarte Nacht/Chamber Symphony No. 2/Accompaniment to a Cinematic Scene",
    "Original Release Date":"9/26/2000",
    "Release Date":"9/26/2000",
    "Format":"MP3",
    "UPC (Barcode)":636943437124,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Franz Schubert",
    "Title":"Trout Quintet, Quartet in A Minor",
    "Original Release Date":"11/1/1990",
    "Release Date":"11/1/1990",
    "Format":"MP3",
    "UPC (Barcode)":717794568424,
    "Label":"Telarc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Franz Schubert; Tokyo String Quartet",
    "Title":"String Quartet No. 12, Death and the Maiden / String Quartet No. 14",
    "Original Release Date":"11/4/1992",
    "Release Date":"11/4/1992",
    "Format":"MP3",
    "UPC (Barcode)":047163720727,
    "Label":"Vox (Classical)",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robert Schumann; Alberni String Quartet; Thomas Rajna",
    "Title":"Piano Quintet Opus 44, Piano Quartet Opus 47",
    "Original Release Date":"6/12/2007",
    "Release Date":"6/12/2007",
    "Format":"MP3",
    "UPC (Barcode)":5015155332420,
    "Label":"CRD/Premiere",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Robert Schumann; Rico Gulda",
    "Title":"Album fur die Jungen",
    "Original Release Date":"10/16/2001",
    "Release Date":"10/16/2001",
    "Format":"MP3",
    "UPC (Barcode)":747313571123,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Scissor Sisters",
    "Title":"Magic Hour",
    "Original Release Date":"5/28/2012",
    "Release Date":"5/28/2012",
    "Format":"CD",
    "UPC (Barcode)":602537050079,
    "Label":"Casacblanca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Scissor Sisters",
    "Title":"Night Work",
    "Original Release Date":"6/29/2010",
    "Release Date":"6/29/2010",
    "Format":"MP3",
    "UPC (Barcode)":602527381107,
    "Label":"Downtown",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Seagull Screaming Kiss Her Kiss Her",
    "Title":"Red Talk",
    "Original Release Date":"7/1/2002",
    "Release Date":"7/1/2002",
    "Format":"CD",
    "UPC (Barcode)":5013929820227,
    "Label":"arrivederci baby",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Seagull Screaming Kiss Her Kiss Her",
    "Title":"Future or No Future",
    "Original Release Date":"5/9/2001",
    "Release Date":"5/9/2001",
    "Format":"CD",
    "UPC (Barcode)":4988023041902,
    "Label":"Trattoria",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Seawind",
    "Title":"What Cha Doin'",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shea Seger",
    "Title":"The May Street Project",
    "Original Release Date":"6/5/2001",
    "Release Date":"6/5/2001",
    "Format":"CD",
    "UPC (Barcode)":078636938226,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sex Pistols",
    "Title":"Never Mind the Bollocks, Here's the Sex Pistols",
    "Original Release Date":"11/10/1977",
    "Release Date":"11/10/1977",
    "Format":"CD",
    "UPC (Barcode)":075992734721,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sex Pistols",
    "Title":"Never Mind the Bollocks, Here's the Sex Pistols",
    "Original Release Date":"11/10/1977",
    "Release Date":"11/10/1977",
    "Format":"MP3",
    "UPC (Barcode)":075992734721,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shakira",
    "Title":"¿Dónde Están Los Ladrones?",
    "Original Release Date":"9/22/1998",
    "Release Date":"9/22/1998",
    "Format":"CD",
    "UPC (Barcode)":037628274629,
    "Label":"Sony Latin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Iroha ni Hoheto/Kodoku no Akatsuki",
    "Original Release Date":"5/27/2013",
    "Release Date":"5/27/2013",
    "Format":"CD",
    "UPC (Barcode)":4988006237902,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Carnation",
    "Original Release Date":"11/2/2011",
    "Release Date":"11/2/2011",
    "Format":"CD",
    "UPC (Barcode)":4988006228870,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Ariamaru Tomi",
    "Original Release Date":"5/27/2009",
    "Release Date":"5/27/2009",
    "Format":"CD",
    "UPC (Barcode)":4988006220225,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Sanmon Gossip",
    "Original Release Date":"6/24/2009",
    "Release Date":"6/24/2009",
    "Format":"CD",
    "UPC (Barcode)":4988006220423,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Watashi to Houden",
    "Original Release Date":"7/6/2008",
    "Release Date":"7/6/2008",
    "Format":"CD",
    "UPC (Barcode)":4988006215795,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo x Saito Neko + Shiina Junpei",
    "Title":"Konoyo no Kagiri",
    "Original Release Date":"1/17/2007",
    "Release Date":"1/17/2007",
    "Format":"CD",
    "UPC (Barcode)":4988006209640,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo x Saito Neko",
    "Title":"Heisei Fuuzoku",
    "Original Release Date":"2/21/2007",
    "Release Date":"2/21/2007",
    "Format":"CD",
    "UPC (Barcode)":4988006210011,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Kuki (Stem) ~Daimyo Ashobi Hen~",
    "Original Release Date":"1/22/2003",
    "Release Date":"1/22/2003",
    "Format":"CD",
    "UPC (Barcode)":4988006181816,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Karuki Zaamen Kurinohana",
    "Original Release Date":"2/23/2003",
    "Release Date":"2/23/2003",
    "Format":"CD",
    "UPC (Barcode)":4988006182103,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Ringo no Uta",
    "Original Release Date":"11/25/2003",
    "Release Date":"11/25/2003",
    "Format":"CD",
    "UPC (Barcode)":4988006188051,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Utaite Myoori",
    "Original Release Date":"5/27/2002",
    "Release Date":"5/27/2002",
    "Format":"CD",
    "UPC (Barcode)":4988006178434,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Mayonaka wa Junketsu",
    "Original Release Date":"3/28/2001",
    "Release Date":"3/28/2001",
    "Format":"CD",
    "UPC (Barcode)":4988006172173,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Gips",
    "Original Release Date":"1/26/2000",
    "Release Date":"1/26/2000",
    "Format":"CD",
    "UPC (Barcode)":4988006165441,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Tsumi to Batsu",
    "Original Release Date":"1/26/2000",
    "Release Date":"1/26/2000",
    "Format":"CD",
    "UPC (Barcode)":4988006165458,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Zechooshuu",
    "Original Release Date":"9/13/2000",
    "Release Date":"9/13/2000",
    "Format":"CD",
    "UPC (Barcode)":4988006169043,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Shouso Strip",
    "Original Release Date":"3/31/2000",
    "Release Date":"3/31/2000",
    "Format":"CD",
    "UPC (Barcode)":4988006166141,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Honnou",
    "Original Release Date":"10/27/1999",
    "Release Date":"10/27/1999",
    "Format":"CD",
    "UPC (Barcode)":4988006160491,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Koufukuron",
    "Original Release Date":"10/27/1999",
    "Release Date":"10/27/1999",
    "Format":"CD",
    "UPC (Barcode)":4988006160507,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Koko de KISU Shite",
    "Original Release Date":"1/20/1999",
    "Release Date":"1/20/1999",
    "Format":"CD",
    "UPC (Barcode)":4988006157613,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Muzai-Moratorium",
    "Original Release Date":"2/24/1999",
    "Release Date":"2/24/1999",
    "Format":"CD",
    "UPC (Barcode)":4988006158429,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiina Ringo",
    "Title":"Kabuki-cho no Jyoou",
    "Original Release Date":"9/9/1998",
    "Release Date":"9/9/1998",
    "Format":"CD",
    "UPC (Barcode)":4988006153493,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shinohara Rika",
    "Title":"Daylight",
    "Original Release Date":"5/19/2001",
    "Release Date":"5/19/2001",
    "Format":"CD",
    "UPC (Barcode)":4948722079378,
    "Label":"Redberry",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shinohara Rika",
    "Title":"Seikatsu No Uta",
    "Original Release Date":"3/23/2000",
    "Release Date":"3/23/2000",
    "Format":"CD",
    "UPC (Barcode)":4517331153429,
    "Label":"Redberry",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Shins",
    "Title":"Oh, Inverted World",
    "Original Release Date":"6/19/2001",
    "Release Date":"6/19/2001",
    "Format":"CD",
    "UPC (Barcode)":098787055023,
    "Label":"Sub Pop",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiratori Maika",
    "Title":"Gemini",
    "Original Release Date":"9/15/2004",
    "Release Date":"9/15/2004",
    "Format":"CD",
    "UPC (Barcode)":4988013766808,
    "Label":"Pony Canyon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiratori Maika",
    "Title":"Toogenkyoo",
    "Original Release Date":"7/16/2003",
    "Release Date":"7/16/2003",
    "Format":"CD",
    "UPC (Barcode)":4988013526600,
    "Label":"Pony Canyon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiratori Maika",
    "Title":"Someday",
    "Original Release Date":"5/21/2003",
    "Release Date":"5/21/2003",
    "Format":"CD",
    "UPC (Barcode)":4988013495500,
    "Label":"Pony Canyon",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Shiratori Maika",
    "Title":"Anata no Ude wo",
    "Original Release Date":"11/20/2002",
    "Release Date":"11/20/2002",
    "Format":"CD",
    "UPC (Barcode)":4988013424500,
    "Label":"Pony Canyon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiratori Maika",
    "Title":"Hanazono",
    "Original Release Date":"4/24/2002",
    "Release Date":"4/24/2002",
    "Format":"CD",
    "UPC (Barcode)":4988013333703,
    "Label":"Pony Canyon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Shiratori Maika",
    "Title":"Red Clover",
    "Original Release Date":"3/6/2002",
    "Release Date":"3/6/2002",
    "Format":"CD",
    "UPC (Barcode)":4988013304703,
    "Label":"Pony Canyon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dmitri Shostakovich",
    "Title":"Chamber Symphony/Symphony for Strings/From Jewish Folk Poetry",
    "Original Release Date":"9/26/2000",
    "Release Date":"9/26/2000",
    "Format":"CD",
    "UPC (Barcode)":095115661727,
    "Label":"Chandos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dmitri Shostakovich",
    "Title":"Symphonies Nos. 1 & 3, The First of May",
    "Original Release Date":"2/15/1994",
    "Release Date":"2/15/1994",
    "Format":"MP3",
    "UPC (Barcode)":730099562324,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dmitri Shostakovich",
    "Title":"Symphonies Nos. 5 & 9",
    "Original Release Date":"6/30/1992",
    "Release Date":"6/30/1992",
    "Format":"MP3",
    "UPC (Barcode)":4891030506329,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dmitri Shostakovich",
    "Title":"Symphony No. 10",
    "Original Release Date":"8/22/1990",
    "Release Date":"8/22/1990",
    "Format":"MP3",
    "UPC (Barcode)":089408024122,
    "Label":"Telarc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dmitri Shostakovich",
    "Title":"Piano Concerto No. 1/Chamber Symphony",
    "Original Release Date":"8/29/1989",
    "Release Date":"8/29/1989",
    "Format":"Cassette",
    "UPC (Barcode)":078635794748,
    "Label":"RCA Victor",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Dmitri Shostakovich; Eder Quartet",
    "Title":"Complete String Quartets, Vol. 6",
    "Original Release Date":"1/1/1998",
    "Release Date":"1/1/1998",
    "Format":"MP3",
    "UPC (Barcode)":730099497725,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dmitri Shostakovich; Fitzwilliam Quartet",
    "Title":"The String Quartets",
    "Original Release Date":"2/10/1998",
    "Release Date":"2/10/1998",
    "Format":"CD",
    "UPC (Barcode)":028945577623,
    "Label":"Decca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dmitri Shostakovich; Stockholm Arts Trio; Anita Soldh",
    "Title":"Piano Trios Nos. 1 & 2",
    "Original Release Date":"3/18/1997",
    "Release Date":"3/18/1997",
    "Format":"MP3",
    "UPC (Barcode)":730099429726,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jean Sibelius",
    "Title":"Symphonies Nos. 4 & 5",
    "Original Release Date":"3/14/2000",
    "Release Date":"3/14/2000",
    "Format":"MP3",
    "UPC (Barcode)":636943437728,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jean Sibelius",
    "Title":"Symphonies Nos. 6 & 7/Tapiola",
    "Original Release Date":"10/1/1996",
    "Release Date":"10/1/1996",
    "Format":"MP3",
    "UPC (Barcode)":789368653621,
    "Label":"BIS",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jean Sibelius",
    "Title":"The Complete Symphonies 1",
    "Original Release Date":"6/13/1995",
    "Release Date":"6/13/1995",
    "Format":"CD",
    "UPC (Barcode)":028944615722,
    "Label":"Philips",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jean Sibelius",
    "Title":"The Complete Symphonies 2",
    "Original Release Date":"6/13/1995",
    "Release Date":"6/13/1995",
    "Format":"CD",
    "UPC (Barcode)":028944615722,
    "Label":"Philips",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jean Sibelius; Tempera Quartet",
    "Title":"Voces Intimae: String Quartets 1890-1922",
    "Original Release Date":"7/2/2007",
    "Release Date":"7/2/2007",
    "Format":"CD",
    "UPC (Barcode)":7318590014660,
    "Label":"BIS",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Jean Sibelius; Tempera Quartet",
    "Title":"Voces Intimae: String Quartets 1890-1922",
    "Original Release Date":"7/2/2007",
    "Release Date":"7/2/2007",
    "Format":"MP3",
    "UPC (Barcode)":7318590014660,
    "Label":"BIS",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sigur Rós",
    "Title":"Kveikur",
    "Original Release Date":"6/18/2013",
    "Release Date":"6/18/2013",
    "Format":"CD",
    "UPC (Barcode)":634904060626,
    "Label":"XL",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sigur Rós",
    "Title":"Valtari",
    "Original Release Date":"5/29/2012",
    "Release Date":"5/29/2012",
    "Format":"CD",
    "UPC (Barcode)":634904057022,
    "Label":"XL",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sigur Rós",
    "Title":"Með suð í eyrum við spilum endalaust",
    "Original Release Date":"6/24/2008",
    "Release Date":"6/24/2008",
    "Format":"CD",
    "UPC (Barcode)":634904036423,
    "Label":"XL",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sigur Rós",
    "Title":"Hvarf/Heim",
    "Original Release Date":"11/6/2007",
    "Release Date":"11/6/2007",
    "Format":"CD",
    "UPC (Barcode)":634904030728,
    "Label":"Geffen",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Sigur Rós",
    "Title":"Takk...",
    "Original Release Date":"9/13/2005",
    "Release Date":"9/13/2005",
    "Format":"CD",
    "UPC (Barcode)":602498845233,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sigur Rós",
    "Title":"( )",
    "Original Release Date":"10/29/2002",
    "Release Date":"10/29/2002",
    "Format":"CD",
    "UPC (Barcode)":008811309121,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sigur Rós",
    "Title":"Ágætis Byrjun",
    "Original Release Date":"5/22/2001",
    "Release Date":"5/22/2001",
    "Format":"CD",
    "UPC (Barcode)":766921859225,
    "Label":"Bad Taste",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Paul Simon",
    "Title":"Graceland (25th Anniversary Edition)",
    "Original Release Date":"8/12/1986",
    "Release Date":"6/5/2012",
    "Format":"CD",
    "UPC (Barcode)":886919147124,
    "Label":"Sony Legacy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Paul Simon",
    "Title":"The Rhythm Of The Saints (Remastered)",
    "Original Release Date":"10/16/1990",
    "Release Date":"10/24/2011",
    "Format":"CD",
    "UPC (Barcode)":886978988027,
    "Label":"Sony Legacy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Paul Simon",
    "Title":"Graceland (Remastered)",
    "Original Release Date":"8/12/1986",
    "Release Date":"10/24/2011",
    "Format":"CD",
    "UPC (Barcode)":886978425027,
    "Label":"Sony Legacy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Paul Simon",
    "Title":"Graceland",
    "Original Release Date":"8/12/1986",
    "Release Date":"4/22/1997",
    "Format":"CD",
    "UPC (Barcode)":093624643029,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Paul Simon",
    "Title":"The Rhythm of the Saints",
    "Original Release Date":"9/17/1990",
    "Release Date":"9/17/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075992609845,
    "Label":"Warner Bros.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Paul Simon",
    "Title":"Graceland",
    "Original Release Date":"8/12/1986",
    "Release Date":"8/12/1986",
    "Format":"LP",
    "UPC (Barcode)":075992544719,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Simple Minds",
    "Title":"Once Upon A Time",
    "Original Release Date":"10/21/1985",
    "Release Date":"6/10/2003",
    "Format":"CD",
    "UPC (Barcode)":724381301623,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Simple Minds",
    "Title":"Once Upon A Time",
    "Original Release Date":"10/21/1985",
    "Release Date":"10/21/1985",
    "Format":"LP",
    "UPC (Barcode)":075021509214,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Simple Minds",
    "Title":"Once Upon A Time",
    "Original Release Date":"10/1/1985",
    "Release Date":"10/1/1985",
    "Format":"MP3",
    "UPC (Barcode)":724381301623,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Simple Minds",
    "Title":"Alive & Kicking",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":075021278370,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Singer Songer",
    "Title":"Shoka Rinrin",
    "Original Release Date":"5/25/2005",
    "Release Date":"5/25/2005",
    "Format":"CD",
    "UPC (Barcode)":4988002478996,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Singer Songer",
    "Title":"Barairo Pop",
    "Original Release Date":"6/29/2005",
    "Release Date":"6/29/2005",
    "Format":"CD",
    "UPC (Barcode)":4988002479009,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Siouxsie & The Banshees",
    "Title":"The Best of Siouxsie & The Banshees",
    "Original Release Date":"11/12/2002",
    "Release Date":"11/12/2002",
    "Format":"CD",
    "UPC (Barcode)":044006515229,
    "Label":"Wonderland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Siouxsie & The Banshees",
    "Title":"Superstition",
    "Original Release Date":"6/11/1991",
    "Release Date":"6/11/1991",
    "Format":"Cassette",
    "UPC (Barcode)":720642438749,
    "Label":"Geffen",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Nadia Sirota",
    "Title":"Baroque",
    "Original Release Date":"3/26/2013",
    "Release Date":"3/26/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"New Amsterdam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Nadia Sirota",
    "Title":"First Things First",
    "Original Release Date":"5/19/2009",
    "Release Date":"5/19/2009",
    "Format":"CD",
    "UPC (Barcode)":884501130417,
    "Label":"New Amsterdam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Skeleton Key",
    "Title":"Obtainium",
    "Original Release Date":"6/25/2002",
    "Release Date":"6/25/2002",
    "Format":"CD",
    "UPC (Barcode)":689230002925,
    "Label":"Ipecac",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Skid Row",
    "Title":"Slave to the Grind",
    "Original Release Date":"6/11/1991",
    "Release Date":"6/11/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075678224249,
    "Label":"Atlantic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Sky/Neville Marriner",
    "Title":"The Mozart Album",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"Cassette",
    "UPC (Barcode)":042283290846,
    "Label":"Polygram",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Sleater-Kinney",
    "Title":"The Woods",
    "Original Release Date":"5/24/2005",
    "Release Date":"5/24/2005",
    "Format":"CD",
    "UPC (Barcode)":098787067927,
    "Label":"Sub Pop",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sleater-Kinney",
    "Title":"One Beat",
    "Original Release Date":"8/20/2002",
    "Release Date":"8/20/2002",
    "Format":"MP3",
    "UPC (Barcode)":759656038720,
    "Label":"Kill Rock Stars",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sleater-Kinney",
    "Title":"All Hands on the Bad One",
    "Original Release Date":"5/2/2000",
    "Release Date":"5/2/2000",
    "Format":"MP3",
    "UPC (Barcode)":759656036023,
    "Label":"Kill Rock Stars",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sleater-Kinney",
    "Title":"All Hands on the Bad One",
    "Original Release Date":"5/2/2000",
    "Release Date":"5/2/2000",
    "Format":"CD",
    "UPC (Barcode)":759656036023,
    "Label":"Kill Rock Stars",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sleater-Kinney",
    "Title":"The Hot Rock",
    "Original Release Date":"2/23/1999",
    "Release Date":"2/23/1999",
    "Format":"MP3",
    "UPC (Barcode)":759656032124,
    "Label":"Kill Rock Stars",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sleater-Kinney",
    "Title":"Dig Me Out",
    "Original Release Date":"4/8/1997",
    "Release Date":"4/8/1997",
    "Format":"MP3",
    "UPC (Barcode)":744861026921,
    "Label":"Matador",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sloth Love Chunks",
    "Title":"Shikakui Vision",
    "Original Release Date":"2/8/2006",
    "Release Date":"2/8/2006",
    "Format":"CD",
    "UPC (Barcode)":4571191280305,
    "Label":"NMNL",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Smashing Pumpkins",
    "Title":"Zeitgeist",
    "Original Release Date":"7/17/2007",
    "Release Date":"7/17/2007",
    "Format":"CD",
    "UPC (Barcode)":093624995944,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Smashing Pumpkins",
    "Title":"Rotten Apples - The Greatest Hits",
    "Original Release Date":"11/20/2001",
    "Release Date":"11/20/2001",
    "Format":"CD",
    "UPC (Barcode)":724381131824,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Smashing Pumpkins",
    "Title":"Machina/the machines of God",
    "Original Release Date":"2/29/2000",
    "Release Date":"2/29/2000",
    "Format":"CD",
    "UPC (Barcode)":724384893620,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Smashing Pumpkins",
    "Title":"Gish",
    "Original Release Date":"7/1/1991",
    "Release Date":"7/1/1991",
    "Format":"CD",
    "UPC (Barcode)":017046170529,
    "Label":"Caroline",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Smashing Pumpkins",
    "Title":"Gish",
    "Original Release Date":"1991",
    "Release Date":"1991",
    "Format":"Cassette",
    "UPC (Barcode)":017046170543,
    "Label":"Caroline",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Smashing Pumpkins",
    "Title":"Oceania",
    "Original Release Date":"6/19/2012",
    "Release Date":"6/19/2012",
    "Format":"CD",
    "UPC (Barcode)":818610010001,
    "Label":"Martha's Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Smashing Pumpkins",
    "Title":"Gish (Deluxe Edition)",
    "Original Release Date":"1991",
    "Release Date":"11/29/2011",
    "Format":"CD",
    "UPC (Barcode)":5099990959622,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Mindy Smith",
    "Title":"Stupid Love",
    "Original Release Date":"8/11/2009",
    "Release Date":"8/11/2009",
    "Format":"Stream",
    "UPC (Barcode)":015707985321,
    "Label":"Vanguard",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Mindy Smith",
    "Title":"Long Island Shores",
    "Original Release Date":"10/10/2006",
    "Release Date":"10/10/2006",
    "Format":"CD",
    "UPC (Barcode)":015707979726,
    "Label":"Vanguard",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Mindy Smith",
    "Title":"One Moment More",
    "Original Release Date":"1/27/2004",
    "Release Date":"1/27/2004",
    "Format":"CD",
    "UPC (Barcode)":015707973625,
    "Label":"Vanguard",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Smiths",
    "Title":"The Smiths Complete",
    "Original Release Date":"1984",
    "Release Date":"10/24/2011",
    "Format":"CD",
    "UPC (Barcode)":825646659074,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Smiths",
    "Title":"The Sound of the Smiths",
    "Original Release Date":"11/4/2008",
    "Release Date":"11/4/2008",
    "Format":"MP3",
    "UPC (Barcode)":603497976652,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Smiths",
    "Title":"The Sound of the Smiths",
    "Original Release Date":"11/4/2008",
    "Release Date":"11/4/2008",
    "Format":"Stream",
    "UPC (Barcode)":603497976652,
    "Label":"Rhino",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Smiths",
    "Title":"The Queen Is Dead",
    "Original Release Date":"6/23/1986",
    "Release Date":"1/31/2006",
    "Format":"CD",
    "UPC (Barcode)":9325583001239,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Smiths",
    "Title":"Strangeways, Here We Come",
    "Original Release Date":"9/28/1987",
    "Release Date":"9/28/1987",
    "Format":"CD",
    "UPC (Barcode)":075992564922,
    "Label":"Sire",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Smiths",
    "Title":"Strangeways, Here We Come",
    "Original Release Date":"9/28/1987",
    "Release Date":"9/28/1987",
    "Format":"MP3",
    "UPC (Barcode)":745099189921,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Smiths",
    "Title":"Meat Is Murder",
    "Original Release Date":"2/11/1985",
    "Release Date":"2/11/1985",
    "Format":"MP3",
    "UPC (Barcode)":075992526920,
    "Label":"Sire",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"So Percussion",
    "Title":"So Percussion",
    "Original Release Date":"5/11/2004",
    "Release Date":"5/11/2004",
    "Format":"MP3",
    "UPC (Barcode)":0713746300029,
    "Label":"Cantaloupe",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"So Percussion; Matmos",
    "Title":"Treasure State",
    "Original Release Date":"7/13/2010",
    "Release Date":"7/13/2010",
    "Format":"MP3",
    "UPC (Barcode)":713746306526,
    "Label":"Cantaloupe",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Solange",
    "Title":"True",
    "Original Release Date":"11/27/2012",
    "Release Date":"11/27/2012",
    "Format":"CD",
    "UPC (Barcode)":020286212508,
    "Label":"Terrible",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Solange",
    "Title":"True",
    "Original Release Date":"11/27/2012",
    "Release Date":"11/27/2012",
    "Format":"MP3",
    "UPC (Barcode)":020286212508,
    "Label":"Terrible",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Stephen Sondheim",
    "Title":"Sondheim Sings Vol. 1",
    "Original Release Date":"5/10/2005",
    "Release Date":"5/10/2005",
    "Format":"CD",
    "UPC (Barcode)":881692952927,
    "Label":"PS Classics Inc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sonic Youth",
    "Title":"Daydream Nation (Deluxe Edition)",
    "Original Release Date":"6/12/2007",
    "Release Date":"6/12/2007",
    "Format":"CD",
    "UPC (Barcode)":602517341128,
    "Label":"DGC",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sonic Youth",
    "Title":"Goo (Deluxe Edition)",
    "Original Release Date":"6/15/1990",
    "Release Date":"9/13/2005",
    "Format":"CD",
    "UPC (Barcode)":602498604939,
    "Label":"DGC",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sonic Youth",
    "Title":"Sister",
    "Original Release Date":"4/7/1987",
    "Release Date":"10/11/1994",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Squeaky Squawk",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sonic Youth",
    "Title":"Daydream Nation",
    "Original Release Date":"1988",
    "Release Date":"11/23/1993",
    "Format":"CD",
    "UPC (Barcode)":720642451526,
    "Label":"DGC",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sonic Youth",
    "Title":"Goo",
    "Original Release Date":"6/15/1990",
    "Release Date":"6/15/1990",
    "Format":"CD",
    "UPC (Barcode)":720642429723,
    "Label":"DGC",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sonic Youth",
    "Title":"Goo",
    "Original Release Date":"6/15/1990",
    "Release Date":"6/15/1990",
    "Format":"Cassette",
    "UPC (Barcode)":720642429747,
    "Label":"DGC",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"soulsberry",
    "Title":"Stone",
    "Original Release Date":"12/10/2003",
    "Release Date":"12/10/2003",
    "Format":"CD",
    "UPC (Barcode)":4988064853328,
    "Label":"Rhythm Republic",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"soulsberry",
    "Title":"The End Of Vacation",
    "Original Release Date":"5/23/2001",
    "Release Date":"5/23/2001",
    "Format":"CD",
    "UPC (Barcode)":4988064119219,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"soulsberry",
    "Title":"Soulsberry",
    "Original Release Date":"8/4/2000",
    "Release Date":"8/4/2000",
    "Format":"CD",
    "UPC (Barcode)":4519552100282,
    "Label":"01.com",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Soundgarden",
    "Title":"Badmotorfinger",
    "Original Release Date":"10/8/1991",
    "Release Date":"10/8/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075021537446,
    "Label":"A&M",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Spangle call Lilli line",
    "Title":"ISOLATION",
    "Original Release Date":"9/24/2008",
    "Release Date":"9/24/2008",
    "Format":"CD",
    "UPC (Barcode)":4544163460043,
    "Label":"felicity",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sam Sparro",
    "Title":"Return To Paradise",
    "Original Release Date":"12/4/2012",
    "Release Date":"12/4/2012",
    "Format":"MP3",
    "UPC (Barcode)":5099964442426,
    "Label":"EMI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sam Sparro",
    "Title":"Sam Sparro",
    "Original Release Date":"6/24/2008",
    "Release Date":"6/24/2008",
    "Format":"CD",
    "UPC (Barcode)":602517765863,
    "Label":"Universal Republic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Gregory Spears",
    "Title":"Requiem",
    "Original Release Date":"11/15/2011",
    "Release Date":"11/15/2011",
    "Format":"MP3",
    "UPC (Barcode)":884501596589,
    "Label":"New Amsterdam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Spiny Norman",
    "Title":"Fanning the Inferno",
    "Original Release Date":"1996",
    "Release Date":"1996",
    "Format":"CD",
    "UPC (Barcode)":704565000221,
    "Label":"Spiny Norman",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Spiny Norman",
    "Title":"Crust",
    "Original Release Date":"1993",
    "Release Date":"1993",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Hedgehog",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"SPITZ",
    "Title":"Sazanami CD",
    "Original Release Date":"10/10/2007",
    "Release Date":"10/10/2007",
    "Format":"MP3",
    "UPC (Barcode)":4988005489173,
    "Label":"Universal Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"SPITZ",
    "Title":"RECYCLE Greatest Hits of SPITZ",
    "Original Release Date":"12/21/1999",
    "Release Date":"12/21/1999",
    "Format":"CD",
    "UPC (Barcode)":4988005243447,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rick Springfield",
    "Title":"Jesse's Girl",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rick Springfield",
    "Title":"I've Done Everything for You",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rick Springfield",
    "Title":"Love Is Alright Tonite",
    "Original Release Date":"1981",
    "Release Date":"1981",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"RCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Spyro Gyra",
    "Title":"Stories Without Words",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":076742204617,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Spyro Gyra",
    "Title":"Catching the Sun",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"LP",
    "UPC (Barcode)":076732148716,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Carl Stalling",
    "Title":"The Carl Stalling Project",
    "Original Release Date":"7/17/1990",
    "Release Date":"7/17/1990",
    "Format":"MP3",
    "UPC (Barcode)":075992602723,
    "Label":"Warner Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Carl Stalling",
    "Title":"The Carl Stalling Project",
    "Original Release Date":"7/17/1990",
    "Release Date":"7/17/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075992602747,
    "Label":"Warner Bros.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Carl Stalling",
    "Title":"The Carl Stalling Project",
    "Original Release Date":"7/17/1990",
    "Release Date":"7/17/1990",
    "Format":"Stream",
    "UPC (Barcode)":075992602747,
    "Label":"Warner Bros.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Lisa Stansfield",
    "Title":"Affection",
    "Original Release Date":"2/20/1990",
    "Release Date":"2/7/2005",
    "Format":"CD",
    "UPC (Barcode)":828765437325,
    "Label":"Arista",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Lisa Stansfield",
    "Title":"Affection",
    "Original Release Date":"2/20/1990",
    "Release Date":"2/7/2005",
    "Format":"MP3",
    "UPC (Barcode)":828765437325,
    "Label":"Arista",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sufjan Stevens",
    "Title":"Illinois",
    "Original Release Date":"7/5/2005",
    "Release Date":"7/5/2005",
    "Format":"MP3",
    "UPC (Barcode)":656605892627,
    "Label":"Asthmatic Kitty",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"Songs From The Labyrinth",
    "Original Release Date":"10/10/2006",
    "Release Date":"10/10/2006",
    "Format":"CD",
    "UPC (Barcode)":602517051119,
    "Label":"Deutsche Grammophon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"Fields Of Gold - The Best Of Sting 1984-1994",
    "Original Release Date":"11/8/1994",
    "Release Date":"11/8/1994",
    "Format":"CD",
    "UPC (Barcode)":731454026925,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"The Soul Cages",
    "Original Release Date":"1/17/1991",
    "Release Date":"1/17/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075021640542,
    "Label":"A&M",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"... Nothing Like The Sun",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":075021640214,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"We'll Be Together",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"7 Inch",
    "UPC (Barcode)":075021298378,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"Be Still My Beating Heart",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"7 Inch",
    "UPC (Barcode)":075021299276,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"Englishman in New York",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"7 Inch",
    "UPC (Barcode)":075021120075,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"... Nothing Like The Sun",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":075021640221,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"... Nothing Like the Sun",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"Cassette",
    "UPC (Barcode)":082839640244,
    "Label":"A&M",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"The Dream of the Blue Turtles",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"LP",
    "UPC (Barcode)":075021375017,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"If You Love Somebody Set Them Free",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"Love Is the Seventh Wave",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"Russians",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":075021279971,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sting",
    "Title":"The Dream of the Blue Turtles",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"CD",
    "UPC (Barcode)":075021375024,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Karlheinz Stockhausen; Alex Fries; Schlagwerk Nordwest",
    "Title":"Tierkreis - 12 Melodien",
    "Original Release Date":"12/20/2006",
    "Release Date":"12/20/2006",
    "Format":"MP3",
    "UPC (Barcode)":4014513022943,
    "Label":"Antes (Bella Musica)",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Stone Roses",
    "Title":"The Stone Roses",
    "Original Release Date":"1989",
    "Release Date":"10/25/1990",
    "Format":"MP3",
    "UPC (Barcode)":012414118424,
    "Label":"Silvertone",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"STRAIGHTENER",
    "Title":"Lost World's Anthology",
    "Original Release Date":"1/21/2004",
    "Release Date":"1/21/2004",
    "Format":"CD",
    "UPC (Barcode)":4988006188990,
    "Label":"Capitol Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Igor Stravinsky",
    "Title":"Works of Igor Stravinksy",
    "Original Release Date":"7/23/2007",
    "Release Date":"7/23/2007",
    "Format":"CD",
    "UPC (Barcode)":886971031126,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Igor Stravinsky",
    "Title":"Le Sacre du Printemps / Symphony in Three Movements",
    "Original Release Date":"10/11/1991",
    "Release Date":"10/11/1991",
    "Format":"CD",
    "UPC (Barcode)":022924642029,
    "Label":"Teldec",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Sugar",
    "Title":"File Under Easy Listening",
    "Original Release Date":"9/6/1994",
    "Release Date":"9/6/1994",
    "Format":"MP3",
    "UPC (Barcode)":9399603125526,
    "Label":"Rykodisc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Sugarcubes",
    "Title":"The Great Crossover Potential",
    "Original Release Date":"7/14/1998",
    "Release Date":"7/14/1998",
    "Format":"CD",
    "UPC (Barcode)":075596210225,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Sugarcubes",
    "Title":"Regina",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"7 Inch",
    "UPC (Barcode)":075596927079,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Sugarcubes",
    "Title":"Life's Too Good",
    "Original Release Date":"5/31/1988",
    "Release Date":"5/31/1988",
    "Format":"LP",
    "UPC (Barcode)":075596080118,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Sugarcubes",
    "Title":"Life's Too Good",
    "Original Release Date":"5/31/1988",
    "Release Date":"5/31/1988",
    "Format":"CD",
    "UPC (Barcode)":075596080125,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Sugarcubes",
    "Title":"Life's Too Good",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"Cassette",
    "UPC (Barcode)":075596080149,
    "Label":"Elektra",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"SuiseiNoboAz",
    "Title":"THE (OVERUSED) END OF THE WORLD and I MISS YOU MUH-FUH",
    "Original Release Date":"7/6/2011",
    "Release Date":"7/6/2011",
    "Format":"CD",
    "UPC (Barcode)":4560124802912,
    "Label":"SuiseiNoRecord",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Sundays",
    "Title":"Reading, Writing And Arithmetic",
    "Original Release Date":"4/4/1990",
    "Release Date":"4/4/1990",
    "Format":"CD",
    "UPC (Barcode)":720642427729,
    "Label":"DGC",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Sundays",
    "Title":"Reading, Writing and Arithmetic",
    "Original Release Date":"4/16/1990",
    "Release Date":"4/16/1990",
    "Format":"Cassette",
    "UPC (Barcode)":720642427743,
    "Label":"DGC",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Suns Owl",
    "Title":"Recharged",
    "Original Release Date":"5/25/2001",
    "Release Date":"5/25/2001",
    "Format":"CD",
    "UPC (Barcode)":4524772000236,
    "Label":"Blood In Blood Out",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"SUPER JUNKY MONKEY",
    "Title":"Songs Are Our Universe",
    "Original Release Date":"7/20/2001",
    "Release Date":"7/20/2001",
    "Format":"CD",
    "UPC (Barcode)":4948722082255,
    "Label":"Condor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"SUPER JUNKY MONKEY",
    "Title":"Parasitic People-Chikyuukiseijin",
    "Original Release Date":"4/12/1996",
    "Release Date":"4/12/1996",
    "Format":"CD",
    "UPC (Barcode)":4988009348421,
    "Label":"Sony Music Entertainment",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"SUPER JUNKY MONKEY",
    "Title":"SCREW UP",
    "Original Release Date":"10/21/1994",
    "Release Date":"10/21/1994",
    "Format":"CD",
    "UPC (Barcode)":766923501528,
    "Label":"Sony Music Entertainment",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"SUPERCAR",
    "Title":"Answer",
    "Original Release Date":"2/18/2004",
    "Release Date":"2/18/2004",
    "Format":"CD",
    "UPC (Barcode)":4582117982487,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"SUPERCAR",
    "Title":"16/50 1997-1999",
    "Original Release Date":"2/14/2003",
    "Release Date":"2/14/2003",
    "Format":"CD",
    "UPC (Barcode)":4582117981190,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"SUPERCAR",
    "Title":"Highvision",
    "Original Release Date":"4/24/2002",
    "Release Date":"4/24/2002",
    "Format":"CD",
    "UPC (Barcode)":4582117980469,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"SUPERCAR",
    "Title":"Futurama",
    "Original Release Date":"11/22/2000",
    "Release Date":"11/22/2000",
    "Format":"CD",
    "UPC (Barcode)":4988009036205,
    "Label":"Ki/oon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Swing Out Sister",
    "Title":"It's Better to Travel",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":042283221314,
    "Label":"Polygram",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Swing Out Sister",
    "Title":"It's Better to Travel",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":042283221321,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The System",
    "Title":"Don't Disturb This Groove",
    "Original Release Date":"1987",
    "Release Date":"10/25/1990",
    "Format":"MP3",
    "UPC (Barcode)":075678169168,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The System",
    "Title":"Don't Disturb This Groove",
    "Original Release Date":"1987",
    "Release Date":"10/25/1990",
    "Format":"Stream",
    "UPC (Barcode)":075678169168,
    "Label":"Atlantic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Ta Mara & the Seen",
    "Title":"Everybody Dance",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Joby Talbot",
    "Title":"The Dying Swan",
    "Original Release Date":"1/21/2003",
    "Release Date":"1/21/2003",
    "Format":"MP3",
    "UPC (Barcode)":680125107824,
    "Label":"Black Box Classics",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Talking Heads",
    "Title":"The Best Of Talking Heads",
    "Original Release Date":"8/17/2004",
    "Release Date":"8/17/2004",
    "Format":"CD",
    "UPC (Barcode)":081227648824,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Talking Heads",
    "Title":"Wild Wild Life",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Talking Heads",
    "Title":"Remain In Light",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"CD",
    "UPC (Barcode)":07599260951,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Talking Heads",
    "Title":"Remain In Light",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"MP3",
    "UPC (Barcode)":07599260951,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Talking Heads",
    "Title":"Fear Of Music",
    "Original Release Date":"8/3/1979",
    "Release Date":"8/3/1979",
    "Format":"CD",
    "UPC (Barcode)":075992742825,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Talking Heads",
    "Title":"Talking Heads '77",
    "Original Release Date":"1977",
    "Release Date":"1977",
    "Format":"MP3",
    "UPC (Barcode)":007599274232,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Thomas Tallis",
    "Title":"Spem in Alium",
    "Original Release Date":"2/28/2006",
    "Release Date":"2/28/2006",
    "Format":"MP3",
    "UPC (Barcode)":635212007129,
    "Label":"Signum UK",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"A Taste of Honey",
    "Title":"Sukiyaki",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Andy Taylor",
    "Title":"Lola",
    "Original Release Date":"1990",
    "Release Date":"1990",
    "Format":"7 Inch",
    "UPC (Barcode)":082839059978,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Andy Taylor",
    "Title":"Thunder",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":076732583722,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Andy Taylor",
    "Title":"Take It Easy",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"12 Inch",
    "UPC (Barcode)":null,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Andy Taylor",
    "Title":"Take It Easy",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Atlantic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Taylor",
    "Title":"Auto Didact",
    "Original Release Date":"1997",
    "Release Date":"1997",
    "Format":"CD",
    "UPC (Barcode)":646333000227,
    "Label":"B5 Atomic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Taylor",
    "Title":"Feelings Are Good And Other Lies",
    "Original Release Date":"2/18/1997",
    "Release Date":"2/18/1997",
    "Format":"CD",
    "UPC (Barcode)":625928009326,
    "Label":"B5 Atomic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tears For Fears",
    "Title":"Songs From The Big Chair (Deluxe Edition)",
    "Original Release Date":"2/17/1985",
    "Release Date":"5/23/2006",
    "Format":"CD",
    "UPC (Barcode)":602498391426,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tears For Fears",
    "Title":"Tears Roll Down (Greatest Hits 82-92)",
    "Original Release Date":"3/17/1992",
    "Release Date":"12/2/2003",
    "Format":"CD",
    "UPC (Barcode)":731451093920,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tears For Fears",
    "Title":"The Hurting (remastered)",
    "Original Release Date":"3/25/1983",
    "Release Date":"3/13/2001",
    "Format":"CD",
    "UPC (Barcode)":731455810424,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tears For Fears",
    "Title":"Songs From The Big Chair (remastered)",
    "Original Release Date":"2/17/1985",
    "Release Date":"3/13/2001",
    "Format":"CD",
    "UPC (Barcode)":731455810622,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tears For Fears",
    "Title":"The Seeds Of Love (remastered)",
    "Original Release Date":"9/15/1989",
    "Release Date":"5/24/1999",
    "Format":"CD",
    "UPC (Barcode)":731455810523,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tears For Fears",
    "Title":"Raoul and the Kings of Spain",
    "Original Release Date":"10/10/1995",
    "Release Date":"10/10/1995",
    "Format":"CD",
    "UPC (Barcode)":074646731826,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tears For Fears",
    "Title":"Elemental",
    "Original Release Date":"6/22/1993",
    "Release Date":"6/22/1993",
    "Format":"CD",
    "UPC (Barcode)":731451487521,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tears For Fears",
    "Title":"The Seeds of Love",
    "Original Release Date":"9/15/1989",
    "Release Date":"9/15/1989",
    "Format":"LP",
    "UPC (Barcode)":042283873018,
    "Label":"Fontana",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tears For Fears",
    "Title":"Songs From the Big Chair",
    "Original Release Date":"2/27/1985",
    "Release Date":"2/27/1985",
    "Format":"LP",
    "UPC (Barcode)":042282430014,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tears For Fears",
    "Title":"Head Over Heels",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tears For Fears",
    "Title":"The Hurting",
    "Original Release Date":"3/25/1983",
    "Release Date":"3/25/1983",
    "Format":"LP",
    "UPC (Barcode)":042281103919,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Technotronic",
    "Title":"Pump Up the Jam",
    "Original Release Date":"11/8/1989",
    "Release Date":"11/8/1989",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Technotronic",
    "Title":"Pump Up the Jam",
    "Original Release Date":"11/8/1989",
    "Release Date":"11/8/1989",
    "Format":"MP3",
    "UPC (Barcode)":077779342242,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Technotronic",
    "Title":"Pump Up the Jam",
    "Original Release Date":"11/8/1989",
    "Release Date":"11/8/1989",
    "Format":"Cassette",
    "UPC (Barcode)":077779342242,
    "Label":"Capitol",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Texas",
    "Title":"I Don't Want a Lover",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"7 Inch",
    "UPC (Barcode)":042287235072,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Thee Michelle Gun Elephant",
    "Title":"Rodeo Tandem Beat Specter",
    "Original Release Date":"11/19/2002",
    "Release Date":"11/19/2002",
    "Format":"CD",
    "UPC (Barcode)":095081004528,
    "Label":"Alive/Total Energy",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Ali Thomson",
    "Title":"Take a Little Rhythm",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tracey Thorn",
    "Title":"Love and Its Opposite",
    "Original Release Date":"5/18/2010",
    "Release Date":"5/18/2010",
    "Format":"MP3",
    "UPC (Barcode)":893468002989,
    "Label":"Merge",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Throwing Muses",
    "Title":"House Tornado",
    "Original Release Date":"1988",
    "Release Date":"11/7/2006",
    "Format":"CD",
    "UPC (Barcode)":664140571020,
    "Label":"Wounded Bird",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Throwing Muses",
    "Title":"Throwing Muses",
    "Original Release Date":"3/4/2003",
    "Release Date":"3/4/2003",
    "Format":"MP3",
    "UPC (Barcode)":652637230123,
    "Label":"4AD",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Throwing Muses",
    "Title":"University",
    "Original Release Date":"1/17/1995",
    "Release Date":"1/17/1995",
    "Format":"CD",
    "UPC (Barcode)":093624579625,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Throwing Muses",
    "Title":"The Real Ramona",
    "Original Release Date":"3/12/1991",
    "Release Date":"3/12/1991",
    "Format":"CD",
    "UPC (Barcode)":075992648929,
    "Label":"Sire",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Throwing Muses",
    "Title":"The Real Ramona",
    "Original Release Date":"3/12/1991",
    "Release Date":"3/12/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075992648943,
    "Label":"Sire",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"TLC",
    "Title":"Now And Forever: The Hits",
    "Original Release Date":"6/21/2005",
    "Release Date":"6/21/2005",
    "Format":"CD",
    "UPC (Barcode)":828765020824,
    "Label":"La Face",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"TLC",
    "Title":"FanMail",
    "Original Release Date":"2/23/1999",
    "Release Date":"2/23/1999",
    "Format":"CD",
    "UPC (Barcode)":730082605526,
    "Label":"La Face",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"TLC",
    "Title":"3D",
    "Original Release Date":"2/23/1999",
    "Release Date":"2/23/1999",
    "Format":"CD",
    "UPC (Barcode)":078221478021,
    "Label":"La Face",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"toddle",
    "Title":"dawn praise the world",
    "Original Release Date":"6/8/2007",
    "Release Date":"6/8/2007",
    "Format":"CD",
    "UPC (Barcode)":4988044610804,
    "Label":"world wide waddle",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"toddle",
    "Title":"I dedicate D chord",
    "Original Release Date":"9/25/2005",
    "Release Date":"2005",
    "Format":"CD",
    "UPC (Barcode)":4988044250093,
    "Label":"world wide waddle",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"TOKIO",
    "Title":"Amagasa / Akireru Kurai Bokura wa Negaou",
    "Original Release Date":"9/3/2008",
    "Release Date":"9/3/2008",
    "Format":"CD",
    "UPC (Barcode)":4580117621443,
    "Label":"J-Storm",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Shinyawaku",
    "Original Release Date":"8/29/2012",
    "Release Date":"8/29/2012",
    "Format":"CD",
    "UPC (Barcode)":4988006232389,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Tokyo Collection",
    "Original Release Date":"2/15/2012",
    "Release Date":"2/15/2012",
    "Format":"CD",
    "UPC (Barcode)":4988006230101,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"color bars",
    "Original Release Date":"1/18/2012",
    "Release Date":"1/18/2012",
    "Format":"CD",
    "UPC (Barcode)":498800622960,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Daihakken",
    "Original Release Date":"6/29/2011",
    "Release Date":"6/29/2011",
    "Format":"CD",
    "UPC (Barcode)":4988006227439,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Sora ga Natteiru / Onna no Ko Dare demo",
    "Original Release Date":"5/3/2011",
    "Release Date":"5/11/2011",
    "Format":"CD",
    "UPC (Barcode)":4988006226128,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Sports",
    "Original Release Date":"2/24/2010",
    "Release Date":"2/24/2010",
    "Format":"CD",
    "UPC (Barcode)":4988006222410,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Noudoteki Sanpunkan",
    "Original Release Date":"12/2/2009",
    "Release Date":"12/2/2009",
    "Format":"CD",
    "UPC (Barcode)":4988006222175,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"OSCA",
    "Original Release Date":"7/11/2007",
    "Release Date":"7/11/2007",
    "Format":"CD",
    "UPC (Barcode)":4988006211216,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Killer Tune",
    "Original Release Date":"9/3/2007",
    "Release Date":"9/3/2007",
    "Format":"CD",
    "UPC (Barcode)":4988006211674,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Goraku (Variety)",
    "Original Release Date":"9/26/2007",
    "Release Date":"9/26/2007",
    "Format":"CD",
    "UPC (Barcode)":4988006212213,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Otona (Adult)",
    "Original Release Date":"1/25/2006",
    "Release Date":"1/25/2006",
    "Format":"CD",
    "UPC (Barcode)":4988006203549,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Shuraba",
    "Original Release Date":"11/2/2005",
    "Release Date":"11/2/2005",
    "Format":"CD",
    "UPC (Barcode)":4988006202399,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Gunjyou Biyori",
    "Original Release Date":"9/8/2004",
    "Release Date":"9/8/2004",
    "Format":"CD",
    "UPC (Barcode)":4988006193093,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Soonan",
    "Original Release Date":"10/20/2004",
    "Release Date":"10/20/2004",
    "Format":"CD",
    "UPC (Barcode)":4988006193123,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tokyo Jihen",
    "Title":"Kyooiku",
    "Original Release Date":"11/25/2004",
    "Release Date":"11/25/2004",
    "Format":"CD",
    "UPC (Barcode)":4988006193130,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tommy heavenly6",
    "Title":"Heavy Starry Heavenly",
    "Original Release Date":"3/7/2007",
    "Release Date":"3/7/2007",
    "Format":"CD",
    "UPC (Barcode)":4562104043704,
    "Label":"DefStar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tommy heavenly6",
    "Title":"Tommy heavenly6",
    "Original Release Date":"8/24/2005",
    "Release Date":"8/24/2005",
    "Format":"CD",
    "UPC (Barcode)":4562104042134,
    "Label":"DefStar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tomosaka Rie",
    "Title":"Toridori",
    "Original Release Date":"6/24/2009",
    "Release Date":"6/24/2009",
    "Format":"CD",
    "UPC (Barcode)":4988006220430,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tomosaka Rie",
    "Title":"Shoujo Robot",
    "Original Release Date":"6/21/2000",
    "Release Date":"6/21/2000",
    "Format":"CD",
    "UPC (Barcode)":4988006165786,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tone Deaf Teens",
    "Title":"1%",
    "Original Release Date":"1997",
    "Release Date":"1997",
    "Format":"CD",
    "UPC (Barcode)":701553010224,
    "Label":"Crash the Luau",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tone Deaf Teens",
    "Title":"Fixed",
    "Original Release Date":"1995",
    "Release Date":"1995",
    "Format":"CD",
    "UPC (Barcode)":701553000126,
    "Label":"Zero-Sum",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Torche",
    "Title":"Harmonicraft",
    "Original Release Date":"4/24/2012",
    "Release Date":"4/24/2012",
    "Format":"MP3",
    "UPC (Barcode)":689640292923,
    "Label":"Volcom",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"TOUMING MAGAZINE",
    "Title":"TOUMING MAGAZINE FOREVER",
    "Original Release Date":"7/18/2012",
    "Release Date":"7/18/2012",
    "Format":"CD",
    "UPC (Barcode)":4988006231733,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"TOUMING MAGAZINE",
    "Title":"Bokutachi no Soul Music",
    "Original Release Date":"10/15/2010",
    "Release Date":"10/15/2010",
    "Format":"CD",
    "UPC (Barcode)":4715104081478,
    "Label":"Chnging",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Joan Tower",
    "Title":"Made in America",
    "Original Release Date":"5/29/2007",
    "Release Date":"5/29/2007",
    "Format":"MP3",
    "UPC (Barcode)":636943932827,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Joan Tower",
    "Title":"Silver Ladders/Island Prelude/Music for Cello and Orchestra/Sequoia",
    "Original Release Date":"8/7/1990",
    "Release Date":"5/28/1992",
    "Format":"CD",
    "UPC (Barcode)":075597924527,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Joan Tower",
    "Title":"Silver Ladders/Island Prelude/Music for Cello and Orchestra/Sequoia",
    "Original Release Date":"8/7/1990",
    "Release Date":"8/7/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075597924541,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Triumph The Insult Comic Dog",
    "Title":"Come Poop With Me",
    "Original Release Date":"11/4/2003",
    "Release Date":"11/4/2003",
    "Format":"CD",
    "UPC (Barcode)":093624832829,
    "Label":"Warner Bros.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Troubled Hubble",
    "Title":"Penturbia",
    "Original Release Date":"7/11/2006",
    "Release Date":"7/11/2006",
    "Format":"MP3",
    "UPC (Barcode)":825576812327,
    "Label":"Latest Flame",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tsu Shi Ma Mi Re",
    "Title":"Soozoo Ninshin",
    "Original Release Date":"8/25/2004",
    "Release Date":"8/25/2004",
    "Format":"CD",
    "UPC (Barcode)":4514306007527,
    "Label":"Benten",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tina Turner",
    "Title":"Private Dancer",
    "Original Release Date":"1984",
    "Release Date":"3/14/2000",
    "Format":"MP3",
    "UPC (Barcode)":724385583322,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tina Turner",
    "Title":"One of the Living",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Turtle Island String Quartet",
    "Title":"Skylife",
    "Original Release Date":"7/18/1990",
    "Release Date":"7/18/1990",
    "Format":"Cassette",
    "UPC (Barcode)":019341012641,
    "Label":"Windham Hill",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"TV Mania",
    "Title":"Bored With Prozac And The Internet?",
    "Original Release Date":"3/12/2013",
    "Release Date":"3/12/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"TapeModern",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Achtung Baby (20th Anniversary Deluxe Edition)",
    "Original Release Date":"11/26/1991",
    "Release Date":"11/1/2011",
    "Format":"CD",
    "UPC (Barcode)":602527788265,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"No Line on the Horizon",
    "Original Release Date":"3/3/2009",
    "Release Date":"3/3/2009",
    "Format":"CD",
    "UPC (Barcode)":602517960374,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"The Unforgettable Fire",
    "Original Release Date":"1984",
    "Release Date":"10/26/2009",
    "Format":"CD",
    "UPC (Barcode)":602517924017,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Boy (Deluxe Edition)",
    "Original Release Date":"10/20/1980",
    "Release Date":"7/22/2008",
    "Format":"CD",
    "UPC (Barcode)":602517616707,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"October (Deluxe Edition)",
    "Original Release Date":"10/16/1981",
    "Release Date":"7/22/2008",
    "Format":"CD",
    "UPC (Barcode)":602517641938,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"War (Remastered)",
    "Original Release Date":"2/28/1983",
    "Release Date":"7/22/2008",
    "Format":"CD",
    "UPC (Barcode)":602517646476,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Live Under a Blood Red Sky",
    "Original Release Date":"1983",
    "Release Date":"9/30/2008",
    "Format":"CD",
    "UPC (Barcode)":602517642867,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"The Joshua Tree - 20th Anniversary Deluxe Edition",
    "Original Release Date":"3/9/1987",
    "Release Date":"11/20/2007",
    "Format":"CD",
    "UPC (Barcode)":602517509474,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"The Best Of 1990-2000",
    "Original Release Date":"5/24/2004",
    "Release Date":"5/24/2004",
    "Format":"CD",
    "UPC (Barcode)":044006343808,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"How To Dismantle An Atomic Bomb",
    "Original Release Date":"11/23/2004",
    "Release Date":"11/23/2004",
    "Format":"CD",
    "UPC (Barcode)":602498678299,
    "Label":"Interscope",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"All That You Can't Leave Behind",
    "Original Release Date":"10/31/2000",
    "Release Date":"10/31/2000",
    "Format":"CD",
    "UPC (Barcode)":731452465320,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"The Best Of 1980-1990",
    "Original Release Date":"11/3/1998",
    "Release Date":"11/3/1998",
    "Format":"CD",
    "UPC (Barcode)":731452461223,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Zooropa",
    "Original Release Date":"7/6/1993",
    "Release Date":"7/6/1993",
    "Format":"CD",
    "UPC (Barcode)":731451804724,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Achtung Baby",
    "Original Release Date":"11/19/1991",
    "Release Date":"11/19/1991",
    "Format":"CD",
    "UPC (Barcode)":731451034725,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Achtung Baby",
    "Original Release Date":"1991",
    "Release Date":"1991",
    "Format":"Cassette",
    "UPC (Barcode)":731451034749,
    "Label":"Island",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"U2",
    "Title":"The Joshua Tree",
    "Original Release Date":"3/9/1987",
    "Release Date":"6/15/1990",
    "Format":"CD",
    "UPC (Barcode)":042284229821,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Boy",
    "Original Release Date":"10/20/1980",
    "Release Date":"4/30/1990",
    "Format":"CD",
    "UPC (Barcode)":042284229623,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"October",
    "Original Release Date":"10/16/1981",
    "Release Date":"6/15/1990",
    "Format":"CD",
    "UPC (Barcode)":042284229722,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Rattle and Hum",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":075679100313,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Angel of Harlem",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"7 Inch",
    "UPC (Barcode)":075679925473,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"The Joshua Tree",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":075679058119,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"With or Without You",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"7 Inch",
    "UPC (Barcode)":075679946973,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"I Still Haven't Found What I'm Looking For",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"7 Inch",
    "UPC (Barcode)":075679943071,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Where The Streets Have No Name",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"7 Inch",
    "UPC (Barcode)":075679940872,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"I Still Haven't Found What I'm Looking For",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":4007196649873,
    "Label":"Island",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"U2",
    "Title":"With or Without You",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":4007196649866,
    "Label":"Island",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Where The Streets Have No Name",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":4007196649880,
    "Label":"Island",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Wide Awake in America",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"Cassette",
    "UPC (Barcode)":042284247948,
    "Label":"Island",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Wide Awake in America",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"Stream",
    "UPC (Barcode)":042284247948,
    "Label":"Island",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"U2",
    "Title":"The Unforgettable Fire",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"LP",
    "UPC (Barcode)":075679023117,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"War",
    "Original Release Date":"2/28/1983",
    "Release Date":"2/28/1983",
    "Format":"LP",
    "UPC (Barcode)":075679006714,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"U2",
    "Title":"Boy",
    "Original Release Date":"10/20/1980",
    "Release Date":"10/20/1980",
    "Format":"LP",
    "UPC (Barcode)":075679004017,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"KABA",
    "Original Release Date":"6/23/2010",
    "Release Date":"6/23/2010",
    "Format":"CD",
    "UPC (Barcode)":4988002597376,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"ATTA",
    "Original Release Date":"7/22/2009",
    "Release Date":"7/22/2009",
    "Format":"CD",
    "UPC (Barcode)":4988002576258,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"Golden green",
    "Original Release Date":"6/20/2007",
    "Release Date":"6/20/2007",
    "Format":"CD",
    "UPC (Barcode)":4988002527700,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"Breathe",
    "Original Release Date":"3/30/2005",
    "Release Date":"3/30/2005",
    "Format":"CD",
    "UPC (Barcode)":4988002476282,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"Sun",
    "Original Release Date":"3/24/2004",
    "Release Date":"3/24/2004",
    "Format":"CD",
    "UPC (Barcode)":4988002457618,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"Uta UUA",
    "Original Release Date":"3/13/2004",
    "Release Date":"3/13/2004",
    "Format":"CD",
    "UPC (Barcode)":4988002456963,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"Senkoo",
    "Original Release Date":"7/24/2002",
    "Release Date":"7/24/2002",
    "Format":"CD",
    "UPC (Barcode)":4988002433254,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"Doroboo",
    "Original Release Date":"9/19/2002",
    "Release Date":"9/19/2002",
    "Format":"CD",
    "UPC (Barcode)":4988002436927,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"turbo",
    "Original Release Date":"10/27/1999",
    "Release Date":"10/27/1999",
    "Format":"CD",
    "UPC (Barcode)":4988002392322,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"Ametora",
    "Original Release Date":"4/22/1998",
    "Release Date":"4/22/1998",
    "Format":"CD",
    "UPC (Barcode)":4988002365883,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"Fine Feathers Make Fine Birds",
    "Original Release Date":"4/23/1997",
    "Release Date":"4/23/1997",
    "Format":"CD",
    "UPC (Barcode)":4988002350650,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"11",
    "Original Release Date":"10/23/1996",
    "Release Date":"10/23/1996",
    "Format":"CD",
    "UPC (Barcode)":4988002341115,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA",
    "Title":"PETIT",
    "Original Release Date":"10/21/1995",
    "Release Date":"10/21/1995",
    "Format":"CD",
    "UPC (Barcode)":4988002324842,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"UA x Kikuchi Naruyoshi",
    "Title":"cure jazz",
    "Original Release Date":"7/19/2006",
    "Release Date":"7/19/2006",
    "Format":"CD",
    "UPC (Barcode)":4988002508518,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Uh Huh Her",
    "Title":"Common Reaction",
    "Original Release Date":"8/19/2008",
    "Release Date":"8/19/2008",
    "Format":"CD",
    "UPC (Barcode)":067003079723,
    "Label":"Nettwerk",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Uh Huh Her",
    "Title":"Common Reaction",
    "Original Release Date":"8/19/2008",
    "Release Date":"8/19/2008",
    "Format":"MP3",
    "UPC (Barcode)":067003079723,
    "Label":"Nettwerk",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Tracey Ullman",
    "Title":"You Broke My Heart in 17 Places",
    "Original Release Date":"4/3/2007",
    "Release Date":"4/3/2007",
    "Format":"MP3",
    "UPC (Barcode)":827912060553,
    "Label":"Stiff Records UK",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Uncle Tupelo",
    "Title":"No Depression",
    "Original Release Date":"1990",
    "Release Date":"4/15/2003",
    "Format":"CD",
    "UPC (Barcode)":696998642720,
    "Label":"Sony Legacy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"unkie",
    "Title":"FOREST VAMP",
    "Original Release Date":"5/8/2013",
    "Release Date":"5/8/2013",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Daphne",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"unkie",
    "Title":"too many secrets",
    "Original Release Date":"10/22/2008",
    "Release Date":"10/22/2008",
    "Format":"CD",
    "UPC (Barcode)":4580245400217,
    "Label":"Zealot/Addict",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"unkie",
    "Title":"the Price of Fame",
    "Original Release Date":"7/11/2007",
    "Release Date":"7/11/2007",
    "Format":"CD",
    "UPC (Barcode)":4571157546339,
    "Label":"Zealot/Addict",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dawn Upshaw",
    "Title":"White Moon: Songs to Morpheus",
    "Original Release Date":"2/27/1996",
    "Release Date":"2/27/1996",
    "Format":"CD",
    "UPC (Barcode)":075597936421,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Dawn Upshaw",
    "Title":"I Wish it So",
    "Original Release Date":"8/2/1994",
    "Release Date":"8/2/1994",
    "Format":"CD",
    "UPC (Barcode)":075597934526,
    "Label":"Nonesuch",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Dawn Upshaw",
    "Title":"The Girl with Orange Lips",
    "Original Release Date":"8/2/1991",
    "Release Date":"8/2/1991",
    "Format":"CD",
    "UPC (Barcode)":075597926224,
    "Label":"Nonesuch",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"Sakura Nagashi",
    "Original Release Date":"11/16/2012",
    "Release Date":"11/16/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"Single Collection Vol. 2",
    "Original Release Date":"11/24/2010",
    "Release Date":"11/24/2010",
    "Format":"CD",
    "UPC (Barcode)":4988006225596,
    "Label":"EMI Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"This Is The One",
    "Original Release Date":"5/12/2009",
    "Release Date":"5/12/2009",
    "Format":"CD",
    "UPC (Barcode)":602527065953,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"This Is the One",
    "Original Release Date":"3/24/2009",
    "Release Date":"3/24/2009",
    "Format":"Stream",
    "UPC (Barcode)":602527065953,
    "Label":"Island",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"Heart Station",
    "Original Release Date":"3/19/2008",
    "Release Date":"3/19/2008",
    "Format":"CD",
    "UPC (Barcode)":4988006214842,
    "Label":"Foozay Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"Fly Me to the Moon -2007 REMIX-",
    "Original Release Date":"6/27/2007",
    "Release Date":"6/27/2007",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Car W.S.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"Ultra Blue",
    "Original Release Date":"6/14/2006",
    "Release Date":"6/14/2006",
    "Format":"CD",
    "UPC (Barcode)":4988006206069,
    "Label":"Foozay Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"Be My Last",
    "Original Release Date":"9/28/2005",
    "Release Date":"2005",
    "Format":"CD",
    "UPC (Barcode)":4988006201422,
    "Label":"Foozay Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"Exodus",
    "Original Release Date":"10/5/2004",
    "Release Date":"10/5/2004",
    "Format":"CD",
    "UPC (Barcode)":602498631621,
    "Label":"Island",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"Deep River",
    "Original Release Date":"6/19/2002",
    "Release Date":"6/19/2002",
    "Format":"CD",
    "UPC (Barcode)":4988006178991,
    "Label":"Foozay Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"Distance",
    "Original Release Date":"3/28/2001",
    "Release Date":"3/28/2001",
    "Format":"CD",
    "UPC (Barcode)":4988006172098,
    "Label":"Foozay Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"Addicted to You",
    "Original Release Date":"11/10/1999",
    "Release Date":"11/10/1999",
    "Format":"CD",
    "UPC (Barcode)":4988006164673,
    "Label":"Foozay Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Utada Hikaru",
    "Title":"First Love",
    "Original Release Date":"3/10/1999",
    "Release Date":"3/10/1999",
    "Format":"CD",
    "UPC (Barcode)":4988006158764,
    "Label":"Foozay Music",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Van Tomiko",
    "Title":"Van.",
    "Original Release Date":"12/10/2008",
    "Release Date":"12/10/2008",
    "Format":"CD",
    "UPC (Barcode)":4988064237128,
    "Label":"Avex Trax",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Vaquero",
    "Title":"Vaquero",
    "Original Release Date":"8/25/2005",
    "Release Date":"8/25/2005",
    "Format":"MP3",
    "UPC (Barcode)":634479159343,
    "Label":"EMI Latin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan Nite Sampler 2011",
    "Original Release Date":"3/19/2011",
    "Release Date":"3/19/2011",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan Nite Sound Sampler 2010",
    "Original Release Date":"3/19/2010",
    "Release Date":"3/19/2010",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan Nite Sound Sampler 2009",
    "Original Release Date":"3/20/2009",
    "Release Date":"3/20/2009",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"It Came From Japan",
    "Original Release Date":"3/20/2009",
    "Release Date":"3/20/2009",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Flightpath",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Flower Dance: Japanese Folk Melodies",
    "Original Release Date":"1969",
    "Release Date":"10/28/2008",
    "Format":"MP3",
    "UPC (Barcode)":075597993783,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Bali: Music for the Shadow Play",
    "Original Release Date":"1970",
    "Release Date":"12/1/2007",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan Nite Sound Sampler 2007",
    "Original Release Date":"3/14/2007",
    "Release Date":"3/14/2007",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Matsuri - Japan Bash Party 2007",
    "Original Release Date":"3/14/2007",
    "Release Date":"3/14/2007",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Music With a Twist: Revolutions",
    "Original Release Date":"5/15/2007",
    "Release Date":"5/15/2007",
    "Format":"CD",
    "UPC (Barcode)":886970039529,
    "Label":"Music with a Twist",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"SXSW 2006 Japan Music Now: New Sounds From Way Out East",
    "Original Release Date":"3/15/2006",
    "Release Date":"3/15/2006",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"SXSW 2005 Japan Music Now Sound Sampler",
    "Original Release Date":"3/16/2005",
    "Release Date":"3/16/2005",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan Nite Sound Sampler 2004",
    "Original Release Date":"2004",
    "Release Date":"2004",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan For Sale Vol. 4",
    "Original Release Date":"10/5/2004",
    "Release Date":"10/5/2004",
    "Format":"CD",
    "UPC (Barcode)":828915001420,
    "Label":"Tofu",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan For Sale Vol. 3",
    "Original Release Date":"3/25/2003",
    "Release Date":"3/25/2003",
    "Format":"CD",
    "UPC (Barcode)":5099751086123,
    "Label":"Sony Music Imports",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan Nite Sound Sampler 2003",
    "Original Release Date":"2003",
    "Release Date":"2003",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan For Sale Vol. 2",
    "Original Release Date":"3/26/2002",
    "Release Date":"3/26/2002",
    "Format":"CD",
    "UPC (Barcode)":5099750630129,
    "Label":"Sony Music Imports",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan Nite Sound Sampler 2002",
    "Original Release Date":"2002",
    "Release Date":"2002",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan For Sale",
    "Original Release Date":"3/27/2001",
    "Release Date":"3/27/2001",
    "Format":"CD",
    "UPC (Barcode)":667340881920,
    "Label":"Sony Music Entertainment",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan Nite Sound Sampler 2001",
    "Original Release Date":"2001",
    "Release Date":"2001",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan Nite Sound Sampler 2000",
    "Original Release Date":"2000",
    "Release Date":"2000",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"The Peek-a-Boo Book of Spells",
    "Original Release Date":"11/27/2000",
    "Release Date":"11/27/2000",
    "Format":"LP",
    "UPC (Barcode)":5031556327469,
    "Label":"Peek-a-Boo",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Supernova Volume1~Uni~",
    "Original Release Date":"1/20/1999",
    "Release Date":"1/20/1999",
    "Format":"CD",
    "UPC (Barcode)":4988006157125,
    "Label":"Parlophone Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan Nite Sound Sampler 1999",
    "Original Release Date":"1999",
    "Release Date":"1999",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"For The Masses",
    "Original Release Date":"8/4/1998",
    "Release Date":"8/4/1998",
    "Format":"CD",
    "UPC (Barcode)":731454091923,
    "Label":"1500",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Andrew Vachss' Safe House (A Collection of the Blues)",
    "Original Release Date":"4/28/1998",
    "Release Date":"4/28/1998",
    "Format":"CD",
    "UPC (Barcode)":088561167325,
    "Label":"Repeat/Relativity",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan Nite Sound Sampler 1998",
    "Original Release Date":"1998",
    "Release Date":"1998",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Japan Nite Sound Sampler 1997",
    "Original Release Date":"1997",
    "Release Date":"1997",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Japan Nite Committee",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Rubaiyat",
    "Original Release Date":"1990",
    "Release Date":"1990",
    "Format":"CD",
    "UPC (Barcode)":07559609402,
    "Label":"Elektra",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"No Place to Play",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"Cassette",
    "UPC (Barcode)":null,
    "Label":"",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists",
    "Title":"Stay Awake",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":075021391819,
    "Label":"A&M",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Friday Night Lights: Original Television Soundtrack, Vol. 2",
    "Original Release Date":"5/18/2010",
    "Release Date":"5/18/2010",
    "Format":"CD",
    "UPC (Barcode)":856195002004,
    "Label":"Arrival",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Friday Night Lights: Original Television Soundtrack, Vol. 2",
    "Original Release Date":"5/18/2010",
    "Release Date":"5/18/2010",
    "Format":"MP3",
    "UPC (Barcode)":856195002004,
    "Label":"Arrival",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Once: Music From The Motion Picture Once",
    "Original Release Date":"5/22/2007",
    "Release Date":"5/22/2007",
    "Format":"CD",
    "UPC (Barcode)":886971058628,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Robotech -- The Original Soundtrack: 20th Anniversary Edition",
    "Original Release Date":"2006",
    "Release Date":"2006",
    "Format":"CD",
    "UPC (Barcode)":898916000869,
    "Label":"Harmony Gold",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"TV Animation Original Bleach Original Soundtrack 1",
    "Original Release Date":"5/24/2005",
    "Release Date":"5/24/2005",
    "Format":"CD",
    "UPC (Barcode)":4534530009548,
    "Label":"Aniplex",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Brokeback Mountain -- Original Motion Picture Soundtrack",
    "Original Release Date":"11/1/2005",
    "Release Date":"11/1/2005",
    "Format":"CD",
    "UPC (Barcode)":602498865859,
    "Label":"Verve Forecast",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Fooly Cooly - Original Soundtrack 2: King of Pirates",
    "Original Release Date":"9/7/2004",
    "Release Date":"9/7/2004",
    "Format":"CD",
    "UPC (Barcode)":013023524422,
    "Label":"Geneon",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Avenue Q -- Original Broadway Cast Recording",
    "Original Release Date":"10/7/2003",
    "Release Date":"10/7/2003",
    "Format":"CD",
    "UPC (Barcode)":828765592321,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Six Feet Under -- Music From the HBO Original Series",
    "Original Release Date":"3/5/2002",
    "Release Date":"3/5/2002",
    "Format":"CD",
    "UPC (Barcode)":044001703126,
    "Label":"Universal",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Hedwig and the Angry Inch",
    "Original Release Date":"7/10/2001",
    "Release Date":"7/10/2001",
    "Format":"MP3",
    "UPC (Barcode)":614992092426,
    "Label":"Hybrid",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"High Fidelity -- Original Soundtrack",
    "Original Release Date":"3/28/2000",
    "Release Date":"3/28/2000",
    "Format":"CD",
    "UPC (Barcode)":720616218827,
    "Label":"Hollywood",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"The Simpsons -- Go Simpsonic With The Simpsons",
    "Original Release Date":"11/2/1999",
    "Release Date":"11/2/1999",
    "Format":"CD",
    "UPC (Barcode)":081227548025,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Schoolhouse Rock -- The Best of Schoolhouse Rock",
    "Original Release Date":"11/3/1998",
    "Release Date":"11/3/1998",
    "Format":"CD",
    "UPC (Barcode)":081227531522,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"William Shakespeare's Romeo + Juliet -- Music from the Motion Picture, Vol. 2",
    "Original Release Date":"4/8/1997",
    "Release Date":"4/8/1997",
    "Format":"CD",
    "UPC (Barcode)":724385556722,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"The Simpsons -- Songs In The Key Of Springfield",
    "Original Release Date":"3/18/1997",
    "Release Date":"3/18/1997",
    "Format":"CD",
    "UPC (Barcode)":081227272326,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Robotech -- Perfect Soundtrack Album",
    "Original Release Date":"1996",
    "Release Date":"1996",
    "Format":"CD",
    "UPC (Barcode)":739991931122,
    "Label":"Streamline Pictures",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Rent -- Original Broadway Cast Recording",
    "Original Release Date":"8/27/1996",
    "Release Date":"8/27/1996",
    "Format":"CD",
    "UPC (Barcode)":600445000322,
    "Label":"Dreamworks",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"William Shakespeare's Romeo + Juliet -- Music from the Motion Picture",
    "Original Release Date":"10/29/1996",
    "Release Date":"10/29/1996",
    "Format":"CD",
    "UPC (Barcode)":724383771509,
    "Label":"Capitol",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Clerks -- Music from the Motion Picture",
    "Original Release Date":"10/11/1994",
    "Release Date":"10/11/1994",
    "Format":"CD",
    "UPC (Barcode)":074646666029,
    "Label":"Chaos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Interview with the Vampire -- Original Motion Picture Soundtrack",
    "Original Release Date":"12/13/1994",
    "Release Date":"12/13/1994",
    "Format":"CD",
    "UPC (Barcode)":720642471920,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Akira -- Original Motion Picture Soundtrack",
    "Original Release Date":"1990",
    "Release Date":"3/10/1994",
    "Format":"CD",
    "UPC (Barcode)":740155800673,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Crayon Shinchan -- Original Soundtrack",
    "Original Release Date":"8/10/1993",
    "Release Date":"8/10/1993",
    "Format":"CD",
    "UPC (Barcode)":4988014707831,
    "Label":"Warner Music Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"The Crying Game -- Original Motion Picture Soundtrack",
    "Original Release Date":"2/23/1993",
    "Release Date":"2/23/1993",
    "Format":"CD",
    "UPC (Barcode)":077778902423,
    "Label":"SBK",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Amadeus -- Original Soundtrack Recording",
    "Original Release Date":"1984",
    "Release Date":"12/22/1992",
    "Format":"CD",
    "UPC (Barcode)":025218179126,
    "Label":"Fantasy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Howards End -- Original Soundtrack Recording",
    "Original Release Date":"11/1/1991",
    "Release Date":"11/1/1991",
    "Format":"CD",
    "UPC (Barcode)":083603533922,
    "Label":"Nimbus",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Bubblegum Crisis -- Blow Up",
    "Original Release Date":"4/12/1991",
    "Release Date":"4/12/1991",
    "Format":"CD",
    "UPC (Barcode)":4988006083967,
    "Label":"Futureland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Bubblegum Crisis -- Revenge Road",
    "Original Release Date":"9/6/1991",
    "Release Date":"9/6/1991",
    "Format":"CD",
    "UPC (Barcode)":4988006088719,
    "Label":"Futureland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Bubblegum Crisis -- Red Eyes",
    "Original Release Date":"9/6/1991",
    "Release Date":"9/6/1991",
    "Format":"CD",
    "UPC (Barcode)":4988006088733,
    "Label":"Futureland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Bubblegum Crisis -- Moonlight Rambler",
    "Original Release Date":"9/6/1991",
    "Release Date":"9/6/1991",
    "Format":"CD",
    "UPC (Barcode)":4988006088726,
    "Label":"Futureland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Bubblegum Crisis -- Born To Kill",
    "Original Release Date":"4/12/1991",
    "Release Date":"4/12/1991",
    "Format":"CD",
    "UPC (Barcode)":4988006083974,
    "Label":"Futureland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Bubblegum Crisis",
    "Original Release Date":"4/12/1991",
    "Release Date":"4/12/1991",
    "Format":"CD",
    "UPC (Barcode)":4988006083950,
    "Label":"Futureland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Beauty and the Beast - Original Motion Picture Soundtrack",
    "Original Release Date":"11/1/1991",
    "Release Date":"11/1/1991",
    "Format":"CD",
    "UPC (Barcode)":050086061822,
    "Label":"Walt Disney",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Bubblegum Crisis -- Complete Vocal Collection Volume 2",
    "Original Release Date":"3/27/1991",
    "Release Date":"3/27/1991",
    "Format":"CD",
    "UPC (Barcode)":4988006082908,
    "Label":"Futureland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Bugs Bunny on Broadway -- Original Broadway Recording",
    "Original Release Date":"1991",
    "Release Date":"1991",
    "Format":"Cassette",
    "UPC (Barcode)":075992649445,
    "Label":"Warner Bros.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Hamlet",
    "Original Release Date":"1/29/1991",
    "Release Date":"1/29/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075679160041,
    "Label":"Virgin",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Bubblegum Crisis -- Double Vision",
    "Original Release Date":"3/7/1990",
    "Release Date":"3/7/1990",
    "Format":"CD",
    "UPC (Barcode)":4988006065697,
    "Label":"Futureland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Bubblegum Crisis -- Scoop Chase",
    "Original Release Date":"12/26/1990",
    "Release Date":"12/26/1990",
    "Format":"CD",
    "UPC (Barcode)":4988006080294,
    "Label":"Futureland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Megazone 23 -- Vocal Collection",
    "Original Release Date":"11/25/1990",
    "Release Date":"11/25/1990",
    "Format":"CD",
    "UPC (Barcode)":4988002211777,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Miss Saigon -- Original London Cast Recording",
    "Original Release Date":"2/2/1990",
    "Release Date":"2/2/1990",
    "Format":"LP",
    "UPC (Barcode)":075992427111,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Twin Peaks",
    "Original Release Date":"9/4/1990",
    "Release Date":"9/4/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075992631648,
    "Label":"Warner Bros.",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Henry V",
    "Original Release Date":"5/8/1990",
    "Release Date":"5/8/1990",
    "Format":"Cassette",
    "UPC (Barcode)":077774991940,
    "Label":"Angel",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Bubblegum Crisis -- Complete Vocal Collection Volume 1",
    "Original Release Date":"10/25/1989",
    "Release Date":"10/25/1989",
    "Format":"CD",
    "UPC (Barcode)":4988006058200,
    "Label":"Futureland",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"The Rocky Horror Picture Show -- Audience Par-Tic-I-Pation Album",
    "Original Release Date":"5/30/1989",
    "Release Date":"5/30/1989",
    "Format":"LP",
    "UPC (Barcode)":081227111212,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Sarafina! -- Original Cast Recording",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":078635930719,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Chess -- Original Broadway Cast Recording",
    "Original Release Date":"1988",
    "Release Date":"1988",
    "Format":"LP",
    "UPC (Barcode)":078635770018,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Les Miserables -- Original Broadway Cast Recording",
    "Original Release Date":"7/7/1987",
    "Release Date":"7/7/1987",
    "Format":"LP",
    "UPC (Barcode)":720642415122,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Megazone 23 -- Song Collection",
    "Original Release Date":"9/21/1986",
    "Release Date":"9/21/1986",
    "Format":"CD",
    "UPC (Barcode)":4988002103584,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Playing for Keeps",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"Cassette",
    "UPC (Barcode)":075678167843,
    "Label":"Atlantic",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Macross -- Song Collection",
    "Original Release Date":"3/21/1985",
    "Release Date":"3/21/1985",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Amadeus -- Original Soundtrack Recording",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"LP",
    "UPC (Barcode)":025218179119,
    "Label":"Fantasy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Chess -- Andersson/Rice/Ulvaeus",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"LP",
    "UPC (Barcode)":078635534016,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Chess -- Andersson, Rice, Ulvaeus",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"CD",
    "UPC (Barcode)":078635534023,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"Tron -- Original Motion Picture Soundtrack",
    "Original Release Date":"1982",
    "Release Date":"1982",
    "Format":"LP",
    "UPC (Barcode)":074643778213,
    "Label":"Walt Disney",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks",
    "Title":"The Empire Strikes Back -- Original Motion Picture Soundtrack",
    "Original Release Date":"1980",
    "Release Date":"1980",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"RSO",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; The Edge",
    "Title":"Captive",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":075679060914,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Kawai Kenji",
    "Title":"Serei no Moribtio Ongakuhen 1",
    "Original Release Date":"7/6/2007",
    "Release Date":"7/6/2007",
    "Format":"CD",
    "UPC (Barcode)":4988102492328,
    "Label":"",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Andrew Lloyd Webber",
    "Title":"Jesus Christ Superstar -- Original Cast Recording",
    "Original Release Date":"1970",
    "Release Date":"9/24/1996",
    "Format":"CD",
    "UPC (Barcode)":008811154226,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Andrew Lloyd Webber",
    "Title":"Aspects of Love -- Original Cast Recording",
    "Original Release Date":"9/27/1989",
    "Release Date":"9/27/1989",
    "Format":"LP",
    "UPC (Barcode)":042284112611,
    "Label":"Polygram",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Andrew Lloyd Webber",
    "Title":"The Phantom of the Opera -- Original London Cast",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":042283127319,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Andrew Lloyd Webber",
    "Title":"The Phantom of the Opera -- Original London Cast",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":042283127326,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Andrew Lloyd Webber",
    "Title":"Song & Dance -- 1982 London Cast",
    "Original Release Date":"1982",
    "Release Date":"1982",
    "Format":"LP",
    "UPC (Barcode)":0042284361729,
    "Label":"Really Useful",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Andrew Lloyd Webber",
    "Title":"Evita -- Premiere American Recording",
    "Original Release Date":"1979",
    "Release Date":"1979",
    "Format":"CD",
    "UPC (Barcode)":076731010724,
    "Label":"MCA",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; John Lunn",
    "Title":"Downton Abbey",
    "Original Release Date":"9/19/2011",
    "Release Date":"9/19/2011",
    "Format":"CD",
    "UPC (Barcode)":602527811468,
    "Label":"Decca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Michael Nyman",
    "Title":"The Piano -- Original Motion Picture Soundtrack",
    "Original Release Date":"10/19/1993",
    "Release Date":"10/19/1993",
    "Format":"CD",
    "UPC (Barcode)":077778827429,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Stephen Sondheim",
    "Title":"Road Show",
    "Original Release Date":"6/30/2009",
    "Release Date":"6/30/2009",
    "Format":"CD",
    "UPC (Barcode)":075597982480,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Stephen Sondheim",
    "Title":"Road Show",
    "Original Release Date":"6/30/2009",
    "Release Date":"6/30/2009",
    "Format":"Stream",
    "UPC (Barcode)":075597982480,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Stephen Sondheim",
    "Title":"Sunday In The Park With George - Original Cast Recording Remastered",
    "Original Release Date":"3/20/2007",
    "Release Date":"3/20/2007",
    "Format":"CD",
    "UPC (Barcode)":828766863826,
    "Label":"Masterworks Broadway",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Stephen Sondheim",
    "Title":"Into The Woods - Original Cast Recording Remastered",
    "Original Release Date":"3/20/2007",
    "Release Date":"3/20/2007",
    "Format":"CD",
    "UPC (Barcode)":828766863628,
    "Label":"Masterworks Broadway",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Stephen Sondheim",
    "Title":"Sweeney Todd - Original Cast Recording Remastered",
    "Original Release Date":"3/20/2007",
    "Release Date":"3/20/2007",
    "Format":"CD",
    "UPC (Barcode)":828766863925,
    "Label":"Masterworks Broadway",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Stephen Sondheim",
    "Title":"Company - 2006 Broadway Production",
    "Original Release Date":"2/20/2007",
    "Release Date":"2/20/2007",
    "Format":"CD",
    "UPC (Barcode)":075597999136,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Stephen Sondheim",
    "Title":"A Little Night Music -- Original Broadway Cast Recording",
    "Original Release Date":"1973",
    "Release Date":"11/10/1998",
    "Format":"CD",
    "UPC (Barcode)":074646528426,
    "Label":"Masterworks Broadway",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Stephen Sondheim",
    "Title":"Into the Woods -- Original Broadway Cast Recording",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":078635679618,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Stephen Sondheim",
    "Title":"Into The Woods -- Original Broadway Cast Recording",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":078635679625,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Stephen Sondheim",
    "Title":"Sunday in the Park with George -- Original Brodway Cast Recording",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"LP",
    "UPC (Barcode)":078635504217,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Artists/Soundtracks; Stephen Sondheim",
    "Title":"Sunday in the Park with George -- Original Cast Recording",
    "Original Release Date":"1984",
    "Release Date":"1984",
    "Format":"CD",
    "UPC (Barcode)":078635504224,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Composers",
    "Title":"Sonic Rebellion: Alternative Classical Collection",
    "Original Release Date":"8/1/2007",
    "Release Date":"8/1/2007",
    "Format":"MP3",
    "UPC (Barcode)":747313076079,
    "Label":"Naxos",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Composers",
    "Title":"Classics From The Movies",
    "Original Release Date":"2/10/2004",
    "Release Date":"2/10/2004",
    "Format":"CD",
    "UPC (Barcode)":056775529024,
    "Label":"Golden Classics",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Composers",
    "Title":"Lesbian American Composers",
    "Original Release Date":"5/19/1998",
    "Release Date":"5/19/1998",
    "Format":"CD",
    "UPC (Barcode)":090438078023,
    "Label":"CRI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Composers",
    "Title":"Gay American Composers: Volume Two",
    "Original Release Date":"5/20/1997",
    "Release Date":"5/20/1997",
    "Format":"CD",
    "UPC (Barcode)":090438075022,
    "Label":"CRI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Composers",
    "Title":"Gay American Composers",
    "Original Release Date":"5/21/1996",
    "Release Date":"5/21/1996",
    "Format":"CD",
    "UPC (Barcode)":090438072120,
    "Label":"CRI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Composers",
    "Title":"Out Classics",
    "Original Release Date":"6/13/1995",
    "Release Date":"6/13/1995",
    "Format":"CD",
    "UPC (Barcode)":090266826124,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Various Composers",
    "Title":"Bang on a Can Live",
    "Original Release Date":"1991",
    "Release Date":"1/12/1994",
    "Format":"CD",
    "UPC (Barcode)":090438062824,
    "Label":"CRI",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Varmint",
    "Title":"Mr. Man in the Moon",
    "Original Release Date":"4/1/2008",
    "Release Date":"4/1/2008",
    "Format":"CD",
    "UPC (Barcode)":809820000425,
    "Label":"Wayne Horvitz",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Värttinä",
    "Title":"Kokko",
    "Original Release Date":"10/15/1996",
    "Release Date":"10/15/1996",
    "Format":"CD",
    "UPC (Barcode)":075597942927,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Värttinä",
    "Title":"Aitara",
    "Original Release Date":"2/10/1995",
    "Release Date":"2/10/1995",
    "Format":"CD",
    "UPC (Barcode)":048248402620,
    "Label":"Green Linnet",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Vasallo Crab 75",
    "Title":"Across the Sky",
    "Original Release Date":"2006",
    "Release Date":"2006",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"Bloom",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Peteris Vasks; Kronos Quartet",
    "Title":"String Quartet No. 4",
    "Original Release Date":"8/19/2003",
    "Release Date":"8/19/2003",
    "Format":"CD",
    "UPC (Barcode)":075597969528,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ralph Vaughn Williams",
    "Title":"Symphony No. 2",
    "Original Release Date":"8/21/1990",
    "Release Date":"8/21/1990",
    "Format":"Cassette",
    "UPC (Barcode)":090266058242,
    "Label":"RCA Victor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Velvet Underground & Nico",
    "Title":"The Velvet Underground & Nico",
    "Original Release Date":"1969",
    "Release Date":"5/7/1996",
    "Format":"CD",
    "UPC (Barcode)":731453125025,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Julieta Venegas",
    "Title":"Aquí",
    "Original Release Date":"3/24/1998",
    "Release Date":"3/24/1998",
    "Format":"CD",
    "UPC (Barcode)":743214718223,
    "Label":"BMG Latin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Victoire",
    "Title":"Cathedral City",
    "Original Release Date":"9/28/2010",
    "Release Date":"9/28/2010",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"New Amsterdam",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Bobby Vinton",
    "Title":"Blue Velvet",
    "Original Release Date":"1963",
    "Release Date":"1963",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Epic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"VOLA & THE ORIENTAL MACHINE",
    "Title":"SA-KA-NA ELECTRIC DEVICE",
    "Original Release Date":"7/29/2009",
    "Release Date":"7/29/2009",
    "Format":"CD",
    "UPC (Barcode)":4988005568472,
    "Label":"Universal Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"VOLA & THE ORIENTAL MACHINE",
    "Title":"Halan’na-ca Darkside",
    "Original Release Date":"10/8/2008",
    "Release Date":"10/8/2008",
    "Format":"CD",
    "UPC (Barcode)":4988005531483,
    "Label":"Universal Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"VOLA & THE ORIENTAL MACHINE",
    "Title":"Android ~like a house mannequin~",
    "Original Release Date":"4/11/2007",
    "Release Date":"4/11/2007",
    "Format":"CD",
    "UPC (Barcode)":4514306009026,
    "Label":"Daizawa",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"VOLA & THE ORIENTAL MACHINE",
    "Title":"Waiting for My Food",
    "Original Release Date":"1/25/2006",
    "Release Date":"1/25/2006",
    "Format":"CD",
    "UPC (Barcode)":4514306008425,
    "Label":"Daizawa",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kevin Volans; Duke Quartet",
    "Title":"Hunting: Gathering",
    "Original Release Date":"11/26/2002",
    "Release Date":"11/26/2002",
    "Format":"CD",
    "UPC (Barcode)":680125106926,
    "Label":"Black Box Classics",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kevin Volans; Duke Quartet",
    "Title":"Hunting: Gathering",
    "Original Release Date":"11/26/2002",
    "Release Date":"11/26/2002",
    "Format":"MP3",
    "UPC (Barcode)":680125106926,
    "Label":"Black Box Classics",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kevin Volans; Kronos Quartet",
    "Title":"Hunting: Gathering",
    "Original Release Date":"1/16/1991",
    "Release Date":"1/16/1991",
    "Format":"CD",
    "UPC (Barcode)":075597925326,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kevin Volans; Kronos Quartet",
    "Title":"Hunting: Gathering",
    "Original Release Date":"1/22/1991",
    "Release Date":"1/22/1991",
    "Format":"Cassette",
    "UPC (Barcode)":075597925340,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Voxtrot",
    "Title":"Voxtrot",
    "Original Release Date":"5/22/2007",
    "Release Date":"5/22/2007",
    "Format":"CD",
    "UPC (Barcode)":609008302125,
    "Label":"Play Louder",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Voxtrot",
    "Title":"Voxtrot",
    "Original Release Date":"5/22/2007",
    "Release Date":"5/22/2007",
    "Format":"MP3",
    "UPC (Barcode)":609008302125,
    "Label":"Play Louder",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rufus Wainwright",
    "Title":"Out Of The Game",
    "Original Release Date":"5/1/2012",
    "Release Date":"5/1/2012",
    "Format":"CD",
    "UPC (Barcode)":602527954288,
    "Label":"Decca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rufus Wainwright",
    "Title":"Out of the Game",
    "Original Release Date":"3/13/2012",
    "Release Date":"3/13/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"Decca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rufus Wainwright",
    "Title":"All Days Are Nights: Songs For Lulu",
    "Original Release Date":"4/20/2010",
    "Release Date":"4/20/2010",
    "Format":"CD",
    "UPC (Barcode)":602527355580,
    "Label":"Decca",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rufus Wainwright",
    "Title":"Release the Stars",
    "Original Release Date":"5/15/2007",
    "Release Date":"5/15/2007",
    "Format":"CD",
    "UPC (Barcode)":602517301610,
    "Label":"Dreamworks",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rufus Wainwright",
    "Title":"Want Two",
    "Original Release Date":"11/16/2004",
    "Release Date":"11/16/2004",
    "Format":"CD",
    "UPC (Barcode)":602498645871,
    "Label":"Dreamworks",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rufus Wainwright",
    "Title":"Want One",
    "Original Release Date":"9/23/2003",
    "Release Date":"9/23/2003",
    "Format":"CD",
    "UPC (Barcode)":600445046108,
    "Label":"Dreamworks",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rufus Wainwright",
    "Title":"Poses",
    "Original Release Date":"6/5/2001",
    "Release Date":"6/5/2001",
    "Format":"CD",
    "UPC (Barcode)":600445023727,
    "Label":"Dreamworks",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Rufus Wainwright",
    "Title":"Rufus Wainwright",
    "Original Release Date":"8/10/1998",
    "Release Date":"8/10/1998",
    "Format":"CD",
    "UPC (Barcode)":600445003927,
    "Label":"Dreamworks",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Waitresses",
    "Title":"Wasn't Tomorrow Wonderful?",
    "Original Release Date":"1982",
    "Release Date":"2008",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"ZE",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Waitresses",
    "Title":"The Best of the Waitresses",
    "Original Release Date":"10/5/1990",
    "Release Date":"10/5/1990",
    "Format":"CD",
    "UPC (Barcode)":042284724920,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Waitresses",
    "Title":"The Best of the Waitresses",
    "Original Release Date":"1990",
    "Release Date":"1990",
    "Format":"Cassette",
    "UPC (Barcode)":042284724944,
    "Label":"Polygram",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"The Waitresses",
    "Title":"Bruiseology",
    "Original Release Date":"May 1983",
    "Release Date":"May 1983",
    "Format":"LP",
    "UPC (Barcode)":042281098017,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Waitresses",
    "Title":"I Could Rule The World If I Only Had The Parts",
    "Original Release Date":"1983",
    "Release Date":"1983",
    "Format":"LP",
    "UPC (Barcode)":null,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Waitresses",
    "Title":"Wasn't Tomorrow Wonderful?",
    "Original Release Date":"1982",
    "Release Date":"1982",
    "Format":"LP",
    "UPC (Barcode)":731451634642,
    "Label":"PolyDor",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Chris Walla",
    "Title":"Field Manual",
    "Original Release Date":"1/29/2008",
    "Release Date":"1/29/2008",
    "Format":"CD",
    "UPC (Barcode)":655173106921,
    "Label":"Barsuk",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Abigail Washburn",
    "Title":"City Of Refuge",
    "Original Release Date":"1/11/2011",
    "Release Date":"1/11/2011",
    "Format":"CD",
    "UPC (Barcode)":0011661328921,
    "Label":"Rounder",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Abigail Washburn",
    "Title":"City Of Refuge",
    "Original Release Date":"1/11/2011",
    "Release Date":"1/11/2011",
    "Format":"MP3",
    "UPC (Barcode)":0011661328921,
    "Label":"Rounder",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Anton Webern",
    "Title":"Complete Works, Opp. 1-31",
    "Original Release Date":"3/4/1991",
    "Release Date":"3/4/1991",
    "Format":"MP3",
    "UPC (Barcode)":074644584523,
    "Label":"Sony Classical",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Anton Webern; Artis Quartett Wein",
    "Title":"Complete Works for String Quartet and String Trio",
    "Original Release Date":"6/5/2001",
    "Release Date":"6/5/2001",
    "Format":"MP3",
    "UPC (Barcode)":710357566820,
    "Label":"Nimbus",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Anton Webern; Emerson String Quartet",
    "Title":"Works for String Quartet",
    "Original Release Date":"5/16/1995",
    "Release Date":"5/16/1995",
    "Format":"CD",
    "UPC (Barcode)":028944582826,
    "Label":"Deutsche Grammophon",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wendy and Lisa",
    "Title":"Snapshots",
    "Original Release Date":"6/2/2011",
    "Release Date":"6/2/2011",
    "Format":"CD",
    "UPC (Barcode)":897262002046,
    "Label":"Girl Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wendy and Lisa",
    "Title":"White Flags of Winter Chimneys",
    "Original Release Date":"3/17/2009",
    "Release Date":"3/17/2009",
    "Format":"CD",
    "UPC (Barcode)":897262002008,
    "Label":"Girl Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wendy and Lisa",
    "Title":"White Flags of Winter Chimneys",
    "Original Release Date":"12/9/2008",
    "Release Date":"12/9/2008",
    "Format":"MP3",
    "UPC (Barcode)":897262002008,
    "Label":"Girl Bros.",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wendy and Lisa",
    "Title":"Wendy And Lisa (Remastered)",
    "Original Release Date":"1987",
    "Release Date":"11/21/2006",
    "Format":"CD",
    "UPC (Barcode)":664140086227,
    "Label":"Wounded Bird",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wendy and Lisa",
    "Title":"Fruit At The Bottom (Remastered)",
    "Original Release Date":"1989",
    "Release Date":"11/21/2006",
    "Format":"CD",
    "UPC (Barcode)":664140434127,
    "Label":"Wounded Bird",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wendy and Lisa",
    "Title":"Girl Bros.",
    "Original Release Date":"10/20/1998",
    "Release Date":"10/20/1998",
    "Format":"CD",
    "UPC (Barcode)":785351008928,
    "Label":"World Domination",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wendy and Lisa",
    "Title":"Eroica",
    "Original Release Date":"9/14/1990",
    "Release Date":"9/14/1990",
    "Format":"CD",
    "UPC (Barcode)":5012981263300,
    "Label":"Virgin",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wendy and Lisa",
    "Title":"Eroica",
    "Original Release Date":"9/17/1990",
    "Release Date":"9/17/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075679137845,
    "Label":"Virgin",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Wendy and Lisa",
    "Title":"Fruit At The Bottom",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"LP",
    "UPC (Barcode)":074644434118,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wendy and Lisa",
    "Title":"Fruit At the Bottom",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"CD",
    "UPC (Barcode)":074644434125,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wendy and Lisa",
    "Title":"Wendy And Lisa",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":074644086218,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wendy and Lisa",
    "Title":"Wendy And Lisa",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"CD",
    "UPC (Barcode)":074644086225,
    "Label":"Columbia",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kanye West",
    "Title":"Late Registration",
    "Original Release Date":"8/30/2005",
    "Release Date":"8/30/2005",
    "Format":"MP3",
    "UPC (Barcode)":602498843680,
    "Label":"Roc-A-Fella",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Kanye West",
    "Title":"The College Dropout",
    "Original Release Date":"2/10/2004",
    "Release Date":"2/10/2004",
    "Format":"CD",
    "UPC (Barcode)":602498617397,
    "Label":"Roc-A-Fella",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The White Stripes",
    "Title":"Get Behind Me Satan",
    "Original Release Date":"6/7/2005",
    "Release Date":"6/7/2005",
    "Format":"MP3",
    "UPC (Barcode)":634904019129,
    "Label":"V2",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The White Stripes",
    "Title":"White Blood Cells",
    "Original Release Date":"1/29/2002",
    "Release Date":"1/29/2002",
    "Format":"CD",
    "UPC (Barcode)":638812712424,
    "Label":"Sympathy for the Record Industry",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Wilco",
    "Title":"Yankee Hotel Foxtrot",
    "Original Release Date":"4/23/2002",
    "Release Date":"4/23/2002",
    "Format":"CD",
    "UPC (Barcode)":075597966923,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Williams",
    "Title":"Star Wars Main Title",
    "Original Release Date":"1977",
    "Release Date":"1977",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"20th Century",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Lucinda Williams",
    "Title":"Car Wheels On A Gravel Road",
    "Original Release Date":"6/30/1998",
    "Release Date":"6/30/1998",
    "Format":"CD",
    "UPC (Barcode)":731455833829,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"WINO",
    "Title":"Everlast",
    "Original Release Date":"8/21/2002",
    "Release Date":"8/21/2002",
    "Format":"CD",
    "UPC (Barcode)":4988002434602,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"WINO",
    "Title":"Dirge No 9",
    "Original Release Date":"5/23/2001",
    "Release Date":"5/23/2001",
    "Format":"CD",
    "UPC (Barcode)":4988002416264,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"WINO",
    "Title":"Useless Music",
    "Original Release Date":"2/24/1999",
    "Release Date":"2/24/1999",
    "Format":"CD",
    "UPC (Barcode)":4988002381067,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"WINO",
    "Title":"WINO",
    "Original Release Date":"12/16/1999",
    "Release Date":"12/16/1999",
    "Format":"CD",
    "UPC (Barcode)":4988002395323,
    "Label":"Speedstar",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Royal Wood",
    "Title":"We Were Born to Glory",
    "Original Release Date":"7/10/2012",
    "Release Date":"7/10/2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"MapleMusic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Royal Wood",
    "Title":"Not Giving Up",
    "Original Release Date":"2012",
    "Release Date":"2012",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"MapleMusic",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Royal Wood",
    "Title":"The Waiting",
    "Original Release Date":"6/29/2010",
    "Release Date":"6/29/2010",
    "Format":"MP3",
    "UPC (Barcode)":823674652425,
    "Label":"",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Royal Wood",
    "Title":"The Lost and Found EP",
    "Original Release Date":"3/24/2009",
    "Release Date":"3/24/2009",
    "Format":"MP3",
    "UPC (Barcode)":625989616624,
    "Label":"Dead Daisy",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Royal Wood",
    "Title":"A Good Enough Day",
    "Original Release Date":"2/12/2008",
    "Release Date":"2/12/2008",
    "Format":"MP3",
    "UPC (Barcode)":836766003524,
    "Label":"Phantom Sound & Vision",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Royal Wood",
    "Title":"Tall Tales",
    "Original Release Date":"9/12/2006",
    "Release Date":"9/12/2006",
    "Format":"MP3",
    "UPC (Barcode)":625989428524,
    "Label":"Sherpa",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"WRENCH",
    "Title":"Bliss",
    "Original Release Date":"6/21/2000",
    "Release Date":"6/21/2000",
    "Format":"CD",
    "UPC (Barcode)":4988002402359,
    "Label":"Victor",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"WRENCH",
    "Title":"Blue Blood Blue",
    "Original Release Date":"5/21/1999",
    "Release Date":"5/21/1999",
    "Format":"CD",
    "UPC (Barcode)":4988002384983,
    "Label":"Victor",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"X",
    "Title":"Under The Big Black Sun (Remastered)",
    "Original Release Date":"1982",
    "Release Date":"9/18/2001",
    "Format":"CD",
    "UPC (Barcode)":081227437220,
    "Label":"Rhino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"XTC",
    "Title":"Drums & Wires",
    "Original Release Date":"8/17/1979",
    "Release Date":"8/6/2002",
    "Format":"MP3",
    "UPC (Barcode)":0724385065354,
    "Label":"Caroline",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"XTC",
    "Title":"Skylarking (Remastered)",
    "Original Release Date":"1986",
    "Release Date":"5/14/2002",
    "Format":"CD",
    "UPC (Barcode)":724385069024,
    "Label":"Caroline",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"XTC",
    "Title":"Drums & Wires",
    "Original Release Date":"8/17/1979",
    "Release Date":"8/6/2002",
    "Format":"Stream",
    "UPC (Barcode)":0724385065354,
    "Label":"Caroline",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"XTC",
    "Title":"English Settlement",
    "Original Release Date":"1982",
    "Release Date":"6/25/2002",
    "Format":"Stream",
    "UPC (Barcode)":0724385066054,
    "Label":"Caroline",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"XTC",
    "Title":"Oranges & Lemons",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"Cassette",
    "UPC (Barcode)":075992421843,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"XTC",
    "Title":"The Mayor of Simpleton",
    "Original Release Date":"1989",
    "Release Date":"1989",
    "Format":"7 Inch",
    "UPC (Barcode)":075992755276,
    "Label":"Geffen",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"XTC",
    "Title":"Skylarking",
    "Original Release Date":"1986",
    "Release Date":"1986",
    "Format":"LP",
    "UPC (Barcode)":075592411718,
    "Label":"Caroline",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yaida Hitomi",
    "Title":"Here today-gone tomorrow",
    "Original Release Date":"8/15/2005",
    "Release Date":"8/15/2005",
    "Format":"CD",
    "UPC (Barcode)":4988006198920,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yaida Hitomi",
    "Title":"i/flancy",
    "Original Release Date":"12/4/2002",
    "Release Date":"12/4/2002",
    "Format":"CD",
    "UPC (Barcode)":4988006180789,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yaida Hitomi",
    "Title":"Candlize",
    "Original Release Date":"10/31/2001",
    "Release Date":"10/31/2001",
    "Format":"CD",
    "UPC (Barcode)":4988006175679,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yaida Hitomi",
    "Title":"daiya-monde",
    "Original Release Date":"10/25/2000",
    "Release Date":"10/25/2000",
    "Format":"CD",
    "UPC (Barcode)":4988006785700,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yaida Hitomi",
    "Title":"Howling",
    "Original Release Date":"5/3/2000",
    "Release Date":"5/3/2000",
    "Format":"CD",
    "UPC (Barcode)":4540078000022,
    "Label":"Aozora",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yano Akiko",
    "Title":"Akiko Yano",
    "Original Release Date":"1991",
    "Release Date":"1991",
    "Format":"Cassette",
    "UPC (Barcode)":075597920543,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yello",
    "Title":"Oh Yeah",
    "Original Release Date":"1985",
    "Release Date":"1985",
    "Format":"7 Inch",
    "UPC (Barcode)":null,
    "Label":"Mercury",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yen Town Band",
    "Title":"Montage",
    "Original Release Date":"9/16/1996",
    "Release Date":"9/16/1996",
    "Format":"CD",
    "UPC (Barcode)":4988010179021,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yerba Buena",
    "Title":"President Alien",
    "Original Release Date":"4/15/2003",
    "Release Date":"4/15/2003",
    "Format":"CD",
    "UPC (Barcode)":793018289429,
    "Label":"Razor & Tie",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Yo Majesty",
    "Title":"Futuristically Speaking ... Never Be Afraid",
    "Original Release Date":"10/7/2008",
    "Release Date":"10/7/2008",
    "Format":"MP3",
    "UPC (Barcode)":801390016820,
    "Label":"Domino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yo Majesty",
    "Title":"Futuristically Speaking ... Never Be Afraid",
    "Original Release Date":"10/7/2008",
    "Release Date":"10/7/2008",
    "Format":"CD",
    "UPC (Barcode)":801390016820,
    "Label":"Domino",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yorico",
    "Title":"Negau",
    "Original Release Date":"1/16/2008",
    "Release Date":"1/16/2008",
    "Format":"CD",
    "UPC (Barcode)":4988006214385,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yorico",
    "Title":"second VERSE",
    "Original Release Date":"2/15/2006",
    "Release Date":"2/15/2006",
    "Format":"CD",
    "UPC (Barcode)":4988006204195,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Yorico",
    "Title":"Cocoon",
    "Original Release Date":"1/26/2005",
    "Release Date":"1/26/2005",
    "Format":"CD",
    "UPC (Barcode)":4988006195172,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Youjeen",
    "Title":"Bewitch",
    "Original Release Date":"9/26/2002",
    "Release Date":"9/26/2002",
    "Format":"CD",
    "UPC (Barcode)":4988004086823,
    "Label":"Teichiku",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Youjeen",
    "Title":"The Doll",
    "Original Release Date":"7/25/2001",
    "Release Date":"7/25/2001",
    "Format":"CD",
    "UPC (Barcode)":4988004082993,
    "Label":"Teichiku",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"The Young Professionals",
    "Title":"9:00 to 17:00, 17:00 to Whenever",
    "Original Release Date":"9/13/2011",
    "Release Date":"9/13/2011",
    "Format":"MP3",
    "UPC (Barcode)":null,
    "Label":"TYP",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"YUKI",
    "Title":"Prismic",
    "Original Release Date":"3/27/2002",
    "Release Date":"3/27/2002",
    "Format":"CD",
    "UPC (Barcode)":4988010001889,
    "Label":"Epic Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Zanzo",
    "Title":"Mdrm",
    "Original Release Date":"7/20/2004",
    "Release Date":"7/20/2004",
    "Format":"CD",
    "UPC (Barcode)":null,
    "Label":"ZzBach",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"Frank Zappa",
    "Title":"The Yellow Shark",
    "Original Release Date":"11/2/1993",
    "Release Date":"5/30/1995",
    "Format":"CD",
    "UPC (Barcode)":014431056024,
    "Label":"Rykodisc",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ZAZEN BOYS",
    "Title":"Stories",
    "Original Release Date":"9/5/2012",
    "Release Date":"9/5/2012",
    "Format":"CD",
    "UPC (Barcode)":4543034033201,
    "Label":"Matsuri Studio",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ZAZEN BOYS",
    "Title":"ZAZEN BOYS 4",
    "Original Release Date":"9/17/2008",
    "Release Date":"9/17/2008",
    "Format":"CD",
    "UPC (Barcode)":4547292200115,
    "Label":"Matsuri Studio",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ZAZEN BOYS",
    "Title":"I Don’t Wanna Be With You",
    "Original Release Date":"12/5/2007",
    "Release Date":"12/5/2007",
    "Format":"CD",
    "UPC (Barcode)":4547292200030,
    "Label":"Matsuri Studio",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ZAZEN BOYS",
    "Title":"Zazen Boys III",
    "Original Release Date":"1/18/2006",
    "Release Date":"1/18/2006",
    "Format":"CD",
    "UPC (Barcode)":4547292200078,
    "Label":"Matsuri Studio",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ZAZEN BOYS",
    "Title":"Himitsu Girl's Top Secret",
    "Original Release Date":"7/16/2005",
    "Release Date":"7/16/2005",
    "Format":"CD",
    "UPC (Barcode)":4547292200023,
    "Label":"Matsuri Studio",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ZAZEN BOYS",
    "Title":"Mayonaka no Yaji-san Kita-san (Original Soundtrack)",
    "Original Release Date":"4/13/2005",
    "Release Date":"4/13/2005",
    "Format":"CD",
    "UPC (Barcode)":4580117620392,
    "Label":"J-Storm",
    "Collection Status":"For Sale"
  },
  {
    "Artist":"ZAZEN BOYS",
    "Title":"Zazen Boys",
    "Original Release Date":"1/10/2004",
    "Release Date":"1/10/2004",
    "Format":"CD",
    "UPC (Barcode)":4547292200016,
    "Label":"Matsuri Studio",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"ZAZEN BOYS",
    "Title":"ZAZEN BOYS II",
    "Original Release Date":"9/1/2004",
    "Release Date":"9/1/2004",
    "Format":"CD",
    "UPC (Barcode)":4547292200047,
    "Label":"Matsuri Studio",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Zoobombs",
    "Title":"Love is funky",
    "Original Release Date":"6/26/2002",
    "Release Date":"6/26/2002",
    "Format":"CD",
    "UPC (Barcode)":4988006178779,
    "Label":"Virgin Japan",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Zoobombs",
    "Title":"Let It Bomb",
    "Original Release Date":"11/16/1999",
    "Release Date":"11/16/1999",
    "Format":"CD",
    "UPC (Barcode)":607217702620,
    "Label":"Emperor Norton",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Zoobombs",
    "Title":"Welcome Back,Zoobombs!",
    "Original Release Date":"9/20/1997",
    "Release Date":"9/20/1997",
    "Format":"CD",
    "UPC (Barcode)":4988001020196,
    "Label":"Quattro/Ricetone",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"Film Works X: In The Mirror Of Maya Deren",
    "Original Release Date":"9/11/2001",
    "Release Date":"9/11/2001",
    "Format":"CD",
    "UPC (Barcode)":702397733324,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"Film Works IX: Trembling Before G-d",
    "Original Release Date":"12/5/2000",
    "Release Date":"12/5/2000",
    "Format":"CD",
    "UPC (Barcode)":702397733126,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"Godard/Spillane",
    "Original Release Date":"7/20/1999",
    "Release Date":"7/20/1999",
    "Format":"CD",
    "UPC (Barcode)":702397732426,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"Taboo and Exile",
    "Original Release Date":"11/16/1999",
    "Release Date":"11/16/1999",
    "Format":"MP3",
    "UPC (Barcode)":702397732525,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"Film Works VIII: 1997",
    "Original Release Date":"2/17/1998",
    "Release Date":"2/17/1998",
    "Format":"CD",
    "UPC (Barcode)":702397731825,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"New Traditions In East Asian Bar Bands",
    "Original Release Date":"5/20/1997",
    "Release Date":"5/20/1997",
    "Format":"CD",
    "UPC (Barcode)":702397731122,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"Film Works IV: S/M + More",
    "Original Release Date":"4/22/1997",
    "Release Date":"4/22/1997",
    "Format":"CD",
    "UPC (Barcode)":702397731023,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"Film Works VII: Cynical Hysterie Hour",
    "Original Release Date":"8/19/1997",
    "Release Date":"8/19/1997",
    "Format":"CD",
    "UPC (Barcode)":702397731528,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"Film Works: 1986-1990",
    "Original Release Date":"8/19/1997",
    "Release Date":"8/19/1997",
    "Format":"CD",
    "UPC (Barcode)":702397731429,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"Film Works II: Music For An Untitled Film",
    "Original Release Date":"5/7/1996",
    "Release Date":"5/7/1996",
    "Format":"CD",
    "UPC (Barcode)":702397730620,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"Elegy",
    "Original Release Date":"9/19/1995",
    "Release Date":"9/19/1995",
    "Format":"CD",
    "UPC (Barcode)":702397730323,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"Kristallnacht",
    "Original Release Date":"9/19/1995",
    "Release Date":"9/19/1995",
    "Format":"CD",
    "UPC (Barcode)":702397730125,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn",
    "Title":"Spillane",
    "Original Release Date":"1987",
    "Release Date":"1987",
    "Format":"LP",
    "UPC (Barcode)":075597917215,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"Sanhedrin",
    "Original Release Date":"4/26/2005",
    "Release Date":"4/26/2005",
    "Format":"CD",
    "UPC (Barcode)":702397734628,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"Tet",
    "Original Release Date":"2/20/1998",
    "Release Date":"2/20/1998",
    "Format":"CD",
    "UPC (Barcode)":4988044009332,
    "Label":"DIW",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"Yod",
    "Original Release Date":"9/18/1998",
    "Release Date":"9/18/1998",
    "Format":"CD",
    "UPC (Barcode)":4988044009356,
    "Label":"DIW",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"The Circle Maker",
    "Original Release Date":"3/17/1998",
    "Release Date":"3/17/1998",
    "Format":"CD",
    "UPC (Barcode)":702397712220,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"Het",
    "Original Release Date":"8/21/1997",
    "Release Date":"8/21/1997",
    "Format":"CD",
    "UPC (Barcode)":4988044009257,
    "Label":"DIW",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"Dalet",
    "Original Release Date":"2/2/1997",
    "Release Date":"2/21/1997",
    "Format":"CD",
    "UPC (Barcode)":4988044009233,
    "Label":"DIW",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"Bar Kokhba",
    "Original Release Date":"8/20/1996",
    "Release Date":"8/20/1996",
    "Format":"CD",
    "UPC (Barcode)":702397710820,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"Zayin",
    "Original Release Date":"10/21/1996",
    "Release Date":"10/21/1996",
    "Format":"CD",
    "UPC (Barcode)":4988044009158,
    "Label":"DIW",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"Hei",
    "Original Release Date":"11/25/1995",
    "Release Date":"11/25/1995",
    "Format":"CD",
    "UPC (Barcode)":4988044008991,
    "Label":"DIW",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"Beit",
    "Original Release Date":"1/21/1995",
    "Release Date":"1/21/1995",
    "Format":"CD",
    "UPC (Barcode)":4988044008892,
    "Label":"DIW",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"Vav",
    "Original Release Date":"12/7/1995",
    "Release Date":"12/7/1995",
    "Format":"CD",
    "UPC (Barcode)":4988044009004,
    "Label":"DIW",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"Gimel",
    "Original Release Date":"3/18/1995",
    "Release Date":"3/18/1995",
    "Format":"CD",
    "UPC (Barcode)":4988044008908,
    "Label":"DIW",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Masada",
    "Title":"Alef",
    "Original Release Date":"12/17/1994",
    "Release Date":"12/17/1994",
    "Format":"CD",
    "UPC (Barcode)":4988044008885,
    "Label":"DIW",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Naked City",
    "Title":"The Complete Studio Recordings",
    "Original Release Date":"3/22/2005",
    "Release Date":"3/22/2005",
    "Format":"CD",
    "UPC (Barcode)":702397734420,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Naked City",
    "Title":"Naked City Live Volume 1",
    "Original Release Date":"5/7/2002",
    "Release Date":"5/7/2002",
    "Format":"CD",
    "UPC (Barcode)":702397733621,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Naked City",
    "Title":"Black Box",
    "Original Release Date":"12/14/2000",
    "Release Date":"12/14/2000",
    "Format":"CD",
    "UPC (Barcode)":702397731221,
    "Label":"Tzadik",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Naked City",
    "Title":"Absinthe",
    "Original Release Date":"12/10/1993",
    "Release Date":"12/10/1993",
    "Format":"CD",
    "UPC (Barcode)":4988044900042,
    "Label":"Avant",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Naked City",
    "Title":"Radio",
    "Original Release Date":"12/10/1993",
    "Release Date":"12/10/1993",
    "Format":"CD",
    "UPC (Barcode)":4988044900035,
    "Label":"Avant",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Naked City",
    "Title":"Heretic: Jeux Des Dames Cruelles",
    "Original Release Date":"7/15/1992",
    "Release Date":"7/15/1992",
    "Format":"CD",
    "UPC (Barcode)":4988044900011,
    "Label":"Avant",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Naked City",
    "Title":"Grand Guignol",
    "Original Release Date":"11/25/1992",
    "Release Date":"11/25/1992",
    "Format":"CD",
    "UPC (Barcode)":4988044900028,
    "Label":"Avant",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Naked City",
    "Title":"Leng Tch'e",
    "Original Release Date":"11/11/1992",
    "Release Date":"11/1/1992",
    "Format":"CD",
    "UPC (Barcode)":4988061886046,
    "Label":"Toy's Factory",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Naked City",
    "Title":"Naked City",
    "Original Release Date":"2/8/1990",
    "Release Date":"2/8/1990",
    "Format":"CD",
    "UPC (Barcode)":075597923827,
    "Label":"Nonesuch",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"John Zorn; Naked City",
    "Title":"Naked City",
    "Original Release Date":"2/8/1990",
    "Release Date":"2/8/1990",
    "Format":"Cassette",
    "UPC (Barcode)":075597923841,
    "Label":"Nonesuch",
    "Collection Status":"Not In Collection"
  },
  {
    "Artist":"Zwan",
    "Title":"Mary Star Of The Sea",
    "Original Release Date":"1/28/2003",
    "Release Date":"1/28/2003",
    "Format":"CD",
    "UPC (Barcode)":093624843627,
    "Label":"Reprise",
    "Collection Status":"In Collection"
  },
  {
    "Artist":"Ellen Taaffe Zwilich",
    "Title":"Chamber Symphony/Double Concerto/Symphony No. 2",
    "Original Release Date":"1990",
    "Release Date":"4/13/2004",
    "Format":"MP3",
    "UPC (Barcode)":809157000044,
    "Label":"First Edition",
    "Collection Status":"In Collection"
  }
];
